// ignore_for_file: prefer_interpolation_to_compose_strings

class FaqModel {
  String? question;

  String? answer;

  FaqModel({this.question, this.answer});
}

List<FaqModel> faqData = [
  FaqModel(
    question: 'How to register/login on AAS?',
    answer: 'Follow the steps to register/login:\n' +
        '\u2022 Install the App from Google Play Store\n' +
        '\u2022 Open the App\n' +
        '\u2022 Click on Register or Login with Google to get registered\n' +
        '\u2022 Check your given email for the OTP\n' +
        '\u2022 Fill the OTP\n' +
        '\u2022 Enjoy using AAS',
  ),
  FaqModel(
    question: 'How to upload videos on AAS?',
    answer: 'Follow the steps to upload videos:\n' +
        '\u2022 Login to the App\n' +
        '\u2022 Click on C-Logs\n' +
        '\u2022 Click on Add button on the bottom of the C-Logs Screen\n' +
        '\u2022 Read the instruction about eligible videos\n' +
        '\u2022 Upload video via your phone camera or phone gallery\n' +
        '\u2022 Your video will be available to the audience once it’s approved by the AAS',
  ),
  FaqModel(
      question:
      'How much time it takes for the video \n to get approved/rejected?',
      answer:
      'It can take up to 24 hours for your video to get approved by AAS team'),


  FaqModel(
    question: 'Which content is allowed to \n upload on AAS?',
    answer: 'The content for AAS to be uploaded can be:\n' +
        '\u2022 Islamic content\n' +
        '\u2022 Informative content\n' +
        '\u2022 Natural videos\n' +
        '\u2022 Animal videos\n' +
        '\u2022 Traveling content\n' +
        '\u2022 Daily routine videos\n' +
        '\u2022 Not be vulgar\n' +
        '\u2022 No nudes\n' +
        '\u2022 No dance contents\n' +
        '\u2022 No 18+ content',
  ),
  FaqModel(
      question: 'How much video duration is \n allowed on AAS?',
      answer:
          'In the beginning, AAS allow a video of 60 seconds video content.'),
  FaqModel(
      question: 'How much coins can be \n earned by prayers daily?',
      answer:
          'There are 5 prayers in a day and 1 prayer attendance reward is 10 coins. I f you mark all the 5 prayers attendance, you will get 50 coins per day for the prayers section.'),
  FaqModel(
      question: 'How much coins can be \n earned by tasks daily?',
      answer:
          'There are 10 tasks in a day and 1 task attendance reward is 10 coins. I f you mark all the 5 prayers attendance, you will get 100 coins per day for the tasks section.'),
  FaqModel(
      question: 'How to earn coins by community?',
      answer:
          'When the community members offered prayers, at the end of the day (midnight, 11:59 pm) the schedular will mark only those persons who have offered all 5 prayers and compare with your Right-side Community with Left side Community, the weaker part of your community will be picked and you will be rewarded for that part.'),
  FaqModel(
    question: 'How to earn coins by tasks?',
    answer: 'Follow the steps to register/login:\n' +
        '\u2022 Login to the App\n' +
        '\u2022 Click on the Task on the bottom\n' +
        '\u2022 Mark the attendace when given task is done\n' +
        '\u2022 You will get the reward instantly',
  ),
  FaqModel(
    question: 'How to earn coins by prayers?',
    answer:
        'You will get rewarded when you offered your all 5 prayers in a day but you have to make sure that you have offered all 5 prayers in a day and marked the attendance on AAS app otherwise your rewards will be considered null and void if you fail to mark all 5 prayers in a day. For example, you have offered 3 prayers or 4 prayers, your rewards will be considered as zero for Prayer section.',
  ),
  FaqModel(
    question: 'How much maximum coins \n can be earned daily?',
    answer: 'There are 3 ways to earn coins.\n' +
        '\u2022 Prayer’s attendance\n' +
        '\u2022 Tasks attendance\n' +
        '\u2022 Community earnings\n' +
        'Anyone with the large community can earn a maximum of 200,000 coins a day.',
  ),
  FaqModel(
    question: 'How much minimum withdrawal \n and donation amount is allowed?',
    answer:
        'The minimum threshold for withdrawal is \$50 and also for the donation purpose, minimum amount in the cash wallet is required to be \$50.',
  ),
  FaqModel(
    question: 'What is the minimum coins \n conversion allowed?',
    answer:
        'There is no restriction on conversion of the coins. Any amount of the coins can be converted any time.',
  ),
  FaqModel(
    question: 'How much coins are equal to 1 \$?',
    answer: '1000 coins equals 1\$.',
  ),
  FaqModel(
    question: 'How to convert coins into \$?',
    answer: 'Follow the steps to convert coins:\n' +
        '\u2022 Go to the wallet\n' +
        '\u2022 Select coins wallet\n' +
        '\u2022 Enter the amount you want to convert\n' +
        '\u2022 Click on the convert button to convert your desired coins\n' +
        '\u2022 Your coins converted in to the cash will appear in the cash wallet',
  ),
  FaqModel(
    question: 'How to withdraw amount?',
    answer: 'Follow the steps to withdraw:\n' +
        '\u2022 Go to the wallet\n' +
        '\u2022 Select cash wallet\n' +
        '\u2022 Enter the amount you want to withdraw\n' +
        '\u2022 Fill all the fields given in the form\n' +
        '\u2022 Click on submit',
  ),
  FaqModel(
    question:
        'How much time it takes for the \n withdrawal amount to get approve?',
    answer:
        'Usually, it takes 02 working days for the withdraw to get approved. In any case, it may take up to 07 working days.',
  ),
  FaqModel(
    question:
        'How much time required for the \n coins earned from the community \n section to appear in the account?',
    answer:
        'It will take 24 hours for the coins earned from the community section to appear into the wallet and the ledger after marking your 5th prayer (Isha Prayer) attendance.',
  ),
  FaqModel(
    question:
        'How much time required for the \n coins earned from the prayer section \n to appear in the account?',
    answer:
        'It will take 24 hours for the coins earned from the prayer section to appear into the wallet and the ledger after marking your 5th prayer (Isha Prayer) attendance.',
  ),
  FaqModel(
    question:
        'How much time required for the \n coins earned from the tasks \n section to appear in the account?',
    answer:
        'There is no time required for the coins earned by the task section to appear in the wallet and the ledger.',
  ),
  FaqModel(
    question: 'Are there any restrictions to \n earn for the task section?',
    answer:
        'There is no restriction on doing all tasks to get rewarded. If you have marked the attendance for the only one task, you will be rewarded for that task.',
  ),
  FaqModel(
    question: 'Are there any restrictions to \n earn for the prayer section?',
    answer:
        'Yes, there are restrictions to earn for the prayer section.You will get rewarded only when you offered your all 5 prayers in a day but you have to make sure that you have offered all 5 prayers in a day and marked the attendance on AAS app otherwise your rewards will be considered null and void if you fail to mark all 5 prayers in a day. For example, you have offered 3 prayers or 4 prayers, your rewards will be considered as zero for Prayer section.',
  ),
  FaqModel(
      question: 'Are there any restrictions to \n earn for the community section?',
      answer:
          'Yes, there are restrictions to earn for the prayer section.\n You will get rewarded only when you offered your all 5 prayers in a day but you have to make sure that you have offered all 5 prayers in a day and marked the attendance on AAS app otherwise your rewards will be considered null and void if you fail to mark all 5 prayers in a day. For example, you have offered 3 prayers or 4 prayers, your rewards will be considered as zero for Prayer section. If you fail to mark your all 5 prayers attendance, you will not get any rewards from the community section as well.'),
  FaqModel(
    question: 'How to join community?',
    answer: 'Follow the steps to join community:\n' +
        '\u2022 Login in to the App\n' +
        '\u2022 Select Join Community from the side menu bar\n' +
        '\u2022 Enter the referral code\n' +
        '\u2022 Click submit\n' +
        '\u2022 You have joined the community',
  ),
  FaqModel(
    question: 'What are the direct and \n indirect members in the community?',
    answer: 'There are two types of members in the community section.\n' +
        ' Direct Members are those who joined anyone’s community with his/her referral code\n' +
        'Indirect members are those who joined the community with the referral code of the members who had already joined the anyone’s community using his/her referral code.\n' +
        'The main parent member of the community will get rewarded by both of the members.',
  ),
  FaqModel(
    question: 'How to add members in community?',
    answer: 'Follow the steps to add members in your community:\n' +
        '\u2022 Login in to the App\n' +
        '\u2022 Share the app to the members with your referral code\n' +
        '\u2022 Ask them to login into the app\n' +
        '\u2022 When they are logged in, ask them to join the community with the referral code you have given to them',
  ),
  FaqModel(
    question: 'How the donation works using AAS?',
    answer: 'AAS provides you the flexible options on withdraw and to donate.\n' +
        'You can donate in two ways:\n' +
        '1. Donate to AAS for the expenditure in good deeds.\n' +
        '2. Donate to any other AAS user directly to his/her account using AAS website or AAS App.',
  ),
  FaqModel(
      question: 'How to report any user on AAS?',
      answer: 'Follow the steps to report users:\n' +
          '\u2022 Login in to the App\n' +
          '\u2022 Go to the C-Logs section\n' +
          '\u2022 Select the user you want to report\n' +
          '\u2022 Go to his/her profile\n' +
          '\u2022 Select report user\n' +
          '\u2022 Submit'),
  FaqModel(
    question: 'How to delete account on AAS?',
    answer:
        'If anyone wants to delete his/her account on AAS, please send an email with your account details to the following email: support@aasonline.co',
  ),
  FaqModel(
    question: 'How to get in touch with the \n support on AAS?',
    answer:
        'If anyone wants to get in touch with AAS team, please send an email with your details and description of your question, to the following email: support@aasonline.co',
  ),
];
