class CashWallet {
  int status;
  String msg;
  CashDetails cashWallet;

  CashWallet({required this.status, required this.msg, required this.cashWallet});

  factory CashWallet.fromJson(Map<String, dynamic> json) {
    return CashWallet(
      status: json['status'],
      msg: json['msg'],
      cashWallet: CashDetails.fromJson(json['cash_wallet']),
    );
  }
}

class CashDetails {
  String cashFromCoins;
  String donationReceived;
  String total;
  String withdrawal;
  String donationGiven;
  String balance;
  int tpin_status;

  CashDetails({
    required this.cashFromCoins,
    required this.donationReceived,
    required this.total,
    required this.withdrawal,
    required this.donationGiven,
    required this.balance,
    required this.tpin_status,
  });

  factory CashDetails.fromJson(Map<String, dynamic> json) {
    return CashDetails(
      cashFromCoins: json['cash_from_coins'],
      donationReceived: json['donation_received'],
      total: json['total'],
      withdrawal: json['withdrawal'],
      donationGiven: json['donation_given'],
      balance: json['balance'],
      tpin_status: json['tpin_status'],
    );
  }
}
