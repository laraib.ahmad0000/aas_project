class CashWithdrawModal {
  final String date;
  final int amount;
  final String bankName;
  final String accountTitle;
  final String accountNumber;
  final int phoneNo;
  final int tpin;

  CashWithdrawModal(
      {required this.date,
      required this.amount,
      required this.bankName,
      required this.accountTitle,
      required this.accountNumber,
      required this.phoneNo,
      required this.tpin
      });
}
