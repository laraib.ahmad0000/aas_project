// class CoinsWallet {
//   int status;
//   String msg;
//   CoinsDetails coinsWallet;

//   CoinsWallet({required this.status, required this.msg, required this.coinsWallet});

//   factory CoinsWallet.fromJson(Map<String, dynamic> json) {
//     return CoinsWallet(
//       status: json['status'],
//       msg: json['msg'],
//       coinsWallet: CoinsDetails.fromJson(json['coins_wallet']),
//     );
//   }
// }

// class CoinsDetails {
//   String selfPrayers;
//   String communityPrayers;
//   String tasks;
//   String total;
//   String converted;
//   int balance; // Change the type to int or double

//   CoinsDetails({
//     required this.selfPrayers,
//     required this.communityPrayers,
//     required this.tasks,
//     required this.total,
//     required this.converted,
//     required this.balance,
//   });

//   factory CoinsDetails.fromJson(Map<String, dynamic> json) {
//     return CoinsDetails(
//       selfPrayers: json['self_prayers'],
//       communityPrayers: json['community_prayers'],
//       tasks: json['tasks'],
//       total: json['total'],
//       converted: json['converted'],
//       balance: int.parse(json['balance']), // Convert balance to an integer
//     );
//   }
// }
