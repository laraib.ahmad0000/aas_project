class CoinsWalletModalTwo {
  int status;
  String msg;
  CoinsDetails coinsWallet;

  CoinsWalletModalTwo({required this.status, required this.msg, required this.coinsWallet});

  factory CoinsWalletModalTwo.fromJson(Map<String, dynamic> json) {
    return CoinsWalletModalTwo(
      status: json['status'],
      msg: json['msg'],
      coinsWallet: CoinsDetails.fromJson(json['coins_wallet']),
    );
  }
}

class CoinsDetails {
  int joining;
  int community;
  int prayers;
  int quran;
  int tasbeeh;
  int tasks;
  int videos;
  int total;
  int converted;
  int balance;

  CoinsDetails({
    required this.joining,
    required this.community,
    required this.prayers,
    required this.quran,
    required this.tasbeeh,
    required this.tasks,
    required this.videos,
    required this.total,
    required this.converted,
    required this.balance,
  });

  factory CoinsDetails.fromJson(Map<String, dynamic> json) {
    return CoinsDetails(
      joining: json['joining'],
      community: json['community'],
      prayers: json['prayers'],
      quran: json['quran'],
      tasbeeh: json['tasbeeh'],
      tasks: json['tasks'],
      videos: json['videos'],
      total: json['total'],
      converted: json['converted'],
      balance: json['balance'],
    );
  }
}
