class CommunityModal {
  final int todayPrayers;
  final int leftMembers;
  final int rightMembers;
  final int leftMembersPrayers;
  final int rightMembersPrayers;
  final int directMembers;
  final int indirectMembers;
  final int totalMembers;

  CommunityModal({
    required this.todayPrayers,
    required this.leftMembers,
    required this.rightMembers,
    required this.leftMembersPrayers,
    required this.rightMembersPrayers,
    required this.directMembers,
    required this.indirectMembers,
    required this.totalMembers,
  });

  factory CommunityModal.fromJson(Map<String, dynamic> json) {
    return CommunityModal(
      todayPrayers: json['today_prayers'] ?? 0,
      leftMembers: json['left_members'] ?? 0,
      rightMembers: json['right_members'] ?? 0,
      leftMembersPrayers: json['left_members_prayers'] ?? 0,
      rightMembersPrayers: json['right_members_prayers'] ?? 0,
      directMembers: json['direct_members'] ?? 0,
      indirectMembers: json['indirect_members'] ?? 0,
      totalMembers: json['total_members'] ?? 0,
    );
  }
}
