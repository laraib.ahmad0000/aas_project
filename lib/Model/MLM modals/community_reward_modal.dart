class CommunityRewardModal {
  String rewardDate;
  int leftMembersWithFivePrayers;
  int rightMembersWithFivePrayers;
  int weakerSideMembers;
  int perMemberReward;
  int rewardToClaim;
  int isClaimed;
  int isEligible;

  CommunityRewardModal({
    required this.rewardDate,
    required this.leftMembersWithFivePrayers,
    required this.rightMembersWithFivePrayers,
    required this.weakerSideMembers,
    required this.perMemberReward,
    required this.rewardToClaim,
    required this.isClaimed,
    required this.isEligible,
  });

  factory CommunityRewardModal.fromJson(Map<String, dynamic> json) {
    return CommunityRewardModal(
      rewardDate: json['reward_date'] ??'',
      leftMembersWithFivePrayers: json['left_members_with_five_prayers'] ??0,
      rightMembersWithFivePrayers: json['right_members_with_five_prayers'] ??0,
      weakerSideMembers: json['weaker_side_members'] ??0,
      perMemberReward: json['per_member_reward'] ??0,
      rewardToClaim: json['reward_to_claim'] ??0,
      isClaimed: json['isClaimed'] ??0,
      isEligible: json['isEligible'] ??0,
    );
  }

 
}
