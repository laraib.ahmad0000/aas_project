class CountDownTimer {
  final int status;
  final String msg;
  final String remainingTime;

  CountDownTimer({
    required this.status,
    required this.msg,
    required this.remainingTime,
  });

  factory CountDownTimer.fromJson(Map<String, dynamic> json) {
    return CountDownTimer(
      status: json['status'] ?? 0,
      msg: json['msg'] ?? '',
      remainingTime: json['remaining_time'] ?? '00:00:00',
    );
  }
}
