class DonationToAASModal {

    final int amount;
    final String tpin;

    DonationToAASModal({
        required this.amount,
        required this.tpin,
    });

}
