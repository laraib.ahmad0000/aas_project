class LeftSideModal {
  final int status;
  final String msg;
  final List<LeftSide> left_members;

  LeftSideModal({
    required this.status,
    required this.msg,
    required this.left_members,
  });

  factory LeftSideModal.fromJson(Map<String, dynamic> json) {
    return LeftSideModal(
      status: json['status'],
      msg: json['msg'],
      left_members: List<LeftSide>.from(
        json['left_members'].map((entry) => LeftSide.fromJson(entry)),
      ),
    );
  }
}

class LeftSide {
  final int id;
  final String profileImage;
  final String name;
  final String referralCode;
  final int todayPrayers;
  // final int leftMembers;
  // final int rightMembers;
  // final int totalMembers;
  // final int leftMembersTodayPrayers;
  // final int rightMembersTodayPrayers;

  LeftSide({
    required this.id,
    required this.profileImage,
    required this.name,
    required this.referralCode,
    required this.todayPrayers,
    // required this.leftMembers,
    // required this.rightMembers,
    // required this.totalMembers,
    // required this.leftMembersTodayPrayers,
    // required this.rightMembersTodayPrayers,
  });

  factory LeftSide.fromJson(Map<String, dynamic> json) {
    return LeftSide(
      id: json['id'],
      profileImage: json['profileimage'],
      name: json['name'],
      referralCode: json['referral_code'],
      todayPrayers: json['today_prayers'],
      // leftMembers: json['left_members'],
      // rightMembers: json['right_members'],
      // totalMembers: json['total_members'],
      // leftMembersTodayPrayers: json['left_members_today_prayers'],
      // rightMembersTodayPrayers: json['right_members_today_prayers'],
    );
  }
}
