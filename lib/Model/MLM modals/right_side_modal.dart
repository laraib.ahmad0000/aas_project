class RightSideModal {
  final int status;
  final String msg;
  final List<RightSide> right_members;

  RightSideModal({
    required this.status,
    required this.msg,
    required this.right_members,
  });

  factory RightSideModal.fromJson(Map<String, dynamic> json) {
    return RightSideModal(
      status: json['status'],
      msg: json['msg'],
      right_members: List<RightSide>.from(
        json['right_members'].map((entry) => RightSide.fromJson(entry)),
      ),
    );
  }
}

class RightSide {
  final int id;
  final String profileImage;
  final String name;
  final String referralCode;
  final int todayPrayers;
  // final int leftMembers;
  // final int rightMembers;
  // final int totalMembers;
  // final int leftMembersTodayPrayers;
  // final int rightMembersTodayPrayers;

  RightSide({
    required this.id,
    required this.profileImage,
    required this.name,
    required this.referralCode,
    required this.todayPrayers,
    // required this.leftMembers,
    // required this.rightMembers,
    // required this.totalMembers,
    // required this.leftMembersTodayPrayers,
    // required this.rightMembersTodayPrayers,
  });

  factory RightSide.fromJson(Map<String, dynamic> json) {
    return RightSide(
      id: json['id'],
      profileImage: json['profileimage'],
      name: json['name'],
      referralCode: json['referral_code'],
      todayPrayers: json['today_prayers'],
      // leftMembers: json['left_members'],
      // rightMembers: json['right_members'],
      // totalMembers: json['total_members'],
      // leftMembersTodayPrayers: json['left_members_today_prayers'],
      // rightMembersTodayPrayers: json['right_members_today_prayers'],
    );
  }
}
