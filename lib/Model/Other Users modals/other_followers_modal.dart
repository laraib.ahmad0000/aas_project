class OtherFollower {
  final int status;
  final String msg;
  final List<OtherFollowerDetails> followers;

  OtherFollower({
    required this.status,
    required this.msg,
    required this.followers,
  });

  factory OtherFollower.fromJson(Map<String, dynamic> json) {
    List<OtherFollowerDetails> followersList = [];

    if (json['followers'] != null) {
      if (json['followers'] is List) {
        for (var follower in json['followers']) {
          followersList.add(OtherFollowerDetails.fromJson(follower));
        }
      }
    }

    return OtherFollower(
      status: json['status'],
      msg: json['msg'],
      followers: followersList,
    );
  }
}

class OtherFollowerDetails {
  final String id;
  final String fname;
  final String profileImage;
  final int posts;

  OtherFollowerDetails({
    required this.id,
    required this.fname,
    required this.profileImage,
    required this.posts,
  });

  factory OtherFollowerDetails.fromJson(Map<String, dynamic> json) {
    return OtherFollowerDetails(
      id: json['id'],
      fname: json['fname'],
      profileImage: json['profileimage'],
      posts: json['posts'],
    );
  }
}
