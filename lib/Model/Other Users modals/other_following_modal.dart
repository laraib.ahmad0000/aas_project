class OtherFollowingModal {
  final int status;
  final String msg;
  final List<OtherFollowingDetail> following;

  OtherFollowingModal({
    required this.status,
    required this.msg,
    required this.following,
  });

  factory OtherFollowingModal.fromJson(Map<String, dynamic> json) {
    List<OtherFollowingDetail> followingList = [];

    if (json['following'] != null) {
      if (json['following'] is List) {
        for (var follower in json['following']) {
          followingList.add(OtherFollowingDetail.fromJson(follower));
        }
      }
    }

    return OtherFollowingModal(
      status: json['status'],
      msg: json['msg'],
      following: followingList,
    );
  }
}

class OtherFollowingDetail {
  final String id;
  final String fname;
  final String profileImage;
  final int posts;

  OtherFollowingDetail({
    required this.id,
    required this.fname,
    required this.profileImage,
    required this.posts,
  });

  factory OtherFollowingDetail.fromJson(Map<String, dynamic> json) {
    return OtherFollowingDetail(
      id: json['id'],
      fname: json['fname'],
      profileImage: json['profileimage'],
      posts: json['posts'],
    );
  }
}
