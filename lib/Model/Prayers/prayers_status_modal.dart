class PrayerStatusModal {
  final int status;
  final Map<String, bool> prayersStatus;

  PrayerStatusModal({required this.status, required this.prayersStatus});

  factory PrayerStatusModal.fromJson(Map<String, dynamic> json) {
    return PrayerStatusModal(
      status: json['status'],
      prayersStatus: Map<String, bool>.from(json['prayers_status']),
    );
  }
}
