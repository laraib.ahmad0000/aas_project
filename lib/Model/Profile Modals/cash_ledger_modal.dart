class CashLedgerDetailModal {
  int status;
  String msg;
  List<CashLedgerEntry> ledger;

  CashLedgerDetailModal({
    required this.status,
    required this.msg,
    required this.ledger,
  });

  factory CashLedgerDetailModal.fromJson(Map<String, dynamic> json) {
    var ledgerEntries = json['ledger'] as List;
    List<CashLedgerEntry> ledgerList =
        ledgerEntries.map((entry) => CashLedgerEntry.fromJson(entry)).toList();

    return CashLedgerDetailModal(
      status: json['status'] as int,
      msg: json['msg'] as String,
      ledger: ledgerList,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['msg'] = msg;
    data['ledger'] = ledger.map((entry) => entry.toJson()).toList();
    return data;
  }
}

class CashLedgerEntry {
  int id;
  int refId;
  String description;
  String label;
  double value;
  double balance;
  String date;

  CashLedgerEntry({
    required this.id,
    required this.refId,
    required this.description,
    required this.label,
    required this.value,
    required this.balance,
    required this.date,
  });

  factory CashLedgerEntry.fromJson(Map<String, dynamic> json) {
    return CashLedgerEntry(
      id: json['id'] as int,
      refId: json['ref_id'] as int,
      description: json['description'] as String,
      label: json['label'] as String,
      value: double.parse(json['value']),
      balance: double.parse(json['balance']),
      date: json['date'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['ref_id'] = refId;
    data['description'] = description;
    data['label'] = label;
    data['value'] = value.toString();
    data['balance'] = balance.toString();
    data['date'] = date;
    return data;
  }
}
