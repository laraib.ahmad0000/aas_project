class ChangePasswordModal {
  final String current_password;
  final String new_password;
  final String new_password_confirmation;

  ChangePasswordModal(
      {required this.current_password,
      required this.new_password,
      required this.new_password_confirmation});
}
