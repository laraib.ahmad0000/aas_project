class LedgerDetailModal {
  int status;
  String msg;
  List<LedgerEntry> ledger;

  LedgerDetailModal({
    required this.status,
    required this.msg,
    required this.ledger,
  });

  factory LedgerDetailModal.fromJson(Map<String, dynamic> json) {
    var ledgerEntries = json['ledger'] as List;
    List<LedgerEntry> ledgerList =
        ledgerEntries.map((entry) => LedgerEntry.fromJson(entry)).toList();

    return LedgerDetailModal(
      status: json['status'] as int,
      msg: json['msg'] as String,
      ledger: ledgerList,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['status'] = status;
    data['msg'] = msg;
    data['ledger'] = ledger.map((entry) => entry.toJson()).toList();
    return data;
  }
}

class LedgerEntry {
  int id;
  int refId;
  String description;
  String label;
  int value;
  int balance;
  String date;

  LedgerEntry({
    required this.id,
    required this.refId,
    required this.description,
    required this.label,
    required this.value,
    required this.balance,
    required this.date,
  });

  factory LedgerEntry.fromJson(Map<String, dynamic> json) {
    return LedgerEntry(
      id: json['id'] as int,
      refId: json['ref_id'] as int,
      description: json['description'] as String,
      label: json['label'] as String,
      value: json['value'] as int,
      balance: json['balance'] as int,
      date: json['date'] as String,
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['id'] = id;
    data['ref_id'] = refId;
    data['description'] = description;
    data['label'] = label;
    data['value'] = value;
    data['balance'] = balance;
    data['date'] = date;
    return data;
  }
}
