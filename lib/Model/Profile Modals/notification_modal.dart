class NotificationModal {
  int status;
  String msg;
  List<NotificationItem> notifications;

  NotificationModal({required this.status, required this.msg, required this.notifications});

  factory NotificationModal.fromJson(Map<String, dynamic> json) {
    List<dynamic> notificationsList = json['notifications'];
    List<NotificationItem> parsedNotifications = List<NotificationItem>.from(
        notificationsList.map((notification) => NotificationItem.fromJson(notification)));

    return NotificationModal(
      status: json['status'],
      msg: json['msg'],
      notifications: parsedNotifications,
    );
  }
}

class NotificationItem {
  int id;
  String notification;
  String date;

  NotificationItem({required this.id, required this.notification, required this.date});

  factory NotificationItem.fromJson(Map<String, dynamic> json) {
    return NotificationItem(
      id: json['id'],
      notification: json['notifications'],
      date: json['date'],
    );
  }
}
