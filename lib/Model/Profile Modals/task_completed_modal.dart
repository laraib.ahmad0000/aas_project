class TaskCompletedModal {
  final int task_id;
  final String date;
  TaskCompletedModal({required this.task_id, required this.date});
}
