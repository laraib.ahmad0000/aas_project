class WalletDetailModal {
  int status;
  String msg;
  Wallet wallet;

  WalletDetailModal({required this.status, required this.msg, required this.wallet});

  factory WalletDetailModal.fromJson(Map<String, dynamic> json) {
    return WalletDetailModal(
      status: json['status'],
      msg: json['msg'],
      wallet: Wallet.fromJson(json['wallet']),
    );
  }
}

class Wallet {
  String totalEarning;
  String communityEarning;
  String tasksEarning;
  String donation;
  String withdraw;

  Wallet({
    required this.totalEarning,
    required this.communityEarning,
    required this.tasksEarning,
    required this.donation,
    required this.withdraw,
  });

  factory Wallet.fromJson(Map<String, dynamic> json) {
    return Wallet(
      totalEarning: json['total_earning'],
      communityEarning: json['community_earning'],
      tasksEarning: json['tasks_earning'],
      donation: json['donation'],
      withdraw: json['withdraw'],
    );
  }
}
