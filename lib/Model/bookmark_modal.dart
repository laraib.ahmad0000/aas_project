class BookmarkModal {
  final String arabic_name;
  final String english_name;
  final int surah_no;

  BookmarkModal(
      {required this.arabic_name,
      required this.english_name,
      required this.surah_no});
}
