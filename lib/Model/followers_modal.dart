class Follower {
  final int status;
  final String msg;
  final List<FollowerDetails> followers;

  Follower({
    required this.status,
    required this.msg,
    required this.followers,
  });

  factory Follower.fromJson(Map<String, dynamic> json) {
    List<FollowerDetails> followersList = [];

    if (json['followers'] != null) {
      if (json['followers'] is List) {
        for (var follower in json['followers']) {
          followersList.add(FollowerDetails.fromJson(follower));
        }
      }
    }

    return Follower(
      status: json['status'],
      msg: json['msg'],
      followers: followersList,
    );
  }
}

class FollowerDetails {
  final int id;
  final String fname;
  final String profileImage;
  final int posts;

  FollowerDetails({
    required this.id,
    required this.fname,
    required this.profileImage,
    required this.posts,
  });

  factory FollowerDetails.fromJson(Map<String, dynamic> json) {
    return FollowerDetails(
      id: json['id'],
      fname: json['fname'],
      profileImage: json['profileimage'],
      posts: json['posts'],
    );
  }
}
