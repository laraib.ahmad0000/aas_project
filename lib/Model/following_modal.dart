class FollowingModal {
  final int status;
  final String msg;
  final List<FollowingDetail> following;

  FollowingModal({
    required this.status,
    required this.msg,
    required this.following,
  });

  factory FollowingModal.fromJson(Map<String, dynamic> json) {
    List<FollowingDetail> followingList = [];

    if (json['following'] != null) {
      if (json['following'] is List) {
        for (var follower in json['following']) {
          followingList.add(FollowingDetail.fromJson(follower));
        }
      }
    }

    return FollowingModal(
      status: json['status'],
      msg: json['msg'],
      following: followingList,
    );
  }
}

class FollowingDetail {
  final int id;
  final String fname;
  final String profileImage;
  final int posts;

  FollowingDetail({
    required this.id,
    required this.fname,
    required this.profileImage,
    required this.posts,
  });

  factory FollowingDetail.fromJson(Map<String, dynamic> json) {
    return FollowingDetail(
      id: json['id'],
      fname: json['fname'],
      profileImage: json['profileimage'],
      posts: json['posts'],
    );
  }
}
