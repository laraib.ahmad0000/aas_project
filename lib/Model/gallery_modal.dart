class GalleryModal {
  int status;
  String msg;
  List<GalleryUrls> islamic_gallery;

  GalleryModal({
    required this.status,
    required this.msg,
    required this.islamic_gallery,
  });

  factory GalleryModal.fromJson(Map<String, dynamic> json) {
    return GalleryModal(
      status: json['status'],
      msg: json['msg'],
      islamic_gallery: List<GalleryUrls>.from(
        json['islamic_gallery'].map((entry) => GalleryUrls.fromJson(entry)),
      ),
    );
  }
}

class GalleryUrls {
  int id;
  String img;
 

  GalleryUrls({
    required this.id,
    required this.img,
  });

  factory GalleryUrls.fromJson(Map<String, dynamic> json) {
    return GalleryUrls(
      id: json['id'],
      img: json['img'],
    );
  }
}
