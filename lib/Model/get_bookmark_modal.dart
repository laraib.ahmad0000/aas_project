class GetBookmarkModal {
  int status;
  List<Bookmark> bookmarks;

  GetBookmarkModal({
    required this.status,
    required this.bookmarks,
  });

  factory GetBookmarkModal.fromJson(dynamic json) {
    if (json is Map<String, dynamic>) {
      // If the response is a map, assume it's a single bookmark
      return GetBookmarkModal(
        status: json["status"],
        bookmarks: [Bookmark.fromJson(json)],
      );
    } else if (json is List) {
      // If the response is a list, assume it's a list of bookmarks
      return GetBookmarkModal(
        status: 0, // You might want to set a default value for status here
        bookmarks: List<Bookmark>.from(
          json.map((x) => Bookmark.fromJson(x)),
        ),
      );
    } else {
      throw FormatException("Unexpected response format");
    }
  }

  Map<String, dynamic> toJson() => {
        "status": status,
        "bookmarks": List<dynamic>.from(bookmarks.map((x) => x.toJson())),
      };
}

class Bookmark {
  int surahNo;
  String arabicName;
  String englishName;

  Bookmark({
    required this.surahNo,
    required this.arabicName,
    required this.englishName,
  });

  factory Bookmark.fromJson(Map<String, dynamic> json) => Bookmark(
        surahNo: json["surah_no"],
        arabicName: json["arabic_name"],
        englishName: json["english_name"],
      );

  Map<String, dynamic> toJson() => {
        "surah_no": surahNo,
        "arabic_name": arabicName,
        "english_name": englishName,
      };
}
