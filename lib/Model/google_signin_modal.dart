class GoogleSignInModal {
  final String email;

  final String name;
  final String profile_url;
  final String id;

  GoogleSignInModal({
    required this.email,
    required this.name,
    required this.profile_url,
    required this.id,
  });
}
