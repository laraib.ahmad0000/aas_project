class UserSatesModel {
  final String name;
  final String profileImage;
  final int followers;
  final int following;
  final int posts;

  UserSatesModel({
    required this.name,
    required this.profileImage,
    required this.followers,
    required this.following,
    required this.posts,
  });
}
