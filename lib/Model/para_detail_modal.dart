class ParaDetailModal {
  final int sura_no;
  final int ayat_no;
  final String arabic;
  final String sura_name;
  final String translation;
  final int total_aayaat;
  final int total_ruku;

  ParaDetailModal({
    required this.sura_no,
    required this.ayat_no,
    required this.arabic,
    required this.sura_name,
    required this.translation,
    required this.total_aayaat,
    required this.total_ruku,
  });

  factory ParaDetailModal.fromJson(Map<String, dynamic> json) {
    return ParaDetailModal(
      sura_no: json['sura_no'],
      ayat_no: json['ayat_no'],
      arabic: json['arabic'],
      sura_name: json['sura_name'],
      translation: json['translation'],
      total_aayaat: json['total_aayaat'],
      total_ruku: json['total_ruku'],
    );
  }
}
