class QariDetailModal {
  final String name;
  final String arabic_name;
  final String relative_path;

  QariDetailModal({
    required this.name,
    required this.arabic_name,
    required this.relative_path,
  });

  factory QariDetailModal.fromJson(Map<String, dynamic> json) {
    return QariDetailModal(
      name: json['name'] ?? 'aa',
      arabic_name: json['arabic_name'] ?? 'aa',
      relative_path: json['relative_path'] ?? 'aa',
    );
  }
}
