class ReportModel {
  final String user_id;
  final String reportDescription;

  ReportModel({required this.reportDescription, required this.user_id});
}
