class User {
  final String phoneno;
  final String email;
  final String fname;
  final String password;
  final String password_confirmation;
  final String referral_code;
  final String device_id;


  User({
    required this.phoneno,
    required this.email,
    required this.fname,
    required this.password,
    required this.password_confirmation,
    required this.referral_code,
    required this.device_id,
  });
}
