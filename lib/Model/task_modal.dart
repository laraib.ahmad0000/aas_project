class TaskModel {
  final int status;
  final String msg;
  final List<TasksDetail> taskdetail;

  TaskModel({
    required this.status,
    required this.msg,
    required this.taskdetail,
  });

  factory TaskModel.fromJson(Map<String, dynamic> json) {
    List<TasksDetail> tasksList = [];

    if (json['tasks'] != null) {
      if (json['tasks'] is List) {
        for (var tasks in json['tasks']) {
          tasksList.add(TasksDetail.fromJson(tasks));
        }
      }
    }

    return TaskModel(
      status: json['status'],
      msg: json['msg'],
      taskdetail: tasksList,
    );
  }
}

class TasksDetail {
  final String id;
  final String task_name;
  final String reward;
  final int isCompleted;

  TasksDetail({
    required this.id,
    required this.task_name,
    required this.reward,
    required this.isCompleted,
  });

  factory TasksDetail.fromJson(Map<String, dynamic> json) {
    return TasksDetail(
      id: json['id'],
      task_name: json['task_name'],
      reward: json['reward'],
      isCompleted: json['isCompleted'],
    );
  }
}
