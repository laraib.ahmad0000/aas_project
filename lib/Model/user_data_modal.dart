class UserModal {
  int id;
  String name;
  String email;
  String profileimage;
  int followers;
  int following;
  int posts;
  String cash_balance;
  String referral_code;
  int isParent;
  int isChild;
  int today_prayers;
  int left_members;
  int right_members;
  int left_members_prayers;
  int right_members_prayers;
  String coins_balance;
  String phoneNumber;
  int isDelete;
  int isQuranReward;
  int isTasbeehReward;
  int isUpdated;
  int isBlocked;
  int kyc;

  UserModal({
    required this.id,
    required this.name,
    required this.email,
    required this.profileimage,
    required this.followers,
    required this.following,
    required this.posts,
    required this.cash_balance,
    required this.referral_code,
    required this.isParent,
    required this.isChild,
    required this.today_prayers,
    required this.left_members,
    required this.right_members,
    required this.left_members_prayers,
    required this.right_members_prayers,
    required this.coins_balance,
    required this.phoneNumber,
    required this.isDelete,
    required this.isQuranReward,
    required this.isTasbeehReward,
    required this.isUpdated,
    required this.isBlocked,
    required this.kyc,
  });

  factory UserModal.fromJson(Map<String, dynamic> json) => UserModal(
        id: json["id"],
        name: json["name"],
        email: json["email"],
        profileimage: json["profileimage"],
        followers: json["followers"],
        following: json["following"],
        posts: json["posts"],
        cash_balance: json["cash_balance"],
        referral_code: json["referral_code"],
        isParent: json["isParent"],
        isChild: json["isChild"],
        today_prayers: json["today_prayers"],
        left_members: json["left_members"],
        right_members: json["right_members"],
        left_members_prayers: json["left_members_prayers"],
        right_members_prayers: json["right_members_prayers"],
        coins_balance: json["coins_balance"],
        phoneNumber: json["phoneno"],
        isDelete: json["isDelete"],
        isQuranReward: json["isQuranReward"],
        isTasbeehReward: json["isTasbeehReward"],
        isUpdated: json["isUpdated"],
        isBlocked: json["isBlocked"],
        kyc: json["kyc"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "email": email,
        "profileimage": profileimage,
        "followers": followers,
        "following": following,
        "posts": posts,
        "cash_balance": cash_balance,
        "referral_code": referral_code,
        "isParent": isParent,
        "isChild": isChild,
        "today_prayers": today_prayers,
        "left_members": left_members,
        "right_members": right_members,
        "left_members_prayers": left_members_prayers,
        "right_members_prayers": right_members_prayers,
        "coins_balance": coins_balance,
        "phoneno": phoneNumber,
        "isDelete": isDelete,
        "isQuranReward": isQuranReward,
        "isTasbeehReward": isTasbeehReward,
        "isUpdated": isUpdated,
        "isBlocked": isBlocked,
        "kyc": isBlocked,
      };

  @override
  String toString() {
    return 'User{id: $id, name: $name, email: $email, profileimage: $profileimage followers: $followers following: $following posts: $posts cash_balance: $cash_balance referral_code: $referral_code isParent: $isParent isChild: $isChild today_prayers: $today_prayers left_members: $left_members right_members: $right_members left_members_prayers: $left_members_prayers right_members_prayers: $right_members_prayers'
        ' coins_balance: $coins_balance phoneno:$phoneNumber isDelete : $isDelete isQuranReward: $isQuranReward isTasbeehReward: $isTasbeehReward isUpdated: $isUpdated isBlocked: $isBlocked kyc: $kyc}';
  }
}
