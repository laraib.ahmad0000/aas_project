class UserPost {
  int? status;
  String? msg;
  List<Videos>? videos;

  UserPost({this.status, this.msg, this.videos});

  UserPost.fromJson(Map<String, dynamic> json) {
    if (json["status"] is int) {
      status = json["status"];
    }
    if (json["msg"] is String) {
      msg = json["msg"];
    }
    if (json["videos"] is List) {
      videos = json["videos"] == null
          ? null
          : (json["videos"] as List).map((e) => Videos.fromJson(e)).toList();
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["status"] = status;
    _data["msg"] = msg;
    if (videos != null) {
      _data["videos"] = videos?.map((e) => e.toJson()).toList();
    }
    return _data;
  }
}

class Videos {
  String? id;
  String? url;
  int? likes;
  int? videoViews;

  Videos({this.id, this.url, this.likes});

  Videos.fromJson(Map<String, dynamic> json) {
    if (json["id"] is String) {
      id = json["id"];
    }
    if (json["url"] is String) {
      url = json["url"];
    }
    if (json["likes"] is int) {
      likes = json["likes"];
    }
    if (json["views"] is int) {
      videoViews = json["views"];
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> _data = <String, dynamic>{};
    _data["id"] = id;
    _data["url"] = url;
    _data["likes"] = likes;
    _data["views"] = videoViews;
    return _data;
  }
}
