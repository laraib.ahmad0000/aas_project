class Video {
  final int id;
  final String url;

  Video({required this.id, required this.url});
}
