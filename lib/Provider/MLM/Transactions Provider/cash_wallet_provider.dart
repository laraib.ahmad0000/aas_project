import 'package:aas/Model/MLM%20modals/cash_wallet_modal.dart';
import 'package:aas/Model/MLM%20modals/coins_wallet_modal.dart';
import 'package:aas/Model/Transaction%20Modal/wallet_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class CashWalletDetailProvider extends ChangeNotifier {
  CashWallet? _getCashWalletDetail;

  CashWallet? get getCashWalletDetail => _getCashWalletDetail;


  Future<void> cashWalletDetail() async {
    try {
      CashWallet? getCashWalletDetail = await ApiService.getCashWalletDetail();
      _getCashWalletDetail = getCashWalletDetail;

      notifyListeners();
    } catch (e) {
         print('error from get coins wallet detail: $e');
    }
  }
}