import 'package:aas/Model/MLM%20modals/coins_wallet_modal.dart';
import 'package:aas/Model/MLM%20modals/coins_wallet_modal_two.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class CoinsWalletDetailTwoProvider extends ChangeNotifier {
  CoinsWalletModalTwo? _getCoinsWalletDetailTwo;

  CoinsWalletModalTwo? get getCoinWalletDetailTwo => _getCoinsWalletDetailTwo;


  Future<void> coinsWalletDetailTwo() async {
    try {
      CoinsWalletModalTwo? getCoinWalletDetailTwo = await ApiService.getCoinsWalletDetailTwo();
      _getCoinsWalletDetailTwo = getCoinWalletDetailTwo;

      notifyListeners();
    } catch (e) {
         print('error from get coins wallet detail two: $e');
    }
  }
}