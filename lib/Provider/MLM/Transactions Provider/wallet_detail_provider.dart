import 'package:aas/Model/Transaction%20Modal/wallet_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class walletDetailProvider extends ChangeNotifier {
  WalletDetailModal? _getWalletDetail;

  WalletDetailModal? get getWalletDetail => _getWalletDetail;


  Future<void> walletDetail() async {
    try {
      WalletDetailModal? getWalletDetail = await ApiService.getWalletDetail();
      _getWalletDetail = getWalletDetail;

      notifyListeners();
    } catch (e) {
         print('error from get wallet detail: $e');
    }
  }
}