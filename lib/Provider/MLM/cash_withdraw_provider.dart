import 'package:aas/Model/MLM%20modals/cash_withdraw_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class CashWithdrawProvider extends ChangeNotifier {
final ApiService apiService;


CashWithdrawProvider({required this.apiService});
cashWithdraw(CashWithdrawModal cashWithdrawModal, BuildContext context) async {
  try {
    await apiService.cashWithdraw(date: cashWithdrawModal.date, amount: cashWithdrawModal.amount, bankName: cashWithdrawModal.bankName, accountTitle: cashWithdrawModal.accountTitle, accountNumber: cashWithdrawModal.accountNumber,
    phoneNo: cashWithdrawModal.phoneNo,
     tpin: cashWithdrawModal.tpin,
     context: context
     
     );
     notifyListeners();
  } catch (e) {
    print('from withdraw cash error: $e');
  }
}


}