import 'package:aas/Model/MLM%20modals/claim_reward_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/cupertino.dart';

class ClaimRewardprovider extends ChangeNotifier{
  final ApiService apiService;

  ClaimRewardprovider({required this.apiService});



  void claimReward(ClaimRewardModal claimRewardModal) async {
    try {
      await apiService.claimReward(date: claimRewardModal.date, rewarded_coins: claimRewardModal.rewarded_coins);
    } catch (e) {
        print('error during claiming reward: $e');
    }
  }
}