import 'package:aas/Model/MLM%20modals/coins_convert_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class CoinsConvertProvider extends ChangeNotifier {
final ApiService apiService;


CoinsConvertProvider({required this.apiService});

 coinsConversion(CoinsConvertModal coinsConvertModal) async {
  try {
    await apiService.convertCoins(coinsToConvert: coinsConvertModal.coins_to_convert);
    notifyListeners();
  } catch (e) {
    print('from coins conversion error: $e');
    
  }
}
}