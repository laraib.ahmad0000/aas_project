import 'package:aas/Model/MLM%20modals/community_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';
class CommunityProvider extends ChangeNotifier {
  CommunityModal? _communityModal;

  CommunityModal? get communityModal => _communityModal;

  Future<void> getCommunity(String date) async {
    try {
      dynamic responseData = await ApiService.community(date: date);
      if (responseData != null) {
        if (responseData is List) {
          // Assuming you're interested in the first item of the list
          CommunityModal communityModal =
              CommunityModal.fromJson(responseData.first);
          _communityModal = communityModal;
        } else if (responseData is Map<String, dynamic>) {
          // Handle the case where responseData is a map
          CommunityModal communityModal = CommunityModal.fromJson(responseData);
          _communityModal = communityModal;
        } else {
          print('Unexpected response format');
        }
        notifyListeners();
      } else {
        print('No data received from the API');
      }
    } catch (e) {
      print('Error updating community data: $e');
    }
  }
}
