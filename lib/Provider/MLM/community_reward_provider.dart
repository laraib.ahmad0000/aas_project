import 'package:aas/Model/MLM%20modals/community_reward_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/cupertino.dart';

class CommunityRewardProvider extends ChangeNotifier{
  CommunityRewardModal? _communityRewardModal;

CommunityRewardModal? get communityRewardModal => _communityRewardModal;

Future<void> getCommunityReward(String date) async {
  try {
    dynamic responseData = await ApiService.communityReward(date: date);
    if (responseData != null) {
        if (responseData is List) {
          // Assuming you're interested in the first item of the list
          CommunityRewardModal communityModal =
              CommunityRewardModal.fromJson(responseData.first);
          _communityRewardModal = communityModal;
        } else if (responseData is Map<String, dynamic>) {
          // Handle the case where responseData is a map
          CommunityRewardModal communityModal = CommunityRewardModal.fromJson(responseData);
          _communityRewardModal = communityModal;
        } else {
          print('Unexpected response format');
        }
        notifyListeners();
      } else {
        print('No data received from the API');
      }
  } catch (e) {
      print('Error updating community data: $e');
  }
}
}