import 'package:aas/Model/MLM%20modals/count_down_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class CountDownTimerProvider extends ChangeNotifier {
  CountDownTimer? _countDownTimer;

  CountDownTimer? get countDownTimer => _countDownTimer;

  Future<void> getTimerValue(
      String current_time, String next_prayer_time) async {
    try {
      final Map<String, dynamic> responseData = await ApiService.getCountDownData(
        current_time: current_time,
        next_prayer_time: next_prayer_time,
      );

      final CountDownTimer countDownTimer = CountDownTimer.fromJson(responseData);

      _countDownTimer = countDownTimer;
      notifyListeners();
    } catch (e) {
      print('Error from get timer detail: $e');
    }
  }
}
