import 'package:aas/Model/MLM%20modals/donate_to_other_modal.dart';
import 'package:aas/Model/MLM%20modals/donation_to_aas_modal.dart';
import 'package:aas/Model/MLM%20modals/join_community_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class DonationToOther extends ChangeNotifier {
 final ApiService apiService;
 DonationToOther({required this.apiService});


 void doationOther(DonateToOtherModal donateToOtherModal, BuildContext context) async {
  try {
    await apiService.donationToOthers(amount: donateToOtherModal.amount, tpin: donateToOtherModal.tpin, user_id: donateToOtherModal.user_id, context: context);
  } catch (e) {
    print('error from donating to others: $e');
  }
 }
}