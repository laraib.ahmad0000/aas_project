import 'package:aas/Model/MLM%20modals/donation_to_aas_modal.dart';
import 'package:aas/Model/MLM%20modals/join_community_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class DonationToAAS extends ChangeNotifier {
 final ApiService apiService;
 DonationToAAS({required this.apiService});


 void doationAAS(DonationToAASModal donationToAASModal, BuildContext context) async {
  try {
    await apiService.donationToAAS(amount: donationToAASModal.amount, tpin: donationToAASModal.tpin, context: context);
  } catch (e) {
    print('error from donating to aas: $e');
  }
 }
}