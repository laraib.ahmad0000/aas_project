import 'package:aas/Model/gallery_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/foundation.dart';

class ForgetTransPinProvider extends ChangeNotifier {
final ApiService apiService;


ForgetTransPinProvider({required this.apiService});

forgetTransPin() async {
  try {
    await ApiService.forgetTransactionsPin();
    notifyListeners();
  } catch (e) {
    print('from forget pin error: $e');
    
  }
}
}