import 'package:aas/Model/MLM%20modals/join_community_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class JoinCommunityProvider extends ChangeNotifier {
 final ApiService apiService;
 JoinCommunityProvider({required this.apiService});


 void joinCommunity(JoinCommunityModal joinCommunityModal) async {
  try {
    await apiService.joinCommunity(referral_code: joinCommunityModal.referral_code);
  } catch (e) {
    print('error from JC provider: $e');
  }
 }
}