import 'package:aas/Model/MLM%20modals/left_side_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class LeftSideProvider extends ChangeNotifier {
  LeftSideModal? _leftSideModal;

  LeftSideModal? get leftSideModal => _leftSideModal;

  Future<void> getLeftSide() async {
    try {
      LeftSideModal leftData = await ApiService.getLeftSideDetail();
      _leftSideModal = leftData;
      notifyListeners();
    } catch (e) {
         print('Error updating getting left data: $e');
    }
  }
}