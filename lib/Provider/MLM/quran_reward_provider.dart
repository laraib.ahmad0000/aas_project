import 'package:aas/Services/services.dart';
import 'package:flutter/cupertino.dart';

class QuranRewardProvider extends ChangeNotifier {
  final ApiService apiService;

  QuranRewardProvider({required this.apiService});

  quranReward() async {
    try {
      await ApiService.quranReward();
      notifyListeners();
    } catch (e) {
      print('from quran reward: $e');
    }
  }
}
