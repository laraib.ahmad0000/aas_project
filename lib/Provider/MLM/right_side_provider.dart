import 'package:aas/Model/MLM%20modals/right_side_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class RightSideProvider extends ChangeNotifier {
  RightSideModal? _rightSideModal;

  RightSideModal? get rightSideModal => _rightSideModal;

  Future<void> getRightSide() async {
    try {
      RightSideModal rightData = await ApiService.getRightSideDetail();
      _rightSideModal = rightData;
      notifyListeners();
    } catch (e) {
         print('Error updating getting right data: $e');
    }
  }
}