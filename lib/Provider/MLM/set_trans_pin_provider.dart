import 'package:aas/Model/MLM%20modals/coins_convert_modal.dart';
import 'package:aas/Model/MLM%20modals/set_t_pin_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class SetTransactionsPinProvider extends ChangeNotifier {
final ApiService apiService;


SetTransactionsPinProvider({required this.apiService});

 setPin(SetTPinModal setTPinModal) async {
  try {
    await apiService.setTransactionsPin(tpin: setTPinModal.tpin);
    notifyListeners();
  } catch (e) {
    print('from set pin error: $e');
    
  }
}
}