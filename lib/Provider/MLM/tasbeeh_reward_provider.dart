import 'package:aas/Services/services.dart';
import 'package:flutter/cupertino.dart';

class TasbeehRewardProvider extends ChangeNotifier {
  final ApiService apiService;

  TasbeehRewardProvider({required this.apiService});

  tasbeehReward() async {
    try {
      await ApiService.tasbeehReward();
      notifyListeners();
    } catch (e) {
      print('from tasbeeh reward: $e');
    }
  }
}
