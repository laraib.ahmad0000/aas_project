import 'package:aas/Model/Other%20Users%20modals/other_followers_modal.dart';
import 'package:aas/Model/follow_unfollow_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

import '../../Model/Other Users modals/other_user_follower_modal.dart';

class OtherFollowersProvider extends ChangeNotifier {
  OtherFollower? _otherfollowers;

  OtherFollower? get getotherFollowers => _otherfollowers;

  Future<void> getOtherFollowersDetail(Follow follow) async {
    try {
      OtherFollower getotherFollowers =
          await ApiService.getOtherFollowersList(id: follow.user_id);
      _otherfollowers = getotherFollowers;
      notifyListeners();
    } catch (e) {
      print('error from BMP: $e');
    }
  }
}
