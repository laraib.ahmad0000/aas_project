import 'package:aas/Model/Other%20Users%20modals/other_following_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

import '../../Model/Other Users modals/other_user_following_modal.dart';
import '../../Model/follow_unfollow_modal.dart';

class OtherFollowingProvider extends ChangeNotifier {
  OtherFollowingModal? _otherfollowing;

  OtherFollowingModal? get getotherFollowing => _otherfollowing;

  Future<void> getOtherFollowingDetail(Follow follow) async {
    try {
      OtherFollowingModal getotherFollowing =
          await ApiService.getOtherFollowingList(id: follow.user_id);
      _otherfollowing = getotherFollowing;
      notifyListeners();
    } catch (e) {
      print('error from BMP: $e');
    }
  }
}
