import 'package:aas/Model/Prayers/prayers_status_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class GetPrayersStatusProvider extends ChangeNotifier {
  PrayerStatusModal? _prayerStatusModal;

  PrayerStatusModal? get prayerStatus=> _prayerStatusModal;


  Future<void> getPrayersStatus(String date) async {
    try {
      PrayerStatusModal prayerStatus = await ApiService.getPrayersStatus(date: date);
      _prayerStatusModal = prayerStatus;
      notifyListeners();
    } catch (e) {
      print('error during get prayers status provider: $e');
      
    }
  }
}
