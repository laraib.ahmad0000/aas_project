import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class MarkPrayerAttendanceProvider extends ChangeNotifier {
  final ApiService apiService;

  MarkPrayerAttendanceProvider({ required this.apiService});


  void markPrayerAttendance(String date, int prayer_number) async {
    try {
      await apiService.markPrayerAttendance(date: date, prayer_number: prayer_number);
      notifyListeners();
    } catch (e) {
      print('error dusring mark attedance privider: $e');
      
    }
  }
}