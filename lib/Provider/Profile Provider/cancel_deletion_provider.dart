import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class CancelDeletionProvider extends ChangeNotifier {
  final ApiService apiService;

  CancelDeletionProvider({required this.apiService});

  canceldeletionP(BuildContext context) async {
    try {
      await ApiService.cancelAccountDeletion(context: context);
      notifyListeners();
    } catch (e) {
        print('from cancelation account deletion: $e');
    }
  }
}