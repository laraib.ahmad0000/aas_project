import 'package:aas/Model/Profile%20Modals/cash_ledger_modal.dart';
import 'package:aas/Model/Profile%20Modals/coins_ledger_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class CashLedgerDetailProvider extends ChangeNotifier {
  CashLedgerDetailModal? _ledgerDetail;

  CashLedgerDetailModal? get CashledgerDetailModal => _ledgerDetail;

  Future<void> getCashLedgerDetail() async {
    try {
      CashLedgerDetailModal CashledgerDetailModal = await ApiService.getCashLedgerDetail();
      _ledgerDetail = CashledgerDetailModal;
      notifyListeners();
    } catch (e) {
      print('error from get ledger detail: $e');
    }
  }
}
