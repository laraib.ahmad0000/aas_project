import 'package:aas/Model/Profile%20Modals/change_password_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class ChangePasswordProvider extends ChangeNotifier {
  final ApiService apiService;

  ChangePasswordProvider({required this.apiService});

  void changePassword(
      ChangePasswordModal changePasswordModal,
      BuildContext context,
      String current_password,
      String new_password,
      String new_password_confirmation) async {
    await apiService.changePassword(
        current_password: changePasswordModal.current_password,
        new_password: changePasswordModal.new_password,
        new_password_confirmation:
            changePasswordModal.new_password_confirmation,
        context: context);
  }
}
