import 'package:aas/Model/Profile%20Modals/delete_account_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/cupertino.dart';

class DeleteAccountProvider extends ChangeNotifier {
  final ApiService apiService;

  DeleteAccountProvider({required this.apiService});


  deleteAccountP(DeleteAccountModal deleteAccountModal, BuildContext context) async {
    try {
      await apiService.deleteAccount(password: deleteAccountModal.password, context: context);
      notifyListeners();
    } catch (e) {
      print('from delete account: $e');
    }
  }
}