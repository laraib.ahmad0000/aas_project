import 'package:aas/Model/Profile%20Modals/coins_ledger_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class LedgerDetailProvider extends ChangeNotifier {
  LedgerDetailModal? _ledgerDetail;

  LedgerDetailModal? get ledgerDetailModal => _ledgerDetail;

  Future<void> getLedgerDetail() async {
    try {
      LedgerDetailModal ledgerDetailModal = await ApiService.getLedgerDetail();
      _ledgerDetail = ledgerDetailModal;
      notifyListeners();
    } catch (e) {
      print('error from get ledger detail: $e');
    }
  }
}
