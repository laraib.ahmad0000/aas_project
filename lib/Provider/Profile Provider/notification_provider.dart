import 'package:aas/Model/Profile%20Modals/notification_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class NotificationDetailProvider extends ChangeNotifier {
  NotificationModal? _notificationDetails;

  NotificationModal?   get notificaionDetail => _notificationDetails;

  Future<void> getNotificationDetails() async {
    try {
      NotificationModal? notiDetail = await ApiService.getNotification();
      _notificationDetails = notiDetail;
      notifyListeners();
    } catch (e) {
      print('error from get noti details from provider: $e');
    }
  }
}