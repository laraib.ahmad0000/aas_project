import 'package:aas/Model/Profile%20Modals/task_completed_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class TaskCompletedProvider extends ChangeNotifier {
  final ApiService apiService;
  TaskCompletedProvider({required this.apiService});

  void taskCompleted(TaskCompletedModal taskCompletedModal) async {
    try {
      await apiService.taskCompleted(
          task_id: taskCompletedModal.task_id, date: taskCompletedModal.date);
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}
