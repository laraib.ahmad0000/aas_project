import 'package:aas/Model/Profile%20Modals/task_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class TaskProvider extends ChangeNotifier {
  TaskModel? _taskData;

  TaskModel? get taskData => _taskData;

  Future<void> updateTasks(String date) async {
    try {
      TaskModel taskData = await ApiService.getTask(date: date);
      _taskData = taskData;
      notifyListeners();
    } catch (error) {
      print('Error updating user data: $error');
    }
  }
}
