import 'dart:io';

import 'package:aas/constants/ad_helper.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

class AdProvider extends ChangeNotifier {
  BannerAd? _bannerAd;
  InterstitialAd? _interstitialAd;
  RewardedAd? _rewardedAd;

  NativeAd? _nativeAd;
  bool _isInterstitialAdLoaded = false;
  bool _isRewardedAdLoaded = false;
  NativeAd? get nativeAd => _nativeAd;
  BannerAd? get bannerAd => _bannerAd;
  RewardedAd? get rewardedAd => _rewardedAd;
  InterstitialAd? get interstitialAd => _interstitialAd;
  bool get isInterstitialAdLoaded => _isInterstitialAdLoaded;
  bool get isRewardedAdLoaded => _isRewardedAdLoaded;

  AdProvider() {
    loadInterstitialAd();
    loadRewardedAd();
  }

  void loadInterstitialAd() {
    InterstitialAd.load(
      adUnitId: AdHelper.interstitialAdUnitId,
      request: AdRequest(),
      adLoadCallback: InterstitialAdLoadCallback(
        onAdLoaded: (ad) {
          _interstitialAd = ad;
          _isInterstitialAdLoaded = true;
          print('Interstitial ad loaded');
          notifyListeners();
        },
        onAdFailedToLoad: (error) {
          _isInterstitialAdLoaded = false;
          print('Interstitial ad failed to load: $error');
        },
      ),
    );
  }

  void showInterstitialAd() {
    if (_isInterstitialAdLoaded) {
      _interstitialAd?.show();
    } else {
      print('Interstitial ad not loaded yet.');
    }
  }

  // Create a new instance of BannerAd for each screen
  BannerAd createBannerAd() {
    return BannerAd(
      size: AdSize.banner,
      adUnitId: AdHelper.bannerAdUnitId,
      listener: BannerAdListener(
        onAdLoaded: (_) {
          print('Banner ad loaded');
          notifyListeners();
        },
        onAdFailedToLoad: (ad, error) {
          ad.dispose();
          print('Banner ad failed to load: $error');
        },
      ),
      request: AdRequest(),
    )..load();
  }

  // static String get rewardedAdUnitId {
  //   if (Platform.isAndroid) {
  //     return 'ca-app-pub-3007541505786179/2802967129';
  //   } else {
  //     throw UnsupportedError('Unsupported platform');
  //   }
  // }

  NativeAd createNativeAd() {
    return NativeAd(
        adUnitId: AdHelper.nativeAdUnitId,
        listener: NativeAdListener(onAdLoaded: (ad) {
          print('Native ad loaded');
          notifyListeners();
        }, onAdFailedToLoad: (ad, error) {
          // ad.dispose();
          print('Banner ad failed to load: $error');
        }),
        request: AdRequest(),
        nativeTemplateStyle:
            NativeTemplateStyle(templateType: TemplateType.small))
      ..load();
  }

  void loadRewardedAd() {
    RewardedAd.load(
      adUnitId: AdHelper.rewardedAdUnitId,
      request: AdRequest(),
      rewardedAdLoadCallback: RewardedAdLoadCallback(
        onAdLoaded: (ad) {
          _rewardedAd = ad;
          _isRewardedAdLoaded = true;
          print('Rewarded ad loaded');
          notifyListeners();
        },
        onAdFailedToLoad: (error) {
          _isRewardedAdLoaded = false;
          print('Rewarded ad failed to load: $error');
        },
      ),
    );
  }

  void showRewardedAd() {
    if (_isRewardedAdLoaded) {
      _rewardedAd?.show(
        onUserEarnedReward: (ad, RewardItem reward) {
          print('User earned reward: ${reward.amount} ${reward.type}');
        },
      );
    } else {
      print('Rewarded ad not loaded yet.');
    }
  }

  @override
  void dispose() {
    _bannerAd?.dispose();
    _rewardedAd?.dispose();
    _interstitialAd?.dispose();
    super.dispose();
  }
}
