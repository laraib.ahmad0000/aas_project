import 'package:aas/Model/bookmark_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class BookmarkProvider extends ChangeNotifier {
  final ApiService apiService;
  BookmarkProvider({required this.apiService});

  void bookmarkSuratz(BookmarkModal bookmarkModal) async {
    try {
      await apiService.toggleBM(
          arabicName: bookmarkModal.arabic_name,
          englishName: bookmarkModal.english_name,
          surahNo: bookmarkModal.surah_no);
      notifyListeners();
    } catch (error) {
      print('Error: $error');
    }
  }
}
