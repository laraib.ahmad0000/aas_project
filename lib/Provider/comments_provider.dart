import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';


class CommentProvider with ChangeNotifier {
  final ApiService apiService;

  CommentProvider({required this.apiService});

  List<dynamic> _comments = [];
  List<dynamic> get comments => _comments;

  Future<void> fetchComments(String videoId) async {
    try {
      _comments = await apiService.getComments(videoId);
      notifyListeners();
    } catch (e) {
      print("Error fetching comments: $e");
    }
  }

  Future<void> addComment(String videoId, String comment) async {
    try {
      await apiService.postComment(videoId, comment);
      await fetchComments(videoId);
    } catch (e) {
      print("Error adding comment: $e");
    }
  }

  Future<void> addReply(int commentId, String videoId, String message) async {
    try {
      await apiService.postReply(commentId, message);
      
      await fetchComments(videoId);
      print("THis isFetchComments:${fetchComments(videoId)}");
    } catch (e) {
      print("Error adding Reply: $e");
    }
  }


  Future<void> addLikeComment(int commentId, String videoId) async {
    try {
      await apiService.postLikeComment(commentId);
      await fetchComments(videoId); // Refresh comments after liking
      notifyListeners(); // Notify listeners to update UI immediately
    } catch (e) {
      print("Error liking comment: $e");
    }
  }

  Future<void> addLikeReply(int replyId, String videoId) async {
    try {
      await apiService.postLikeReply(replyId);
      await fetchComments(videoId); // Refresh comments after liking
      notifyListeners(); // Notify listeners to update UI immediately
    } catch (e) {
      print("Error liking reply: $e");
    }
  }



  Future<void> deleteComment(int commentId, String videoId) async {
    try {
      await apiService.deleteComment(commentId);
      await fetchComments(videoId);
      notifyListeners();
    } catch (e) {
      print("Error deleting comment: $e");
    }
  }
}
