import 'dart:io';

import 'package:flutter/material.dart';

class FileProvider extends ChangeNotifier {
  File? _pickedFile;
  File? get pickedFile => _pickedFile;

  void setPickedFile(File file) {
    _pickedFile = file;
    notifyListeners();
  }
}
