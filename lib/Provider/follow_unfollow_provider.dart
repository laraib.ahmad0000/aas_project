import 'package:aas/Model/follow_unfollow_modal.dart';
import 'package:flutter/material.dart';

import '../Services/services.dart';

class UserFollow extends ChangeNotifier {
  Set<int> _followedUserIds = {};

  bool isUserFollowed(int userId) {
    return _followedUserIds.contains(userId);
  }

  Future<void> toggleFollow(Follow follow) async {
    try {
      Map<String, dynamic> responseData = await ApiService.fetchFollow(user_id: follow.user_id);
      print("This is Response Data: $responseData");

      if (responseData['status'] == 1 && responseData['msg'] == 'Follow success') {
        _followedUserIds.add(int.parse(follow.user_id));
      } else {
        _followedUserIds.remove(int.parse(follow.user_id));
      }

      notifyListeners();
    } catch (e) {
      throw e.toString();
    }
  }
}
