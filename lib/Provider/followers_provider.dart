import 'package:aas/Model/followers_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class FollowersProvider extends ChangeNotifier {
  Follower? _followers;

  Follower? get getFollowers => _followers;

  Future<void> getFollowersDetail() async {
    try {
      Follower getFollowers = await ApiService.getFollowersList();
      _followers = getFollowers;
      notifyListeners();
    } catch (e) {
      print('error from BMP: $e');
    }
  }
}
