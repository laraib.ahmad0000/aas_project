import 'package:aas/Model/user_block_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class FollowingBlockProvider extends ChangeNotifier {
  final ApiService apiService;

  FollowingBlockProvider({required this.apiService});

  void folowingblocking(UserBlockModal userBlockModal) async {
    try {
      await apiService.userBlock(
        user_id: userBlockModal.user_id,
      );

      notifyListeners();
    } catch (error) {}
  }
}
