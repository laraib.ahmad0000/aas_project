import 'package:aas/Model/following_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class FollowingProvider extends ChangeNotifier {
  FollowingModal? _followingModal;

  FollowingModal? get getFollowing => _followingModal;

  Future<void> getFollowingList() async {
    try {
      FollowingModal getFollowing = await ApiService.getFollowingList();
      _followingModal = getFollowing;
      notifyListeners();
    } catch (e) {
      print(e);
    }
  }
}
