import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:shared_preferences/shared_preferences.dart';

class FollowingScreenProvider with ChangeNotifier {
  List<String>? videoUrls;
  int currentIndex = 0;
  List<dynamic> videoList = [];

  Future<void> fetchVideos() async {
    String apiUrl = 'https://aasonline.co/api/c-logs';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forvideos = prefs.getString('sessionToken');

    try {
      final data = {'id': 'f'};
      final dio = Dio();
      dio.options.headers["Authorization"] = "Bearer $forvideos";

      final response = await dio.post(apiUrl, data: data);

      if (response.statusCode == 200) {
        videoList = response.data['videos'];
        print("This is VideoList:${videoList}");
        videoUrls = response.data['videos']
            .map<String>((video) => '${video['url']}')
            .toList();
        notifyListeners();
      } else {
        throw Exception('Failed to load videos: ${response.statusCode}');
      }
    } catch (e) {
      throw Exception('Failed to load videos: $e');
    }
  }
  void incrementComments(int videoIndex) {
    videoList[videoIndex]['comments']++;
    notifyListeners();
  }

}