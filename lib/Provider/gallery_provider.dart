import 'package:aas/Model/gallery_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/foundation.dart';

class GalleryProvider extends ChangeNotifier {
  GalleryModal? _gallery;

  GalleryModal? get gallery => _gallery;


  Future<void> getGallery() async {
    try {
      GalleryModal? gallery = await ApiService.getGalleryData();
    _gallery = gallery;
    notifyListeners();
    } catch (e) {print('Error getting gallery data: $e');
      
    }
  }
}