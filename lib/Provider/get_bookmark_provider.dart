import 'package:aas/Model/get_bookmark_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class GetBookmarkProvider extends ChangeNotifier {
  GetBookmarkModal? _getBookmark;

  GetBookmarkModal? get getBookmark => _getBookmark;

  Future<void> getBookmarkData() async {
    try {
      GetBookmarkModal getBookmark = await ApiService.getBookmarkData();
      _getBookmark = getBookmark;
      notifyListeners();
    } catch (e) {
      print('error from BMP: $e');
    }
  }
}
