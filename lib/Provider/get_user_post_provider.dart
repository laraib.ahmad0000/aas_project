import 'package:aas/Model/user_post.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/widgets.dart';

class GetUserPostProvider extends ChangeNotifier {
  UserPost? _userPost;

  UserPost? get userPost => _userPost;

  Future<void> getUserPostDetail() async {
    try {
      UserPost getUserPost = await ApiService.getUserPost();
      _userPost = getUserPost;
      notifyListeners();
    } catch (e) {
      print('error from get user post: $e');
    }
  }
}
