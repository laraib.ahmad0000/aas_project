import 'package:aas/Model/google_signin_modal.dart';
import 'package:aas/Model/login_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:flutter/material.dart';

class GoogleSignInProvider extends ChangeNotifier {
  final ApiService apiService;
  GoogleSignInProvider({required this.apiService});
  void googleSignInUser(GoogleSignInModal googleSignInModal) async {
    try {
      await apiService.googleSignIn(
        email: googleSignInModal.email,
        id: googleSignInModal.id,
        name: googleSignInModal.name,
        profile_url: googleSignInModal.profile_url,
      );
    } catch (error) {
      print('Error: $error');
    }
  }
}
