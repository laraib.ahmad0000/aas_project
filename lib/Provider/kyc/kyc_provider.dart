import 'dart:io';
import 'package:flutter/material.dart';

import '../../Services/services.dart';

class KycProvider extends ChangeNotifier {
  Future<void> registerKYC({
    required String? token,
    required String fullName,
    required String phone,
    required String address,
    required String idNumber,
    required String accType,
    required String bankName,
    required String accTitle,
    required String accNumber,
    required String dob,
    required String country,
    required String number,
    required BuildContext context,
    File? imgFile,
    File? imgFile2,
  }) async {
    await ApiService.registerKYC(
      token: token,
      fullName: fullName,
      phone: phone,
      address: address,
      idNumber: idNumber,
      accType: accType,
      bankName: bankName,
      imgFile2: imgFile2,
      accTitle: accTitle,
      accNumber: accNumber,
      imgFile: imgFile,
      dob: dob,
      country: country,
      number: number,
      context: context,
    );
    notifyListeners();
  }
}
