import 'package:aas/Model/likeUnlike_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class LikeUnlikeUserProvider extends ChangeNotifier {
  final ApiService apiService;

  LikeUnlikeUserProvider({required this.apiService});

   likeUnLikeUser(LikeUnlikeModal likeUnlikeModal) async {
    try {
      await apiService.likeUnlikeUser(videoId: likeUnlikeModal.video_id);
    } catch (e) {
      print('error from change notifierP: $e');
    }
  }
}
