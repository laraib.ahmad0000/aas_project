import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class ParaDetailProvider extends ChangeNotifier {
  List<Map<String, dynamic>> _paraDetail = [];
  final ApiService apiService = ApiService();

  List<Map<String, dynamic>> get paraDetail => _paraDetail;
  Future<void> fetchParaDetail(int parah_no) async {
    try {
      _paraDetail = await apiService.fetchParaDetails(parah_no);
      notifyListeners();
    } catch (e) {
      throw Exception('Failed to load verse $e');
    }
  }
}
