import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class QariDetailProvider extends ChangeNotifier {
  List<Map<String, dynamic>> _qariDetail = [];
  final ApiService apiService = ApiService();

  List<Map<String, dynamic>> get qariDetail => _qariDetail;

  Future<void> fetchQariDetail() async {
    try {
      _qariDetail = await apiService.fetchQariDetails();
      notifyListeners();
    } catch (e) {
      throw Exception('Failed to load qari data');
    }
  }
}
