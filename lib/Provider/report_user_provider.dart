import 'package:aas/Model/report_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class ReportProvider extends ChangeNotifier {
  final ApiService apiService;

  ReportProvider({required this.apiService});

  void submitReport(ReportModel reportModel) async {
    try {
      await apiService.userReport(
        user_id: reportModel.user_id,
        report: reportModel.reportDescription,
      );

      notifyListeners();
    } catch (error) {}
  }
}
