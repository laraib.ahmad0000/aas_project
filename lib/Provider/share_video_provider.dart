import 'package:aas/Model/share_video_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class ShareVideoProvier extends ChangeNotifier {
  final ApiService apiService;


  ShareVideoProvier({required this.apiService});

  void shareVideo(ShareVideoModal shareVideoModal, BuildContext context) async {
    try {
      await apiService.shareVideo(video_id: shareVideoModal.video_id, context: context);
    } catch (e) {
      print('error from change notifierP: $e');
    }
  }
}
