import 'package:aas/Model/Surah_modal.dart';
import 'package:aas/Model/surah_detail_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class SurahDetailProvider extends ChangeNotifier {
  List<Map<String, dynamic>> _surahDetail = [];
  final ApiService apiService = ApiService();

  List<Map<String, dynamic>> get surahDetail => _surahDetail;
  Future<void> fetchSurahDetail(int SurahId) async {
    try {
      _surahDetail = await apiService.fetchSurahDetails(SurahId);
      notifyListeners();
    } catch (e) {
      throw Exception('Failed to load verse $e');
    }
  }
}
