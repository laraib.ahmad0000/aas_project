import 'package:flutter/material.dart';

class SurahNumberProvider extends ChangeNotifier {
  String _formatNumber = '';

  String oneZero = '0';
  String twoZero = '00';
  String get formatNumber => _formatNumber;

  void formatSurahNumbers(int number) {
    if (number < 10) {
      _formatNumber = twoZero.toString() + number.toString();
    } else if (number >= 10 && number < 100) {
      _formatNumber = oneZero.toString() + number.toString();
    } else if (number >= 100 && number < 114) {
      _formatNumber = number.toString();
    }
    notifyListeners();
  }
}
