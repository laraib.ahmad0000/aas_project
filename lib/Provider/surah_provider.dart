import 'package:aas/Model/Surah_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class SurahProvider extends ChangeNotifier {
  final ApiService apiService;
  SurahProvider({required this.apiService});
  List<SurahModal> _surah = [];
  List<SurahModal> get surah => _surah;
  Future<void> fetchData() async {
    try {
      print('12');
      final List<Map<String, dynamic>> data = await apiService.fetchSurahData();
      _surah = data.map((json) => SurahModal.fromJson(json)).toList();
      notifyListeners();
    } catch (e) {
      print('Error fetching Quran data: $e');
    }
  }
}
