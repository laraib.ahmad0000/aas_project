import 'package:flutter/material.dart';

class TermsConditionProvider extends ChangeNotifier {
  bool isChecked = false;

  void toggleCheckBox() {
    isChecked = !isChecked;
    notifyListeners();
  }
}
