import 'package:aas/Model/total_prayers_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class TotalPrayersProvider extends ChangeNotifier{
  TotalPrayers? _totalPrayers;


  TotalPrayers? get totalPrayers => _totalPrayers;

  Future<void> getTotalPrayers()  async {
    try {
      TotalPrayers totalPrayers = await ApiService.totalPrayers();
      _totalPrayers = totalPrayers;
      notifyListeners();
    } catch (e) {
        print('Error during geting total prayers: $e');
    }
  }
}