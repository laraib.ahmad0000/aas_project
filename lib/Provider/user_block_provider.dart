import 'package:aas/Model/user_block_modal.dart';
import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';

class UserBlockProvider extends ChangeNotifier {
  final ApiService apiService;

  UserBlockProvider({required this.apiService});

  void blocking(UserBlockModal userBlockModal) async {
    try {
      await apiService.userBlock(
        user_id: userBlockModal.user_id,
      );

      notifyListeners();
    } catch (error) {
      print(error);
    }
  }
}
