import 'package:aas/Services/services.dart';
import 'package:flutter/material.dart';
import '../Model/user_data_modal.dart';

class UserDataProvider extends ChangeNotifier {
  UserModal? _userData;

  UserModal? get userData => _userData;

  Future<void> updateUserData(
      {double? lat,
      double? long,
      String? app_version,
      String? device_key}) async {
    try {
      UserModal userData = await ApiService.fetchUserData(
          lat: lat,
          long: long,
          app_version: '$app_version',
          device_key: device_key);
      _userData = userData;
      notifyListeners();

      // Access and print user data here
      // print('User Data: ${userData.name}');
    } catch (error) {
      print('Error updating user data: $error');
    }
  }
}
