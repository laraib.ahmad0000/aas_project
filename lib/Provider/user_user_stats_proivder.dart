import 'dart:convert';

import 'package:aas/Model/other_user_stats_modal.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';

import '../../Model/Other Users modals/other_user_stats_modal.dart';
import '../../Model/follow_unfollow_modal.dart';

class StatsProvider extends ChangeNotifier {
  UserSatesModel? _userStates;

  UserSatesModel? get userStates => _userStates;

  void setUser(UserSatesModel newUser) {
    _userStates = newUser;
    notifyListeners();
  }

  Future<void> fetchUserStates(Follow follow) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');
    String url =
        'https://aasonline.co/api/stats-other?user_id=${follow.user_id}';

    try {
      final response = await http.get(
        Uri.parse(url),
        headers: {'Authorization': 'Bearer $token'},
      );

      if (response.statusCode == 200) {
        final List<dynamic> responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          final Map<String, dynamic> userData = responseData.first['user'];
          setUser(UserSatesModel(
            name: userData['name'],
            profileImage: userData['profileimage'],
            followers: userData['followers'],
            following: userData['following'],
            posts: userData['posts'],
          ));
        }
      } else {
        throw Exception('Failed to load user data');
      }
    } catch (error) {
      throw Exception('Error fetching user data: $error');
    }
  }
}
