import 'package:aas/Model/viewModel.dart';
import 'package:flutter/cupertino.dart';

import '../Services/services.dart';

class ViewProvider extends ChangeNotifier {
  final ApiService apiService;

  ViewProvider({required this.apiService});

  Future<void> viewVideo(ViewModel viewModel) async {
    try {
      await apiService.viewClogs(viewModel.id);
      // Notify listeners to update UI
      notifyListeners();
    } catch (e) {
      print('error from change notifier: $e');
    }
  }
}
