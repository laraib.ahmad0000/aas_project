import 'package:aas/Model/MLM%20modals/claim_reward_modal.dart';
import 'package:aas/Model/MLM%20modals/community_reward_modal.dart';
import 'package:aas/Provider/MLM/claim_reward_provider.dart';
import 'package:aas/Provider/MLM/community_reward_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/constants/ad_helper.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

class ClaimRewardScreen extends StatefulWidget {
  const ClaimRewardScreen({super.key});

  @override
  State<ClaimRewardScreen> createState() => _ClaimRewardScreenState();
}

class _ClaimRewardScreenState extends State<ClaimRewardScreen> {
  NativeAd? nativeAd;
  bool isNativeAdLoaded = false;
  late BannerAd _bannerAd;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    loadNativeAd();
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    final DateFormat todayformatter = DateFormat('yyyy-MM-dd');
    final currentDate = DateTime.now();
    final String currentDateIs = todayformatter.format(currentDate);
    Provider.of<CommunityRewardProvider>(context, listen: false)
        .getCommunityReward(currentDateIs.toString());
  }

  void loadNativeAd() {
    nativeAd = NativeAd(
        adUnitId: AdHelper.nativeAdUnitId,
        listener: NativeAdListener(onAdLoaded: (ad) {
          setState(() {
            isNativeAdLoaded = true;
          });
        }, onAdFailedToLoad: (ad, error) {
          nativeAd!.dispose();
        }),
        request: const AdRequest(),
        nativeTemplateStyle:
            NativeTemplateStyle(templateType: TemplateType.small));
    nativeAd!.load();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _bannerAd.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final DateFormat todayformatter = DateFormat('yyyy-MM-dd');
    final currentDate = DateTime.now();
    final String currentDateIs = todayformatter.format(currentDate);
    CommunityRewardModal? communityReward =
        Provider.of<CommunityRewardProvider>(context).communityRewardModal;
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text(
          'Community Reward',
          style: TextStyle(color: yellowC),
        ),
        backgroundColor: primaryColor,
        iconTheme: const IconThemeData(color: white),
      ),
      body: communityReward != null
          ? Column(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Container(
                  child: Column(
                    children: [
                      Card(
                        margin: const EdgeInsets.only(top: 10, left: 10, right: 10),
                        elevation: 10,
                        color: Colors.yellow[50],
                        child: Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Column(
                              children: [
                                Center(
                                    child: Text(
                                  'Reward Date: ${communityReward!.rewardDate.toString()}',
                                  style: const TextStyle(
                                      color: primaryColor,
                                      fontWeight: FontWeight.w500),
                                )),
                                const Divider(
                                  indent: 20,
                                  endIndent: 20,
                                  thickness: 3,
                                  color: black,
                                ),
                                const SizedBox(
                                  height: 6.0,
                                ),
                                Container(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Left Members with 5 Prayers',
                                      style: rewardStyle,
                                    ),
                                    Text(
                                      '${communityReward.leftMembersWithFivePrayers}',
                                      style: rewardStyle,
                                    ),
                                  ],
                                )),
                                const SizedBox(
                                  height: 6.0,
                                ),
                                Container(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Right Members with 5 Prayers',
                                      style: rewardStyle,
                                    ),
                                    Text(
                                      '${communityReward.rightMembersWithFivePrayers}',
                                      style: rewardStyle,
                                    ),
                                  ],
                                )),
                                const SizedBox(
                                  height: 6.0,
                                ),
                                Container(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Reward       =',
                                      style: rewardStyle,
                                    ),
                                    // Text('='),
                                    Text(
                                      'Weaker Side Members X 5 Coins',
                                      style: rewardStyle,
                                    ),
                                  ],
                                )),
                                const SizedBox(
                                  height: 6.0,
                                ),
                                Container(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Reward       =',
                                      style: rewardStyle,
                                    ),
                                    // Text('='),
                                    Text(
                                      '${communityReward.weakerSideMembers} X ${communityReward.perMemberReward}',
                                      style: rewardStyle,
                                    ),
                                  ],
                                )),
                                const SizedBox(
                                  height: 6.0,
                                ),
                                Container(
                                    child: Row(
                                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      'Reward       =',
                                      style: rewardStyle,
                                    ),
                                    // Text('='),
                                    Text(
                                      '${communityReward.rewardToClaim} Coins',
                                      style: const TextStyle(
                                          color: primaryColor,
                                          fontSize: 15,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                )),
                                const SizedBox(
                                  height: 13.0,
                                ),
                                communityReward.rewardToClaim == 0
                                    ? Container(
                                        height:
                                            MediaQuery.of(context).size.height * 0.05,
                                        width:
                                            MediaQuery.of(context).size.width * 0.4,
                                        decoration: BoxDecoration(
                                          color: lightBlack,
                                          borderRadius: BorderRadius.circular(8.0),
                                        ),
                                        child: const Center(
                                          child: Text(
                                            'CLAIM NOW',
                                            style: TextStyle(
                                                color: yellowC,
                                                fontWeight: FontWeight.bold),
                                          ),
                                        ),
                                      )
                                    : InkWell(
                                        onTap: () async {
                                          await _loadAndShowInterstitialAd(context);
                                          final CR = Provider.of<ClaimRewardprovider>(
                                              context,
                                              listen: false);
                                          final claimR = ClaimRewardModal(
                                              date: currentDateIs.toString(),
                                              rewarded_coins:
                                                  communityReward.rewardToClaim);
                      
                                          CR.claimReward(claimR);
                                        },
                                        child: Container(
                                          height: MediaQuery.of(context).size.height *
                                              0.05,
                                          width:
                                              MediaQuery.of(context).size.width * 0.4,
                                          decoration: BoxDecoration(
                                            color: primaryColor,
                                            borderRadius: BorderRadius.circular(8.0),
                                          ),
                                          child: const Center(
                                            child: Text(
                                              'CLAIM NOW',
                                              style: TextStyle(
                                                  color: yellowC,
                                                  fontWeight: FontWeight.bold),
                                            ),
                                          ),
                                        ),
                                      )
                              ],
                            )),
                      ),
                         Container(
                  margin: const EdgeInsets.symmetric(horizontal: 10.0),
                  width: MediaQuery.of(context).size.width,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      communityReward.isEligible == 0
                          ? Text(
                              'Not eligible due to self prayers.',
                              style: redText,
                            )
                          : const Text(''),
                      communityReward.isClaimed == 1
                          ? const Text(
                              'Your reward has been added to your wallet successfully.',
                              style: TextStyle(
                                fontSize: 14.5,
                                color: black,
                                fontWeight: FontWeight.bold,
                                fontFamily: 'Amiri',
                              ),
                            )
                          : const Text(''),
                    ],
                  ),
                ),
                    ],
                  ),
                ),
             
                isNativeAdLoaded
                    ? Container(
                        decoration: const BoxDecoration(color: white),
                        height: 130,
                        child: AdWidget(ad: nativeAd!),
                      )
                    : const SizedBox(),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 50,
                  child: AdWidget(ad: _bannerAd),
                ),
              ],
            )
          : const Center(child: CircularProgressIndicator()),
    );
  }

  _loadAndShowInterstitialAd(BuildContext context) {
    // Access the AdProvider using the Provider.of method
    AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

    // Load the interstitial ad
    adProvider.loadInterstitialAd();

    // Check if the interstitial ad is loaded
    if (adProvider.isInterstitialAdLoaded) {
      // resetNumber == 0;
      // Show the interstitial ad
      adProvider.showInterstitialAd();
    } else {
      // The ad is not loaded yet, you can handle this case accordingly
      print('Interstitial ad is not loaded yet. Please try again later.');
    }
  }
}

TextStyle rewardStyle = const TextStyle(color: black, fontSize: 14);
final redText = const TextStyle(
  fontSize: 14.5,
  color: red,
  fontWeight: FontWeight.bold,
  fontFamily: 'Amiri',
);
