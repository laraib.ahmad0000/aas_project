import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Screens/BottomNavScreens/MLM/left_members_tab.dart';
import 'package:aas/Screens/BottomNavScreens/MLM/right_members_tab.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/cash_wallet_tab.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/coins_tab_screen.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class CommunityDetailScreen extends StatefulWidget {
  const CommunityDetailScreen({super.key});

  @override
  State<CommunityDetailScreen> createState() => _CommunityDetailScreenState();
}

class _CommunityDetailScreenState extends State<CommunityDetailScreen>with SingleTickerProviderStateMixin {
 late BannerAd _bannerAd;


  @override
  void initState() {
    super.initState();
     _bannerAd = context.read<AdProvider>().createBannerAd();
    // _bannerAd = context.read<AdProvider>().createBannerAd();
    // Provider.of<walletDetailProvider>(context, listen: false).walletDetail();
    _tabController = TabController(length: 2, vsync: this);

    _tabController.addListener(() {
      setState(() {});
    });
  }
   late TabController _tabController;

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
     _bannerAd.dispose();
  }

  @override
  Widget build(BuildContext context) {final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      // appBar: AppBar(centerTitle: true,),
      body: SafeArea(
        
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: primaryColor,
          child: Column(

            children: [
               Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: 60,
                  child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,

                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5.0, top: 20),
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  // color: lightBlack,
                                  border: Border.all(color: white, width: 2)),
                              child: Center(
                                child: Icon(Icons.arrow_back,
                                    color: white, size: 20),
                              ),
                            ),
                          )),
                      // SizedBox(
                      //   width: 100,
                      // ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Community Members",
                              style: TextStyle(
                                  color: Color.fromRGBO(
                                      252, 216, 138, 1.0), // or any other color
                                  decoration: TextDecoration.none,
                                  fontSize: 22 // remove underline
                                  ),
                            ),
                               
                          
                          ],
                        ),
                      ),

                         Padding(
                           padding: const EdgeInsets.only(top: 30.0),
                           child: InkWell(
                                     onTap: (){
                                        dialogeShow2(context);
                                     },
                                     child: FaIcon(CupertinoIcons.info, color: white,)),
                         ),
                   
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.0,),
            
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                         SizedBox(height: 30.0,),
                      // 30.heightBox,
                      Container(
                        height: screenHeight * .05,
                        width: screenWidth * .8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xFF111111),
                            ),
                          ),
                          child: TabBar(
                            labelColor: white,
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                              color: Color.fromRGBO(20, 77, 70, 1.0),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            unselectedLabelColor: Color(0xFFABA9A9),
                            controller: _tabController,
                            tabs: [
                              Tab(text: 'Left Side'),
                              Tab(text: 'Right Side'),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            
                          LeftMembersTab(),
                            RightMembersTab()
                            
                            ],
                        ),
                      ),
                      // Container(
                      //   width: MediaQuery.of(context).size.width,
                      //   height: 60,
                      //   child: AdWidget(ad: _bannerAd),
                      // ),
                    ],
                  ),
                ),
              ),

              Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: AdWidget(ad: _bannerAd),
                    ),
            ],
          ),
        ),
      ),
      
    );
  }

  dialogeShow2(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(15.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 130.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Text(
                      "You can view your community members details here divided into Left-Side Members and Right-Side Members with the number of their team and number of their prayers marked by them and marked by their community members.",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}