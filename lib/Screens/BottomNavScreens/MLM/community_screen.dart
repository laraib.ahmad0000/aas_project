import 'dart:math';

import 'package:aas/Model/MLM%20modals/community_modal.dart';
import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/MLM/community_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/MLM/claim_reward_screen.dart';
import 'package:aas/Screens/BottomNavScreens/MLM/community_detail_screen.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class CommunityScreen extends StatefulWidget {
  const CommunityScreen({super.key});

  @override
  State<CommunityScreen> createState() => _CommunityScreenState();
}

class _CommunityScreenState extends State<CommunityScreen> {
  late BannerAd _bannerAd;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    final DateFormat todayformatter = DateFormat('yyyy-MM-dd');
    final currentDate = DateTime.now();
    final String currentDateIs = todayformatter.format(currentDate);
    print('formated current date is: $currentDateIs');
    Provider.of<CommunityProvider>(context, listen: false)
        .getCommunity(currentDateIs.toString());
  }

  void dispose() {
    super.dispose();
    _bannerAd.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    CommunityModal? communityData =
        Provider.of<CommunityProvider>(context).communityModal;
    // print('left members: ${communityData!.leftMembers}');
    // print('right members: ${communityData.rightMembers}');
    // print('direct members: ${communityData.directMembers}');
    // print('indirect members: ${communityData.indirectMembers}');
    // print('total members: ${communityData.totalMembers}');
    // print('total prayers: ${communityData.todayPrayers}');

    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              communityData != null
                  ? upperContainer(
                      height, width, context, userData, communityData)
                  : Center(
                      child: CircularProgressIndicator(),
                    ),
              // treeContainer(height, width),
              SizedBox(
                height: 20.0,
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
            ],
          ),
        ),
      ),
    );
  }

  upperContainer(double height, double width, BuildContext context, userData,
      communityData) {
        final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      // height: height * 0.55,
      width: width,
      color: primaryColor,
      child: Column(
        children: [
          Container(
            height: screenHeight * .080,
            child: Center(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    width: 40.0,
                  ),
                  // SizedBox(),
                  Text(
                    userData.name,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: Color.fromRGBO(252, 216, 138, 1.0),
                      fontSize: 19,
                    ),
                  ),
                  SizedBox(
                    width: 10.w,
                  ),
                  // Image.asset(
                  //   'assets/images/flag.png',
                  //   height: 22.h,
                  //   width: 22.w,
                  //   fit: BoxFit.fill,
                  // ),
                  Padding(
                    padding: const EdgeInsets.only(right: 10.0),
                    child: InkWell(
                        onTap: () {
                          dialogeShow(context);
                        },
                        child: FaIcon(
                          CupertinoIcons.info,
                          color: Colors.white,
                        )),
                  ),
                ],
              ),
            ),
          ),
          Container(
            //margin: EdgeInsets.symmetric(horizontal: 2),
            //padding: EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
              color: white,
              borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(30.0),
                  topRight: Radius.circular(30.0)),
            ),
            child: Column(
              // crossAxisAlignment: CrossAxisAlignment.
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                   SizedBox(height: 30.0,),
                // 30.heightBox,
                Center(
                  child: Container(
                    height: 50,
                    width: 200,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          child:
                          //  "Today's Prayers"
                          //     .text
                          //     .bold
                          //     .size(20)
                          //     .color(
                          //       Color.fromRGBO(),
                          //     )
                          //     .make(),
                          Text("Today's prayer", style: TextStyle(color: Color.fromRGBO(20, 77, 70, 1.0)),)
                        ),
                        SizedBox(
                          width: 10.0,
                        ),
                        Text(
                          communityData.todayPrayers.toString(),
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        Text(
                          "/5",
                          style: TextStyle(
                              fontSize: 20,
                              color: Colors.black,
                              fontWeight: FontWeight.bold),
                        ),
                        // Row(
                        //   // mainAxisAlignment: MainAxisAlignment.start,
                        //   crossAxisAlignment: CrossAxisAlignment.center,
                        //   children: [
                        //     // Container(
                        //     //   child: CircleAvatar(
                        //     //     radius: 10,
                        //     //     backgroundColor: Colors.transparent,
                        //     //   ),
                        //     //   decoration: BoxDecoration(
                        //     //       shape: BoxShape.circle,
                        //     //       border:
                        //     //           Border.all(color: black, width: 2.0)),
                        //     // ),
                        //     Center(
                        //       child:
                        //     ),
                        //     Center(
                        //       child: Text(
                        //         "/5",
                        //         style: TextStyle(
                        //             fontSize: 20,
                        //             color: Colors.black,
                        //             fontWeight: FontWeight.bold),
                        //       ),
                        //     )
                        //   ],
                        // ),
                      ],
                    ),
                  ),
                ),
                // SizedBox(
                //   height: 10,
                // ),
                Container(
                  // height: 0,
                  // width: 600,
                  // color: Colors.purple,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Padding(
                            padding: const EdgeInsets.only(left: 15.0),
                            child: Text(
                              'Community Members',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                                fontSize: 19,
                              ),
                            ),
                          ),
                          Text(
                            communityData.totalMembers.toString(),
                            style: TextStyle(
                                color: black,
                                fontSize: 17,
                                fontWeight: FontWeight.w900),
                          ),
                          Padding(
                            padding: const EdgeInsets.only(right: 10.0),
                            child: InkWell(
                                onTap: () {
                                  dialogeShow2(context);
                                },
                                child: FaIcon(
                                  CupertinoIcons.info,
                                  color: black,
                                )),
                          ),
                        ],
                      ),
                      Column(
                        children: [
                          Card(
                            margin: EdgeInsets.all(20),
                            elevation: 10,
                            color: white,
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Left Side Members:'),
                                      Text(
                                        communityData.leftMembers.toString(),
                                        style: TextStyle(
                                            color: black, fontSize: 20),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Right Side Members:'),
                                      Text(
                                        communityData.rightMembers.toString(),
                                        style: TextStyle(
                                            color: black, fontSize: 20),
                                      )
                                    ],
                                  ),

                                  // Row(
                                  //   mainAxisAlignment:
                                  //       MainAxisAlignment.spaceBetween,
                                  //   children: [
                                  //     Text('Direct Members:'),
                                  //     Text(
                                  //       communityData.directMembers.toString(),
                                  //       style: TextStyle(
                                  //           color: black, fontSize: 20),
                                  //     )
                                  //   ],
                                  // ),
                                  // Row(
                                  //   mainAxisAlignment:
                                  //       MainAxisAlignment.spaceBetween,
                                  //   children: [
                                  //     Text('In-direct Members:'),
                                  //     Text(
                                  //       communityData.indirectMembers
                                  //           .toString(),
                                  //       style: TextStyle(
                                  //           color: black, fontSize: 20),
                                  //     )
                                  //   ],
                                  // ),
                                ],
                              ),
                            ),
                          ),
                          Card(
                            margin: EdgeInsets.all(20),
                            elevation: 10,
                            color: white,
                            child: Padding(
                              padding: EdgeInsets.all(10.0),
                              child: Column(
                                children: [
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('Direct Members:'),
                                      Text(
                                        communityData.directMembers.toString(),
                                        style: TextStyle(
                                            color: black, fontSize: 20),
                                      )
                                    ],
                                  ),
                                  Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: [
                                      Text('In-direct Members:'),
                                      Text(
                                        communityData.indirectMembers
                                            .toString(),
                                        style: TextStyle(
                                            color: black, fontSize: 20),
                                      )
                                    ],
                                  ),
                                  // Row(
                                  //   mainAxisAlignment:
                                  //       MainAxisAlignment.spaceBetween,
                                  //   children: [
                                  //     Text('Direct Members:'),
                                  //     Text(
                                  //       communityData.directMembers.toString(),
                                  //       style: TextStyle(
                                  //           color: black, fontSize: 20),
                                  //     )
                                  //   ],
                                  // ),
                                  // Row(
                                  //   mainAxisAlignment:
                                  //       MainAxisAlignment.spaceBetween,
                                  //   children: [
                                  //     Text('In-direct Members:'),
                                  //     Text(
                                  //       communityData.indirectMembers
                                  //           .toString(),
                                  //       style: TextStyle(
                                  //           color: black, fontSize: 20),
                                  //     )
                                  //   ],
                                  // ),
                                ],
                              ),
                            ),
                          ),
                          // Container(
                          //   height: 60,
                          //   width: context.screenWidth / 2,
                          //   // color: Colors.red,
                          //   child: Stack(
                          //     children: <Widget>[
                          //       Positioned(
                          //         // top: 7,
                          //         left: 10,
                          //         height: 65,
                          //         width: 150,
                          //         child: Image.asset(
                          //           "assets/images/left members.png",

                          //           // fit: BoxFit.fill,
                          //         ),
                          //       ),
                          //       Positioned(
                          //         left: 140,
                          //         // height:80,
                          //         // width: 140,
                          //         child: Container(
                          //           width: 45,
                          //           child: Stack(
                          //             children: [
                          //               CircleAvatar(
                          //                 radius: 30,
                          //                 backgroundColor: Color(0xff144d46),
                          //               ),
                          //               Positioned(
                          //                   top: 15.0,
                          //                   left: 15,
                          //                   right: 10,
                          // child: Text(
                          //   communityData.leftMembers
                          //       .toString(),
                          //   style: TextStyle(
                          //       color: white, fontSize: 20),
                          // ))
                          //             ],
                          //           ),
                          //           decoration: BoxDecoration(
                          //               shape: BoxShape.circle,
                          //               border: Border.all(
                          //                   color: Colors.yellow, width: 2.0)),
                          //         ),
                          //       ),
                          //     ],
                          //   ),
                          // ),
                          // Expanded(
                          //   child: Container(
                          //     height: 60,
                          //     //width: 110,
                          //     // color: Colors.red,
                          //     child: Stack(
                          //       children: <Widget>[
                          //         Positioned(
                          //           // top: 7,
                          //           left: 10,
                          //           height: 65,
                          //           width: 150,
                          //           child: Image.asset(
                          //             "assets/images/right members.png",

                          //             // fit: BoxFit.fill,
                          //           ),
                          //         ),
                          //         Positioned(
                          //           left: 140,
                          //           // height:80,
                          //           // width: 140,
                          //           child: Container(
                          //             width: 45,
                          //             child: Stack(
                          //               children: [
                          //                 CircleAvatar(
                          //                   radius: 30,
                          //                   backgroundColor: Color(0xff144d46),
                          //                 ),
                          //                 Positioned(
                          //                     top: 15.0,
                          //                     left: 15,
                          //                     right: 10,
                          //                     child: Text(
                          //                         communityData.rightMembers
                          //                           .toString(),
                          //                       style: TextStyle(
                          //                           color: white, fontSize: 20),
                          //                     ))
                          //               ],
                          //             ),
                          //             decoration: BoxDecoration(
                          //                 shape: BoxShape.circle,
                          //                 border: Border.all(
                          //                     color: Colors.yellow,
                          //                     width: 2.0)),
                          //           ),
                          //         ),
                          //       ],
                          //     ),
                          //   ),
                          // ),
                        ],
                      ),
                      // Row(
                      //   children: [
                      //     Container(
                      //       height: 60,
                      //       width: context.screenWidth / 2,
                      //       // color: Colors.red,
                      //       child: Stack(
                      //         children: <Widget>[
                      //           Positioned(
                      //             // top: 7,
                      //             left: 10,
                      //             height: 65,
                      //             width: 150,
                      //             child: Image.asset(
                      //               "assets/images/direct_members.png",

                      //               // fit: BoxFit.fill,
                      //             ),
                      //           ),
                      //           Positioned(
                      //             left: 140,
                      //             // height:80,
                      //             // width: 140,
                      //             child: Container(
                      //               width: 45,
                      //               child: Stack(
                      //                 children: [
                      //                   CircleAvatar(
                      //                     radius: 30,
                      //                     backgroundColor: Color(0xff144d46),
                      //                   ),
                      //                   Positioned(
                      //                       top: 15.0,
                      //                       left: 15,
                      //                       right: 10,
                      //                       child: Text(
                      //                        communityData.directMembers.toString(),
                      //                         style: TextStyle(
                      //                             color: white, fontSize: 20),
                      //                       ))
                      //                 ],
                      //               ),
                      //               decoration: BoxDecoration(
                      //                   shape: BoxShape.circle,
                      //                   border: Border.all(
                      //                       color: Colors.yellow, width: 2.0)),
                      //             ),
                      //           ),
                      //         ],
                      //       ),
                      //     ),
                      //     Expanded(
                      //       child: Container(
                      //         height: 60,
                      //         //width: 110,
                      //         // color: Colors.red,
                      //         child: Stack(
                      //           children: <Widget>[
                      //             Positioned(
                      //               // top: 7,
                      //               left: 10,
                      //               height: 65,
                      //               width: 150,
                      //               child: Image.asset(
                      //                 "assets/images/indirect_members.png",

                      //                 // fit: BoxFit.fill,
                      //               ),
                      //             ),
                      //             Positioned(
                      //               left: 140,
                      //               // height:80,
                      //               // width: 140,
                      //               child: Container(
                      //                 width: 45,
                      //                 child: Stack(
                      //                   children: [
                      //                     CircleAvatar(
                      //                       radius: 30,
                      //                       backgroundColor: Color(0xff144d46),
                      //                     ),
                      //                     Positioned(
                      //                         top: 15.0,
                      //                         left: 15,
                      //                         right: 10,
                      //                         child: Text(
                      //                           communityData.indirectMembers
                      //                               .toString(),
                      //                           style: TextStyle(
                      //                               color: white, fontSize: 20),
                      //                         ))
                      //                   ],
                      //                 ),
                      //                 decoration: BoxDecoration(
                      //                     shape: BoxShape.circle,
                      //                     border: Border.all(
                      //                         color: Colors.yellow,
                      //                         width: 2.0)),
                      //               ),
                      //             ),
                      //           ],
                      //         ),
                      //       ),
                      //     ),
                      //   ],
                      // ),
                      // Padding(
                      //   padding: const EdgeInsets.only(left: 15.0),
                      //   child: Row(
                      //     // crossAxisAlignment: CrossAxisAlignment.center,
                      //     mainAxisAlignment: MainAxisAlignment.center,
                      //     children: [
                      //       Text(
                      //         'Total Members',
                      //         style: TextStyle(
                      //           fontWeight: FontWeight.bold,
                      //           color: primaryColor,
                      //           fontSize: 19,
                      //         ),
                      //       ),
                      //       SizedBox(
                      //         width: 15.0,
                      //       ),
                      // Text(
                      //   communityData.totalMembers.toString(),
                      //   style: TextStyle(
                      //       color: black,
                      //       fontSize: 17,
                      //       fontWeight: FontWeight.w900),
                      // )
                      //     ],
                      //   ),
                      // ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                // Padding(
                //   padding: EdgeInsets.symmetric(horizontal: 15.w),
                //   child: Divider(
                //     thickness: 1,
                //     color: Colors.black26,
                //   ),
                // ),
                SizedBox(
                  height: 10,
                ),
                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(left: 30),
                      child: Text(
                        "Community Prayers: ",
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: primaryColor,
                          fontSize: 19,
                        ),
                      ),
                    )
                  ],
                ),
                Card(
                    margin: EdgeInsets.all(20),
                    elevation: 10,
                    color: white,
                    child: Padding(
                      padding: EdgeInsets.all(10.0),
                      child: Column(
                        children: [
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Left Side Members Prayers:'),
                              Text(
                                communityData.leftMembersPrayers.toString(),
                                style: TextStyle(color: black, fontSize: 20),
                              )
                            ],
                          ),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: [
                              Text('Right Side Members Prayers:'),
                              Text(
                                communityData.rightMembersPrayers.toString(),
                                style: TextStyle(color: black, fontSize: 20),
                              )
                            ],
                          ),
                        ],
                      ),
                    )),
                // Container(
                // height: 60,
                // width: 600,
                // color: Colors.purple,
                // child: Row(
                //   children: [
                //     Container(
                //       height: 90,
                //       width: width / 2,
                //       // color: Colors.orange,
                //       child: Stack(
                //         children: <Widget>[
                //           Positioned(
                //             // top: 7,
                //             left: 10,
                //             height: 65,
                //             width: 150,
                //             child: Image.asset(
                //               "assets/images/left members.png",

                //               // fit: BoxFit.fill,
                //             ),
                //           ),
                //           Positioned(
                //             left: 140,
                //             // height:80,
                //             // width: 140,
                //             child: Container(
                //               width: 45,
                //               child: Stack(
                //                 children: [
                //                   CircleAvatar(
                //                     radius: 30,
                //                     backgroundColor: Color(0xff144d46),
                //                   ),
                //                   Positioned(
                //                       top: 15.0,
                //                       left: 15,
                //                       right: 10,
                //                       child: Text(
                //                        communityData.leftMembersPrayers
                //                             .toString(),
                //                         style: TextStyle(
                //                             color: white, fontSize: 20),
                //                       ))
                //                 ],
                //               ),
                //               decoration: BoxDecoration(
                //                   shape: BoxShape.circle,
                //                   border: Border.all(
                //                       color: Colors.yellow, width: 2.0)),
                //             ),
                //           ),
                //         ],
                //       ),
                //     ),
                //     Expanded(
                //       child: Container(
                //         height: 90,
                //         //width: 110,
                //         // color: Colors.red,
                //         child: Stack(
                //           children: <Widget>[
                //             Positioned(
                //               // top: 7,
                //               left: 10,
                //               height: 65,
                //               width: 150,
                //               child: Image.asset(
                //                 "assets/images/right members.png",

                //                 // fit: BoxFit.fill,
                //               ),
                //             ),
                //             Positioned(
                //               left: 140,
                //               // height:80,
                //               // width: 140,
                //               child: Container(
                //                 width: 45,
                //                 child: Stack(
                //                   children: [
                //                     CircleAvatar(
                //                       radius: 30,
                //                       backgroundColor: Color(0xff144d46),
                //                     ),
                //                     Positioned(
                //                         top: 15.0,
                //                         left: 15,
                //                         right: 10,
                //                         child: Text(
                //                         communityData.rightMembersPrayers
                //                               .toString(),
                //                           style: TextStyle(
                //                               color: white, fontSize: 20),
                //                         ))
                //                   ],
                //                 ),
                //                 decoration: BoxDecoration(
                //                     shape: BoxShape.circle,
                //                     border: Border.all(
                //                         color: Colors.yellow, width: 2.0)),
                //               ),
                //             ),
                //           ],
                //         ),
                //       ),
                //     ),
                //   ],
                // ),
                // ),

                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 15.0),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      // InkWell(
                      //   onTap: () {
                      //     // _loadAndShowInterstitialAd(context);
                      //     Navigator.push(
                      //         context,
                      //         MaterialPageRoute(
                      //             builder: (context) =>
                      //                 CommunityDetailScreen()));
                      //   },
                      //   child: Container(
                      //     height: MediaQuery.of(context).size.height * 0.05,
                      //     width: MediaQuery.of(context).size.width * 0.4,
                      //     decoration: BoxDecoration(
                      //       color: primaryColor,
                      //       borderRadius: BorderRadius.circular(8.0),
                      //     ),
                      //     child: Center(
                      //       child: Text(
                      //         'Community Details',
                      //         style: TextStyle(color: white),
                      //       ),
                      //     ),
                      //   ),
                      // ),
                      InkWell(
                        onTap: () {
                          _loadAndShowInterstitialAd(context);
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => ClaimRewardScreen()));
                        },
                        child: Container(
                          height: MediaQuery.of(context).size.height * 0.05,
                          width: MediaQuery.of(context).size.width * 0.3,
                          decoration: BoxDecoration(
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          child: Center(
                            child: Text(
                              'Claim Reward',
                              style: TextStyle(color: white),
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          // Container(
          //   height: height * 0.5,
          //   width: width,
          //   color: white,
          //   child: Column(
          //     mainAxisSize: MainAxisSize.max,
          //     children: [
          //       Expanded(
          //         child: InteractiveViewer(
          //           constrained: false,
          //           boundaryMargin: EdgeInsets.all(100),
          //           minScale: 0.01,
          //           maxScale: 2.6,
          //           child: Padding(
          //             padding: const EdgeInsets.only(left: 40.0),
          //             child: GraphView(
          //               graph: graph,
          //               algorithm: BuchheimWalkerAlgorithm(
          //                 builder,
          //                 TreeEdgeRenderer(builder),
          //               ),
          //               paint: Paint()
          //                 ..color = Colors.black
          //                 ..strokeWidth = 3
          //                 ..style = PaintingStyle.stroke,
          //               builder: (Node node) {
          //                 var a = node.key!.value as int;
          //                 return rectangleWidget(a);
          //               },
          //             ),
          //           ),
          //         ),
          //       ),
          //     ],
          //   ),
          // )
        ],
      ),
    );
  }

  dialogeShow(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(15.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 220.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Text(
                      "Community section is the main part of the earning where anyone can join anyone’s community to earn more coins as everyone in the community can earn coins from their team as their team marked 5 prayers. Community section works as binary tree where team is divided in to 2 main parts Left-Side Members and Right-Side Members. At the end of the day, both sides of the community are compared with the number of prayers marked done. The lower number of the community part will be picked for the rewards and their prayers count will be multiplied with 5 coins each prayer marked done and the amount will be sent to the community parent.",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  dialogeShow2(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(15.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 130.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Text(
                      "Direct Members are those who have joined your community with your referral code while the indirect members are those who have joined the community with the referral code of the members of your direct members.",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _loadAndShowInterstitialAd(BuildContext context) {
    // Access the AdProvider using the Provider.of method
    AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

    // Load the interstitial ad
    adProvider.loadInterstitialAd();

    // Check if the interstitial ad is loaded
    if (adProvider.isInterstitialAdLoaded) {
      // resetNumber == 0;
      // Show the interstitial ad
      adProvider.showInterstitialAd();
    } else {
      // The ad is not loaded yet, you can handle this case accordingly
      print('Interstitial ad is not loaded yet. Please try again later.');
    }
  }
}
