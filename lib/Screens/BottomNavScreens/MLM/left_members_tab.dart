import 'package:aas/Model/MLM%20modals/left_side_modal.dart';
import 'package:aas/Model/MLM%20modals/right_side_modal.dart';
import 'package:aas/Provider/MLM/left_side_provider.dart';
import 'package:aas/Provider/MLM/right_side_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class LeftMembersTab extends StatefulWidget {
  const LeftMembersTab({super.key});

  @override
  State<LeftMembersTab> createState() => _LeftMembersTabState();
}

class _LeftMembersTabState extends State<LeftMembersTab> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<LeftSideProvider>(context, listen: false).getLeftSide();
  }

  @override
  Widget build(BuildContext context) {
    LeftSideModal? leftSideData =
        Provider.of<LeftSideProvider>(context).leftSideModal;
    return Scaffold(
      body: leftSideData != null
          ? ListView.separated(
              shrinkWrap: true,
              scrollDirection: Axis.vertical,
              physics: BouncingScrollPhysics(),
              padding: EdgeInsets.zero,
              itemCount: leftSideData.left_members.length,
              itemBuilder: (context, index) {
                // final ledger = earningLedgerList[index];
                return InkWell(
                  onTap: () {
                    // popupForUserData(context, leftSideData, index);
                  },
                  child: Card(
                    margin: EdgeInsets.all(20),
                    elevation: 10,
                    color: Colors.yellow[50],
                    child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  CircleAvatar(
                                      radius: 22,
                                      backgroundImage: NetworkImage(leftSideData
                                          .left_members[index].profileImage)),
                                  SizedBox(
                                    width: 10.0,
                                  ),
                                  Container(
                                    height: 44,
                                    // color: lightBlack,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Text(leftSideData
                                            .left_members[index].name),
                                        Text(
                                          'ID: ${leftSideData.left_members[index].referralCode}',
                                          style: TextStyle(fontSize: 12),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Column(
                                children: [
                                  Container(
                                    height: 30,
                                    width: 29,
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(8.0),
                                        border: Border.all(color: lightBlack)),
                                    child: Row(
                                      mainAxisAlignment:
                                          MainAxisAlignment.center,
                                      children: [
                                        Text(
                                          leftSideData
                                              .left_members[index].todayPrayers
                                              .toString(),
                                          style: TextStyle(fontSize: 12),
                                        ),
                                        Text(
                                          '/5',
                                          style: TextStyle(fontSize: 12),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            )
                          ],
                        )),
                  ),
                );
              },
              separatorBuilder: (context, index) {
                return Divider(
                  color: Colors.black26,
                  indent: 50.0,
                  endIndent: 50.0,
                );
              },
            )
          : Center(child: CircularProgressIndicator()),
    );
  }

  // Future<dynamic> popupForUserData(
  //     BuildContext context, LeftSideModal leftSideData, int index) {
  //   return showDialog(
  //       context: context,
  //       builder: (BuildContext dialogContext) {
  //         final screenSize = MediaQuery.of(context).size;
  //         return AlertDialog(
  //           insetPadding: const EdgeInsets.all(5.0),
  //           backgroundColor: Colors.transparent,
  //           contentPadding: const EdgeInsets.all(5.0),
  //           content: Container(
  //             height: 150.0,
  //             width: screenSize.width * .5,
  //             decoration: BoxDecoration(
  //               borderRadius: BorderRadius.circular(20.0),
  //               color: primaryColor,
  //               border: Border.all(color: white),
  //             ),
  //             child: Column(
  //               children: <Widget>[
  //                 Padding(
  //                   padding:
  //                       const EdgeInsets.only(left: 10, right: 10, top: 10.0),
  //                   child: Text(
  //                     "Total Community Members: ${leftSideData.left_members[index].totalMembers}",
  //                     textAlign: TextAlign.left,
  //                     //                     style: SC.appNormalText,
  //                     style: TextStyle(
  //                       color: white,
  //                       fontSize: 11.6,
  //                     ),
  //                   ),
  //                 ),
  //                 Padding(
  //                   padding:
  //                       const EdgeInsets.only(left: 10, right: 10, top: 10.0),
  //                   child: Text(
  //                     "Left Members: ${leftSideData.left_members[index].leftMembers}",
  //                     textAlign: TextAlign.left,
  //                     //                     style: SC.appNormalText,
  //                     style: TextStyle(
  //                       fontSize: 11.6,
  //                       color: white,
  //                     ),
  //                   ),
  //                 ),
  //                 Padding(
  //                   padding:
  //                       const EdgeInsets.only(left: 10, right: 10, top: 10.0),
  //                   child: Text(
  //                     "Right Members: ${leftSideData.left_members[index].rightMembers}",
  //                     textAlign: TextAlign.left,
  //                     //                     style: SC.appNormalText,
  //                     style: TextStyle(
  //                       fontSize: 11.6,
  //                       color: white,
  //                     ),
  //                   ),
  //                 ),
  //                 Padding(
  //                   padding:
  //                       const EdgeInsets.only(left: 10, right: 10, top: 10.0),
  //                   child: Text(
  //                     "Today left members prayers: ${leftSideData.left_members[index].leftMembersTodayPrayers}",
  //                     textAlign: TextAlign.left,
  //                     //                     style: SC.appNormalText,
  //                     style: TextStyle(
  //                       fontSize: 11.6,
  //                       color: white,
  //                     ),
  //                   ),
  //                 ),
  //                 Padding(
  //                   padding:
  //                       const EdgeInsets.only(left: 10, right: 10, top: 10.0),
  //                   child: Text(
  //                     "Today right members prayers: ${leftSideData.left_members[index].rightMembersTodayPrayers}",
  //                     textAlign: TextAlign.left,
  //                     //                     style: SC.appNormalText,
  //                     style: TextStyle(
  //                       fontSize: 11.6,
  //                       color: white,
  //                     ),
  //                   ),
  //                 ),
  //                 // Padding(
  //                 //   padding: const EdgeInsets.only(
  //                 //       left: 10, right: 10, top: 10.0),
  //                 //   child: Text(
  //                 //     "T: ${rightSideData.right_members[index].totalMembers}",
  //                 //     //                     style: SC.appNormalText,
  //                 //     style: TextStyle(fontSize: 11.6),
  //                 //   ),
  //                 // ),
  //               ],
  //             ),
  //           ),
  //         );
  //       });
  // }
}
