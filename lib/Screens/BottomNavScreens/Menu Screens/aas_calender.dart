import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
import 'package:table_calendar/table_calendar.dart';
import 'package:hijri/hijri_calendar.dart';
import 'package:intl/intl.dart';

class AASCalendar extends StatefulWidget {
  const AASCalendar({Key? key}) : super(key: key);

  @override
  State<AASCalendar> createState() => _AASCalendarState();
}

class _AASCalendarState extends State<AASCalendar> {

  @override
  void initState() {
    // TODO: implement initState
    super.initState(); _bannerAd = context.read<AdProvider>().createBannerAd();
  }
    late BannerAd _bannerAd;



  DateTime selectedDate = DateTime.now();

  HijriCalendar hijriDate = HijriCalendar.now();

  void _onDaySelected(DateTime day, DateTime? focusDay) {
    setState(() {
      selectedDate = day;
      hijriDate = HijriCalendar.fromDate(day);
    });
  }

  @override
  Widget build(BuildContext context) {
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    String formattedTime = DateFormat.jm().format(selectedDate);
    String formattedDate = DateFormat('d MMMM, yyyy').format(selectedDate);
    return SafeArea(
      child: Scaffold(
        backgroundColor: white,
        appBar: AppBar(
          backgroundColor: Color(0xff144d46),
          leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 5.0, top: 20),
                child: Container(
                  height: 5,
                  width: 5,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      // color: lightBlack,
                      border: Border.all(color: white, width: 2)),
                  child: Center(
                    child: Icon(Icons.arrow_back, color: white, size: 20),
                  ),
                ),
              )),
          title: Text(
            "Islamic Calendar",
            style: TextStyle(color: Colors.white, fontSize: 20),
          ),
          centerTitle: true,
        ),
        body: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            Container(
              child: Column(
                children: [
                  timeTopContainer(context, formattedTime),
                  SizedBox(
                    height: 20,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(1.0),
                    child: Text(
                      hijriDate.longMonthName +
                          ' ' +
                          hijriDate.hDay.toString() +
                          ' ' +
                          hijriDate.hYear.toString() +
                          ' ' +
                          'AH',
                      style: TextStyle(
                          color: Color(0xff144d46),
                          fontSize: 20,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                  SizedBox(
                    height: 8.0,
                  ),
                  Text(
                    formattedDate,
                    style: TextStyle(color: Color(0xff144d46)),
                  ),
                  calenderContainer(),
                ],
              ),
            ),
        
            Padding(
              padding: const EdgeInsets.all(8.0),
              child:  Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
            ),
            // Container(
            //   color: primaryColor,
            //   child: ListView.separated(
            //     padding: EdgeInsets.zero,
            //     shrinkWrap: true,
            //     scrollDirection: Axis.vertical,
            //     physics: BouncingScrollPhysics(),
            //     itemCount: islamicEvents.length,
            //     itemBuilder: (context, index) {
            //       final item = islamicEvents[index];
            //       return ListTile(
            //         title: Text(
            //           item['title'],
            //           style: TextStyle(
            //             color: yellowC,
            //             fontSize: 20,
            //             fontWeight: FontWeight.bold,
            //           ),
            //           textAlign: TextAlign.start,
            //         ),
            //         subtitle: Text(
            //           item['subtitle'],
            //           style: TextStyle(
            //             color: white,
            //             fontSize: 12,
            //             fontWeight: FontWeight.bold,
            //           ),
            //           textAlign: TextAlign.start,
            //         ),
            //         trailing: Text(
            //           item['EnglishDate'],
            //           style: TextStyle(
            //             color: white,
            //             fontSize: 12,
            //             fontWeight: FontWeight.bold,
            //           ),
            //           textAlign: TextAlign.start,
            //         ),
            //       );
            //     },
            //     separatorBuilder: (context, index) {
            //       if (index == islamicEvents.length - 1) {
            //         return Container();
            //       }
            //       // For other items, return a divider.
            //       return Padding(
            //         padding: const EdgeInsets.symmetric(horizontal: 15.0),
            //         child: Divider(color: yellowC),
            //       );
            //     },
            //   ),
            // ),
          ],
        ),
      ),
    );
  }

  Padding calenderContainer() {
    return Padding(
      padding: const EdgeInsets.all(20.0),
      child: TableCalendar(
        selectedDayPredicate: (day) => isSameDay(day, selectedDate),
        onDaySelected: _onDaySelected,
        focusedDay: selectedDate,
        calendarFormat: CalendarFormat.month,
        headerVisible: false,
        firstDay: DateTime.utc(2010, 10, 16),
        lastDay: DateTime.utc(2030, 3, 14),
        rowHeight: 50,
        calendarBuilders: CalendarBuilders(
          defaultBuilder: (context, date, _) {
            final hijriDateForDay = HijriCalendar.fromDate(date);

            return Column(
              children: [
                Container(
                  child: Center(
                    child: Text(
                      '${hijriDateForDay.hDay}',
                      style: TextStyle(
                        fontSize: 20,
                        color: isSameDay(date, selectedDate)
                            ? Colors.white
                            : Colors.black,
                      ),
                    ),
                  ),
                ),
                Text(
                  '${date.day}',
                  style: TextStyle(
                    fontSize: 14,
                    color: isSameDay(date, selectedDate)
                        ? Colors.white
                        : Colors.grey,
                  ),
                ),
              ],
            );
          },
          selectedBuilder: (context, date, _) {
            final hijriDateForDay = HijriCalendar.fromDate(date);

            return Column(
              children: [
                Container(
                  width: 28,
                  height: 28,
                  decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    color: Colors.black,
                  ),
                  child: Center(
                    child: Text(
                      '${hijriDateForDay.hDay}',
                      style: TextStyle(
                        fontSize: 16,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ),
                Text(
                  '${date.day}',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
              ],
            );
          },
          todayBuilder: (context, date, _) {
            final hijriDateForDay = HijriCalendar.fromDate(date);

            return Column(
              children: [
                Center(
                  child: Text(
                    '${hijriDateForDay.hDay}',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black,
                    ),
                  ),
                ),
                Text(
                  '${date.day}',
                  style: TextStyle(
                    fontSize: 14,
                    color: Colors.grey,
                  ),
                ),
              ],
            );
          },
        ),
      ),
    );
  }

  Container timeTopContainer(BuildContext context, String formattedTime) {
    return Container(
      width: MediaQuery.of(context).size.width,
      height: 80.h,
      color: Color(0xff144d46),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            formattedTime,
            style: TextStyle(color: Color(0xfffcd88a), fontSize: 30),
          ),
        ],
      ),
    );
  }
}
