import 'dart:io';
import 'package:aas/Services/loading_services.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';
import 'dart:typed_data';
import 'package:aas/Model/gallery_modal.dart';
import 'package:aas/Provider/gallery_provider.dart';

import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';

import 'package:provider/provider.dart';

import 'package:dio/dio.dart' as dio;



class ASSGallery extends StatefulWidget {
  @override
  State<ASSGallery> createState() => _ASSGalleryState();
}

class _ASSGalleryState extends State<ASSGallery> {

  late BannerAd _bannerAd;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    Provider.of<GalleryProvider>(context, listen: false).getGallery();
  }

  @override
  Widget build(BuildContext context) {
    GalleryModal? galleryData = Provider.of<GalleryProvider>(context).gallery;

    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: primaryColor,
        automaticallyImplyLeading: false,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: [
            Container(
              margin: EdgeInsets.only(right: 30),
              child: GestureDetector(
                onTap: () {
                  Get.back();
                },
                child: Container(
                  height: 30,
                  width: 30,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      border: Border.all(color: Colors.white, width: 2)),
                  child: Center(
                    child:
                    Icon(Icons.arrow_back, color: Colors.white, size: 20),
                  ),
                ),
              ),
            ),
            Text(
              "Islamic Gallery",
              style: TextStyle(
                  color: Color.fromRGBO(252, 216, 138, 1.0),
                  fontWeight: FontWeight.bold),
            ),
            Container(),
          ],
        ),
        centerTitle: true,
      ),
      body: SafeArea(
        child: Column(
          children: [
            galleryData != null && galleryData.islamic_gallery.isNotEmpty
                ? Expanded(
              child: GridView.builder(
                scrollDirection: Axis.vertical,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                  crossAxisCount: 3,
                  mainAxisSpacing: 5,
                  crossAxisSpacing: 5,
                  childAspectRatio: .6, // Maintain square aspect ratio
                ),
                itemCount: galleryData.islamic_gallery.length,
                itemBuilder: (context, index) {
                  return InkWell(
                    onTap: () {
                      print('on tap img: ${galleryData.islamic_gallery[index].img}');
                      _loadAndShowInterstitialAd(context);
                      Get.to(() => ImageFullScreen(
                        imagePath: galleryData.islamic_gallery[index].img,
                        index: galleryData.islamic_gallery[index].id,
                      ));
                    },
                    child: Image.network(galleryData.islamic_gallery[index].img, fit: BoxFit.fill,),
                    // child: Image.asset(
                    //   imagePaths[index],
                    //   fit: BoxFit.fill,
                    // ),
                  );
                },
              ),
            )
                : Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text('No any Media to display.'),
              ],
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 60,
              child: AdWidget(ad: _bannerAd),
            ),
          ],
        ),
      ),
    );
  }

  void _loadAndShowInterstitialAd(BuildContext context) {
    // Access the AdProvider using the Provider.of method
    AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

    // Load the interstitial ad
    adProvider.loadInterstitialAd();

    // Check if the interstitial ad is loaded
    if (adProvider.isInterstitialAdLoaded) {
      // resetNumber == 0;
      // Show the interstitial ad
      adProvider.showInterstitialAd();
    } else {
      // The ad is not loaded yet, you can handle this case accordingly
      print('Interstitial ad is not loaded yet. Please try again later.');
    }
  }
}

class ImageFullScreen extends StatefulWidget {
  final String imagePath;
  final int index;

  const ImageFullScreen(
      {super.key, required this.imagePath, required this.index});

  @override
  State<ImageFullScreen> createState() => _ImageFullScreenState();
}

class _ImageFullScreenState extends State<ImageFullScreen> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          backgroundColor: primaryColor,
          automaticallyImplyLeading: false,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              Container(
                margin: EdgeInsets.only(right: 50.w),
                child: GestureDetector(
                  onTap: () {
                    Get.back();
                  },
                  child: Container(
                    height: 30,
                    width: 30,
                    decoration: BoxDecoration(
                        shape: BoxShape.circle,
                        border: Border.all(color: Colors.white, width: 2)),
                    child: Center(
                      child:
                      Icon(Icons.arrow_back, color: Colors.white, size: 20),
                    ),
                  ),
                ),
              ),
              Text(
                "Islamic Gallery",
                style: TextStyle(
                    color: Color.fromRGBO(252, 216, 138, 1.0),
                    fontWeight: FontWeight.bold),
              ),
              Container(),
            ],
          ),
          centerTitle: true,
          actions: [
            Container(
              margin: EdgeInsets.only(right: 10.w),
              child: InkWell(
                onTap: () async {
             
                  final urlPreview = widget.imagePath;

                  try {
                    dio.Dio dioClient = dio.Dio();
                    dio.Response<List<int>> response = await dioClient.get<List<int>>(
                      urlPreview,
                      options: dio.Options(responseType: dio.ResponseType.bytes),
                    );

                    final Directory appDocDir = await getApplicationDocumentsDirectory();
                    final String filePath = '${appDocDir.path}/image.jpg';
                    File file = File(filePath);
                    await file.writeAsBytes(response.data!);

                    Share.shareXFiles([XFile(filePath)], text: 'Install AAS app and start earning for Free!');
                  } catch (e) {
                    print('Error downloading and sharing image: $e');
                  }
                },
                child: ImageIcon(
                  AssetImage("assets/images/share.png"),
                  size: 33,
                  color: Colors.white,
                ),
              ),
            ),
          ],
        ),
        body: Image.network(widget.imagePath,
          fit: BoxFit.fill,
          height: MediaQuery.sizeOf(context).height,
          filterQuality: FilterQuality.high,
        )
      // Image.asset(
      //   widget.imagePath[widget.index],
      // fit: BoxFit.fill,
      // height: MediaQuery.sizeOf(context).height,
      // filterQuality: FilterQuality.high,
      // ),
    );
  }
}
