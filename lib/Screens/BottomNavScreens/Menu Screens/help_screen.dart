import 'package:aas/Constants/colors.dart';
import 'package:aas/Model/FAQ_modal.dart';
import 'package:aas/Model/dua_modal.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Dua%20Screens/dua_tab_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Dua%20Screens/hadith_tab_screen.dart';
import 'package:accordion/accordion.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

// import 'package:velocity_x/velocity_x.dart';

import '../../../Constants/text_constants.dart';

class HelpScreen extends StatefulWidget {
  const HelpScreen({Key? key}) : super(key: key);

  @override
  State<HelpScreen> createState() => _HelpScreenState();
}

class _HelpScreenState extends State<HelpScreen>
    with SingleTickerProviderStateMixin {
  int _expandedIndex = -1;

  late BannerAd _bannerAd;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryColor,
        title: Row(
          children: [
            GestureDetector(
              onTap: () {
                Navigator.pop(context);
              },
              child: Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: white, width: 2)),
                child: Center(
                  child: Icon(Icons.arrow_back, color: white, size: 20),
                ),
              ),
            ),
            SizedBox(
              width: 50,
            ),
            Padding(
              padding: const EdgeInsets.only(left: 50),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text(
                    'Help',
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      color: white,
                      fontSize: 25,
                    ),
                  ),
                    SizedBox(
                            width: 10.0,
                          ),
                  // 10.widthBox,
                  Container(
                    height: 30,
                    width: 30,
                    child: Image.asset("assets/images/ic_help.png"),
                  ),
                ],
              ),
            ),
          ],
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
      ),
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: primaryColor,
          child: Column(
            children: [
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: SingleChildScrollView(
                    scrollDirection: Axis.vertical,
                    child: Column(
                      children: [
                        SizedBox(
                          height: 5,
                        ),
                        FAQDropdown(),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class FAQDropdown extends StatefulWidget {
  @override
  _FAQDropdownState createState() => _FAQDropdownState();
}

class _FAQDropdownState extends State<FAQDropdown> {
  bool isExpanded = false;

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    return Container(
      margin: EdgeInsets.symmetric(
        horizontal: screenWidth * .05,
      ),
      padding: EdgeInsets.all(
        screenWidth * .03,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Text(
                "Email",
                style: TextStyle(
                  color: primaryColor,
                  fontWeight: FontWeight.bold,
                  fontSize: 22,
                ),
              ),
              Text(
                "support@aasonline.co",
                style: TextStyle(
                    color: Colors.indigo, fontWeight: FontWeight.bold),
              ),
            ],
          ),
          SizedBox(
            height: 20,
          ),
          InkWell(
            onTap: () {
              setState(() {
                isExpanded = !isExpanded;
              });
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Text(
                  "FAQ's",
                  style: TextStyle(
                    fontSize: 22,
                    color: primaryColor,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                // Icon(
                //   isExpanded ? Icons.arrow_drop_up : Icons.arrow_drop_down,
                //   size: 30,
                //   color: primaryColor,
                // ),
              ],
            ),
          ),
          // if (isExpanded)
            Accordion(
              disableScrolling: true,
              // Set contentDivider to null
              headerBackgroundColor: Colors.white,
              children: List.generate(faqData.length, (index) {
                return AccordionSection(
                  contentBorderWidth: 0,
                  contentBorderColor: Colors.white,
                  contentBackgroundColor: Colors.white,
                  rightIcon: const Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.black54,
                    size: 20,
                  ),
                  header: Column(
                    children: [
                      Row(
                        children: [
                          Text(
                            faqData[index].question.toString(),
                            style: TextStyle(
                              overflow: TextOverflow.fade,
                              fontSize: 15.sp,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                    ],
                  ),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        faqData[index].answer.toString(),
                        style: TextStyle(fontSize: 13, color: primaryColor, fontWeight: FontWeight.w600),
                      ),
                    ],
                  ),
                );
              }),
            ),
        ],
      ),
    );
  }
}

class FAQItem extends StatelessWidget {
  final String question;
  final String answer;

  FAQItem({
    required this.question,
    required this.answer,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        SizedBox(height: 20),
        Text(
          question,
          style: TextStyle(
            fontSize: 20,
            color: primaryColor,
            fontWeight: FontWeight.bold,
          ),
        ),
        SizedBox(height: 10),
        Text(
          answer,
          style: TextStyle(
            fontSize: 18,
            color: Colors.black,
          ),
        ),
        Divider(
          indent: 0,
          endIndent: 0,
          color: Colors.grey[300],
        ),
      ],
    );
  }
}
