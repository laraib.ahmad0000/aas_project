import 'dart:io';

import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Dua%20Screens/DuaScreen.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/help_screen.dart'
    as hs;
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/aas_calender.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/aas_gallery.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/settings_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/withdraw_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/aas_tasbee.dart';
import 'package:aas/Screens/BottomNavScreens/Quran%20Section/quran_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/wallet_screen.dart';
import 'package:aas/constants/ad_helper.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/namaz_timing_screen.dart';

import 'package:flutter/material.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
// import 'package:velocity_x/velocity_x.dart';
import 'package:get/get.dart';

class MoreScreen extends StatefulWidget {
  const MoreScreen({super.key});

  @override
  State<MoreScreen> createState() => _MoreScreenState();
}

class _MoreScreenState extends State<MoreScreen> {
  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();

    // _bannerAd = context.read<AdProvider>().
  }

  late BannerAd _bannerAd;

  NativeAd? nativeAd;
  bool isNativeAdLoaded = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    loadNativeAd();
  }

  void loadNativeAd() {
    nativeAd = NativeAd(
        adUnitId: AdHelper.nativeAdUnitId,
        listener: NativeAdListener(onAdLoaded: (ad) {
          setState(() {
            isNativeAdLoaded = true;
          });
        }, onAdFailedToLoad: (ad, error) {
          nativeAd!.dispose();
        }),
        request: const AdRequest(),
        nativeTemplateStyle:
            NativeTemplateStyle(templateType: TemplateType.small));
    nativeAd!.load();
  }

  @override
  Widget build(BuildContext context) {
    // final adProvider = context.read<AdProvider>();
     UserModal? userData = Provider.of<UserDataProvider>(context).userData;
     String referralCode = userData?.referral_code ?? '';
    return Scaffold(
      backgroundColor: white,
        body: SafeArea(
      child: Stack(
        children: [
          Image(
            image: const AssetImage('assets/images/mbg.png'),
            width: MediaQuery.of(context).size.width,
            fit: BoxFit.fill,
          ),
          // Positioned(
          //     left: context.screenWidth * .27,
          //     top: context.screenHeight * .05,
          //     child: 'Log Your Prayers'.text.bold.size(25).white.make()),
          Positioned(
              top: 7,
              left: 20,
              right: 20,
              child: Column(
                children: [
                 Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: AdWidget(ad: _bannerAd),
            ),
                const  SizedBox(
                    height: 60,
                  ),
                  Card(
                    shadowColor: const Color(0xffd3d3d3),
                    color: const Color(0xFFEBEBEB),
                    elevation: 25,
                    child: GridView.count(
                      shrinkWrap: true,
                      crossAxisCount: 4, // Number of columns in the grid
                      padding: const EdgeInsets.all(10.0),
                      childAspectRatio:
                          0.8, // Maintain a square aspect ratio for GridItems
                      mainAxisSpacing: 10.0,
                      children: [
                        // Your grid items here
                        GridItem(
                          image: 'assets/icons/12.png',
                          text: 'Prayer Times',
                          onTap: () {
                            Get.to(() =>const NamazTimingScreen());
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/11.png',
                          text: 'AAS Gallery',
                          onTap: () {
                            Get.to(() => ASSGallery());
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/10.png',
                          text: 'AAS Calender',
                          onTap: () {
                            Get.to(const AASCalendar());
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/9.png',
                          text: 'Quran',
                          onTap: () {
                            Get.to(QuranScreen());
                            //  _showExitDialogue(context);
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/8.png',
                          text: 'App Settings',
                          onTap: () {
                            Get.to(const SettingsScreen());
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/7.png',
                          text: 'Wallet',
                          onTap: () {
                            Get.to(() => WalletScreen());
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/6.png',
                          text: 'Tasbeeh',
                          onTap: () {
                            Get.to(() =>const AasTasbee());
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/5.png',
                          text: 'Dua',
                          onTap: () {
                            Get.to(const DuaScreen());
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/3.png',
                          text: 'Reffer Friend',
                          onTap: () async {
                         await Share.share(
                    'Hi, \nWelcome to AAS -  A Premium Islamic Social Media App. \nJoin AAS with my code $referralCode and join my Community to earn free! \nhttps://aasonline.co/ \nDownload the App and install it here: \nhttps://play.google.com/store/apps/details?id=com.aasonline.app');
                          },
                        ),
                        GridItem(
                          image: 'assets/icons/4.png',
                          text: 'Help',
                          onTap: () {
                            Get.to(const hs.HelpScreen());
                          },
                        ),
                        // GridItem(
                        //   image: 'assets/icons/2.png',
                        //   text: 'Remove Ads',
                        //   onTap: () {},
                        // ),
                        GridItem(
                          image: 'assets/icons/1.png',
                          text: 'Share App',
                          onTap: () async {
                            await Share.share(
                                'https://play.google.com/store/apps/details?id=com.aasonline.appid=web_share');
                          },
                        ),
                      ],
                    ),
                  ),
                ],
              )),
          // Positioned(
          //   top: context.screenHeight * .65,
          //   left: 30,
          //   right: 30,
          //   child: Row(
          //     mainAxisAlignment: MainAxisAlignment.center,
          //     crossAxisAlignment: CrossAxisAlignment.center,
          //     children: [
          //       GestureDetector(
          //         onTap: () {
          //           Get.to(WithdrawScreen());
          //         },
          //         child: Image(
          //             height: 45,
          //             image: AssetImage('assets/images/become_donar.png')),
          //       ),
          //     ],
          //   ),
          // ),
          // Positioned(
          //   bottom: 0,
          //   child: Container(
          //     width: MediaQuery.of(context).size.width,
          //     height: 60,
          //     child: AdWidget(ad: _bannerAd),
          //   ),
          // )
          // isNativeAdLoaded
          //     ? Container(
          //         decoration: BoxDecoration(color: white),
          //         height: 130,
          //         child: AdWidget(ad: nativeAd!),
          //       )
          //     : SizedBox(),
          Positioned(
            bottom: 10,
            child: Container(
              height: 100,
              width: MediaQuery.of(context).size.width,
              // color: lightBlack,
              child: isNativeAdLoaded
                  ? Container(
                      decoration: const BoxDecoration(color: white),
                      height: 130,
                      child: AdWidget(ad: nativeAd!),
                    )
                  : const SizedBox(),
            ),
          ),
          // Positioned(
          //   bottom: 0,
          //   child: 
          // ),
        ],
      ),
    ));
  }
}
_showExitDialogue(BuildContext context) {
    // final auth = FirebaseAuth.instance;
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(30.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 150.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    const SizedBox(
                      height: 1.0,
                    ),
                    const Text(
                      "Updating and Coming Very Soon!",
                      style: appNormalText,
                      textAlign: TextAlign.center,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextButton(
                          style: TextButton.styleFrom(
                            foregroundColor: Colors.white,
                            padding: const EdgeInsets.all(16.0),
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 33,
                            width: 63,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  7.0,
                                ),
                                border: Border.all(color: black)),
                            child: const Center(
                              child: Text(
                                'Close',
                                style: appNormalText,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
const menuIconsTextStyle =
    TextStyle(fontSize: 11, color: Colors.black, fontWeight: FontWeight.bold);

class GridItem extends StatelessWidget {
  final String image;
  final String text;
  final VoidCallback onTap;

  GridItem({
    required this.image,
    required this.text,
    required this.onTap,
  });

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onTap,
      child: SingleChildScrollView(
        child: Column(
          children: [
            Image.asset(
              image,
              height: 70,
              width: 70,
            ),
            // 5.heightBox, // You can use SizedBox to create spacing
            Text(
              text,
              style: menuIconsTextStyle,
              maxLines: 1, // Set maxLines to 1 to prevent text from wrapping
              overflow: TextOverflow.ellipsis,
            )
          ],
        ),
      ),
    );
  }
}
