import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/donate_to_others_screen.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class WithdrawScreen extends StatefulWidget {
  @override
  State<WithdrawScreen> createState() => _WithdrawScreenState();
}

class _WithdrawScreenState extends State<WithdrawScreen> {
  late BannerAd _bannerAd;
  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Color.fromRGBO(20, 77, 70, 1.0),
          child: Column(
            children: [
              Container(
                height: screenHeight * .1,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Text(
                        "Withdraw",
                        style: TextStyle(
                            color: Color.fromRGBO(
                                252, 216, 138, 1.0), // or any other color
                            decoration: TextDecoration.none,
                            fontSize: 22 // remove underline
                            ),
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: EdgeInsets.symmetric(horizontal: 2),
                  padding: EdgeInsets.all(10),
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      // 30.heightBox,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Total Amount", style: normaltextBlackC),
                        ],
                      ),
                      SizedBox(
                        height: 5.0,
                      ),
                      // 5.heightBox,
                      Text(
                        "\$10.750",
                        style: TextStyle(
                            color: Color.fromRGBO(
                                20, 77, 70, 1.0), // or any other color
                            decoration: TextDecoration.none,
                            fontSize: 35,
                            fontWeight: FontWeight.bold // remove underline
                            ),
                      ),
                      SizedBox(
                        height: 30.0,
                      ),
                      // 30.heightBox,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          // 20.widthBox,
                          Image.asset(
                            'assets/icons/icon 1.png',
                            height: 50,
                            width: 50,
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          // 20.widthBox,
                          Text(
                            "Earned by Community: \$50",
                            style: TextStyle(
                                color: Color.fromRGBO(
                                    20, 77, 70, 1.0), // or any other color

                                fontSize: 20,
                                fontWeight: FontWeight.bold // remove underline
                                ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      // 10.heightBox,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 20.0,
                          ),
                          // 20.widthBox,
                          Image.asset(
                            'assets/icons/icon 2.png',
                            height: 50,
                            width: 50,
                          ),
                          // 20.widthBox,
                          Text(
                            "Earned by Tasks: \$50",
                            style: TextStyle(
                                color: Color.fromRGBO(
                                    20, 77, 70, 1.0), // or any other color

                                fontSize: 20,
                                fontWeight: FontWeight.bold // remove underline
                                ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      // 10.heightBox,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 20.0,
                          ),
                          // 20.widthBox,
                          Image.asset(
                            'assets/icons/icon3.png',
                            height: 50,
                            width: 50,
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          // 20.widthBox,
                          Text(
                            "Total Earning: \$50",
                            style: TextStyle(
                                color: Color.fromRGBO(
                                    20, 77, 70, 1.0), // or any other color
                                decoration: TextDecoration.none,
                                fontSize: 20,
                                fontWeight: FontWeight.bold // remove underline
                                ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10.0,
                      ),
                      // 10.heightBox,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.start,
                        children: [
                          SizedBox(
                            width: 20.0,
                          ),
                          // 20.widthBox,
                          Image.asset(
                            'assets/icons/icon4.png',
                            height: 50,
                            width: 50,
                          ),
                          SizedBox(
                            width: 20.0,
                          ),
                          // 20.widthBox,
                          Text(
                            "Total Withdraw: \$50",
                            style: TextStyle(
                                color: Color.fromRGBO(
                                    20, 77, 70, 1.0), // or any other color
                                decoration: TextDecoration.none,
                                fontSize: 20,
                                fontWeight: FontWeight.bold // remove underline
                                ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenWidth * .1,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.to(DonateScreen());
                            },
                            child: Image(
                                height: 52,
                                width: 140,
                                image:
                                    AssetImage('assets/images/withdraw.png')),
                          ),
                          SizedBox(
                            width: screenWidth * .1,
                          ),
                          GestureDetector(
                            onTap: () {
                              Get.to(DonateScreen());
                            },
                            child: Image(
                                height: 52,
                                width: 140,
                                image: AssetImage('assets/images/donate.png')),
                          ),
                        ],
                      )
                    ],
                  ), // Adjust the radius as needed
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
            ],
          ), // Set the background color here
        ),
      ),
    );
  }
}
