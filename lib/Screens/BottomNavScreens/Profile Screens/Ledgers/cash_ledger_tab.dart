import 'package:aas/Model/Profile%20Modals/cash_ledger_modal.dart';
import 'package:aas/Model/Profile%20Modals/coins_ledger_modal.dart';
import 'package:aas/Provider/Profile%20Provider/cash_ledger_provider.dart';
import 'package:aas/Provider/Profile%20Provider/ledger_detail_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CashLedgerTab extends StatefulWidget {
  const CashLedgerTab({super.key});

  @override
  State<CashLedgerTab> createState() => _CashLedgerTabState();
}

class _CashLedgerTabState extends State<CashLedgerTab> {

    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<CashLedgerDetailProvider>(context,listen: false).getCashLedgerDetail();
   
  }

  @override
  Widget build(BuildContext context) {
    CashLedgerDetailModal? cashLedgerDetail =  Provider.of<CashLedgerDetailProvider>(context).CashledgerDetailModal;
    return Scaffold(
      backgroundColor: white,
      body:
    cashLedgerDetail != null && cashLedgerDetail.ledger.isNotEmpty
        ?
           ListView.separated(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.zero,
                  itemCount: cashLedgerDetail.ledger.length,
                  itemBuilder: (context, index) {
                    // final ledger = earningLedgerList[index];
                    return Card(
                      margin: EdgeInsets.all(20),
                      elevation: 10,
                      color: white,
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Row(
                                    children: [
                                      Text(
                                       ' ${cashLedgerDetail.ledger[index].label}'.toString(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15),
                                      ),
                                      SizedBox(width: 8.0,),
                                      Text('\$',style: TextStyle(color: const Color.fromARGB(255, 255, 145, 0), fontWeight: FontWeight.bold),),
                                          SizedBox(width: 3.0,),
                                      Text('${cashLedgerDetail.ledger[index].value}'.toString(),
                                          style: TextStyle(fontSize: 12))
                                    ],
                                  ),
                                ),
                                Text('${cashLedgerDetail.ledger[index].date}'.toString(),
                                    style: TextStyle(fontSize: 12))
                              ],
                            ),
                            Divider(
                              color: primaryColor,
                              indent: 50.0,
                              endIndent: 50.0,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Ref.ID:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                    Text('${cashLedgerDetail.ledger[index].refId}'.toString(),
                                        style: TextStyle(fontSize: 12)),
                                  ],
                                ),
                                Text(
                                  "${cashLedgerDetail.ledger[index].description}".toString(),
                                  style: TextStyle(fontSize: 11),
                                ),
                              ],
                            ),
                            Divider(
                              color: primaryColor,
                              indent: 50.0,
                              endIndent: 50.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "Balance: ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                Text('\$ ${cashLedgerDetail.ledger[index].balance}'.toString(),
                                    style: TextStyle(fontSize: 12))
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider(
                      color: Colors.black26,
                      indent: 50.0,
                      endIndent: 50.0,
                    );
                  },
                )
            : Center(child: Text('No Ledger Found')),
    );
  }
}