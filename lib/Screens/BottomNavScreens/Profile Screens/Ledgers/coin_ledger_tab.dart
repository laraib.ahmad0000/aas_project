import 'package:aas/Model/Profile%20Modals/coins_ledger_modal.dart';
import 'package:aas/Provider/Profile%20Provider/ledger_detail_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/reusable_widgets.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class CoinLedgerTab extends StatefulWidget {
  const CoinLedgerTab({super.key});

  @override
  State<CoinLedgerTab> createState() => _CoinLedgerTabState();
}

class _CoinLedgerTabState extends State<CoinLedgerTab> {

    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<LedgerDetailProvider>(context,listen: false).getLedgerDetail();
   
  }

  @override
  Widget build(BuildContext context) {
    LedgerDetailModal? ledgerDetail =  Provider.of<LedgerDetailProvider>(context).ledgerDetailModal;
    return Scaffold(
      backgroundColor: white,
      body:
    ledgerDetail != null && ledgerDetail.ledger.isNotEmpty
        ?
           ListView.separated(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: BouncingScrollPhysics(),
                  padding: EdgeInsets.zero,
                  itemCount: ledgerDetail.ledger.length,
                  itemBuilder: (context, index) {
                    // final ledger = earningLedgerList[index];
                    return Card(
                      margin: EdgeInsets.all(20),
                      elevation: 10,
                      color: white,
                      child: Padding(
                        padding: EdgeInsets.all(10.0),
                        child: Column(
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                              children: [
                                Container(
                                  child: Row(
                                    children: [
                                      Text(
                                     '${ledgerDetail.ledger[index].label}'.toString(),
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 15),
                                      ),
                                               SizedBox(width: 10.0,),
                           CoinsPng(),
                                      SizedBox(width: 3.0,),
                                      Text('${ledgerDetail.ledger[index].value}'.toString(),
                                          style: TextStyle(fontSize: 12))
                                    ],
                                  ),
                                ),
                                Text('${ledgerDetail.ledger[index].date}'.toString(),
                                    style: TextStyle(fontSize: 12))
                              ],
                            ),
                            Divider(
                              color: primaryColor,
                              indent: 50.0,
                              endIndent: 50.0,
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  children: [
                                    Text(
                                      "Ref.ID:",
                                      style: TextStyle(
                                          fontWeight: FontWeight.bold,
                                          fontSize: 15),
                                    ),
                                    Text('${ledgerDetail.ledger[index].refId}'.toString(),
                                        style: TextStyle(fontSize: 12)),
                                  ],
                                ),
                                Text(
                                  "${ledgerDetail.ledger[index].description}".toString(),
                                  style: TextStyle(fontSize: 11),
                                ),
                              ],
                            ),
                            Divider(
                              color: primaryColor,
                              indent: 50.0,
                              endIndent: 50.0,
                            ),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: [
                                Text(
                                  "Balance: ",
                                  style: TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontSize: 15),
                                ),
                                   CoinsPng(),
                                Text('  ${ledgerDetail.ledger[index].balance}'.toString(),
                                    style: TextStyle(fontSize: 12))
                              ],
                            ),
                          ],
                        ),
                      ),
                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider(
                      color: Colors.black26,
                      indent: 50.0,
                      endIndent: 50.0,
                    );
                  },
                )
            : Center(child: Text('No Ledger Found')),
    );
  }
}