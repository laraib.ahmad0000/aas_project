import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/Profile%20Provider/ledger_detail_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/Ledgers/cash_ledger_tab.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/Ledgers/coin_ledger_tab.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class LedgerScreen extends StatefulWidget {
  const LedgerScreen({super.key});

  @override
  State<LedgerScreen> createState() => _LedgerScreenState();
}

class _LedgerScreenState extends State<LedgerScreen> with SingleTickerProviderStateMixin{
   late BannerAd _bannerAd;
  late TabController _tabController;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // Provider.of<LedgerDetailProvider>(context,listen: false).getLedgerDetail();
     _bannerAd = context.read<AdProvider>().createBannerAd();
    _tabController = TabController(length: 2, vsync: this);

    _tabController.addListener(() {
      setState(() {});
    });
  }

    @override
  void dispose() {
    _tabController.dispose();
    _bannerAd.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
      //  LedgerDetailModal? ledgerDetail =  Provider.of<LedgerDetailProvider>(context).ledgerDetailModal;
UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    return SafeArea(
      child: Scaffold(
        backgroundColor: white,
        body: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              height: 130.h,
              width: double.infinity,
              decoration: BoxDecoration(
                  color: primaryColor,
                  borderRadius: BorderRadius.only(
                      bottomRight: Radius.circular(20.w),
                      bottomLeft: Radius.circular(20.w))),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Get.back();
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20.0, top: 20),
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  // color: lightBlack,
                                  border: Border.all(color: white, width: 2)),
                              child: Center(
                                child: Icon(Icons.arrow_back,
                                    color: white, size: 20),
                              ),
                            ),
                          )),
                      InkWell(
                        onTap: (){
                          dialogeShow(context);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(right: 10, top: 20),
                          child: FaIcon(CupertinoIcons.info, color: Colors.white,),
                        ),
                      ),
                    ],
                  ),
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      Align(
                          alignment: Alignment.center,
                          child: Text(
                            "Ledger's Detail",
                            style: TextStyle(
                                fontSize: 22,
                                fontWeight: FontWeight.bold,
                                color: yellowC),
                          )),
                      // Padding(
                      //   padding: EdgeInsets.only(top: 8.h, bottom: 10.h),
                      //   child: Text(
                      //     "Total Amount \$ ${userData!.cash_balance}",
                      //     style: TextStyle(
                      //         fontSize: 13,
                      //         fontWeight: FontWeight.bold,
                      //         color: white),
                      //   ),
                      // ),
                      // Text(
                      //   "Total Withdraw Amount 3\$",
                      //   style: TextStyle(
                      //       fontSize: 13,
                      //       fontWeight: FontWeight.bold,
                      //       color: white),
                      // ),
                    ],
                  )
                ],
              ),
            ),
            // SizedBox(
            //   height: 10.h,
            // ),
        
             Expanded(
              child: Container(
                width: double.infinity,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30))),
                child: Column(
                  children: [
                       SizedBox(height: 30.0,),
                    // 30.heightBox,
                    Container(
                      height: screenHeight * .05,
                      width: screenWidth * .8,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                          border: Border.all(
                            color: Color(0xFF111111),
                          ),
                        ),
                        child: TabBar(
                          labelColor: white,
                          indicatorSize: TabBarIndicatorSize.tab,
                          indicator: BoxDecoration(
                            color: Color.fromRGBO(20, 77, 70, 1.0),
                            borderRadius: BorderRadius.circular(10),
                          ),
                          unselectedLabelColor: Color(0xFFABA9A9),
                          controller: _tabController,
                          tabs: [
                            Tab(text: 'Coins Ledger'),
                            Tab(text: 'Cash Ledger'),
                          ],
                        ),
                      ),
                    ),
                    Expanded(
                      child: TabBarView(
                        controller: _tabController,
                        children: [
                          
                        CoinLedgerTab(),
                          CashLedgerTab()
                          
                          ],
                      ),
                    ),
                    // Container(
                    //   width: MediaQuery.of(context).size.width,
                    //   height: 60,
                    //   child: AdWidget(ad: _bannerAd),
                    // ),
                  ],
                ),
              ),
            ),
            // ledgerDetail != null ** ledgerDetail.
            // ?
        // ledgerDetail != null && ledgerDetail.ledger.isNotEmpty
        // ?
          //  ListView.separated(
                  // shrinkWrap: true,
            //       scrollDirection: Axis.vertical,
            //       physics: BouncingScrollPhysics(),
            //       padding: EdgeInsets.zero,
            //       itemCount: ledgerDetail.ledger.length,
            //       itemBuilder: (context, index) {
            //         // final ledger = earningLedgerList[index];
            //         return Card(
            //           margin: EdgeInsets.all(20),
            //           elevation: 10,
            //           color: Colors.yellow[50],
            //           child: Padding(
            //             padding: EdgeInsets.all(10.0),
            //             child: Column(
            //               mainAxisSize: MainAxisSize.min,
            //               children: <Widget>[
            //                 Row(
            //                   mainAxisAlignment:
            //                       MainAxisAlignment.spaceBetween,
            //                   children: [
            //                     Container(
            //                       child: Row(
            //                         children: [
            //                           Text(
            //                             "Credit:  ",
            //                             style: TextStyle(
            //                                 fontWeight: FontWeight.bold,
            //                                 fontSize: 15),
            //                           ),
            //                           Text('\$ ${ledgerDetail.ledger[index].credit}'.toString(),
            //                               style: TextStyle(fontSize: 12))
            //                         ],
            //                       ),
            //                     ),
            //                     Text('${ledgerDetail.ledger[index].date}'.toString(),
            //                         style: TextStyle(fontSize: 12))
            //                   ],
            //                 ),
            //                 Divider(
            //                   color: primaryColor,
            //                   indent: 50.0,
            //                   endIndent: 50.0,
            //                 ),
            //                 Column(
            //                   crossAxisAlignment: CrossAxisAlignment.start,
            //                   children: [
            //                     Row(
            //                       mainAxisAlignment:
            //                           MainAxisAlignment.spaceBetween,
            //                       children: [
            //                         Text(
            //                           "Ref.ID:",
            //                           style: TextStyle(
            //                               fontWeight: FontWeight.bold,
            //                               fontSize: 15),
            //                         ),
            //                         Text('${ledgerDetail.ledger[index].refId}'.toString(),
            //                             style: TextStyle(fontSize: 12)),
            //                       ],
            //                     ),
            //                     Text(
            //                       "${ledgerDetail.ledger[index].description}".toString(),
            //                       style: TextStyle(fontSize: 11),
            //                     ),
            //                   ],
            //                 ),
            //                 Divider(
            //                   color: primaryColor,
            //                   indent: 50.0,
            //                   endIndent: 50.0,
            //                 ),
            //                 Row(
            //                   mainAxisAlignment: MainAxisAlignment.end,
            //                   children: [
            //                     Text(
            //                       "Balance: ",
            //                       style: TextStyle(
            //                           fontWeight: FontWeight.bold,
            //                           fontSize: 15),
            //                     ),
            //                     Text('\$ ${ledgerDetail.ledger[index].balance}'.toString(),
            //                         style: TextStyle(fontSize: 12))
            //                   ],
            //                 ),
            //               ],
            //             ),
            //           ),
            //         );
            //       },
            //       separatorBuilder: (context, index) {
            //         return Divider(
            //           color: Colors.black26,
            //           indent: 50.0,
            //           endIndent: 50.0,
            //         );
            //       },
            //     )
            // : Center(child: Text('No Leadger Found')),
             Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: AdWidget(ad: _bannerAd),
                    ),
          ],
        ),
      ),
    );
  }
  dialogeShow(BuildContext context) {

    return showDialog(
        context: context,

        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(

            insetPadding: const EdgeInsets.all(5.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(5.0),
            content: Container(
              height: 100.0,
              width: screenSize.width * .5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(


                children: <Widget>[


                  SizedBox(height: 5,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.info, color: primaryColor,),
                    ],
                  ),
                  SizedBox(height: 10,),

                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      "The ledger shows how and when you earned the coins, converted the coins and withdraw or donated the amount.",
                      //                     style: SC.appNormalText,
                      style:TextStyle(fontSize:11),

                    ),
                  ),




                ],
              ),
            ),
          );
        });
  }
}
