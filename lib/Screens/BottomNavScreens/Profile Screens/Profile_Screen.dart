import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/Ledgers/ledger_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/wallet_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/EventsScreen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/edit_profile.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/notification_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/badges_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/TaskScreen.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/reusable_widgets.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:percent_indicator/circular_percent_indicator.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({
    super.key,
  });

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen>
    with SingleTickerProviderStateMixin {
  late BannerAd _bannerAd;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
  }

  void dispose() {
    super.dispose();
    _bannerAd.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Theme.of(context).copyWith(
      appBarTheme: Theme.of(context)
          .appBarTheme
          .copyWith(systemOverlayStyle: SystemUiOverlayStyle.dark),
    );
    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    print('this is user data: $userData');
    return Scaffold(
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: primaryColor,
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              // Padding(
              //   padding: const EdgeInsets.only(right: 30, top: 10),
              //   child: Row(
              //     mainAxisAlignment: MainAxisAlignment.end,
              //     children: [
              //       GestureDetector(
              //           onTap: () {
              //             Get.to(WalletScreen());
              //           },
              //           child: Icon(Icons.account_balance_wallet,
              //               color: white, size: 28)),
              //     ],
              //   ),
              // ),
              Padding(
                padding: EdgeInsets.only(left: 5),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  child: AdWidget(ad: _bannerAd),
                ),
              ),
              profileContainer(context, userData),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15))),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20.0,
                      ),
                      // 20.heightBox,
                      Container(
                        height: 50,
                        width: 200,
                        decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(12),
                        ),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              "Badges",
                              style: TextStyle(color: Colors.white),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            InkWell(
                                onTap: () {
                                  dialogeShow(context);
                                },
                                child: FaIcon(
                                  CupertinoIcons.info,
                                  color: Colors.white,
                                )),
                          ],
                        ),
                      ),
                      Expanded(child: BadgesScreen()),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  profileContainer(BuildContext context, userData) {
    return SingleChildScrollView(
      child: Container(
        // height: MediaQuery.sizeOf(context).height * .26,
        width: double.infinity,
        decoration: BoxDecoration(
            color: primaryColor,
            borderRadius: BorderRadius.only(
                bottomLeft: Radius.circular(15),
                bottomRight: Radius.circular(15))),
        child: Column(
          children: [
            Container(
              height: MediaQuery.sizeOf(context).height * .14,
              child: Row(
                children: [
                  Stack(children: [
                    Container(
                      width: 120,
                      //height: 40,
                      height: 100,

                      child: Column(
                        children: [
                          CircularPercentIndicator(
                            radius: 49.0,
                            lineWidth: 3.0,
                            percent: 0.30,
                            // header: Text("Icon header"),
                            center: CircleAvatar(
                              radius: 39,
                              backgroundColor: white,
                              child: CircleAvatar(
                                radius: 53,
                                backgroundImage:
                                    NetworkImage(userData.profileimage),
                              ),
                            ),
                            backgroundColor: white,
                            progressColor: primaryColor,
                          )
                        ],
                      ),
                    ),
                    // Positioned(
                    //   left: context.screenWidth * 0.2,
                    //   child: GestureDetector(
                    //     onTap: () {
                    //       Get.to(EditProfileScreen());
                    //     },
                    //     child: Image(
                    //         image: AssetImage(
                    //       "assets/images/GallryImage.png",
                    //     )),
                    //   ),
                    //   height: context.screenHeight * 0.05,
                    //   width: context.screenHeight * 0.05,
                    // )
                  ]),
                  Container(
                    child: Row(
                      children: [
                        Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              height: 22,
                              width: 220,
                              child: Row(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Text(
                                    userData.name,
                                    style: TextStyle(
                                        fontSize: 18.sp,
                                        color: Color(0xffFCD88A),
                                        fontWeight: FontWeight.bold),
                                  ),
                                  GestureDetector(
                                      onTap: () {
                                        Get.to(WalletScreen());
                                      },
                                      child: Icon(Icons.account_balance_wallet,
                                          color: white, size: 28)),
                                ],
                              ),
                            ),
                            SizedBox(
                              height: 10.0,
                            ),
                            //               Row(
                            //
                            //                 children: [

                            //                    Padding(
                            //   padding: const EdgeInsets.only(right: 30, top: 10),
                            //   child: Row(
                            //     mainAxisAlignment: MainAxisAlignment.end,
                            //     children: [
                            // GestureDetector(
                            //     onTap: () {
                            //       Get.to(WalletScreen());
                            //     },
                            //     child: Icon(Icons.account_balance_wallet,
                            //         color: white, size: 28)),
                            //     ],
                            //   ),
                            // ),
                            //                 ],
                            //               ),
                            Row(
                              children: [
                                Text(
                                  "ID: ",
                                  style: TextStyle(
                                      fontSize: 13.sp,
                                      color: Color(0xffFFFFFF),
                                      fontWeight: FontWeight.bold),
                                ),
                                SizedBox(
                                  width: 2.0,
                                ),
                                Text(
                                  userData.referral_code,
                                  style: TextStyle(
                                      fontSize: 13.sp,
                                      color: Color(0xffFCD88A),
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(
                                  "Balance: ",
                                  style: TextStyle(
                                      fontSize: 13.sp,
                                      color: Color(0xffFFFFFF),
                                      fontWeight: FontWeight.bold),
                                ),
                                CoinsPng(),
                                SizedBox(
                                  width: 2.0,
                                ),
                                Text(
                                  userData.coins_balance,
                                  style: TextStyle(
                                      fontSize: 13.sp,
                                      color: Color(0xffFCD88A),
                                      fontWeight: FontWeight.bold),
                                ),
                              ],
                            ),
                          ],
                        ),
                      ],
                    ),
                  )
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(bottom: 15, left: 15.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: ElevatedButton(
                      onPressed: () {
                        Get.to(() => EditProfileScreen());
                      },
                      child: Container(
                        child: Text(
                          "Edit Profile",
                          style: TextStyle(
                              color: mediumText.color,
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 1.0),
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(right: 15),
                    child: ElevatedButton(
                      onPressed: () {
                        _loadAndShowInterstitialAd(context);
                        Get.to(() => LedgerScreen());
                      },
                      child: Container(
                        child: Text(
                          "Wallet History",
                          style: TextStyle(
                              color: mediumText.color,
                              fontSize: 13,
                              fontWeight: FontWeight.bold,
                              letterSpacing: 1.0),
                        ),
                      ),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: white,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  dialogeShow(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(5.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(5.0),
            content: Container(
              height: 100.0,
              width: screenSize.width * .5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      "This section is for the self-motivation for the users who can see how much prayers they have marked so far.",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  void _loadAndShowInterstitialAd(BuildContext context) {
    // Access the AdProvider using the Provider.of method
    AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

    // Load the interstitial ad
    adProvider.loadInterstitialAd();

    // Check if the interstitial ad is loaded
    if (adProvider.isInterstitialAdLoaded) {
      // resetNumber == 0;
      // Show the interstitial ad
      adProvider.showInterstitialAd();
    } else {
      // The ad is not loaded yet, you can handle this case accordingly
      print('Interstitial ad is not loaded yet. Please try again later.');
    }
  }
}
