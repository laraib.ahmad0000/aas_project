import 'dart:io';
import 'package:aas/Constants/colors.dart';
import 'package:aas/Model/Profile%20Modals/task_completed_modal.dart';
import 'package:aas/Model/Profile%20Modals/task_modal.dart';
import 'package:aas/Provider/Profile%20Provider/task_completed_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/constants/ad_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';

import '../../../Provider/Profile Provider/task_provider.dart';

class TaskScreen extends StatefulWidget {
  const TaskScreen({Key? key}) : super(key: key);

  @override
  State<TaskScreen> createState() => _TaskScreenState();
}

class _TaskScreenState extends State<TaskScreen> {
  RewardedAd? _rewardedAd;
  late BannerAd _bannerAd;
  bool _isTaskClickable = true;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    loadRewardedAd();
    // var result =

    Provider.of<TaskCompletedProvider>(context, listen: false);
    // .taskCompleted(TaskCompletedModal(task_id: , date: date));

    final DateFormat todayformatter = DateFormat('yyyy-MM-dd');
    final currentDate = DateTime.now();
    final String currentDateIs = todayformatter.format(currentDate);
    print('formated current date is: $currentDateIs');
    Provider.of<TaskProvider>(context, listen: false)
        .updateTasks(currentDateIs);
  }

  void loadRewardedAd() {
    RewardedAd.load(
        adUnitId: Platform.isIOS
            ? AdHelper.rewardedAdUnitId
            : AdHelper.rewardedAdUnitId,
        request: const AdRequest(),
        rewardedAdLoadCallback:
        RewardedAdLoadCallback(onAdLoaded: (RewardedAd ad) {
          _rewardedAd = ad;
        }, onAdFailedToLoad: (LoadAdError error) {
          print('failed to load reward: $error');
          _rewardedAd = null;
        }));
  }
  // void loadRewardedAd() {
  //   RewardedAd.load(
  //       adUnitId: Platform.isIOS
  //           ? "ca-app-pub-3007541505786179/2802967129"
  //           : "ca-app-pub-3007541505786179/2802967129",
  //       request: const AdRequest(),
  //       rewardedAdLoadCallback:
  //           RewardedAdLoadCallback(onAdLoaded: (RewardedAd ad) {
  //         _rewardedAd = ad;
  //       }, onAdFailedToLoad: (LoadAdError error) {
  //         _rewardedAd = null;
  //       }));
  // }

  void showRewardedAd(formatdate) {
    print('format date from show reward $formatdate');

    if (_rewardedAd != null) {
      _rewardedAd!.fullScreenContentCallback = FullScreenContentCallback(
          onAdShowedFullScreenContent: (RewardedAd ad) {
            Provider.of<TaskProvider>(context, listen: false)
                .updateTasks(formatdate);

            print("Ad onAdShowedFullScreenContent");
          }, onAdDismissedFullScreenContent: (RewardedAd ad) {
        ad.dispose();
        loadRewardedAd();
        InitLoading().dismissLoading();
      }, onAdFailedToShowFullScreenContent: (RewardedAd ad, AdError error) {
        ad.dispose();
        loadRewardedAd();
      });

      _rewardedAd!.setImmersiveMode(true);
      _rewardedAd!.show(
          onUserEarnedReward: (AdWithoutView ad, RewardItem reward) {
            print("${reward.amount} ${reward.type}");
          });
    } else {
      print('add is: $_rewardedAd');
      print('ad is null');
    }
  }

  void dispose() {
    super.dispose();
    _bannerAd.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final DateFormat todayformatter = DateFormat('yyyy-MM-dd');
    final currentDate = DateTime.now();
    final String currentDateIs = todayformatter.format(currentDate);

    TaskModel? taskData = Provider.of<TaskProvider>(context).taskData;

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        automaticallyImplyLeading: false,
        backgroundColor: primaryColor,
        title: Text(
          'Tasks',
          style: TextStyle(fontWeight: FontWeight.bold, color: Colors.white),
        ),
        centerTitle: true,
        actions: [
          InkWell(
            onTap: () {
              dialogeShow(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: FaIcon(
                CupertinoIcons.info,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.only(left: 20.0, top: 7.0),
              child: Text('Watch Ad to claim reward'),
            ),
            Expanded(
              child: Padding(
                padding: EdgeInsets.only(top: 10.h, left: 7.w, right: 7.w),
                child: taskData != null && taskData.taskdetail.isNotEmpty
                    ? ListView.builder(
                  shrinkWrap: true,
                  physics: const ScrollPhysics(),
                  scrollDirection: Axis.vertical,
                  itemCount: taskData.taskdetail.length,
                  itemBuilder: (BuildContext context, int index) {
                    bool lastDividerRemove =
                        index == taskData.taskdetail.length - 1;
                    final taskList = taskData.taskdetail[index];

                    return Column(
                      children: [
                        ListTile(
                          title: Text(
                            taskList.task_name ?? '', // null-aware access
                            style: const TextStyle(
                              color: Color(0xff144d46),
                              fontSize: 14,
                              fontWeight: FontWeight.bold,
                            ),
                          ),
                          trailing: Row(
                            mainAxisSize: MainAxisSize.min,
                            children: [
                              SvgPicture.asset(
                                'assets/images/coins.svg',
                                height: 20,
                                width: 20,
                              ),
                              SizedBox(
                                width: 5.0,
                              ),
                              Text(
                                taskList.reward ??
                                    '', // null-aware access
                                style: const TextStyle(
                                    color: Color(0xff144d46)),
                              ),
                              SizedBox(
                                width: 8.w,
                              ),
                              InkWell(
                                  onTap: () {
                                    if (_isTaskClickable) {
                                      if (taskList.isCompleted == 0) {
                                        String taskId = taskList.id;
                                        taskCompleted(
                                            currentDateIs, taskId);
                                        setState(() {});
                                        _disableTaskClick(); // Disable task click after first click
                                      } else {
                                        String taskId = taskList.id;
                                        taskCompleted(
                                            currentDateIs, taskId);
                                        _disableTaskClick(); // Disable task click after first click
                                      }
                                    } else {
                                      // Show a pop-up with a message to wait
                                      showDialog(
                                        context: context,
                                        builder:
                                            (BuildContext dialogContext) {
                                          return AlertDialog(
                                            title: Text("Please wait"),
                                            content: Text(
                                                "Please wait 10 seconds before attempting to claim another reward."),
                                            actions: <Widget>[
                                              TextButton(
                                                child: Text("OK"),
                                                onPressed: () {
                                                  Navigator.of(context)
                                                      .pop();
                                                },
                                              ),
                                            ],
                                          );
                                        },
                                      );
                                    }
                                  },
                                  child: taskList.isCompleted == 0
                                      ? CircleAvatar(
                                    maxRadius: 10,
                                    backgroundColor: primaryColor,
                                    child: Icon(
                                      Icons.play_arrow,
                                      color: white,
                                      size: 15,
                                    ),
                                  )
                                      : CircleAvatar(
                                    maxRadius: 10,
                                    backgroundColor: Colors.green,
                                    child: Icon(
                                      Icons.done,
                                      color: Colors.white,
                                      size: 15,
                                    ),
                                  )),
                            ],
                          ),
                        ),
                        if (!lastDividerRemove)
                          const Divider(
                            thickness: 1,
                            color: Color(0xff144d46),
                            height: 0,
                          ),
                      ],
                    );
                  },
                )
                    : Center(child: CircularProgressIndicator()),
              ),
            ),
            Container(
              width: MediaQuery.of(context).size.width,
              height: 50,
              child: AdWidget(ad: _bannerAd),
            ),
          ],
        ),
      ),
    );
  }

  void _disableTaskClick() {
    setState(() {
      _isTaskClickable = false;
    });
    // Enable task icon click after 2 minutes
    Future.delayed(Duration(seconds: 10), () {
      setState(() {
        _isTaskClickable = true;
      });
    });
  }

  taskCompleted(formatdate, taskId) {
    print('format date from task completed: $formatdate');
    final taskCompleted =
    Provider.of<TaskCompletedProvider>(context, listen: false);
    final taskC =
    TaskCompletedModal(task_id: int.parse(taskId), date: formatdate);
    taskCompleted.taskCompleted(taskC);
    Provider.of<TaskProvider>(context, listen: false).updateTasks(formatdate);
    // showRewardedAd(formatdate);
    _loadAndShowInterstitialAd(context);
  }

  void _loadAndShowInterstitialAd(BuildContext context) {
    // Access the AdProvider using the Provider.of method
    AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

    // Load the interstitial ad
    adProvider.loadInterstitialAd();

    // Check if the interstitial ad is loaded
    if (adProvider.isInterstitialAdLoaded) {
      // resetNumber == 0;
      // Show the interstitial ad
      adProvider.showInterstitialAd();
    } else {
      // The ad is not loaded yet, you can handle this case accordingly
      print('Interstitial ad is not loaded yet. Please try again later.');
    }
  }

  dialogeShow(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(5.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(5.0),
            content: Container(
              height: 90.0,
              width: screenSize.width * .5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      "Tasks section is where you can earn coins by marking the given tasks as completed.",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
