import 'package:aas/Constants/colors.dart';
import 'package:aas/Model/total_prayers_modal.dart';
import 'package:aas/Provider/total_prayers_provider.dart';
import 'package:aas/constants/ad_helper.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

class BadgesScreen extends StatefulWidget {
  @override
  State<BadgesScreen> createState() => _BadgesScreenState();
}

class _BadgesScreenState extends State<BadgesScreen> {
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Provider.of<TotalPrayersProvider>(context, listen: false).getTotalPrayers();
  }

  NativeAd? nativeAd;
  bool isNativeAdLoaded = false;
  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    loadNativeAd();
  }

  void loadNativeAd() {
    nativeAd = NativeAd(
        adUnitId: AdHelper.nativeAdUnitId,
        listener: NativeAdListener(onAdLoaded: (ad) {
          setState(() {
            isNativeAdLoaded = true;
          });
        }, onAdFailedToLoad: (ad, error) {
          nativeAd!.dispose();
        }),
        request: AdRequest(),
        nativeTemplateStyle:
            NativeTemplateStyle(templateType: TemplateType.small));
    nativeAd!.load();
  }

  int level = 0;
  @override
  Widget build(BuildContext context) {
    TotalPrayers? totalPrayer =
        Provider.of<TotalPrayersProvider>(context).totalPrayers;
    // print('this is total prayers: ${totalPrayer!.totalPrayers}');
    // int total = 80;
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Padding(
            padding: EdgeInsets.only(
              top: 15.h,
            ),
            child: Container(
              child: Column(
                // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 5 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 5)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 1',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '5 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 10 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 10)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 2',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '10 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 20 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 20)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 3',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '20 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 40 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 40)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 4',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '40 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  //       this is second row   ////************************* */

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 80 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 80)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 5',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '80 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 160 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 160)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 6',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '160 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 320 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 320)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 7',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '320 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 640 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 640)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 8',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '640 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),

                  //      this is 3rd row   ///////// **************

                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 1280 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 1280)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 9',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '1280 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 320 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 320)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 10',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              'Prayers Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 640 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 640)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 11',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              'Prayers Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 80,
                        width: 80,
                        child: Column(
                          children: [
                            totalPrayer?.totalPrayers == 1280 ||
                                    (totalPrayer != null &&
                                        totalPrayer.totalPrayers > 1280)
                                ? const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: primaryColor,
                                  )
                                : const Icon(
                                    Icons.emoji_events,
                                    size: 50,
                                    color: lightBlack,
                                  ),
                            const Text(
                              'Level 12',
                              style: TextStyle(
                                fontSize: 10,
                                fontWeight: FontWeight.bold,
                                color: primaryColor,
                              ),
                            ),
                            const Text(
                              '10240 Prayers',
                              style: TextStyle(
                                color: primaryColor,
                                fontSize: 9.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                  //      this is 4th row   ///////// **************
                  Row(
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    children: [],
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  isNativeAdLoaded
                      ? Container(
                          decoration: BoxDecoration(color: white),
                          height: 130,
                          child: AdWidget(ad: nativeAd!),
                        )
                      : SizedBox(),
                ],
              ),
            ),

            // GridView.builder(
            //   scrollDirection: Axis.vertical,
            //   physics: const ScrollPhysics(),
            //   shrinkWrap: true,
            //   itemCount: 10,
            //   // Number of items in each row
            //   gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
            //     childAspectRatio: 0.75,
            //     crossAxisCount: 4,
            //     crossAxisSpacing: 0.0,
            //     mainAxisSpacing: 0.0,
            //   ),
            //   itemBuilder: (context, index) {
            //     return const Column(
            //       children: [
            //         GridItem(
            //           icon: Icon(Icons.emoji_events),
            //           levelText: 'Level 1',
            //           pointsText: 'Points 5',
            //         ),
            //         Divider(
            //           thickness: 1,
            //           color: primaryColor, // Divider color
            //         ),
            //       ],
            //     );
            //   },
            // ),
          ),
        ),
      ),
    );
  }
}
