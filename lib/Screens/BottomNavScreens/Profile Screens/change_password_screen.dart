import 'package:aas/Model/Profile%20Modals/change_password_modal.dart';
import 'package:aas/Provider/Profile%20Provider/change_password_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:aas/constants/reusable_widgets.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({super.key});

  @override
  State<ChangePasswordScreen> createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> {
  TextEditingController currentPasswordController = TextEditingController();
  TextEditingController newPasswordController = TextEditingController();
  TextEditingController confirmNewPasswordController = TextEditingController();
  late BannerAd _bannerAd;
  NativeAd? nativeAd;
  bool isNativeAdLoaded = false;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
  }

  void dispose() {
    super.dispose();
    _bannerAd.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: primaryColor,
        title: Text(
          'Change Password',
          style: TextStyle(color: white),
        ),
        iconTheme: IconThemeData(color: white),
      ),
      // backgroundColor: black,
      body: SafeArea(
          child: Padding(
        padding: EdgeInsets.all(10),
        child: Column(
          children: [
            Container(
              margin: EdgeInsets.only(top: 15.0),
              height: MediaQuery.of(context).size.height / 2.4,
              width: MediaQuery.of(context).size.width,
              // color: yellowC,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    child: Column(
                      children: [
                        CustomTextField(
                          obscureText: true,
                          hint: 'Enter Current Password',
                          textEditingController: currentPasswordController,
                          textInputType: TextInputType.text,
                          prefixIcon: Icon(
                            Icons.lock_clock_outlined,
                            size: 20,
                            color: primaryColor,
                          ),
                          validatorMessage: 'Enter Current Password',
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomTextField(
                          obscureText: true,
                          hint: 'Enter New Password',
                          textEditingController: newPasswordController,
                          textInputType: TextInputType.text,
                          prefixIcon: Icon(
                            Icons.lock_outline,
                            size: 20,
                            color: primaryColor,
                          ),
                          validatorMessage: 'Enter New Password',
                        ),
                        SizedBox(
                          height: 20,
                        ),
                        CustomTextField(
                          obscureText: true,
                          hint: 'Confirm new Password',
                          textEditingController: confirmNewPasswordController,
                          textInputType: TextInputType.text,
                          prefixIcon: Icon(
                            Icons.lock_outline,
                            size: 20,
                            color: primaryColor,
                          ),
                          validatorMessage: 'Confirm new Password',
                        ),
                        SizedBox(
                          height: 40,
                        ),
                        GestureDetector(
                          onTap: () {
                            if (currentPasswordController.text.isEmpty) {
                              CustomSnackbar.error(
                                  message: 'Please enter Current Password');
                            } else if (newPasswordController.text.isEmpty) {
                              CustomSnackbar.error(
                                  message: 'Please enter New Password');
                            } else if (confirmNewPasswordController
                                .text.isEmpty) {
                              CustomSnackbar.error(
                                  message: 'Please enter Confirm Password');
                            } else if (newPasswordController.text !=
                                confirmNewPasswordController.text) {
                              CustomSnackbar.error(
                                  message: 'Password doesn\'t match');
                            } else {
                              print('api hit');
                              final changePasswordProvider =
                                  Provider.of<ChangePasswordProvider>(context,
                                      listen: false);
                              final changePassword = ChangePasswordModal(
                                  current_password:
                                      currentPasswordController.text,
                                  new_password: newPasswordController.text,
                                  new_password_confirmation:
                                      confirmNewPasswordController.text);

                              changePasswordProvider.changePassword(
                                  changePassword,
                                  context,
                                  currentPasswordController.text,
                                  newPasswordController.text,
                                  confirmNewPasswordController.text);
                            }
                          },
                          child: Container(
                              height: 45,
                              width: MediaQuery.of(context).size.width / 2,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(30.0),
                                  color: primaryColor),
                              child: Center(
                                child: Text('Change Password',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 17,
                                        color: white)),
                              )),
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Expanded(
              child: Container(
                // height: MediaQuery.of(context).size.height / 3,
                width: MediaQuery.of(context).size.width,
                // color: lightBlack,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    isNativeAdLoaded
                        ? Container(
                            decoration: BoxDecoration(color: white),
                            height: 130,
                            child: AdWidget(ad: nativeAd!),
                          )
                        : SizedBox(),
                    Padding(
                      padding: EdgeInsets.only(left: 5),
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        height: 60,
                        child: AdWidget(ad: _bannerAd),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      )),
    );
  }
}
