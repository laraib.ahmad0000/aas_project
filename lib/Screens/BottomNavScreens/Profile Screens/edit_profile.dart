import 'dart:io';

import 'package:aas/Model/Profile%20Modals/delete_account_modal.dart';
import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/Profile%20Provider/cancel_deletion_provider.dart';
import 'package:aas/Provider/Profile%20Provider/delete_account_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/settings_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/change_password_screen.dart';
import 'package:aas/constants/reusable_widgets.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:http/http.dart' as http;
import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Provider/ad_provider.dart';

import 'package:aas/Constants/colors.dart';

import '../../../constants/pop-up.dart';

class EditProfileScreen extends StatefulWidget {
  @override
  State<EditProfileScreen> createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  late BannerAd _bannerAd;
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController deleteAccountPasswordController =
      TextEditingController();

  String? _profileImagePath; // Store profile image path
  File? _imgFile;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    Provider.of<UserDataProvider>(context, listen: false).updateUserData();
    final userDataProvider =
        Provider.of<UserDataProvider>(context, listen: false);
    nameController.text = userDataProvider.userData?.name ?? '';
    phoneController.text = userDataProvider.userData?.phoneNumber ?? '';
    _profileImagePath = userDataProvider.userData?.profileimage;
  }

  void dispose() {
    super.dispose();
    nameController.dispose();
    phoneController.dispose();
    deleteAccountPasswordController.dispose();
    _bannerAd.dispose();
  }

  Future<void> _pickImageFromGallery() async {
    final picker = ImagePicker();
    final XFile? image = await picker.pickImage(
      source: ImageSource.gallery,
      maxWidth: 200,
    );

    setState(() {
      _imgFile = File(image!.path);
    });
  }

  Future<void> updateUserProfile() async {
    String apiUrl = 'https://aasonline.co/api/user-profile-update';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    var request = http.MultipartRequest('POST', Uri.parse(apiUrl));
    request.headers['Authorization'] = 'Bearer $token';

    request.fields['name'] = nameController.text;
    request.fields['phoneno'] = phoneController.text;

    if (_imgFile != null) {
      var imageStream = http.ByteStream(_imgFile!.openRead());
      var length = await _imgFile!.length();

      var multipartFile = http.MultipartFile(
        'profileimage',
        imageStream,
        length,
        filename: _imgFile!.path.split('/').last,
      );
      request.files.add(multipartFile);
    }

    FocusScope.of(context).unfocus();

    try {
      var response = await request.send();
      var responseData = await response.stream.bytesToString();

      if (response.statusCode == 200) {
        final userDataProvider =
            Provider.of<UserDataProvider>(context, listen: false);
        userDataProvider.updateUserData();
        CustomSnackbar.successful(message: "Profile is Updated");
      } else {
        CustomSnackbar.error(message: "Profile is Not Updated");
        print('Failed to update profile. Status: ${response.statusCode}');
      }
    } catch (error) {
      CustomSnackbar.error(
          message: "Profile is Not Updated SomeThing Went Wrong");
      print('Error occurred while updating profile: $error');
    }
  }

  @override
  Widget build(BuildContext context) {
    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    final userDataProvider =
        Provider.of<UserDataProvider>(context, listen: false);
    double height = MediaQuery.of(context).size.height;
    double width = MediaQuery.of(context).size.width;
    int isDeleted =
        int.parse(userDataProvider.userData?.isDelete.toString() ?? '');
    return Scaffold(
      resizeToAvoidBottomInset: true,
      backgroundColor: white,
      appBar: AppBar(
        backgroundColor: primaryColor,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Get.back();
          },
          child: Padding(
            padding: const EdgeInsets.only(left: 20.0),
            child: Container(
              height: 30,
              width: 30,
              decoration: BoxDecoration(
                  shape: BoxShape.circle,
                  // color: lightBlack,
                  border: Border.all(color: white, width: 2)),
              child: Center(
                child: Icon(Icons.arrow_back, color: white, size: 20),
              ),
            ),
          ),
        ),
        actions: [
          IconButton(
            onPressed: () {
              _pickImageFromGallery();
            },
            icon: Icon(
              Icons.add_a_photo,
              color: white,
              size: 26,
            ),
          )
        ],
      ),
      body: SingleChildScrollView(
        // Wrap with SingleChildScrollView
        child: SafeArea(
          child: Consumer<UserDataProvider>(
            builder: (context, userDataProvider, _) {
              return Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    height: height * 0.2,
                    width: width,
                    decoration: BoxDecoration(
                      color: primaryColor,
                      borderRadius:
                          BorderRadius.only(bottomRight: Radius.circular(70.0)),
                    ),
                    child: Column(
                      children: [
                        Container(
                          height: 100,
                          width: 100,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10.0),
                            child: _imgFile != null
                                ? Image(
                                    fit: BoxFit.fill,
                                    image: FileImage(_imgFile!),
                                  )
                                : _profileImagePath != null
                                    ? Image.network(
                                        _profileImagePath!,
                                        fit: BoxFit.fill,
                                      )
                                    : Image.asset(
                                        "assets/images/person.jpg",
                                        fit: BoxFit.fill,
                                      ),
                          ),
                        ),
                        SizedBox(height: 10.0),
                        Text(
                          userDataProvider.userData?.email ?? '',
                          style: TextStyle(color: white, fontSize: 12),
                        ),
                      ],
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: GestureDetector(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => ChangePasswordScreen()));
                      },
                      child: Container(
                          height: 45,
                          width: 180,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(30.0),
                              color: primaryColor),
                          child: Center(
                            child: Text('Change Password',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 17,
                                    color: white)),
                          )),
                    ),
                  ),
                  textFieldContainers(),
                  isDeleted == 0
                      ? deleteAccountContainer()
                      : calcelationContainer(),
                  SizedBox(
                    height: 20.0,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 5),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: AdWidget(ad: _bannerAd),
                    ),
                  ),
                ],
              );
            },
          ),
        ),
      ),
    );
  }

  textFieldContainers() {
    return Container(
      width: Get.width,
      decoration: BoxDecoration(
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
        color: white,
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 30),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: [
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: SizedBox(
                height: 45,
                child: TextFormField(
                  controller: nameController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: primaryColor, width: 1.0),
                    ),
                    prefixIcon: Icon(
                      Icons.person,
                      size: 20,
                      color: primaryColor,
                    ),
                    hintText: "Name",
                    hintStyle: TextStyle(fontSize: 15, color: primaryColor),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              ),
            ),
            SizedBox(height: 20),
            Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20),
              child: SizedBox(
                height: 45,
                child: TextFormField(
                  controller: phoneController,
                  decoration: InputDecoration(
                    contentPadding: EdgeInsets.all(10),
                    focusedBorder: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                      borderSide: BorderSide(color: primaryColor, width: 1.0),
                    ),
                    prefixIcon: Icon(
                      Icons.phone,
                      size: 20,
                      color: primaryColor,
                    ),
                    hintText: "Phone No.",
                    hintStyle: TextStyle(fontSize: 15, color: primaryColor),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(8),
                    ),
                  ),
                ),
              ),
            ),
            // SizedBox(height: 20),
            InkWell(
              onTap: () {
                updateUserProfile();
              },
              child: Image.asset(
                "assets/images/submit button.png",
                width: 130.w,
                height: 130.h,
              ),
            ),
          ],
        ),
      ),
    );
  }

  deleteAccountContainer() {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 15.0),
      child: Container(
        // height: 140,
        width: MediaQuery.of(context).size.width,
        // color: lightBlack,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Row(
              children: [
                Text(
                  'Delete Account Permanently',
                  style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.bold,
                      color: primaryColor),
                ),
                SizedBox(
                  width: 5.0,
                ),
                InkWell(
                    onTap: () {
                      dialogeShow(context);
                    },
                    child: FaIcon(
                      CupertinoIcons.info,
                      color: black,
                    )),
              ],
            ),
            SizedBox(
              height: 10.0,
            ),
            SizedBox(
              height: 45,
              child: TextFormField(
                obscureText: true,
                controller: deleteAccountPasswordController,
                decoration: InputDecoration(
                  contentPadding: EdgeInsets.all(10),
                  focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                    borderSide: BorderSide(color: primaryColor, width: 1.0),
                  ),
                  prefixIcon: Icon(
                    Icons.password,
                    size: 20,
                    color: primaryColor,
                  ),
                  hintText: "Enter your password",
                  hintStyle: TextStyle(fontSize: 15, color: lightBlack),
                  border: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(8),
                  ),
                ),
              ),
            ),
            SizedBox(
              height: 10.0,
            ),
            InkWell(
              onTap: () {
                if (deleteAccountPasswordController.text.trim().isEmpty) {
                  CustomSnackbar.error(message: 'Please Enter Password');
                } else {
                  _showDeleteDialogue(context);
                }
              },
              child: Container(
                height: 43,
                width: 160,
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(8.0),
                  color: red,
                ),
                child: Center(
                  child: Text(
                    'Delete Account',
                    style: TextStyle(
                        color: white,
                        fontSize: 16,
                        fontWeight: FontWeight.bold),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  calcelationContainer() {
    return Container(
      height: 140,
      width: MediaQuery.of(context).size.width,
      // color: Colors.pink[100],
      child: Column(
        children: [
          InkWell(
            onTap: () {
              Provider.of<CancelDeletionProvider>(context, listen: false)
                  .canceldeletionP(context);
            },
            child: Container(
              height: 43,
              width: 170,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(8.0),
                color: greenC,
              ),
              child: Center(
                child: Text(
                  'Cancel Account Deletion',
                  style: TextStyle(
                      letterSpacing: 0.6,
                      color: white,
                      fontSize: 13,
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }

  _showDeleteDialogue(BuildContext context) {
    // final auth = FirebaseAuth.instance;
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(30.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 150.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    const SizedBox(
                      height: 1.0,
                    ),
                    Text(
                      "Are you sure you want to delete your account?",
                      style: appNormalText,
                      textAlign: TextAlign.center,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextButton(
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              padding: const EdgeInsets.all(16.0),
                              textStyle: const TextStyle(fontSize: 20),
                            ),
                            onPressed: () {
                              // logoutUser();
                              final deleteProvider =
                                  Provider.of<DeleteAccountProvider>(context,
                                      listen: false);

                              final deleteAccount = DeleteAccountModal(
                                  password: deleteAccountPasswordController.text
                                      .trim());
                              deleteProvider.deleteAccountP(
                                  deleteAccount, context);
                            },
                            child: Container(
                              height: 33,
                              width: 63,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    7.0,
                                  ),
                                  border: Border.all(color: red)),
                              child: const Center(
                                child: Text(
                                  'Yes',
                                  style: redText,
                                ),
                              ),
                            )),
                        const SizedBox(
                          width: 30.0,
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            foregroundColor: Colors.white,
                            padding: const EdgeInsets.all(16.0),
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 33,
                            width: 63,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  7.0,
                                ),
                                border: Border.all(color: black)),
                            child: const Center(
                              child: Text(
                                'No',
                                style: appNormalText,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }

  dialogeShow(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(5.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(5.0),
            content: Container(
              height: 255.0,
              width: screenSize.width * .5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      "Before proceeding with the deletion of your account, kindly ensure that you have downloaded any essential data or information you wish to retain. Once your account deletion request is confirmed, all associated resources and data will be permanently removed from our system. Your account deletion will be scheduled to take place 48 hours from the time of your request. This waiting period allows you time to reconsider your decision. Should you change your mind during this period, you have the option to cancel the deletion request.",
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
