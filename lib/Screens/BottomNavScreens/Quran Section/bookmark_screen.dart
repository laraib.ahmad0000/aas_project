import 'package:aas/Model/bookmark_modal.dart';
import 'package:aas/Model/get_bookmark_modal.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/bookmark_provider.dart';
import 'package:aas/Provider/get_bookmark_provider.dart';
import 'package:aas/Provider/surah_number_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Quran%20Section/surah_detail_Screen.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

class BookmarksScreen extends StatefulWidget {
  @override
  State<BookmarksScreen> createState() => _BookmarksScreenState();
}

class _BookmarksScreenState extends State<BookmarksScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<GetBookmarkProvider>(context, listen: false).getBookmarkData();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<GetBookmarkProvider>(
      builder: (context, getBookmarkProvider, child) {
        GetBookmarkModal? getBookmarkData = getBookmarkProvider.getBookmark;

        if (getBookmarkData == null || getBookmarkData.bookmarks.isEmpty) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Image(
                height: 170,
                width: 170,
                image: AssetImage('assets/images/bookmark.png'),
              ),
              SizedBox(
                height: 100,
              ),
              Text(
                'There are no Bookmarks yet, go ahead and add it!',
                style: TextStyle(
                  fontSize: 15,
                  fontWeight: FontWeight.w500,
                  fontFamily: 'Amiri',
                ),
              ),
            ],
          );
        }

        return Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: getBookmarkData!.bookmarks.isEmpty
              ? const Center(
                  child: CircularProgressIndicator(),
                )
              : Stack(children: [
                  ListView.separated(
                    itemCount: getBookmarkData.bookmarks.length,
                    itemBuilder: (context, index) {
                      final surahsData = getBookmarkData.bookmarks[index];
                      return InkWell(
                        onTap: () {
                          var surahNumberProvider =
                              Provider.of<SurahNumberProvider>(context,
                                  listen: false);
                          surahNumberProvider.formatSurahNumbers(
                              getBookmarkData.bookmarks[index].surahNo);
                          Get.to(const SurahDetailScreen(), arguments: [
                            getBookmarkData.bookmarks[index].surahNo,
                            surahNumberProvider.formatNumber
                          ]);
                        },
                        child: ListTile(
                          leading: Stack(
                            children: [
                              const Image(
                                height: 40,
                                width: 45,
                                image: AssetImage(
                                    'assets/images/quran-screen-counting.png'),
                              ),
                              Positioned(
                                top: 13,
                                left: 0,
                                right: 0,
                                child: Center(
                                  child: Text(
                                    '${getBookmarkData.bookmarks[index].surahNo}',
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      fontFamily: 'Amiri',
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          title: Transform.translate(
                            offset: const Offset(-10, 0),
                            child: Text(
                              'Surah ${getBookmarkData.bookmarks[index].surahNo}',
                              style: const TextStyle(
                                fontSize: 11,
                                fontFamily: 'Amiri',
                              ),
                            ),
                          ),
                          subtitle: Transform.translate(
                            offset: const Offset(-10, 0),
                            child: Text(
                              surahsData.englishName,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: primaryColor,
                                  fontFamily: 'Amiri',
                                  fontSize: 13.5),
                            ),
                          ),
                          trailing: SizedBox(
                            width: 150,
                            child: Padding(
                              padding: const EdgeInsets.only(top: 18),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: [
                                  Text(
                                    surahsData.arabicName,
                                    style: const TextStyle(
                                      fontWeight: FontWeight.bold,
                                      color: primaryColor,
                                      fontFamily: 'Amiri',
                                      fontSize: 16.5,
                                    ),
                                  ),
                                  const SizedBox(width: 3.0),
                                  InkWell(
                                    onTap: () {
                                      bookmarkSurah(
                                        surahsData.arabicName,
                                        surahsData.englishName,
                                        surahsData.surahNo,
                                        getBookmarkProvider,
                                      );
                                    },
                                    child: Container(
                                      height: 40,
                                      width: 40,
                                      child: Icon(
                                        CupertinoIcons.heart_fill,
                                        color: Colors.red,
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                      );
                    },
                    separatorBuilder: (context, index) => Divider(),
                  ),
                ]),
        );
      },
    );
  }

  bookmarkSurah(
    String arabic_name,
    String english_name,
    int surah_no,
    GetBookmarkProvider getBookmarkProvider,
  ) {
    final bookmarkProvider =
        Provider.of<BookmarkProvider>(context, listen: false);
    final bookmarkSurah = BookmarkModal(
      arabic_name: arabic_name,
      english_name: english_name,
      surah_no: surah_no,
    );
    bookmarkProvider.bookmarkSuratz(bookmarkSurah);

    // Remove the item from the list in GetBookmarkProvider
    getBookmarkProvider.getBookmark?.bookmarks.removeWhere(
      (bookmark) => bookmark.surahNo == surah_no.toString(),
    );

    // Notify listeners after updating the list
    // ignore: invalid_use_of_protected_member, invalid_use_of_visible_for_testing_member
    getBookmarkProvider.notifyListeners();
  }
}
