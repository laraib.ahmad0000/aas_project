import 'dart:ui';

import 'package:aas/Screens/BottomNavScreens/Quran%20Section/para_detail_screen.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';

class ParaScreen extends StatefulWidget {
  @override
  State<ParaScreen> createState() => _ParaScreenState();
}

class _ParaScreenState extends State<ParaScreen> {
  @override
  Widget build(BuildContext context) {
    // Define a function to build each list item

    // Replace with your Sura screen implementation
    return Scaffold(
      
      backgroundColor: white,
      body: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Container(
          // Staggered Grid View starts here
          child: GridView.builder(
            gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 3),
            itemCount: 30,
            itemBuilder: (BuildContext context, int index) {
              return Padding(
                padding: const EdgeInsets.all(8.0),
                child: InkWell(
                  child: Container(
                      decoration: BoxDecoration(
                          color: primaryColor,
                          borderRadius: BorderRadius.circular(6.0),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.black,
                              blurRadius: 2.0,
                              spreadRadius: 0.0,
                              offset: Offset(
                                  2.0, 2.0), // shadow direction: bottom right
                            )
                          ]),
                      child: Stack(
                        children: [
                          Positioned(
                              child: Center(
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                Text(
                                  'Para',
                                  style: TextStyle(
                                      fontFamily: 'Amiri',
                                      color: white,
                                      fontSize: 24,
                                      fontWeight: FontWeight.bold,
                                      letterSpacing: 1.3),
                                ),
                                Text(
                                  '${index + 1}',
                                  style: TextStyle(
                                    color: white,
                                    fontSize: 24,
                                    fontFamily: 'Amiri',
                                  ),
                                ),
                              ],
                            ),
                          ))
                        ],
                      )),
                  onTap: () {
                    int paraIndex = index + 1;
                    print(paraIndex);
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                ParaDetailScreen(paraIndex: paraIndex)));
                  },
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

// class BackGroundTile extends StatelessWidget {
//   final Color backgroundColor;
//   final IconData icondata;

//   BackGroundTile({required this.backgroundColor, required this.icondata});

//   @override
//   Widget build(BuildContext context) {
//     return Card(
//       color: backgroundColor,
//       child: Icon(icondata, color: Colors.white),
//     );
//   }
// }
