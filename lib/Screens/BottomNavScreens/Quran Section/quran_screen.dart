import 'dart:async';

import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/MLM/quran_reward_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/get_bookmark_provider.dart';
import 'package:aas/Provider/surah_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/settings_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Quran%20Section/bookmark_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Quran%20Section/para_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Quran%20Section/surah_screen.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';
import 'package:quran/quran.dart' as quran;

class QuranScreen extends StatefulWidget {
  @override
  _MyTabbedPageState createState() => _MyTabbedPageState();
}

class _MyTabbedPageState extends State<QuranScreen>
    with TickerProviderStateMixin {
  late TabController _tabController;
  late BannerAd _bannerAd;
  AnimationController? _controller;
  int levelClock = 300;

  Timer? t;

  @override
  void initState() {
    super.initState();
    // Provider.of<UserDataProvider>(context, listen: false);

    _tabController = TabController(length: 3, vsync: this);
    _bannerAd = context.read<AdProvider>().createBannerAd();

    _tabController.addListener(() {
      setState(() {}); // Rebuild the widget when the tab changes
    });

    // userData!.isQuranReward == 1
    // ?
    // Future.delayed(Duration(milliseconds: 700), () {
    // UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    // });

    // t = Timer(Duration(seconds: 8), () {
    // });

    _controller = AnimationController(
      vsync: this,
      duration: Duration(seconds: levelClock),
    );
    _controller!.forward(from: 0);

    _controller!.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        Provider.of<UserDataProvider>(context, listen: false).updateUserData();
        quranRewardedApi();
      }
    });
  }

  quranRewardedApi() {
    Provider.of<QuranRewardProvider>(context, listen: false).quranReward();
  }

  @override
  void dispose() {
    _tabController.dispose();
    _controller!.dispose();
    super.dispose();
  }

  // void _incrementCounter() {
  //   setState(() {
  //     _counter++;
  //   });
  // }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    // final quranSurahProvider = Provider.of<SurahProvider>(context);

    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    Future.delayed(Duration(milliseconds: 700), () {
      userData!.isQuranReward == 0 ? _showRewardPopup(context) : Text('');
    });
    // final surahs = quranSurahProvider.surah;
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: primaryColor,
          child: Column(
            children: [
              Container(
                height: screenHeight * .1,
                child: Center(
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Get.back();

                            t!.cancel();
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 20.0),
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  // color: lightBlack,
                                  border: Border.all(color: white, width: 2)),
                              child: const Center(
                                child: Icon(Icons.arrow_back,
                                    color: white, size: 20),
                              ),
                            ),
                          )),
                      SizedBox(width: screenWidth * .27),
                      const Text(
                        'QURAN',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          color: Colors.white,
                          fontSize: 20,
                        ),
                      ),
                      const SizedBox(width: 10),
                      Container(
                        height: 40,
                        width: 40,
                        child: Image.asset("assets/images/ic_quranp.png"),
                      )
                    ],
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                      userData!.isQuranReward == 0
                          ? Countdown(
                              animation: StepTween(
                                begin:
                                    levelClock, // THIS IS A USER ENTERED NUMBER
                                end: 0,
                              ).animate(_controller!),
                            )
                          :
                          // Text(
                          //           'اَلْحَمْدُ لِلّٰهِ رَبِّ الْعٰلَمِیْنَۙ(۱)',
                          //           style: const TextStyle(
                          //               fontFamily: 'al-qalam',
                          //               fontWeight: FontWeight.bold,
                          //               fontSize: 33,
                          //               color: black),
                          // ),
                          
                           Text(
                            textAlign: TextAlign.center,
                              'Today\'s reward added to your Wallet successfully.',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold,
                                  color: primaryColor),
                            ),
                               SizedBox(height: 30.0,),
                      // 30.heightBox,
                      Container(
                        height: screenHeight * .05,
                        width: screenWidth * .85,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: const Color(
                                  0xFF111111), // Change to black border color
                            ),
                          ),
                          child: TabBar(
                            labelColor: white,
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                              color: const Color.fromRGBO(
                                  20, 77, 70, 1.0), // Active tab color
                              borderRadius: BorderRadius.circular(10),
                            ),
                            unselectedLabelColor: const Color(0xFFABA9A9),
                            controller: _tabController,
                            tabs: const [
                              Tab(text: 'Sura'),
                              Tab(text: 'Para'),
                              Tab(text: 'Bookmarks'),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            SuraScreen(),
                            ParaScreen(),
                            BookmarksScreen(),
                          ],
                        ),
                      ),
                      // Container(
                      //   width: MediaQuery.of(context).size.width,
                      //   height: 60,
                      //   child: AdWidget(ad: _bannerAd),
                      // ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  _showRewardPopup(BuildContext context) {
    TextEditingController JoinCommunityController = TextEditingController();
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(30.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 150.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  gradient: LinearGradient(colors: [
                    primaryColor,
                    Color.fromARGB(255, 29, 221, 163)
                  ])),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    const SizedBox(
                      height: 1.0,
                    ),
                    Text(
                      "Stay on this screen for 5 minutes to get your reward.",
                      style: TextStyle(
                        fontSize: 16,
                        color: white,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                          height: 40,
                          width: 100,
                          decoration: BoxDecoration(
                              border: Border.all(color: black, width: 0.6),
                              color: white,
                              borderRadius: BorderRadius.circular(7.0)),
                          child: Center(
                            child: Text(
                              'OK',
                              style: TextStyle(color: black),
                            ),
                          )),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}

class Countdown extends AnimatedWidget {
  Countdown({Key? key, required this.animation})
      : super(key: key, listenable: animation);
  final Animation<int> animation;

  @override
  build(BuildContext context) {
    Duration clockTimer = Duration(seconds: animation.value);
    String timerText =
        '${clockTimer.inMinutes.remainder(60).toString()}:${clockTimer.inSeconds.remainder(60).toString().padLeft(2, '0')}';

    return Text(
      "Remaining Time: $timerText",
      style: TextStyle(
        fontSize: 30,
        color: Colors.black,
      ),
    );
  }
}
