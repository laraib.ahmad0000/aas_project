import 'package:aas/Provider/qari_detail_provider.dart';
import 'package:aas/Provider/surah_detail_provider.dart';
import 'package:aas/Provider/surah_provider.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:audioplayers/audioplayers.dart' as ad;
import 'package:just_audio/just_audio.dart';

import 'package:provider/provider.dart';

class SurahDetailScreen extends StatefulWidget {
  const SurahDetailScreen({super.key});

  @override
  State<SurahDetailScreen> createState() => _SurahDetailScreenState();
}

class _SurahDetailScreenState extends State<SurahDetailScreen> {
  late SurahDetailProvider quranSurahDetailProvider;
  late QariDetailProvider qariDetailProvider;
  late List<Map<String, dynamic>> surahsDetail;
  late List<Map<String, dynamic>> qariDetail;
  String? qariRelativePath;

  Map<String, dynamic>? qariDataDetail;
  String playPauseBtnState = 'Play';

  @override
  void initState() {
    super.initState();
    var getData = Get.arguments;
    quranSurahDetailProvider =
        Provider.of<SurahDetailProvider>(context, listen: false);
    quranSurahDetailProvider.fetchSurahDetail(getData[0]);

    qariDetailProvider =
        Provider.of<QariDetailProvider>(context, listen: false);

    qariDetailProvider.fetchQariDetail();
    // if (result != null) {
    print('this is index of surah: ${Get.arguments[1]}');
    _audioPlayer = AudioPlayer();
  }

  late AudioPlayer _audioPlayer;
  bool isPlaying = false;

  @override
  void dispose() {
    _audioPlayer.dispose();
    super.dispose();
  }

  void _playPause(String audioUrl) {
    InitLoading().showLoading('Loading...');
    if (isPlaying) {
      _audioPlayer.pause();
      setState(() {
        playPauseBtnState = 'Play';
      });
    } else {
      InitLoading().dismissLoading();
      _audioPlayer.setUrl(audioUrl).then((_) {
        _audioPlayer.play();
      });
      setState(() {
        playPauseBtnState = 'Pause';
      });
    }
    setState(() {
      InitLoading().dismissLoading();
      isPlaying = !isPlaying;
    });
  }

  @override
  Widget build(BuildContext context) {
    surahsDetail = Provider.of<SurahDetailProvider>(context).surahDetail;
    qariDetail = Provider.of<QariDetailProvider>(context).qariDetail;

    // print('detail from 29: $surahsDetail');
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          // scrollDirection: Axis.vertical,
          child: Column(
            children: [
              Padding(
                padding:
                    const EdgeInsets.only(top: 10.0, left: 10.0, right: 10.0),
                child: Container(
                  // height: 70,
                  width: MediaQuery.of(context).size.width,
                  // color: lightBlack,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          elevation: 7,
                          backgroundColor: primaryColor,
                          textStyle: const TextStyle(
                              color: Colors.white,
                              fontSize: 25,
                                   fontFamily: 'quran-font',
                              fontStyle: FontStyle.normal),
                          shape: const BeveledRectangleBorder(
                              borderRadius:
                                  BorderRadius.all(Radius.circular(10))),
                        ),
                        onPressed: () {
                          dialogueContainer(context);
                        },
                        child: const Text(
                          'Select Qari',
                          style: TextStyle(color: white,
                          fontSize: 20
                          ),
                        ),
                      ),
                      ElevatedButton(
                        style: playPauseBtnState == 'Play'
                            ? ElevatedButton.styleFrom(
                                elevation: 7,
                                backgroundColor: primaryColor,
                                // side: BorderSide(color: Colors.yellow, width: 5),
                                textStyle: const TextStyle(
                                    color: Colors.white,
                                         fontFamily: 'quran-font',
                                   fontSize: 20,
                                    fontStyle: FontStyle.normal),
                                shape: const BeveledRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                              )
                            : ElevatedButton.styleFrom(
                                elevation: 7,
                                backgroundColor: Colors.red[400],
                                // side: BorderSide(color: Colors.yellow, width: 5),
                                textStyle: const TextStyle(
                                    color: Colors.white,
                                    fontFamily: 'quran-font',
                                   fontSize: 20,
                                    fontStyle: FontStyle.normal),
                                shape: const BeveledRectangleBorder(
                                    borderRadius:
                                        BorderRadius.all(Radius.circular(10))),
                                side: const BorderSide(
                                    width: 0.4, color: lightBlack)),
                        onPressed: () {
                          if (qariDataDetail == null) {
                            _playPause(
                                'https://download.quranicaudio.com/quran/mishaari_raashid_al_3afaasee//${Get.arguments[1]}.mp3');
                          } else {
                            _playPause(
                                'https://download.quranicaudio.com/quran/${qariDataDetail!["relative_path"]}/${Get.arguments[1]}.mp3');
                          }
                        },
                        child: Text(
                          playPauseBtnState == 'Play'
                              ? 'Play'
                              : playPauseBtnState,
                          style: const TextStyle(
                              color: white,    fontFamily: 'quran-font',),
                        ),
                      ),
                      // ElevatedButton(
                      //   child: Text(
                      //     'Pause',
                      //     style: TextStyle(color: Colors.red.shade300),
                      //   ),
                      //   style: ElevatedButton.styleFrom(
                      //     primary: black,
                      //     // side: BorderSide(color: Colors.yellow, width: 5),
                      //     textStyle: const TextStyle(
                      //         color: red,
                      //         fontSize: 25,
                      //         fontStyle: FontStyle.normal),
                      //     shape: BeveledRectangleBorder(
                      //         borderRadius:
                      //             BorderRadius.all(Radius.circular(10))),
                      //   ),
                      //   onPressed: () {
                      //     print('this is index of surah: ${Get.arguments[1]}');
                      //     print('start');
                      //     Get.to(AudioPlayerWidget(
                      //         audioUrl:
                      //             'https://download.quranicaudio.com/quran/${qariDataDetail["relative_path"]}/${Get.arguments[1]}.mp3'));
                      //     // AudioPlayerWidget(
                      //     //   audioUrl:
                      //     //       ,
                      //     // );
                      //   },
                      // ),
                    ],
                  ),
                ),
              ),
              ListView.builder(
                primary: false,
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                scrollDirection: Axis.vertical,
                itemCount: surahsDetail.length,
                itemBuilder: (context, index) {
                  final surahsDataDetail = surahsDetail[index];
                  return Card(
                    elevation: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Container(
                        // height: 85,
                        width: MediaQuery.of(context).size.width,
                        decoration: BoxDecoration(
                            border: Border.all(color: Colors.black12)),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            surahsDataDetail["ayat_no"] == 0
                                ? Container(
                                    height: 80,
                                    decoration: const BoxDecoration(
                                      color: primaryColor,
                                      borderRadius: BorderRadius.only(
                                          topLeft: Radius.circular(8),
                                          topRight: Radius.circular(8)),
                                    ),
                                    child: Row(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      children: [
                                        Container(
                                            height: 60,
                                            width: 60,
                                            child: Column(
                                              children: [
                                                const Text('ركوعاتها',
                                                    style: TextStyle(
                                                        color: white,
                                                    fontFamily: 'quran-font',
                                                        fontSize: 18,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                                Text(
                                                    '${surahsDataDetail["total_ruku"].toString()}',
                                                    style: const TextStyle(
                                                        color: white,
                                                            fontFamily: 'quran-font',
                                                        fontSize: 16,
                                                        fontWeight:
                                                            FontWeight.bold)),
                                              ],
                                            )),
                                        Container(
                                            width: 0.8, color: Colors.white24),
                                        Container(
                                          child: Column(
                                            children: [
                                              Text(
                                                  '${surahsDataDetail["sura_name"].toString()}',
                                                  style: TextStyle(
                                                      color: white,
                                                          fontFamily: 'quran-font',
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              Text(
                                                  '${surahsDataDetail["arabic"].toString()}',
                                                  style: TextStyle(
                                                      color: white,
                                                        fontFamily: 'quran-font',
                                                      fontSize: 20,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        ),
                                        Container(
                                            width: 0.8, color: Colors.white24),
                                        Container(
                                          height: 60,
                                          width: 60,
                                          child: Column(
                                            children: [
                                              Text('اٰياتها',
                                                  style: TextStyle(
                                                      color: white,
                                                         fontFamily: 'quran-font',
                                                      fontSize: 18,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                              Text(
                                                  '${surahsDataDetail["total_aayaat"].toString()}',
                                                  style: TextStyle(
                                                      color: white,
                                                         fontFamily: 'quran-font',
                                                      fontSize: 16,
                                                      fontWeight:
                                                          FontWeight.bold)),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  )
                                : Container(),
                            surahsDataDetail["ayat_no"] != 0
                                ? Column(
                                    crossAxisAlignment: CrossAxisAlignment.end,
                                    mainAxisAlignment: MainAxisAlignment.end,
                                    children: [
                                      Text(
                                          surahsDataDetail["arabic"].toString(),
                                          textAlign: TextAlign.end,
                                          style: const TextStyle(
                                              color: primaryColor,
                                                  fontFamily: 'quran-font',
                                              fontSize: 20,
                                              fontWeight: FontWeight.bold)),
                                      const SizedBox(
                                        height: 5.0,
                                      ),
                                      surahsDataDetail["ayat_no"] != 0
                                          ? Text(
                                              surahsDataDetail["translation"]
                                                  .toString(),
                                              textAlign: TextAlign.end,
                                              style: const TextStyle(
                                                    fontFamily: 'quran-font',
                                                  fontSize: 15,
                                                  color: black))
                                          : Text('')
                                    ],
                                  )
                                : Container(),
                          ],
                        ),
                      ),
                    ),
                  );
                  // return
                  // Padding(
                  //   padding: const EdgeInsets.all(0.0),
                  //   child: Container(
                  //     height:112,
                  //     decoration: const BoxDecoration(
                  //         color: white,
                  //         shape: BoxShape.rectangle,
                  //         borderRadius: BorderRadius.all(Radius.circular(10.0)),
                  //         boxShadow: [BoxShadow(blurRadius: 1)]),
                  //     child:
                  // Column(
                  //       crossAxisAlignment: CrossAxisAlignment.end,
                  //       children: [
                  //         Stack(
                  //           children: [
                  //             surahsDataDetail["ayat_no"] == 0
                  // ? Container(
                  //     height: 80,
                  //     decoration: const BoxDecoration(
                  //       color: primaryColor,
                  //       borderRadius: BorderRadius.only(
                  //           topLeft: Radius.circular(8),
                  //           topRight: Radius.circular(8)),
                  //     ),
                  //                     padding: const EdgeInsets.all(18),
                  //                     width: double.infinity,
                  //                   )
                  //                 : Container(),
                  //             Positioned(
                  //                 top: 10,
                  //                 left: 10,
                  // child: Column(
                  //   children: [
                  //     Text('ركوعاتها',
                  //         style: TextStyle(
                  //             color: white,
                  //             fontFamily: 'al-qalam',
                  //             fontSize: 18,
                  //             fontWeight: FontWeight.bold)),
                  //     Text(
                  //         '${surahsDataDetail["total_aayaat"].toString()}',
                  //         style: TextStyle(
                  //             color: white,
                  //             fontFamily: 'al-qalam',
                  //             fontSize: 12,
                  //             fontWeight: FontWeight.bold)),
                  //   ],
                  // )),
                  //             surahsDataDetail["ayat_no"] == 0
                  //                 ? Positioned(
                  //                     top: 15,
                  //                     right: 0,
                  //                     left: 0,
                  //                     child: Center(
                  // child:
                  // Column(
                  //   children: [
                  //     Text(
                  //         '${surahsDataDetail["sura_name"].toString()}',
                  //         style: TextStyle(
                  //             color: white,
                  //             fontFamily: 'al-qalam',
                  //             fontSize: 20,
                  //             fontWeight: FontWeight.bold)),
                  //     Text(
                  //         '${surahsDataDetail["arabic"].toString()}',
                  //         style: TextStyle(
                  //             color: white,
                  //             fontFamily: 'al-qalam',
                  //             fontSize: 20,
                  //             fontWeight: FontWeight.bold)),
                  //   ],
                  // ),
                  //                     ))
                  //                 : Text(''),
                  //             Positioned(
                  //                 top: 10,
                  //                 right: 10,
                  // child: Column(
                  //   children: [
                  //     Text('اٰياتها',
                  //         style: TextStyle(
                  //             color: white,
                  //             fontFamily: 'al-qalam',
                  //             fontSize: 18,
                  //             fontWeight: FontWeight.bold)),
                  //     Text(
                  //         '${surahsDataDetail["total_aayaat"].toString()}',
                  //         style: TextStyle(
                  //             color: white,
                  //             fontFamily: 'al-qalam',
                  //             fontSize: 12,
                  //             fontWeight: FontWeight.bold)),
                  //   ],
                  // )),
                  //           ],
                  //         ),

                  //   surahsDataDetail["ayat_no"] != 0
                  //   ?
                  // Container(

                  //   width: double.infinity,
                  //   margin: const EdgeInsets.all(8.0),
                  //   padding: const EdgeInsets.all(8.0),
                  //   child: Column(
                  //     crossAxisAlignment: CrossAxisAlignment.end,
                  //     children: [
                  //       Text(surahsDataDetail["arabic"].toString(),
                  //           textAlign: TextAlign.end,
                  //           style: const TextStyle(
                  //               color: primaryColor,
                  //               fontFamily: 'al-qalam',
                  //               fontSize: 20,
                  //               fontWeight: FontWeight.bold)),
                  //       const SizedBox(
                  //         height: 5.0,
                  //       ),
                  //         surahsDataDetail["ayat_no"] != 0
                  //         ?
                  //       Text(surahsDataDetail["translation"].toString(),
                  //           textAlign: TextAlign.end,
                  //           style: const TextStyle(
                  //               fontFamily: 'Amiri',
                  //               fontSize: 15,
                  //               color: black))
                  //               :
                  //               Text('')
                  //     ],
                  //   ),
                  // )
                  // :
                  // Container()
                  //       ],
                  //     ),
                  //   ),
                  // );
                },
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<dynamic> dialogueContainer(BuildContext context) {
    return showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: const Center(child: Text("Select Qari")),
        content: Container(
          width: double.maxFinite,
          child: ListView.builder(
            itemCount: qariDetail.length,
            itemBuilder: (BuildContext context, int index) {
              qariDataDetail = qariDetail[index];
              return Padding(
                padding: const EdgeInsets.only(bottom: 8.0),
                child: InkWell(
                  child: Container(
                    height: 130,
                    decoration: BoxDecoration(
                      border: Border.all(color: lightBlack),
                      borderRadius: BorderRadius.circular(10.0),
                      boxShadow: const [
                        BoxShadow(
                          color: Colors.black12,
                          offset: Offset(0.0, 1.0), //(x,y)
                          blurRadius: 0.6,
                        ),
                      ],
                    ),
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 5.0, horizontal: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Expanded(
                            child: Text(
                              qariDataDetail!["arabic_name"] ?? '',
                              style: const TextStyle(
                                  color: primaryColor,
                                    fontFamily: 'quran-font',
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                              overflow: TextOverflow.fade,
                            ),
                          ),
                          Expanded(
                            child: Text(
                              qariDataDetail!["name"] ?? '',
                              style: const TextStyle(
                                color: black,
                                fontSize: 17,
                              fontFamily: 'quran-font',
                              ),
                              overflow: TextOverflow.fade,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  onTap: () {
                    // print(
                    //     'this is relative path of qari: ${qariDataDetail!["relative_path"]}');
                    setState(() {
                      qariRelativePath = qariDataDetail!["relative_path"];
                    });

                    Get.back();
                  },
                ),
              );
            },
          ),
        ),
        actions: <Widget>[
          ElevatedButton(
            onPressed: () {
              Get.back(result: 'Data from Screen B');
            },
            child: const Text(
              'Close',
            ),
          ),
        ],
      ),
    );
  }
}
