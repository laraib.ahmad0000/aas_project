import 'package:aas/Model/bookmark_modal.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/bookmark_provider.dart';
import 'package:aas/Provider/get_bookmark_provider.dart';
import 'package:aas/Provider/surah_number_provider.dart';
import 'package:aas/Provider/surah_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Quran%20Section/surah_detail_Screen.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SuraScreen extends StatefulWidget {
  const SuraScreen({super.key});

  @override
  State<SuraScreen> createState() => _SuraScreenState();
}

class _SuraScreenState extends State<SuraScreen> {
  List<bool> isLikedList = [];
  List<String> bookmarkList = [];

  late GetBookmarkProvider bookmarkProvider;
  late BannerAd _bannerAd;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();

    fetchSurahsData();
    loadLikedSurahs();

    // print('comming');
  }

  void fetchSurahsData() async {
    await Provider.of<SurahProvider>(context, listen: false).fetchData();

    final quranSurahProvider =
        Provider.of<SurahProvider>(context, listen: false);
    final surahs = quranSurahProvider.surah;

    print(surahs.length);
    setState(() {
      isLikedList = List<bool>.filled(surahs.length, false);
    });

    if (bookmarkProvider.getBookmark != null &&
        bookmarkProvider.getBookmark!.bookmarks.isNotEmpty) {
      for (int i = 0; i < bookmarkProvider.getBookmark!.bookmarks.length; i++) {
        final surahNo =
          bookmarkProvider.getBookmark!.bookmarks[i].surahNo;
        setState(() {
          isLikedList[surahNo - 1] = true;
        });
      }
    } else {
      print('no data');
    }

    // print('isliked list is: ${isLikedList[0]}');
  }

  void loadLikedSurahs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<int> likedIndices = prefs
            .getStringList('likedIndices')
            ?.map((e) => int.parse(e))
            ?.toList() ??
        [];

    setState(() {
      for (int i = 0; i < isLikedList.length; i++) {
        isLikedList[i] = likedIndices.contains(i);
      }
    });
  }

  void saveLikedSurahs() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<int> likedIndices = [];
    for (int i = 0; i < isLikedList.length; i++) {
      if (isLikedList[i]) {
        likedIndices.add(i);
      }
    }
    prefs.setStringList(
        'likedIndices', likedIndices.map((e) => e.toString()).toList());
  }

  @override
  Widget build(BuildContext context) {
    final quranSurahProvider =
        Provider.of<SurahProvider>(context, listen: false);
    final surahs = quranSurahProvider.surah;

    bookmarkProvider = Provider.of<GetBookmarkProvider>(context);

    return Scaffold(
      backgroundColor: white,
      body: Padding(
        padding: const EdgeInsets.only(
          top: 20.0,
        ),
        child: surahs.isEmpty
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Stack(children: [
                ListView.separated(
                  itemCount: surahs.length,
                  itemBuilder: (context, index) {
                    final surahsData = surahs[index];
                    return InkWell(
                      onTap: () {
                        var surahNumberProvider =
                            Provider.of<SurahNumberProvider>(context,
                                listen: false);
                        surahNumberProvider
                            .formatSurahNumbers(surahs[index].number);
                        setState(() {
                          isLikedList[index] = !isLikedList[index];
                          print(index);
                          saveLikedSurahs();
                        });
                        Get.to(const SurahDetailScreen(), arguments: [
                          surahs[index].number,
                          surahNumberProvider.formatNumber
                        ]);
                      },
                      child: ListTile(
                          leading: Stack(
                            children: [
                              const Image(
                                height: 40,
                                width: 45,
                                image: AssetImage(
                                    'assets/images/quran-screen-counting.png'),
                              ),
                              Positioned(
                                top: 13,
                                left: 0,
                                right: 0,
                                child: Center(
                                  child: Text(
                                    '${surahsData.number}',
                                    style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontFamily: 'Amiri'),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          title: Transform.translate(
                            offset: const Offset(-10, 0),
                            child: Text(
                              'Surah ${surahsData.number}',
                              style: const TextStyle(
                                  fontSize: 11, fontFamily: 'Amiri'),
                            ),
                          ),
                          subtitle: Transform.translate(
                            offset: const Offset(-10, 0),
                            child: Text(
                              surahsData.englishName,
                              style: const TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: primaryColor,
                                  fontSize: 13.5,
                                  fontFamily: 'Amiri'),
                            ),
                          ),
                          trailing: SizedBox(
                              width: 150,
                              child: Padding(
                                padding: const EdgeInsets.only(top: 18),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  children: [
                                    Text(
                                      surahsData.name,
                                      style: const TextStyle(
                                          fontWeight: FontWeight.bold,
                                          color: primaryColor,
                                          fontFamily: 'Amiri',
                                          fontSize: 16.5),
                                    ),
                                    const SizedBox(width: 3.0),
                                    InkWell(
                                      onTap: () {
                                        setState(() {
                                          isLikedList[index] =
                                              !isLikedList[index];
                                          print(
                                              "This is Index ${surahsData.number}");
                                        });

                                        bookmarkSurah(
                                            surahsData.name,
                                            surahsData.englishName,
                                            surahsData.number);
                                        saveLikedSurahs();
                                      },
                                      child: Container(
                                        height: 40,
                                        width: 40,
                                        child: Icon(
                                          CupertinoIcons.heart_fill,
                                          color: isLikedList.isNotEmpty &&
                                                  index < isLikedList.length
                                              ? isLikedList[index]
                                                  ? Colors.red
                                                  : Colors.grey
                                              : Colors.grey,
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ))),
                    );
                  },
                  separatorBuilder: (context, index) => Divider(),
                ),
                // Positioned(
                //   bottom: 2,
                //   child: Container(
                //     width: MediaQuery.of(context).size.width,
                //     height: 60,
                //     child: AdWidget(ad: _bannerAd),
                //   ),
                // )
              ]),
      ),
    );
  }

  void bookmarkSurah(String arabic_name, String english_name, int surah_no) {
    final bookmarkProvider =
        Provider.of<BookmarkProvider>(context, listen: false);
    final bookmarkSurah = BookmarkModal(
        arabic_name: arabic_name,
        english_name: english_name,
        surah_no: surah_no);
    bookmarkProvider.bookmarkSuratz(bookmarkSurah);
  }
}
