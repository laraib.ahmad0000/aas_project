import 'package:aas/Model/Other%20Users%20modals/other_followers_modal.dart';
import 'package:aas/Model/follow_unfollow_modal.dart';
import 'package:aas/constants/colors.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../../../Model/Other Users modals/other_user_follower_modal.dart';

import '../../../../Provider/Other Users Provider/other_user_follower_provider.dart';

class OtherFollowersScreen extends StatefulWidget {
  final String? id;

  const OtherFollowersScreen({super.key, required this.id});

  @override
  State<OtherFollowersScreen> createState() => _OtherFollowersScreenState();
}

class _OtherFollowersScreenState extends State<OtherFollowersScreen> {
  late String? followerId;

  @override
  void initState() {
    followerId = widget.id;
    super.initState();
    final Follow follow = Follow(user_id: followerId.toString());
    Provider.of<OtherFollowersProvider>(context, listen: false)
        .getOtherFollowersDetail(follow);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OtherFollowersProvider>(
      builder: (context, otherfollowerprovider, child) {
        OtherFollower? otherfollwData = otherfollowerprovider.getotherFollowers;

        return otherfollwData != null
            ? Padding(
                padding: const EdgeInsets.only(top: 20.0),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: const BouncingScrollPhysics(),
                  itemCount: otherfollwData.followers.length,
                  itemBuilder: (context, index) {
                    final allotherFollower = otherfollwData.followers[index];

                    print("THis is all other followers: ${allotherFollower}");
                    followerId = allotherFollower.id;
                    print("THis is following pass id: $followerId");
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 6.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                CircleAvatar(
                                  radius: 22,
                                  backgroundImage: NetworkImage(
                                    '${allotherFollower.profileImage}',
                                  ),
                                ),
                                SizedBox(
                                  width: 15.w,
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        allotherFollower.fname,
                                        style: const TextStyle(
                                            color: primaryColor,
                                            fontSize: 15,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.start,
                                      ),
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        allotherFollower.posts.toString(),
                                        style: const TextStyle(
                                            color: lightBlack,
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  height: 35.h,
                                  width: 90.w,
                                  decoration: BoxDecoration(
                                    color: primaryColor,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "Following",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: white,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 20.w,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              )
            : Center(
                child: Text(
                  "No Any Followers",
                  style: TextStyle(color: Colors.black, fontSize: 30),
                ),
              );
      },
    );
  }
}
