import 'package:aas/Constants/colors.dart';
import 'package:aas/Model/Other%20Users%20modals/other_following_modal.dart';
import 'package:aas/Model/follow_unfollow_modal.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

import '../../../../Model/Other Users modals/other_user_following_modal.dart';

import '../../../../Provider/Other Users Provider/other_user_following_provider.dart';

class OtherFollowingListScreen extends StatefulWidget {
  final String? id;

  const OtherFollowingListScreen({super.key, required this.id});

  @override
  State<OtherFollowingListScreen> createState() => _FollowingListScreenState();
}

class _FollowingListScreenState extends State<OtherFollowingListScreen> {
  late String? followingId;

  @override
  void initState() {
    followingId = widget.id;
    super.initState();
    final Follow follow = Follow(user_id: followingId.toString());

    Provider.of<OtherFollowingProvider>(context, listen: false)
        .getOtherFollowingDetail(follow);
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<OtherFollowingProvider>(
      builder: (context, otherfollwoingprovider, child) {
        OtherFollowingModal? otherfollowingData =
            otherfollwoingprovider.getotherFollowing;

        return otherfollowingData != null
            ? Padding(
                padding: EdgeInsets.symmetric(vertical: 30.h),
                child: ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: BouncingScrollPhysics(),
                  itemCount: otherfollowingData.following.length,
                  itemBuilder: (context, index) {
                    final otherollowingList =
                        otherfollowingData.following[index];

                    followingId = otherollowingList.id;
                    return Padding(
                      padding: const EdgeInsets.only(bottom: 8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: Row(
                              children: [
                                CircleAvatar(
                                  backgroundImage: NetworkImage(
                                      otherollowingList.profileImage),
                                ),
                                SizedBox(
                                  width: 15.w,
                                ),
                                Align(
                                  alignment: Alignment.topLeft,
                                  child: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Text(
                                        otherollowingList.fname,
                                        style: TextStyle(
                                            color: primaryColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.start,
                                      ),
                                      SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        otherollowingList.posts.toString(),
                                        style: const TextStyle(
                                            color: lightBlack,
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  height: 35.h,
                                  width: 90.w,
                                  decoration: BoxDecoration(
                                    color: primaryColor,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "Following",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: white,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 20.w,
                                ),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                ),
              )
            : Center(
                child: Text(
                  "No Any Following",
                  style: TextStyle(color: Colors.black, fontSize: 30),
                ),
              );
      },
    );
  }
}
