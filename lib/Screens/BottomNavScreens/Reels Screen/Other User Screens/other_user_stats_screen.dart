import 'package:aas/Provider/user_user_stats_proivder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';
import 'package:video_player/video_player.dart';

import '../../../../Constants/colors.dart';
import '../../../../Model/follow_unfollow_modal.dart';
import '../../../../Provider/Other Users Provider/other_user_stats_provider.dart';
import '../../../../Provider/ad_provider.dart';

import 'other_followers_screen.dart';
import 'other_following_screen.dart';

class OtherUserStatesScreen extends StatefulWidget {
  final int? id;
 final VideoPlayerController? videoPlayerController;

  const OtherUserStatesScreen({Key? key, required this.id, this.videoPlayerController}) : super(key: key);

  @override
  State<OtherUserStatesScreen> createState() => _OtherUserStatesScreenState();
}

class _OtherUserStatesScreenState extends State<OtherUserStatesScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  late BannerAd _bannerAd;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this);
    widget.videoPlayerController?.pause();
    _bannerAd = context.read<AdProvider>().createBannerAd();

    final Follow follow = Follow(user_id: widget.id.toString());
    Provider.of<StatsProvider>(context, listen: false).fetchUserStates(follow);
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 27.0),
              child: Container(
                height: 27,
                width: 27,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    // color: lightBlack,
                    border: Border.all(color: black, width: 2)),
                child: Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Icon(Icons.arrow_back_ios, color: black, size: 18),
                  ),
                ),
              ),
            )),
        actions: [
          Padding(
            padding: const EdgeInsets.only(right: 10.0),
            child: Icon(
              Icons.share,
              color: black,
            ),
          ),
          SizedBox(
            width: 5,
          ),
          // Icon(
          //   Icons.more_vert_outlined,
          //   color: black,
          // ),
          // SizedBox(
          //   width: 15,
          // ),
        ],
      ),
      body: Consumer<StatsProvider>(
        builder: (context, statsProvider, child) {
          final user = statsProvider.userStates;

          if (user == null) {
            return Center(child: CircularProgressIndicator());
          }

          return Padding(
            padding: EdgeInsets.symmetric(horizontal: 15.h),
            child: Column(
              children: [
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Column(
                      children: [
                        CircleAvatar(
                            backgroundColor: Colors.transparent,
                            radius: 35,
                            backgroundImage: NetworkImage(user.profileImage)),
                        SizedBox(
                          height: 10.0,
                        ),
                        SizedBox(
                          width: 100,
                          child: Text(
                            user.name,
                            style: TextStyle(
                                letterSpacing: 1.2,
                                fontWeight: FontWeight.bold,
                                fontSize: 16,
                                color: primaryColor),
                            textAlign: TextAlign.center,
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          user.followers.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        Text(
                          "Followers",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          user.following.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        Text(
                          "Following",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                          ),
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Text(
                          user.posts.toString(),
                          style: TextStyle(
                            fontWeight: FontWeight.bold,
                            fontSize: 18,
                          ),
                        ),
                        Text(
                          "Post",
                          style: TextStyle(
                            fontWeight: FontWeight.w600,
                            fontSize: 14,
                          ),
                        )
                      ],
                    ),
                  ],
                ),
                Expanded(
                  child: Container(
                    width: double.infinity,
                    decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.only(
                            topLeft: Radius.circular(15),
                            topRight: Radius.circular(15))),
                    child: Column(
                      children: [
                        
   SizedBox(height: 20.0,),
                        // 20.heightBox,
                        Container(
                          height: screenHeight * .053,
                          width: screenWidth * .8,
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                          ),
                          child: Container(
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(10),
                              border: Border.all(
                                color: Color(
                                    0xFF111111), // Change to black border color
                              ),
                            ),
                            child: TabBar(
                              indicatorSize: TabBarIndicatorSize.tab,
                              labelColor: white,
                              indicator: BoxDecoration(
                                color: Color.fromRGBO(
                                    20, 77, 70, 1.0), // Active tab color
                                borderRadius: BorderRadius.circular(10),
                              ),
                              unselectedLabelColor: Color(0xFFABA9A9),
                              controller: _tabController,
                              tabs: [
                                Tab(text: 'Followers'),
                                Tab(text: 'Following'),
                              ],
                            ),
                          ),
                        ),
                        Expanded(
                          child: TabBarView(
                            controller: _tabController,
                            children: [
                              OtherFollowersScreen(id: widget.id.toString()),
                              OtherFollowingListScreen(
                                id: widget.id.toString(),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Container(
                  width: MediaQuery.of(context).size.width,
                  height: 60,
                  child: AdWidget(ad: _bannerAd),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
