import 'package:aas/Constants/colors.dart';
import 'package:aas/Provider/following_video_provider.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../Model/user_data_modal.dart';
import '../../../Provider/comments_provider.dart';
import '../../../Provider/reels_video_provider.dart';
import '../../../Provider/user_data_provider.dart';

class CommentBottomSheet extends StatefulWidget {
  final String videoId;
  final int videoIndex;

  CommentBottomSheet({required this.videoId, required this.videoIndex});

  @override
  _CommentBottomSheetState createState() => _CommentBottomSheetState();
}

class _CommentBottomSheetState extends State<CommentBottomSheet> {
  TextEditingController messageController = TextEditingController();
  String? selectedCommentMessage;
  int? selectedCommentIndex;
  bool isLiked = false;

  @override
  void initState() {
    super.initState();
    Provider.of<CommentProvider>(context, listen: false).fetchComments(widget.videoId);
  }

  replyToComment(String message, int index) {
    setState(() {
      selectedCommentMessage = message;
      selectedCommentIndex = index;
      messageController.text = ""; // Clear text field for new reply
    });
  }

  @override
  Widget build(BuildContext context) {
    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    print("This is UserData: ${userData?.name}");

    return Padding(
      padding: EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
      child: Consumer<CommentProvider>(
        builder: (context, commentProvider, child) {
          return Column(
            children: [
              SizedBox(height: 10),
              Expanded(
                child: commentProvider.comments.isEmpty
                    ? Center(child: Text("No Comments"))
                    : ListView.separated(
                  itemCount: commentProvider.comments.length,
                  itemBuilder: (context, index) {
                    final comment = commentProvider.comments[index];
                    final replies = comment['replies'] as List<dynamic>;
                    print("This is COmments ID: ${comment['id']}");
                    print("This Is Equal COmment fname: ${comment['fname']}");

                    return (userData?.name == comment['fname'] ? ListTile(

                      trailing:   InkWell(
                          onTap: (){

                            Provider.of<CommentProvider>(context, listen: false)
                                .deleteComment(comment['id'], widget.videoId);
                          },
                          child: Icon(Icons.delete, color: Colors.black,)),

                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(

                            children: [

                              ClipRRect(
                                borderRadius: BorderRadius.circular(15),
                                child: Image.network(
                                  comment['profileimage'],
                                  fit: BoxFit.cover,
                                  width: 30,
                                  height: 30,
                                ),
                              ),
                              SizedBox(width: 20,),
                              Text('${comment['fname']}\n${comment['comment']}'),



                            ],
                          ),
                          SizedBox(height: 5,),
                          ...replies.map((reply) {
                            return Container(
                              width: double.infinity, // Ensure it takes the full width
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  Expanded(
                                    child: ListTile(
                                      leading: ClipRRect(
                                        borderRadius: BorderRadius.circular(15),
                                        child: Image.network(
                                          reply['reply_profileimage'],
                                          fit: BoxFit.cover,
                                          width: 30,
                                          height: 30,
                                        ),
                                      ),
                                      title: Text(
                                        '${reply['reply_fname']}',
                                        style: TextStyle(
                                          color: primaryColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                      subtitle: Text('${reply['reply']}'),
                                      trailing: InkWell(
                                        onTap: () {
                                          setState(() {
                                            if (reply['isLiked'] == 1) {
                                              reply['isLiked'] = 0; // Toggle to not liked
                                              reply['likes'] -= 1; // Update likes count (if needed)
                                            } else {
                                              reply['isLiked'] = 1; // Toggle to liked
                                              reply['likes'] += 1; // Update likes count (if needed)
                                            }
                                          });
                                        },
                                        child: Column(
                                          children: [
                                            reply['isLiked'] == 1
                                                ? Icon(Icons.thumb_up, color: primaryColor)
                                                : Icon(Icons.thumb_up_alt_outlined, color: Colors.black),
                                            SizedBox(height: 10,),
                                            Text(reply['likes'].toString()),
                                          ],
                                        ),
                                      ),

                                    ),
                                  )
                                ],
                              ),
                            );
                          }).toList(),
                        ],
                      ),

                    ) : ListTile(

                      trailing: InkWell(
                        onTap: () {
                          setState(() {
                            if (comment['isLiked'] == 1) {
                              comment['isLiked'] = 0; // Toggle to not liked
                              comment['likes'] -= 1; // Update likes count (if needed)
                            } else {
                              comment['isLiked'] = 1; // Toggle to liked
                              comment['likes'] += 1; // Update likes count (if needed)
                            }
                          });
                        },
                        child: Column(
                          children: [
                            comment['isLiked'] == 1
                                ? Icon(Icons.thumb_up, color: primaryColor)
                                : Icon(Icons.thumb_up_alt_outlined, color: Colors.black),
                            SizedBox(height: 10,),
                            Text(comment['likes'].toString()),
                          ],
                        ),
                      ),


                      


                      subtitle: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Row(

                            children: [

                              ClipRRect(
                                borderRadius: BorderRadius.circular(15),
                                child: Image.network(
                                  comment['profileimage'],
                                  fit: BoxFit.cover,
                                  width: 30,
                                  height: 30,
                                ),
                              ),
                              SizedBox(width: 20,),
                              Text('${comment['fname']}\n${comment['comment']}'),

                            ],
                          ),
                          TextButton(onPressed: (){
                            replyToComment(comment['comment'], index);

                          }, child: Text("Reply",style: TextStyle(color: primaryColor,fontWeight: FontWeight.bold,),), ),


                          ...replies.map((reply) {
                            return Container(
                              width: double.infinity, // Ensure it takes the full width
                              margin: EdgeInsets.only(bottom: 20),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [

                                  Expanded(
                                    child: ListTile(
                                      leading: ClipRRect(
                                        borderRadius: BorderRadius.circular(15),
                                        child: Image.network(
                                          reply['reply_profileimage'],
                                          fit: BoxFit.cover,
                                          width: 30,
                                          height: 30,
                                        ),
                                      ),
                                      title: Text(
                                        '${reply['reply_fname']}',
                                        style: TextStyle(
                                          color: primaryColor,
                                          fontSize: 14,
                                        ),
                                      ),
                                      subtitle: Text('${reply['reply']}'),
                                    ),
                                  )
                                ],
                              ),
                            );
                          }).toList(),
                        ],
                      ),
                    )

                    );
                  },
                  separatorBuilder: (context, index) {
                    return Divider(color: Colors.black, thickness: 1);
                  },
                ),
              ),
              Container(
                padding: EdgeInsets.all(8),
                color: Colors.white,
                child: Row(
                  children: [
                    Expanded(
                      child: TextFormField(
                        autofocus: true,
                        controller: messageController,
                        decoration: InputDecoration(
                          labelText: selectedCommentMessage == null
                              ? null
                              : 'Replying to: $selectedCommentMessage',
                          hintText: "Write message...",
                        ),
                      ),
                    ),
                    SizedBox(width: 8),
                    FloatingActionButton(
                      onPressed: () async {
                        if ( selectedCommentMessage != null && selectedCommentIndex != null) {

                          print("THis is Selected COmment ID: ${commentProvider.comments[selectedCommentIndex!]['id']}");
                          Provider.of<CommentProvider>(context, listen: false)
                              .addReply(commentProvider.comments[selectedCommentIndex!]['id'], widget.videoId, messageController.text);



                          setState(() {
                            messageController.clear();
                            selectedCommentMessage = null; // Clear the selected comment message after sending the reply
                            selectedCommentIndex = null; // Clear the selected comment index
                          });
                        }else{
                          await commentProvider.addComment(widget.videoId, messageController.text);


                          setState(() {

                            messageController.clear();

                          });
                        }
                        Provider.of<ReelsVideoProvider>(context, listen: false)
                            .incrementComments(widget.videoIndex);

                        Provider.of<FollowingScreenProvider>(context, listen: false)
                            .incrementComments(widget.videoIndex);
                      } ,
                      backgroundColor: Colors.blue,
                      elevation: 0,
                      child: Icon(Icons.send, color: Colors.white),
                    ),
                  ],
                ),
              ),
            ],
          );
        },
      ),
    );
  }





}
