import 'dart:async';
import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/options_screen.dart';
import 'package:flutter/material.dart';
import 'package:video_player/video_player.dart';

class FeedItem extends StatefulWidget {
  final String url;
  final String profileImage;
  final String userName;
  final int videoFollowed;
  final int videoIndex;
  final String description;
  final String shares;
  final Map<String, dynamic> videoItem;
  final int videoId;
  final int videoLikes;
  final int isLiked;
  final int videoIdForLike;
  final int view;

  const FeedItem(
      {Key? key,
        required this.url,
        required this.videoId,
        required this.shares,
        required this.userName,
        required this.profileImage,
        required this.videoLikes,
        required this.isLiked,
        required this.videoIdForLike,
        required this.description,
        required this.videoItem,
        required this.videoFollowed,
        required this.videoIndex, required this.view})
      : super(key: key);

  @override
  State<FeedItem> createState() => _FeedItemState();
}

class _FeedItemState extends State<FeedItem> {
  bool _isPlaying = true;
  bool _showIcon = false;
  Timer? _iconTimer;
  VideoPlayerController? _controller;
  late ValueNotifier<bool> isVideoPlaying;

  @override
  void initState() {
    super.initState();
    _controller != null
        ? isVideoPlaying = ValueNotifier<bool>(true)
        : Container();

    initializePlayer(widget.url);
  }

  void _togglePlayPause() {
    setState(() {
      if (_controller!.value.isPlaying) {
        _controller!.pause();
        _isPlaying = false;
      } else {
        _controller!.play();
        _isPlaying = true;
      }
      _showIcon = true;
    });

    _iconTimer?.cancel();
    _iconTimer = Timer(Duration(seconds: 1), () {
      setState(() {
        _showIcon = false;
      });
    });
  }

  void initializePlayer(String url) async {
    _controller = VideoPlayerController.network(url);
    _controller!.initialize().then((value) {
      setState(() {
        _controller?.setLooping(true);
        _controller!.play();
      });
    });
  }

  @override
  void dispose() {
    if (_controller != null) {
      _controller!.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return (_controller == null)
        ? Container(
      color: Colors.black,
      child: const Center(
          child: Text(
            'Please Wait..',
            style: TextStyle(color: Colors.white),
          )),
    )
        : ((_controller!.value.isInitialized)
        ? GestureDetector(
      onTap: _togglePlayPause,
      child: Stack(
        fit: StackFit.expand,
        children: [
          Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Flexible(
                child: AspectRatio(
                    aspectRatio: _controller!.value.aspectRatio,
                    child: VideoPlayer(_controller!)),
              ),
            ],
          ),
          AnimatedOpacity(
            opacity: _showIcon ? 1.0 : 0.0,
            duration: Duration(milliseconds: 300),
            child: Center(
              child: Icon(
                _isPlaying ? Icons.pause : Icons.play_arrow,
                color: Colors.white,
                size: 100,
              ),
            ),
          ),
          OptionsScreen(
            videoIdForLike: widget.videoIdForLike,
            isLiked: widget.isLiked,
            videoList: widget.videoItem,
            videoLikes: widget.videoLikes,
            description: widget.description,
            profileImage: widget.profileImage,
            userName: widget.userName,
            videoId: widget.videoId,
            videoUrl: widget.url,
            shares: widget.shares,
            videoController: _controller,
            videoFollowed: widget.videoFollowed,
            videoIndex: widget.videoIndex,
            view: widget.view
          ),

        ],
      ),
    )
        : Container(
        color: Colors.black,
        child: const Center(
            child: CircularProgressIndicator(
              color: Colors.blue,
            ))));
  }
}
