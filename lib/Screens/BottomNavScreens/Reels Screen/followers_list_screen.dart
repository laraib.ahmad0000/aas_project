import 'package:aas/Model/followers_modal.dart';
import 'package:aas/Model/user_block_modal.dart';
import 'package:aas/Provider/followers_provider.dart';
import 'package:aas/Provider/user_block_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/Other%20User%20Screens/other_user_stats_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/report_screen.dart';
import 'package:aas/constants/colors.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class FollowersListScreen extends StatefulWidget {
  const FollowersListScreen({super.key});

  @override
  State<FollowersListScreen> createState() => _FollowersListScreenState();
}

class _FollowersListScreenState extends State<FollowersListScreen> {
  String? id;
  List<Map<String, dynamic>> options = [
    {"menu": "Block"},
    {"menu": "Report", "selected": false},
  ];

  @override
  void initState() {
    super.initState();
    Provider.of<FollowersProvider>(context, listen: false).getFollowersDetail();
  }

  void handleOptionSelected(String option) {
    final blockProvider =
        Provider.of<UserBlockProvider>(context, listen: false);
    if (option == "Report") {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ReportScreen(id: id),
        ),
      );
    }
    if (option == "Block") {
      final blockModel = UserBlockModal(user_id: id.toString());

      blockProvider.blocking(blockModel);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FollowersProvider>(
      builder: (context, followersProvider, child) {
        Follower? followersData = followersProvider.getFollowers;

        if (followersData == null) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'No Any Followers.',
                style: TextStyle(
                    fontSize: 22, fontWeight: FontWeight.bold, color: black),
              )
            ],
          );
        }
        return Padding(
          padding: const EdgeInsets.only(top: 20.0),
          child: followersData.followers.isNotEmpty
              ? ListView.builder(
                  shrinkWrap: true,
                  scrollDirection: Axis.vertical,
                  physics: const BouncingScrollPhysics(),
                  itemCount: followersData.followers.length,
                  itemBuilder: (context, index) {
                    final allFollower = followersData.followers[index];

                    // id = allFollower.id[index];
                    // print("THis is following pass id: $id");
                    return Padding(
                      padding: const EdgeInsets.symmetric(
                          vertical: 10.0, horizontal: 6.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            child: InkWell(
                              onTap: () {
                                // in this there is an issue about id which we send on next screen
                                // print('this is id from on press: $id');
                                // Navigator.push(context, MaterialPageRoute(builder: (context)=> OtherUserStatesScreen(id: 4)));
                              },
                              child: Row(
                                children: [
                                  CircleAvatar(
                                    radius: 22,
                                    backgroundImage: NetworkImage(
                                      '${allFollower.profileImage}',
                                    ),
                                  ),
                                  SizedBox(
                                    width: 15.w,
                                  ),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Container(
                                       width:120, 
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
Text(
                                        allFollower.fname,
                                        style: TextStyle(
                                            overflow: TextOverflow.fade,
                                            color: primaryColor,
                                            fontSize: 12,
                                            fontWeight: FontWeight.bold),
                                        textAlign: TextAlign.start,
                                        maxLines: 2,
                                      ),
                                        ],),
                                      ),
                                      // 
                                      const SizedBox(
                                        height: 5,
                                      ),
                                      Text(
                                        allFollower.posts.toString(),
                                        style: const TextStyle(
                                            color: lightBlack,
                                            fontSize: 11,
                                            fontWeight: FontWeight.bold),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          ),
                          Container(
                            child: Row(
                              children: [
                                Container(
                                  height: 35.h,
                                  width: 90.w,
                                  decoration: BoxDecoration(
                                    color: primaryColor,
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: const Center(
                                    child: Text(
                                      "Following",
                                      style: TextStyle(
                                        fontSize: 14,
                                        fontWeight: FontWeight.bold,
                                        color: white,
                                      ),
                                    ),
                                  ),
                                ),
                                SizedBox(
                                  width: 20.w,
                                ),
                                FollowDialoge.followdialoge(options, context),
                              ],
                            ),
                          ),
                        ],
                      ),
                    );
                  },
                )
              : Center(
                  child: Text(
                    'No any Followers',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                  ),
                ),
        );
      },
    );
  }
}

class FollowDialoge {
  static followdialoge(
      List<Map<String, dynamic>> options, BuildContext context) {
    return PopupMenuButton(
      iconSize: 24.0,
      padding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      icon: Icon(
        Icons.more_horiz_rounded,
        color: Colors.black,
        size: 24.0,
      ),
      offset: Offset(0, 15),
      itemBuilder: (BuildContext context) {
        return options
            .map(
              (selectedOption) => PopupMenuItem(
                height: 30.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      selectedOption['menu'] ?? "",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14.0),
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        color: Colors.black,
                      ),
                    ),
                    options.length == options.indexOf(selectedOption) + 1
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 8.0,
                            ),
                            child: Divider(
                              color: Colors.grey,
                              height: ScreenUtil().setHeight(1.0),
                            ),
                          ),
                  ],
                ),
                value: selectedOption,
              ),
            )
            .toList();
      },
      onSelected: (value) {
        String selectedOption = value['menu'] ?? "";
        _FollowersListScreenState? followersDetailScreenState =
            context.findAncestorStateOfType<_FollowersListScreenState>();
        followersDetailScreenState!.handleOptionSelected(selectedOption);
      },
    );
  }
}
