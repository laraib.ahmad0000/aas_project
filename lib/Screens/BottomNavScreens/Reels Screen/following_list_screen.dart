import 'package:aas/Constants/colors.dart';
import 'package:aas/Model/following_modal.dart';
import 'package:aas/Model/user_block_modal.dart';
import 'package:aas/Provider/following_block_provider.dart';
import 'package:aas/Provider/following_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/report_screen.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:provider/provider.dart';

class FollowingListScreen extends StatefulWidget {
  const FollowingListScreen({super.key});

  @override
  State<FollowingListScreen> createState() => _FollowingListScreenState();
}

class _FollowingListScreenState extends State<FollowingListScreen> {
  String? id;

  List<Map<String, dynamic>> options2 = [
    {"menu": "Block"},
    {"menu": "Report", "selected": false},
  ];

  void handleOptionSelected2(String option2) {
    final followingprovider =
        Provider.of<FollowingBlockProvider>(context, listen: false);
    if (option2 == "Report") {
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ReportScreen(id: id),
        ),
      );
    }
    if (option2 == "Block") {
      final blockModel = UserBlockModal(user_id: id.toString());

      followingprovider.folowingblocking(blockModel);
    }
  }

  @override
  void initState() {
    super.initState();
    Provider.of<FollowingProvider>(context, listen: false).getFollowingList();
  }

  @override
  Widget build(BuildContext context) {
    return Consumer<FollowingProvider>(
      builder: (context, followingProvider, child) {
        FollowingModal? followingData = followingProvider.getFollowing;
        if (followingData == null) {
          return Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Text(
                'No Any Following.',
                style: TextStyle(
                    fontSize: 22, fontWeight: FontWeight.bold, color: black),
              )
            ],
          );
        }
        return Padding(
            padding: EdgeInsets.symmetric(vertical: 20.h),
            child: followingData.following.isNotEmpty
                ? ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.vertical,
                    physics: BouncingScrollPhysics(),
                    itemCount: followingData.following.length,
                    itemBuilder: (context, index) {
                      final followingList = followingData.following[index];

                      // id = followingList.id;
                      return Padding(
                        padding: const EdgeInsets.only(bottom: 8.0),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              child: Row(
                                children: [
                                  CircleAvatar(
                                    backgroundImage: NetworkImage(
                                        followingList.profileImage),
                                  ),
                                  SizedBox(
                                    width: 15.w,
                                  ),
                                  Align(
                                    alignment: Alignment.topLeft,
                                    child: Column(
                                      crossAxisAlignment:
                                          CrossAxisAlignment.start,
                                      children: [
                                        Container( width:120, 
                                          child: Column( crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Text(
                                                followingList.fname,
                                                style: TextStyle( overflow: TextOverflow.fade,
                                                    color: primaryColor,
                                                    fontSize: 12,
                                                    fontWeight: FontWeight.bold),
                                                textAlign: TextAlign.start,  maxLines: 2,
                                              ),
                                            ],
                                          ),
                                        ),
                                        SizedBox(
                                          height: 5,
                                        ),
                                        Text(
                                          followingList.posts.toString(),
                                          style: const TextStyle(
                                              color: lightBlack,
                                              fontSize: 11,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ],
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              child: Row(
                                children: [
                                  Container(
                                    height: 35.h,
                                    width: 90.w,
                                    decoration: BoxDecoration(
                                      color: primaryColor,
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: const Center(
                                      child: Text(
                                        "Following",
                                        style: TextStyle(
                                          fontSize: 14,
                                          fontWeight: FontWeight.bold,
                                          color: white,
                                        ),
                                      ),
                                    ),
                                  ),
                                  SizedBox(
                                    width: 20.w,
                                  ),
                                  FollowDialoge2.followdialoge(
                                      options2, context),
                                ],
                              ),
                            ),
                          ],
                        ),
                      );
                    },
                  )
                : Center(
                    child: Text(
                      'No any Followings',
                      style:
                          TextStyle(fontSize: 22, fontWeight: FontWeight.bold),
                    ),
                  ));
      },
    );
  }
}

class FollowDialoge2 {
  static followdialoge(
      List<Map<String, dynamic>> options2, BuildContext context) {
    return PopupMenuButton(
      iconSize: 24.0,
      padding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      icon: Icon(
        Icons.more_horiz_rounded,
        color: Colors.black,
        size: 24.0,
      ),
      offset: Offset(0, 15),
      itemBuilder: (BuildContext context) {
        return options2
            .map(
              (selectedOption2) => PopupMenuItem(
                height: 30.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      selectedOption2['menu'] ?? "",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14.0),
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        color: Colors.black,
                      ),
                    ),
                    options2.length == options2.indexOf(selectedOption2) + 1
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 8.0,
                            ),
                            child: Divider(
                              color: Colors.grey,
                              height: ScreenUtil().setHeight(1.0),
                            ),
                          ),
                  ],
                ),
                value: selectedOption2,
              ),
            )
            .toList();
      },
      onSelected: (value) {
        String selectedOption2 = value['menu'] ?? "";
        _FollowingListScreenState? followingDetailScreenState =
            context.findAncestorStateOfType<_FollowingListScreenState>();
        followingDetailScreenState!.handleOptionSelected2(selectedOption2);
      },
    );
  }
}
