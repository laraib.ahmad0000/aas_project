import 'dart:convert';

import 'package:aas/Provider/following_video_provider.dart';
import 'package:flutter/material.dart';
import 'package:dio/dio.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../../Model/viewModel.dart';
import '../../../Provider/reels_video_provider.dart';
import '../../../Provider/viewProvider.dart';
import 'feed_item.dart';

class FollowingScreen extends StatefulWidget {
  const FollowingScreen({Key? key});

  @override
  State<FollowingScreen> createState() => _FollowingScreenState();
}

class _FollowingScreenState extends State<FollowingScreen> {

  @override
  void initState() {
    super.initState();
    fetchVideosAndViewFirst();
  }

  void fetchVideosAndViewFirst() {
    final videoProvider = Provider.of<FollowingScreenProvider>(context, listen: false);
    videoProvider.fetchVideos().then((_) {
      final viewProvider = Provider.of<ViewProvider>(context, listen: false);
      if (videoProvider.videoUrls != null && videoProvider.videoUrls!.isNotEmpty) {
        viewProvider.viewVideo(
          ViewModel(id: videoProvider.videoList[0]['id']),
        );
        setState(() {
          videoProvider.videoList[0]['views'] += 1;
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      body: Consumer2<FollowingScreenProvider, ViewProvider>(
        builder: (context, videoProvider, viewProvider, _) {
          if (videoProvider.videoUrls != null) {
            return PageView.builder(
              scrollDirection: Axis.vertical,
              itemCount: videoProvider.videoUrls!.isEmpty ? 0 : null,
              onPageChanged: (index) {
                viewProvider.viewVideo(
                  ViewModel(id: videoProvider.videoList[index]['id']),
                );
                setState(() {
                  videoProvider.currentIndex = index;
                  // Increment the view count locally
                  videoProvider.videoList[index]['views'] += 1;
                });
              },
              itemBuilder: (ctx, index) {
                final videoIndex = index % videoProvider.videoUrls!.length;

                return FeedItem(
                  url: videoProvider.videoUrls![videoIndex],
                  videoId: videoProvider.videoList[videoIndex]['created_by'],
                  shares: videoProvider.videoList[videoIndex]['shares'],
                  userName: videoProvider.videoList[videoIndex]['fname'],
                  profileImage: videoProvider.videoList[videoIndex]['profileimage'],
                  videoLikes: videoProvider.videoList[videoIndex]['likes'],
                  isLiked: videoProvider.videoList[videoIndex]['isLiked'],
                  description: videoProvider.videoList[videoIndex]['description'] ?? '',
                  videoIdForLike: videoProvider.videoList[videoIndex]['id'],
                  videoItem: videoProvider.videoList[videoIndex],
                  videoFollowed: videoProvider.videoList[videoIndex]['isFollowed'],
                  videoIndex: videoIndex,
                  view: videoProvider.videoList[videoIndex]['views'],
                );
              },
            );
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        },
      ),
    );
  }
}