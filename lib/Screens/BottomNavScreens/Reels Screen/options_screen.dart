// ignore_for_file: must_be_immutable
import 'dart:convert';
import 'dart:io';

import 'package:aas/Model/follow_unfollow_modal.dart';
import 'package:aas/Model/likeUnlike_modal.dart';
import 'package:aas/Model/share_video_modal.dart';
import 'package:aas/Provider/follow_unfollow_provider.dart';
import 'package:aas/Provider/likeUnlike_user_provider.dart';
import 'package:aas/Provider/share_video_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/Other%20User%20Screens/other_user_stats_screen.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:path_provider/path_provider.dart';

import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:dio/dio.dart' as dio;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:video_player/video_player.dart';
import 'package:video_thumbnail/video_thumbnail.dart';

import '../../../Constants/colors.dart';
import '../../../Provider/comments_provider.dart';
import '../../../Provider/following_video_provider.dart';
import '../../../Provider/reels_video_provider.dart';
import 'comments_bottom_sheet.dart';

class OptionsScreen extends StatefulWidget {
  final int? videoId;
  int? videoLikes;
  final int videoIndex;
  final String? videoUrl;
  final String? description;
  final String? profileImage;
  final Map<String, dynamic>? videoList;
  final String? userName;
  final String? shares;
  final int? videoFollowed;
  int? isLiked;
  final VideoPlayerController? videoController;
  final int? videoIdForLike;
  final int view;

  bool _isLoading = false;

  OptionsScreen(
      {super.key,
      this.videoId,
      this.description,
      this.videoUrl,
      this.videoLikes,
      this.shares,
      this.videoIdForLike,
      this.profileImage,
      this.userName,
      this.isLiked,
      this.videoController,
      this.videoList,
      this.videoFollowed,

      required this.videoIndex, required this.view});

  @override
  State<OptionsScreen> createState() => _OptionsScreenState();
}

class _OptionsScreenState extends State<OptionsScreen>
    with SingleTickerProviderStateMixin {
  late AnimationController _animationController;
  List<dynamic> comments = [];

  @override
  void initState() {
    super.initState();
    isLiked =
        (widget.isLiked == 1); // Initialize isLiked based on widget property
    print("This is VIdeo ID: ${widget.videoId.toString()}");
    Provider.of<CommentProvider>(context, listen: false)
        .fetchComments(widget.videoId.toString());

    _animationController = AnimationController(
        vsync: this, duration: const Duration(milliseconds: 450));
  }

  @override
  void didUpdateWidget(covariant OptionsScreen oldWidget) {
    super.didUpdateWidget(oldWidget);
    // Handle updates to the widget properties here
    if (widget.isLiked != null) {
      setState(() {
        isLiked = (widget.isLiked == 1);
      });
    }
  }

  // int? isLikedOrNot;

  bool isLiked = false;

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    _animationController.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final screenWidth = MediaQuery.of(context).size.width;
    final screenHeight = MediaQuery.of(context).size.height;

    final double followButtonWidth = screenWidth * 0.16;
    final double followButtonFontSize = screenWidth * 0.035;
    final bool isFollowed =
        Provider.of<UserFollow>(context).isUserFollowed(widget.videoId!);

    return Builder(builder: (context) {
      return SingleChildScrollView(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Padding(
              padding: EdgeInsets.only(top: screenHeight * .21),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.end,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  Padding(
                    padding: EdgeInsets.only(
                        right: screenWidth * 0.01, top: screenHeight * 0.12),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.end,
                      children: [
                        Container(
                          margin: EdgeInsets.only(left: screenWidth * 0.06),
                          child: Column(
                            children: [
                              InkWell(onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            OtherUserStatesScreen(
                                              id: widget.videoId,
                                              videoPlayerController:
                                                  widget.videoController,
                                            )));
                              }, child: Consumer<UserFollow>(
                                builder: (context, userFollow, _) {
                                  return Stack(
                                    children: [
                                      Padding(
                                        padding: EdgeInsets.only(left: 3.w),
                                        child: CircleAvatar(
                                          radius: 25,
                                          backgroundImage: NetworkImage(
                                              widget.profileImage.toString()),
                                        ),
                                      ),
                                      !isFollowed && widget.videoFollowed == 0
                                          ? InkWell(
                                              onTap: () {
                                                fetchFollow();
                                              },
                                              child: Container(
                                                width: followButtonWidth,
                                                margin: EdgeInsets.only(
                                                    top: screenHeight * 0.046),
                                                decoration: BoxDecoration(
                                                  borderRadius:
                                                      BorderRadius.circular(45),
                                                  color: const Color.fromRGBO(
                                                      20, 77, 70, 1.0),
                                                ),
                                                child: Padding(
                                                  padding:
                                                      const EdgeInsets.all(8.0),
                                                  child: Text(
                                                    "Follow",
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize:
                                                          followButtonFontSize,
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            )
                                          : Container(),
                                      // Hide the follow button if already followed
                                    ],
                                  );
                                },
                              )),
                              SizedBox(
                                height: 15.h,
                              ),
                              Consumer<LikeUnlikeUserProvider>(
                                  builder: (context, likeDislikeProvider, _) {
                                return Column(
                                  children: [
                                    InkWell(
                                      onTap: () {
                                        handleLikeUnlikeAction(); // Call the updated method
                                      },
                                      child: Icon(
                                        Icons.thumb_up,
                                        size: 30,
                                        color:
                                            isLiked == true || widget.isLiked == 1
                                                ? primaryColor
                                                : Colors.white,
                                      ),
                                    ),
                                    Text(
                                      widget.videoLikes.toString(),
                                      style: const TextStyle(color: Colors.white),
                                    )
                                  ],
                                );
                              }),
                              SizedBox(
                                height: 15.h,
                              ),



                              InkWell(
                                onTap: () {
                                  showModalBottomSheet(
                                    context: context,
                                    builder: (context) {
                                      return CommentBottomSheet(
                                          videoId:
                                              widget.videoIdForLike.toString(),
                                          videoIndex: widget.videoIndex);
                                    },
                                  );
                                },
                                child: Icon(Icons.comment,
                                    color: Colors.white, size: 30),
                              ),
                              Consumer<ReelsVideoProvider>(
                                builder: (context, videoProvider, _) {
                                  return Text(
                                    videoProvider.videoList[widget.videoIndex]
                                            ['comments']
                                        .toString(),
                                    style: TextStyle(color: Colors.white),
                                  );
                                },
                              ),
                              SizedBox(
                                height: 20.h,
                              ),
                              Consumer(builder: (context, provider,child){

                                return Column(
                                  children: [

                                    InkWell(
                                      onTap: () async {

                                        // widget.videoController?.pause();

                                        // final fileName =
                                        // await VideoThumbnail.thumbnailFile(
                                        //   video:
                                        //   "https://flutter.github.io/assets-for-api-docs/assets/videos/butterfly.mp4",
                                        //   thumbnailPath:
                                        //   (await getTemporaryDirectory()).path,
                                        //   imageFormat: ImageFormat.WEBP,
                                        //   maxHeight:
                                        //   64, // specify the height of the thumbnail, let the width auto-scaled to keep the source aspect ratio
                                        //   quality: 75,
                                        // );
                                        //
                                        // print('file name is: $fileName');
                                        // // widget.videoUrl;
                                        //
                                        // await Share.share('$fileName');


                                        print("This is Video URL: ${widget.videoUrl}");

                                        String thumbnailPath =
                                        await generateThumbnail(widget.videoUrl!);
                                        await shareContent(
                                            thumbnailPath, widget.videoUrl!);

                                        shareVideo(context);





                                        //  InitLoading().showLoading('Loading');

                                        // dio.Dio dioClient = dio.Dio();
                                        // dio.Response<List<int>> response =
                                        //     await dioClient.get<List<int>>(
                                        //   widget.videoUrl.toString(),
                                        //   options: dio.Options(
                                        //       responseType: dio.ResponseType.bytes),
                                        // );
                                        // final Directory appDocDir =
                                        //     await getApplicationDocumentsDirectory();

                                        // final String filePath =
                                        //     '${appDocDir.path}/video.mp4';
                                        // print("This is FilePath: ${filePath}");
                                        // File file = File(filePath);
                                        // print("This is File: ${file}");

                                        // // Write the video bytes to the file
                                        // await file.writeAsBytes(response.data!);

                                        // // InitLoading().dismissLoading();

                                        // // Share the video file
                                        // Share.shareFiles([filePath],
                                        //     text: 'Shared Video');
                                      },
                                      child: const FaIcon(
                                        FontAwesomeIcons.share,
                                        color: Colors.white,
                                        size: 30,
                                      ),
                                    ),
                                    Text(
                                      widget.videoList!['shares'] ?? '0',
                                      style: const TextStyle(color: Colors.white),
                                    ),
                                    SizedBox(height: 10,),
                                    Icon(Icons.bookmark_border, color: Colors.white,),

                                  ],

                                );

                              })
                            ],
                          ),
                        )
                      ],
                    ),
                  ),
                ],
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(left: 3),
              child: Row(
                children: [
                  Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      InkWell(
                        onTap: () {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OtherUserStatesScreen(
                                      id: widget.videoId,
                                      videoPlayerController:
                                          widget.videoController)));
                        },
                        child: Row(
                          children: [
                            CircleAvatar(
                              radius: 19,
                              backgroundImage:
                                  NetworkImage(widget.profileImage.toString()),
                            ),
                            SizedBox(width: 6.w),
                            Text('${widget.userName}',
                                style: const TextStyle(
                                    color: Colors.white,
                                    fontWeight: FontWeight.bold,
                                    fontSize: 16,
                                    wordSpacing: 2.0)),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5.h,
                      ),
                      Container(
                        width: MediaQuery.sizeOf(context).width * .96,
                        height: MediaQuery.sizeOf(context).height * .06,
                        margin: const EdgeInsets.only(left: 10),
                        child: Text(
                          widget.description!,
                          maxLines: 2,
                          style: const TextStyle(
                            color: Colors.white,
                            overflow: TextOverflow.ellipsis,
                          ),
                        ),
                      )
                    ],
                  ),
                ],
              ),
            ),
          ],
        ),
      );
    });
  }

// In OptionsScreen widget

  void handleLikeUnlikeAction() {
    final likeUnlikeProvider =
        Provider.of<LikeUnlikeUserProvider>(context, listen: false);

    setState(() {
      isLiked = !isLiked;
      widget.isLiked = (isLiked == true) ? 1 : 0;
      if (widget.videoLikes != null) {
        if (isLiked) {
          widget.videoLikes = (widget.videoLikes! + 1);
        } else {
          widget.videoLikes = (widget.videoLikes! - 1);
        }
      }
    });

    likeUnlikeProvider.likeUnLikeUser(
      LikeUnlikeModal(video_id: widget.videoIdForLike!),
    );
    if (isLiked == true) {
      widget.videoList!['isLiked'] = 1;
      widget.videoList!['likes'] = widget.videoLikes!;
    } else {
      widget.videoList!['isLiked'] = 0;
      widget.videoList!['likes'] = widget.videoLikes!;
    }
  }



  void fetchFollow() async {
    try {
      final userFollow = Provider.of<UserFollow>(context, listen: false);
      final follow =
          Follow(user_id: widget.videoList!['created_by'].toString());
      userFollow.toggleFollow(follow);


    } catch (e) {
      throw e.toString();
    }
  }

  Future<String> generateThumbnail(String videoUrl) async {
    final thumbnailPath = await VideoThumbnail.thumbnailFile(
      video: videoUrl,
      thumbnailPath: (await getTemporaryDirectory()).path,
      imageFormat: ImageFormat.JPEG,
      maxWidth: 128,
      quality: 25,
    );
    return thumbnailPath!;
  }
  void shareVideo(BuildContext context) {
    final shareVideoProvider = Provider.of<ShareVideoProvier>(context, listen: false);
    final shareVideoU = ShareVideoModal(video_id: widget.videoIdForLike.toString(), context: context);
    shareVideoProvider.shareVideo(shareVideoU, context);

    // Increment the share count locally
    setState(() {

        widget.videoList!['shares'] = "1";

    });


  }

  Future<void> shareContent(String thumbnailPath, String videoUrl) async {
    await Share.shareXFiles(
      [XFile(thumbnailPath)],
      subject: 'Video Thumbnail Share',
      text: videoUrl,
    );


  }
}