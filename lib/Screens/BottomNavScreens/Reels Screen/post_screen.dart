import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';
import 'package:aas/Model/user_post.dart';
import 'package:aas/Provider/get_user_post_provider.dart';

class PostsScreen extends StatefulWidget {
  const PostsScreen({Key? key}) : super(key: key);

  @override
  State<PostsScreen> createState() => _PostsScreenState();
}

class _PostsScreenState extends State<PostsScreen> {
  @override
  void initState() {
    super.initState();
    Provider.of<GetUserPostProvider>(context, listen: false)
        .getUserPostDetail();
  }

  final String baseUrlForVideos = 'https://aasonline.co/storage/app/';

  @override
  Widget build(BuildContext context) {
    return Consumer<GetUserPostProvider>(
      builder: (context, getUserFollowersData, child) {
        UserPost? getUserData = getUserFollowersData.userPost;

        if (getUserData == null) {
          return Center(
            child: Text(
              'Loading...',
              style: TextStyle(
                fontSize: 22,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          );
        }

        return Padding(
          padding: EdgeInsets.only(top: 20.0),
          child: GridView.builder(
            shrinkWrap: true,
            scrollDirection: Axis.vertical,
            physics: BouncingScrollPhysics(),
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 3,
              mainAxisSpacing: 5,
              crossAxisSpacing: 5,
              childAspectRatio: .6, // Maintain square aspect ratio
            ),
            itemCount: getUserData.videos!.length,
            itemBuilder: (context, index) {
              final allData = getUserData.videos![index];
              print("THis is AllData: ${allData}");
              var videoLink = baseUrlForVideos + allData.url.toString();
              print('Video link: $videoLink');

              return VideoWidget(
                  videoLink: videoLink, videoLikes: allData.likes!, videoViews: allData.videoViews.toString());
            },
          ),
        );
      },
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

class VideoWidget extends StatefulWidget {
  final int videoLikes;
  final String videoLink;
  final String videoViews;

  const VideoWidget(
      {required this.videoLink, required this.videoLikes, Key? key, required this.videoViews})
      : super(key: key);

  @override
  _VideoWidgetState createState() => _VideoWidgetState();
}

class _VideoWidgetState extends State<VideoWidget> {
  late VideoPlayerController _controller;
  late Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(widget.videoLink)
      ..addListener(() {
        // Listen for changes in the playback state
        if (!_controller.value.isPlaying &&
            _controller.value.position == _controller.value.duration) {
          // Video has reached the end, rewind to the beginning
          _controller.seekTo(Duration.zero);
        }
      });
    _initializeVideoPlayerFuture = _controller.initialize();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        _openFullScreen(context);
      },
      child: FutureBuilder(
        future: _initializeVideoPlayerFuture,
        builder: (context, snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return Stack(
              children: [
                Container(
                  height: MediaQuery.of(context).size.height,
                  width: 180,
                  color: Colors.black,
                  child: AspectRatio(
                    aspectRatio: _controller.value.aspectRatio,
                    child: VideoPlayer(_controller),
                  ),
                ),
                Positioned(
                    bottom: 10,
                    child: Container(
                      height: 33,
                      width: 90,
                      // color: primaryColor,
                      child: Row(
                        children: [
                          Icon(Icons.favorite, size: 20, color: white),
                          SizedBox(
                            width: 5.0,
                          ),
                          Text(widget.videoLikes.toString(),

                              style: TextStyle(color: white)),
                          SizedBox(width: 11.0,),
                          Icon(Icons.remove_red_eye, size: 20, color: white),
                      Text(widget.videoViews.toString(),style: TextStyle(color: Colors.white),),
                        ],
                      ),
                    ))
              ],
            );
          } else if (snapshot.hasError) {
            print('Error: ${snapshot.error}');
            return Center(
              child: Text('Error loading video'),
            );
          } else {
            return Center(child: CircularProgressIndicator());
          }
        },
      ),
    );
  }

  void _openFullScreen(BuildContext context) async {
    if (_controller.value.isInitialized) {
      await _controller.pause(); // Pause the video before navigating
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => FullScreenVideoScreen(controller: _controller),
        ),
      );
    }
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }
}

class FullScreenVideoScreen extends StatelessWidget {
  final VideoPlayerController controller;

  const FullScreenVideoScreen({required this.controller});

  @override
  Widget build(BuildContext context) {
    controller.play();
    return PopScope(
      onPopInvoked: (canPop) {
        controller.pause();
      },
      canPop: true,
      child: Scaffold(
        body: Center(
          child: AspectRatio(
            aspectRatio: controller.value.aspectRatio,
            child: VideoPlayer(controller),
          ),
        ),
      ),
    );
  }
}
