import 'package:aas/Model/followers_modal.dart';
import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/followers_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/followers_list_screen.dart';

import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/following_list_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/post_screen.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/reusable_list.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class ReelsProfileScreen extends StatefulWidget {
  const ReelsProfileScreen({Key? key}) : super(key: key);

  @override
  State<ReelsProfileScreen> createState() => _ReelsProfileScreenState();
}

class _ReelsProfileScreenState extends State<ReelsProfileScreen>
    with TickerProviderStateMixin {
  late TabController _tabController;
  late BannerAd _bannerAd;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();

    _tabController = TabController(length: 3, vsync: this);
    _tabController.addListener(() {
      setState(() {});
    });
    Provider.of<FollowersProvider>(context, listen: false).getFollowersDetail();
    Provider.of<UserDataProvider>(context, listen: false).updateUserData();
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    return Consumer<FollowersProvider>(
        builder: (context, followersProvider, child) {
      Follower? followersData = followersProvider.getFollowers;

      if (followersData == null) {
        InitLoading().showLoading('Loading...');

        // return Column(
        //   mainAxisAlignment: MainAxisAlignment.center,
        //   crossAxisAlignment: CrossAxisAlignment.center,
        //   children: [
        //     Center(
        //       child: Text(
        //         'Loading...',
        //         style: TextStyle(
        //             fontSize: 22, fontWeight: FontWeight.bold, color: black),
        //       ),
        //     )
        //   ],
        // );
      } else {
        InitLoading().dismissLoading();
      }
      return Scaffold(
        backgroundColor: white,
        appBar: AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: GestureDetector(
              onTap: () {
                Get.back();
              },
              child: Padding(
                padding: const EdgeInsets.only(left: 27.0),
                child: Container(
                  height: 27,
                  width: 27,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      // color: lightBlack,
                      border: Border.all(color: black, width: 2)),
                  child: Center(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Icon(Icons.arrow_back_ios, color: black, size: 18),
                    ),
                  ),
                ),
              )),
          actions: [
            // Padding(
            //   padding: const EdgeInsets.only(right: 10.0),
            //   child: Icon(
            //     Icons.share,
            //     color: black,
            //   ),
            // ),
            SizedBox(
              width: 5,
            ),
            // Icon(
            //   Icons.more_vert_outlined,
            //   color: black,
            // ),
            // SizedBox(
            //   width: 15,
            // ),
          ],
        ),
        body: Padding(
          padding: EdgeInsets.symmetric(horizontal: 15.h),
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: [
                  Column(
                    children: [
                      CircleAvatar(
                          backgroundColor: Colors.transparent,
                          radius: 35,
                          backgroundImage:
                              NetworkImage(userData!.profileimage)),
                      SizedBox(
                        height: 10.0,
                      ),
                      SizedBox(
                        width: 100,
                        child: Text(
                          userData.name,
                          style: TextStyle(
                              letterSpacing: 1.2,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                              color: primaryColor),
                          textAlign: TextAlign.center,
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        userData.followers.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        "Followers",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        userData.following.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        "Following",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                        ),
                      )
                    ],
                  ),
                  Column(
                    children: [
                      Text(
                        userData.posts.toString(),
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: 18,
                        ),
                      ),
                      Text(
                        "Post",
                        style: TextStyle(
                          fontWeight: FontWeight.w600,
                          fontSize: 14,
                        ),
                      )
                    ],
                  ),
                ],
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(15),
                          topRight: Radius.circular(15))),
                  child: Column(
                    children: [
                      // 20.heightBox,
                      Container(
                        height: screenHeight * .053,
                        width: screenWidth * .8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(
                                  0xFF111111), // Change to black border color
                            ),
                          ),
                          child: TabBar(
                            indicatorSize: TabBarIndicatorSize.tab,
                            labelColor: white,
                            indicator: BoxDecoration(
                              color: Color.fromRGBO(
                                  20, 77, 70, 1.0), // Active tab color
                              borderRadius: BorderRadius.circular(10),
                            ),
                            unselectedLabelColor: Color(0xFFABA9A9),
                            controller: _tabController,
                            tabs: [
                              Tab(text: 'Followers'),
                              Tab(text: 'Following'),
                              Tab(text: 'Post'),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            FollowersListScreen(),
                            FollowingListScreen(),
                            PostsScreen(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
            ],
          ),
        ),
      );
    });
  }
}

