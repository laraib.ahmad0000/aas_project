import 'package:flutter/material.dart';

import 'package:aas/Screens/BottomNavScreens/Reels%20Screen/trending_screen.dart';

import 'following_screen.dart';



class ReelsScreen extends StatefulWidget {
  const ReelsScreen({Key? key}) : super(key: key);

  @override
  State<ReelsScreen> createState() => _ReelsScreenState();
}

class _ReelsScreenState extends State<ReelsScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    super.initState();
    _tabController = TabController(length: 2, vsync: this, initialIndex: 1); // Set initial index to 1 for the Trending screen
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.black,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Colors.black.withOpacity(0.1),
        flexibleSpace: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
              begin: Alignment.topCenter,
              end: Alignment.bottomCenter,
              colors: [Colors.black.withOpacity(0.4), Colors.transparent],
            ),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            children: [
              TabBar(
                controller: _tabController, // Assign TabController here
                indicator: BoxDecoration(
                  color: Colors.transparent,
                ),
                tabs: [
                  Tab(
                    text: "Following",
                  ),
                  Tab(
                    text: "Trending",
                  ),
                ],
                labelColor: Colors.white,
                unselectedLabelColor: Colors.white.withOpacity(0.3),
                dividerHeight: 0,
              ),
            ],
          ),
        ),
      ),
      body: TabBarView(
        controller: _tabController, // Assign TabController here as well
        children: [
          FollowingScreen(),
          TrendingScreen(),
        ],
      ),
    );
  }
}
