import 'dart:convert';

import 'package:aas/Model/report_modal.dart';
import 'package:aas/Provider/report_user_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:provider/provider.dart';

class ReportScreen extends StatefulWidget {
  final String? id;

  ReportScreen({required this.id});

  @override
  _ReportScreenState createState() => _ReportScreenState();
}

class _ReportScreenState extends State<ReportScreen> {
  TextEditingController reportController = TextEditingController();
  final int maxCharacters = 120;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(
            'Report Screen',
            style: TextStyle(color: white),
          ),
          centerTitle: true,
          backgroundColor: primaryColor,
          iconTheme: IconThemeData(color: white),
          elevation: 0,
        ),
        body: Consumer<ReportProvider>(
          builder: (context, userReport, _) {
            return Center(
              child: Column(
                children: [
                  Container(
                    height: 200,
                    child: Padding(
                      padding: const EdgeInsets.all(15.0),
                      child: TextFormField(
                        keyboardType: TextInputType.multiline,
                        maxLines: 100,
                        maxLength: maxCharacters,
                        controller: reportController,
                        decoration: InputDecoration(
                            contentPadding: const EdgeInsets.symmetric(
                                vertical: 16.0, horizontal: 12.0),
                            hintText: "Enter your Report",
                            border: OutlineInputBorder()),
                        onChanged: (text) {
                          if (text.length > maxCharacters) {
                            reportController.text =
                                text.substring(0, maxCharacters);

                            reportController.selection =
                                TextSelection.collapsed(offset: maxCharacters);
                          }
                        },
                      ),
                    ),
                  ),
                  SizedBox(height: 5.0),
                  Container(
                    width: 150,
                    child: ElevatedButton(
                      onPressed: () {
                        final reportModal = ReportModel(
                            reportDescription: reportController.text,
                            user_id: widget.id!);

                        userReport.submitReport(reportModal);

                        // CustomSnackbar.successful(
                        //     message: 'User report Successfully.');
                        // Future.delayed(Duration(seconds: 2));
                        reportController.clear();
                        Navigator.pop(context);
                        // Get.back();
                      },
                      child: Text(
                        'Submit',
                        style: TextStyle(color: Colors.white),
                      ),
                      style: ElevatedButton.styleFrom(
                        backgroundColor: primaryColor,
                      ),
                    ),
                  ),
                ],
              ),
            );
          },
        ));
  }
}
