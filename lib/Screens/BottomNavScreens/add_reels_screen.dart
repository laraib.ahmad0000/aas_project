import 'dart:async';
import 'dart:io';

import 'package:aas/Provider/file_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/settings_screen.dart' as SC;

import 'package:aas/Screens/BottomNavScreens/bottomnav.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/Services/services.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/foundation.dart';


import 'package:flutter/material.dart';

import 'package:get/get.dart';

import 'package:image_picker/image_picker.dart';
import 'package:provider/provider.dart';
import 'package:video_player/video_player.dart';


import 'homescreen/BottomNav2_screens/bottom_nav2.dart';

class AddReelsScreen extends StatefulWidget {
  const AddReelsScreen({super.key});

  @override
  State<AddReelsScreen> createState() => _AddReelsScreenState();
}

class _AddReelsScreenState extends State<AddReelsScreen> {
  VideoPlayerController? _videoPlayerController;
  VideoPlayerController? _cameraVideoPlayerController;
  final TextEditingController _descriptionController = TextEditingController();
  final int maxCharacters = 80;
  final picker = ImagePicker();
  bool _dialogShown = false;
  File? _video;
  File? file;
 dynamic pickedFile;

  Future<void> pickVideoFromGallery(String descriptionController) async {
    pickedFile = await picker.pickVideo(source: ImageSource.gallery);

    if (pickedFile != null) {
      _video = File(pickedFile!.path);
      _videoPlayerController = VideoPlayerController.file(_video!);

      await _videoPlayerController!.initialize(); // Wait for initialization

      if (mounted) { // Check if the widget is still mounted
        descDialogue(context);
      }
    } else {
      if (kDebugMode) {
        print('File picked is null');
      }
    }
  }

  Future<void> pickVideoFromCamera(String descriptionController) async {
    pickedFile = await picker.pickVideo(source: ImageSource.camera);

    if (pickedFile != null) {
      _video = File(pickedFile!.path);
      _videoPlayerController = VideoPlayerController.file(_video!);

      await _videoPlayerController!.initialize(); // Wait for initialization

     if(mounted){
       descDialogue(context);
     }
    } else {
      if (kDebugMode) {
        print('file picked in null');
      }
    }


  }

  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) {
      setState(() {
        _dialogShown = true;
      });
    });
  }


  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    
    if (_videoPlayerController != null) {
      _videoPlayerController!.dispose();
    }
    if (_cameraVideoPlayerController != null) {
      _cameraVideoPlayerController!.dispose();
    }
  }

  void dialogue(context) {
    double screenHeight = MediaQuery.of(context).size.height;
    showDialog(
        context: context,
        barrierDismissible: false,
        barrierColor: Colors.black,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            content: SizedBox(
              height: screenHeight * .16,
              child: Column(
                children: [
                  InkWell(
                    child: const ListTile(
                      leading: Icon(Icons.camera_alt),
                      title: Text('Camera'),
                    ),
                    onTap: () {
                      pickVideoFromCamera(
                          _descriptionController.text.toString());
                      Navigator.pop(context);
                    },
                  ),
                  InkWell(
                    child: const ListTile(
                      leading: Icon(Icons.photo),
                      title: Text('Gallery'),
                    ),
                    onTap: () {
                      // print(
                      //     'description on popup: ${_descriptionController.text.toString()}');
                      pickVideoFromGallery(
                          _descriptionController.text.toString());
                      Navigator.pop(context);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return WillPopScope(
      onWillPop: () async {
        if (_dialogShown) {
          Navigator.pop(context);
        // Navigator.push(context, MaterialPageRoute(builder: (context)=> BottomNav2(initialIndex: 2)));
          return false;
        } else{

          return true;
        }
      },
      child: Scaffold(
        backgroundColor: Colors.grey,

        body: Center(
          child: _dialogShown
              ?  AlertDialog(
//           title: Text('Note..',style:TextStyle(color: Colors.red)),
        insetPadding: const EdgeInsets.all(30.0),
        backgroundColor: Colors.transparent,
        contentPadding: const EdgeInsets.all(0.0),
        content: Container(
          height: 150.0,
          width: screenSize.width,
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(20.0),
            color: Colors.white,
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const SizedBox(
                  height: 1.0,
                ),
                Text(
                  "1. Video size must be less than 10mb.",
//                     style: SC.appNormalText,
                  style:TextStyle(fontSize:11),
                  textAlign: TextAlign.center,
                ),
                Text(
                  "2. Video content should not be 18+",
//                     style: SC.appNormalText,
                  style:TextStyle(fontSize:11),
                  textAlign: TextAlign.center,
                ),
                Text(
                  "3. Wait untill your video get approved by admin.",
//                     style: SC.appNormalText,
                  style:TextStyle(fontSize:11),
                  textAlign: TextAlign.center,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[

                    const SizedBox(
                      width: 30.0,
                    ),
                    TextButton(
                      style: TextButton.styleFrom(
                        foregroundColor: Colors.white,
                        padding: const EdgeInsets.all(16.0),
                        textStyle: const TextStyle(fontSize: 20),
                      ),
                      onPressed: () {
                        Future.delayed(const Duration(milliseconds: 100), () {
                          dialogue(context);
                        });
                      },
                      child: Container(
                        height: 30,
                        width: 60,
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(
                              7.0,
                            ),
                            border: Border.all(color: Colors.black)),
                        child: const Center(
                          child: Text(
                            'Okay',
                            style: TextStyle(color:Colors.black, fontSize:13),
                          ),
                        ),
                      ),
                    ),
                  ],
                )
              ],
            ),
          ),
        ),
      )
              : Text("Add Video"),
        ),
      ),
    );
  }


  descDialogue(BuildContext context) {

    showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
            content: SingleChildScrollView(
              child: Padding(
                padding: const EdgeInsets.all(10.0),
                child: Column(
                  children: [
                    SizedBox(
                      height: 150,
                      width: 250,
                      child: _video != null &&
                              _videoPlayerController != null &&
                              _videoPlayerController!.value.isInitialized
                          ? AspectRatio(
                              aspectRatio:
                                  _videoPlayerController!.value.aspectRatio,
                              child: VideoPlayer(_videoPlayerController!),
                            )
                          : Container(),
                    ),
                    const SizedBox(
                      height: 15,
                    ),
                    SizedBox(
                      height: 150,
                      child: TextField(
                        controller: _descriptionController,
                        decoration: InputDecoration(
                          hintText: "Enter Your Description",
                          border: OutlineInputBorder(
                            borderSide: const BorderSide(),
                            borderRadius: BorderRadius.circular(8.0),
                          ),
                          contentPadding: const EdgeInsets.symmetric(
                              vertical: 16.0,
                              horizontal:
                                  12.0), // Adjust padding inside the border
                        ),
                        keyboardType: TextInputType.multiline,
                        maxLines: 100,
                        maxLength: maxCharacters,
                        onChanged: (text) {
                          if (text.length > maxCharacters) {
                            _descriptionController.text =
                                text.substring(0, maxCharacters);

                            _descriptionController.selection =
                                TextSelection.collapsed(
                                    offset: maxCharacters);
                          }
                        },
                      ),
                    ),
                    ElevatedButton(
                        style: ElevatedButton.styleFrom(
                            backgroundColor:
                                const Color.fromARGB(232, 30, 101, 16)),
                        onPressed: () async {
                          InitLoading().showLoading("Loading...");
                          Future.delayed(const Duration(seconds: 4), () {
                            InitLoading().dismissLoading();
                            Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => BottomNav2(initialIndex: 2)));
                          });
                          file = File(pickedFile.path);
                          Provider.of<FileProvider>(context, listen: false)
                              .setPickedFile(file!);
                          await ApiService()
                              .uploadFile(file!, _descriptionController.text);
                        },
                        child: const Text(
                          "Add",
                          style: TextStyle(color: Colors.white),
                        ))
                  ],
                ),
              ),
            ),
          );
        });
  }
}
