import 'package:aas/Model/Profile%20Modals/notification_modal.dart';
import 'package:aas/Provider/Profile%20Provider/notification_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({super.key});

  @override
  State<ChatScreen> createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  late BannerAd _bannerAd;
  @override
  void initState() {
    _bannerAd = context.read<AdProvider>().createBannerAd();
    super.initState();
    Provider.of<NotificationDetailProvider>(context, listen: false)
        .getNotificationDetails();
  }

  void dispose() {
    super.dispose();
    _bannerAd.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    NotificationModal? notifiDetails =
        Provider.of<NotificationDetailProvider>(context).notificaionDetail;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          width: double.infinity,
          // decoration: BoxDecoration(
          //   color: white,
          //   borderRadius: BorderRadius.only(
          //       topLeft: Radius.circular(30.0),
          //       topRight: Radius.circular(30.0)),
          // ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
                      Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
                 SizedBox(height: 30.0,),
              // 30.heightBox,
              Container(
                height: screenHeight * 1,
                width: double.infinity,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    notifiDetails != null &&
                            notifiDetails.notifications.isNotEmpty
                        ? Container(
                            child: Expanded(
                                child: Padding(
                              padding: EdgeInsets.only(left: 10.w, right: 10.w),
                              child: ListView.separated(
                                itemCount: notifiDetails.notifications
                                    .length, // Total number of items
                                separatorBuilder:
                                    (BuildContext context, int index) {
                                  return const Divider(
                                    color: Colors.black,
                                    thickness: 0.25,
                                  );
                                },
                                itemBuilder: (BuildContext context, int index) {
                                  return ListTile(
                                    // leading: Container(
                                    //     width: 48.w,
                                    //     height: 58.h,
                                    //     decoration: BoxDecoration(
                                    //       color: primaryColor,
                                    //       borderRadius: BorderRadius.circular(8),
                                    //     ),
                                    //     child: Padding(
                                    //       padding: const EdgeInsets.all(4.0),
                                    //       child: Image.asset(
                                    //         'assets/images/address.png',
                                    //       ),
                                    //     )),
                                    title: Text(
                                      notifiDetails
                                          .notifications[index].notification
                                          .toString(),
                                      style: const TextStyle(
                                          fontSize: 14,
                                          color: primaryColor,
                                          fontWeight: FontWeight.bold),
                                    ), // Title
                                    subtitle: Text(
                                      notifiDetails.notifications[index].date
                                          .toString(),
                                      style: const TextStyle(
                                          fontSize: 11,
                                          color: Colors.grey,
                                          fontWeight: FontWeight.bold),
                                    ), // Subtitle
                                    // onTap: () {
                                    //   print("Yes");
                                    // },
                                  );
                                },
                              ),
                            )),
                          )
                        : const Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              Center(child: Text('No any Notifications'))
                            ],
                          ),
                  ],
                ),
              ),
              // Container(
              //   width: MediaQuery.of(context).size.width,
              //   height: 60,
              //   child: AdWidget(ad: _bannerAd),
              // ),
    
            ],
          ),
        ),
      ),
    );
  }
}

// import 'dart:developer';

// import 'package:aas/Model/chat_user_modal.dart';
// import 'package:aas/Screens/BottomNavScreens/homescreen/BottomNav2_screens/Chat%20Screen/chat_user_card.dart';
// import 'package:aas/constants/colors.dart';
// import 'package:cloud_firestore/cloud_firestore.dart';
// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:flutter/services.dart';

// //home screen -- where all available contacts are shown
// class ChatScreen extends StatefulWidget {
//   const ChatScreen({super.key});

//   @override
//   State<ChatScreen> createState() => _ChatScreenState();
// }

// class _ChatScreenState extends State<ChatScreen> {
//   // for storing all users
//   List<ChatUser> _list = [];

//   // for storing searched items
//   final List<ChatUser> _searchList = [];
//   // for storing search status
//   bool _isSearching = false;

//   final FirebaseFirestore firestore = FirebaseFirestore.instance;
//   List<ChatUser> usersList = [];

//   @override
//   Widget build(BuildContext context) {
//     return GestureDetector(
//       //for hiding keyboard when a tap is detected on screen
//       onTap: () => FocusScope.of(context).unfocus(),
//       child: Scaffold(
//         //app bar
//         appBar: AppBar(
//           centerTitle: true,
//           automaticallyImplyLeading: false,
//           backgroundColor: primaryColor,
//           title: Text('Chat'),
//         ),
//         // appBar: AppBar(
//         //   leading: const Icon(CupertinoIcons.home),
//         //   title: _isSearching
//         //       ? TextField(
//         //           decoration: const InputDecoration(
//         //               border: InputBorder.none, hintText: 'Name, Email, ...'),
//         //           autofocus: true,
//         //           style: const TextStyle(fontSize: 17, letterSpacing: 0.5),
//         //           //when search text changes then updated search list
//         //           onChanged: (val) {
//         //             //search logic
//         //             _searchList.clear();

//         //             for (var i in _list) {
//         //               if (i.name.toLowerCase().contains(val.toLowerCase()) ||
//         //                   i.email.toLowerCase().contains(val.toLowerCase())) {
//         //                 _searchList.add(i);
//         //                 setState(() {
//         //                   _searchList;
//         //                 });
//         //               }
//         //             }
//         //           },
//         //         )
//         //       : const Text('We Chat'),
//         //   actions: [
//         //     //search user button
//         //     IconButton(
//         //         onPressed: () {
//         //           setState(() {
//         //             _isSearching = !_isSearching;
//         //           });
//         //         },
//         //         icon: Icon(_isSearching
//         //             ? CupertinoIcons.clear_circled_solid
//         //             : Icons.search)),
//         //   ],
//         // ),

//         //body
//         body: StreamBuilder(
//           stream: firestore.collection('Users').snapshots(),

//           //get id of only known users
//           builder: (context, snapshot) {
//             switch (snapshot.connectionState) {
//               //if data is loading
//               case ConnectionState.waiting:
//               case ConnectionState.none:
//                 return const Center(child: CircularProgressIndicator());

//               //if some or all data is loaded then show it
//               case ConnectionState.active:
//               case ConnectionState.done:
//                 final data = snapshot.data!.docs;
//                 usersList =
//                     data.map((e) => ChatUser.fromJson(e.data())).toList() ?? [];

//                 if (usersList.isNotEmpty) {
//                   return ListView.builder(
//                     physics: BouncingScrollPhysics(),
//                     shrinkWrap: true,
//                     itemCount: usersList.length,
//                     itemBuilder: (BuildContext context, int index) {
//                       return ChatUserCard(user: usersList[index]);
//                     },
//                   );
//                 } else {
//                   return Center(child: Text('No Any User Found.'));
//                 }
//             }
//           },
//         ),
//       ),
//     );
//   }
// }
