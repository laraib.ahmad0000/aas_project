import 'package:aas/Constants/colors.dart';
import 'package:aas/Model/dua_modal.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Dua%20Screens/dua_tab_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Dua%20Screens/hadith_tab_screen.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';

// import 'package:velocity_x/velocity_x.dart';

class DuaScreen extends StatefulWidget {
  const DuaScreen({Key? key}) : super(key: key);

  @override
  State<DuaScreen> createState() => _DuaScreenState();
}

class _DuaScreenState extends State<DuaScreen>
    with SingleTickerProviderStateMixin {
  duaAZikarModel? duaZikr;
  int _expandedIndex = -1;

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  final bool _customIcon = false;
  late TabController _tabController;
  late BannerAd _bannerAd;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();

    _tabController = TabController(length: 2, vsync: this);

    _tabController.addListener(() {
      setState(() {});
    });
  }

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
      final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: primaryColor,
        title: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              'Dua & Zikr',
              style: TextStyle(fontSize: 20.sp, color: Colors.yellow),
            ),
            SizedBox(width: screenWidth * .03),
            Image.asset(
              'assets/images/dua.png',
              color: Colors.white,
              width: screenWidth * .09,
              height: screenHeight * .09,
            ),
          ],
        ),
        centerTitle: true,
        automaticallyImplyLeading: false,
        leading: GestureDetector(
            onTap: () {
              Get.back();
            },
            child: Padding(
              padding: const EdgeInsets.only(left: 20.0),
              child: Container(
                height: 30,
                width: 30,
                decoration: BoxDecoration(
                    shape: BoxShape.circle,
                    border: Border.all(color: white, width: 2)),
                child: const Center(
                  child: Icon(Icons.arrow_back, color: white, size: 20),
                ),
              ),
            )),
        actions: [
          IconButton(
            icon: const Icon(Icons.search),
            onPressed: () {},
          ),
        ],
      ),
      body: SafeArea(
        child: Container(
          // height: double.infinity,
          width: double.infinity,
          color: primaryColor,
          child: Column(
            children: [
              Container(
                child: Column(
                  children: [
                    const Center(
                      child: Text(
                        'رَبِّ انْصُرْنِیْ عَلَی الْقَوْمِ الْمُفْسِدِیْنَِ',
                        style: TextStyle(
                          fontSize: 20,
                          color: Colors.white,
                          fontFamily: 'Amiri',
                        ),
                      ),
                    ),
                    const Center(
                      child: Text(
                        'Lord, help me against the corrupt people',
                        style: TextStyle(fontSize: 13, color: Colors.white),
                      ),
                    ),
                    SizedBox(
                      height: screenHeight * .009,
                    ),
                    Center(
                      child: Text(
                        'The Quran 29:30',
                        style: TextStyle(
                            fontSize: 13, color: Colors.yellow.withOpacity(.6)),
                      ),
                    ),
                    const SizedBox(
                      height: 20,
                    )
                  ],
                ),
              ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: const BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                         SizedBox(height: 30.0,),
                      // 30.heightBox,
                      Container(
                        height: screenHeight * .05,
                        width: screenWidth * .8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: const Color(0xFF111111),
                            ),
                          ),
                          child: TabBar(
                            labelColor: white,
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                              color: const Color.fromRGBO(20, 77, 70, 1.0),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            unselectedLabelColor: const Color(0xFFABA9A9),
                            controller: _tabController,
                            tabs: [
                              const Tab(text: 'Dua'),
                              const Tab(text: 'Hadith'),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [DuaTab(), HadithTab()],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 60,
                        child: AdWidget(ad: _bannerAd),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
