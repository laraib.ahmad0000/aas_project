import 'package:aas/Model/dua_modal.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:accordion/accordion.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

// import 'package:velocity_x/velocity_x.dart';

class DuaTab extends StatefulWidget {
  DuaTab({super.key});

  @override
  State<DuaTab> createState() => _DuaTabState();
}

class _DuaTabState extends State<DuaTab> {
  @override
  Widget build(BuildContext context) {  final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.only(
        top: screenHeight * .025,
        left: screenWidth * .03,
        right: screenWidth * .03,
      ),
      child: Card(
        elevation: 0,
        color: white,
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Accordion(
            // Set contentDivider to null
            headerBackgroundColor: Colors.white,
            children: List.generate(data.length, (index) {
              return AccordionSection(
                contentBorderWidth: 0,
                contentBorderColor: Colors.white,
                contentBackgroundColor: Colors.white,
                rightIcon: const Icon(
                  Icons.keyboard_arrow_down,
                  color: Colors.black54,
                  size: 20,
                ),
                header: Column(
                  children: [
                    Row(
                      children: [
                        CircleAvatar(
                          backgroundImage: AssetImage(data[index].image!),
                          backgroundColor: Colors.transparent,
                          radius: screenWidth * 0.06,
                        ),
                        SizedBox(width: screenWidth * 0.03),
                        Text(
                          data[index].text.toString(),
                          style: TextStyle(
                            fontFamily: 'Amiri',
                            overflow: TextOverflow.fade,
                            fontSize: 15.sp,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                      ],
                    ),
                    const Divider(),
                  ],
                ),
                content: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(7.0),
                        color: primaryColor,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: GestureDetector(
                          onTap: () async {},
                          child: Text(
                            data[index].arabicDua.toString(),
                            style: whiteArabicText,
                          ),
                        ),
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Text(
                      data[index].inUrdu.toString(),
                      style: const TextStyle(
                        fontSize: 16,
                        color: primaryColor,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Amiri',
                      ),
                    ),
                    const SizedBox(height: 10.0),
                    Text(
                      data[index].inEnglish.toString(),
                      style: const TextStyle(
                        fontSize: 16,
                        color: primaryColor,
                        fontWeight: FontWeight.bold,
                        fontFamily: 'Amiri',
                      ),
                    ),
                  ],
                ),
              );
            }),
          ),
        ),
      ),
    );
  }
}
