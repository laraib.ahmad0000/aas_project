import 'package:flutter/material.dart';
import 'package:accordion/accordion.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:aas/Model/hadith_modal.dart';
// import 'package:velocity_x/velocity_x.dart';

class HadithTab extends StatefulWidget {
  HadithTab({super.key});

  @override
  State<HadithTab> createState() => _HadithTabState();
}

class _HadithTabState extends State<HadithTab> {
  
  @override
  Widget build(BuildContext context) {  final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Padding(
      padding: EdgeInsets.only(
        top: screenHeight * .025,
        left: screenWidth * .03,
        right: screenWidth * .03,
      ),
      child: Card(
        elevation: 0,
        color: white,
        child: Padding(
          padding: const EdgeInsets.only(top: 10.0, bottom: 10.0),
          child: Accordion(
            headerBackgroundColor: Colors.white,
            children: List.generate(hadithData.length, (index) {
              if (index < hadithData.length) {
                return AccordionSection(
                  contentBorderWidth: 0,
                  contentBorderColor: Colors.white,
                  contentBackgroundColor: Colors.white,
                  rightIcon: const Icon(
                    Icons.keyboard_arrow_down,
                    color: Colors.black54,
                    size: 20,
                  ),
                  header: Column(
                    children: [
                      Row(
                        children: [
                          CircleAvatar(
                            backgroundImage:
                                AssetImage(hadithData[index].image!),
                            backgroundColor: Colors.transparent,
                            radius: screenWidth * 0.06,
                          ),
                          SizedBox(width: screenWidth * 0.03),
                          Text(
                            hadithData[index].text.toString(),
                            style: TextStyle(
                              overflow: TextOverflow.fade,
                              fontSize: 15.sp,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Amiri',
                            ),
                          ),
                        ],
                      ),
                      Divider(),
                    ],
                  ),
                  content: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Container(
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(7.0),
                          color: primaryColor,
                        ),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: GestureDetector(
                            onTap: () async {},
                            child: Text(
                              hadithData[index].arabicHadith.toString(),
                              style: whiteArabicText,
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        hadithData[index].inUrdu.toString(),
                        style: TextStyle(
                          fontSize: 16,
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Amiri',
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        hadithData[index].inEnglish.toString(),
                        style: TextStyle(
                          fontSize: 16,
                          color: primaryColor,
                          fontWeight: FontWeight.bold,
                          fontFamily: 'Amiri',
                        ),
                      ),
                    ],
                  ),
                );
              } else {
                return AccordionSection(
                  header: Text("Header Text"),
                  content: Container(
                    child: Text(
                      "Index out of bounds: $index",
                      style: TextStyle(
                        fontFamily: 'Amiri',
                      ),
                    ),
                  ),
                );
              }
            }),
          ),
        ),
      ),
    );
  }
}
