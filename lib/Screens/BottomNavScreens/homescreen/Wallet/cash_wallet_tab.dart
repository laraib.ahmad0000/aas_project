import 'package:aas/Model/MLM%20modals/cash_wallet_modal.dart';
import 'package:aas/Model/Transaction%20Modal/wallet_modal.dart';
import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/MLM/Transactions%20Provider/cash_wallet_provider.dart';
import 'package:aas/Provider/MLM/Transactions%20Provider/wallet_detail_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:flutter/material.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/donate_to_others_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/wallet_withdraw_screen.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class CashTabScreen extends StatefulWidget {
  const CashTabScreen({super.key});

  @override
  State<CashTabScreen> createState() => _CashTabScreenState();
}

class _CashTabScreenState extends State<CashTabScreen> {
  late BannerAd _bannerAd;
  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    Provider.of<CashWalletDetailProvider>(context, listen: false)
        .cashWalletDetail();
  }

// TextEditingController setPinController = TextEditingController();
  @override
  Widget build(BuildContext context) {
      final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    CashWallet? getCashWalletDetail =
        Provider.of<CashWalletDetailProvider>(context).getCashWalletDetail;

    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    return Scaffold(
      backgroundColor: white,
      body: SingleChildScrollView(
        child: SafeArea(
          child: Container(
            margin: EdgeInsets.symmetric(horizontal: 2),
            padding: EdgeInsets.all(10),
            width: double.infinity,
            decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30),
                    topRight: Radius.circular(30))),
            child: getCashWalletDetail == null || userData == null
                ? Center(child: CircularProgressIndicator())
                : Column(
                    children: [
                       SizedBox(
                        height: 15.0,
                      ),
                      // 15.heightBox,
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text("Balance", style:  TextStyle(fontSize: 15, color: black, fontWeight: FontWeight.bold)),
                        ],
                      ),
                         SizedBox(
                        height: 5.0,
                      ),
                      // 5.heightBox,
                      Text(
                        '\$ ${getCashWalletDetail.cashWallet.balance ?? ''}',
                        style: TextStyle(
                            color: Color.fromRGBO(
                                20, 77, 70, 1.0), // or any other color
                            decoration: TextDecoration.none,
                            fontSize: 35,
                            fontWeight: FontWeight.bold // remove underline
                            ),
                      ),
                         SizedBox(
                        height: 20.0,
                      ),
                      // 20.heightBox,
                      Card(
                          margin: EdgeInsets.all(8.0),
                       elevation: 10,
                            color: white,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.only(right: 10.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [

                                    // 20.widthBox,
                                    // Image.asset(
                                    //   'assets/icons/icon 1.png',
                                    //   height: 45,
                                    //   width: 45,
                                    // ),
                                    // 20.widthBox,
                                    Expanded(
                                      child: Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Donation Received: ",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      20,
                                                      77,
                                                      70,
                                                      1.0), // or any other color
                          
                                                  fontSize: 17,
                                                  fontWeight: FontWeight
                                                      .bold // remove underline
                                                  ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "${getCashWalletDetail.cashWallet.donationReceived ?? ''}",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            20,
                                                            77,
                                                            70,
                                                            1.0), // or any other color
                          
                                                        fontSize: 14.5,
                                                        fontWeight: FontWeight
                                                            .bold // remove underline
                                                        ),
                                                  ),
                                                  Text(
                                                    " \$",
                                                    style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 235, 159, 18),
                                                        fontSize: 17,
                                                        fontWeight: FontWeight
                                                            .w900 // remove underline
                                                        ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                               SizedBox(
                        height: 10.0,
                      ),
                              // 10.heightBox,
                              Padding(
                                padding: const EdgeInsets.only(right: 10.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    // 20.widthBox,
                                    // Image.asset(
                                    //   'assets/icons/icon 2.png',
                                    //   height: 45,
                                    //   width: 45,
                                    // ),
                                    // 20.widthBox,

                                    Expanded(
                                      child: Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Cash from Coins: ",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      20,
                                                      77,
                                                      70,
                                                      1.0), // or any other color
                          
                                                  fontSize: 17,
                                                  fontWeight: FontWeight
                                                      .bold // remove underline
                                                  ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "${getCashWalletDetail.cashWallet.cashFromCoins ?? ''}",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            20,
                                                            77,
                                                            70,
                                                            1.0), // or any other color
                          
                                                        fontSize: 17,
                                                        fontWeight: FontWeight
                                                            .bold // remove underline
                                                        ),
                                                  ),
                                                  Text(
                                                    " \$",
                                                    style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 235, 159, 18),
                                                        fontSize: 17,
                                                        fontWeight: FontWeight
                                                            .w900 // remove underline
                                                        ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                                 SizedBox(
                        height: 10.0,
                      ),
                              // 10.heightBox,
                              Padding(
                                padding: const EdgeInsets.only(right: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    // 20.widthBox,
                                    // Image.asset(
                                    //   'assets/icons/icon3.png',
                                    //   height: 45,
                                    //   width: 45,
                                    // ),
                                    // 20.widthBox,
                                    Expanded(
                                      child: Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Donation Given:  ",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      20,
                                                      77,
                                                      70,
                                                      1.0), // or any other color
                                                  decoration: TextDecoration.none,
                                                  fontSize: 17,
                                                  fontWeight: FontWeight
                                                      .bold // remove underline
                                                  ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "${getCashWalletDetail.cashWallet.donationGiven ?? ''}",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            20,
                                                            77,
                                                            70,
                                                            1.0), // or any other color
                                                        decoration:
                                                            TextDecoration.none,
                                                        fontSize: 14.5,
                                                        fontWeight: FontWeight
                                                            .bold // remove underline
                                                        ),
                                                  ),
                                                  Text(
                                                    " \$",
                                                    style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 235, 159, 18),
                                                        fontSize: 17,
                                                        fontWeight: FontWeight
                                                            .w900 // remove underline
                                                        ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                                 SizedBox(
                        height: 10.0,
                      ),
                              // 10.heightBox,
                              Padding(
                                padding: const EdgeInsets.only(right: 5.0),
                                child: Row(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  children: [
                                    // 20.widthBox,
                                    // Image.asset(
                                    //   'assets/icons/icon4.png',
                                    //   height: 45,
                                    //   width: 45,
                                    // ),
                                    // 20.widthBox,
                                    Expanded(
                                      child: Container(
                                        child: Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Total Withdraw: ",
                                              style: TextStyle(
                                                  color: Color.fromRGBO(
                                                      20,
                                                      77,
                                                      70,
                                                      1.0), // or any other color
                                                  decoration: TextDecoration.none,
                                                  fontSize: 17,
                                                  fontWeight: FontWeight
                                                      .bold // remove underline
                                                  ),
                                            ),
                                            Container(
                                              child: Row(
                                                children: [
                                                  Text(
                                                    "${getCashWalletDetail.cashWallet.withdrawal ?? ''}",
                                                    style: TextStyle(
                                                        color: Color.fromRGBO(
                                                            20,
                                                            77,
                                                            70,
                                                            1.0), // or any other color
                                                        decoration:
                                                            TextDecoration.none,
                                                        fontSize: 14.5,
                                                        fontWeight: FontWeight
                                                            .bold // remove underline
                                                        ),
                                                  ),
                                                  Text(
                                                    " \$",
                                                    style: TextStyle(
                                                        color: Color.fromARGB(
                                                            255, 235, 159, 18),
                                                        fontSize: 17,
                                                        fontWeight: FontWeight
                                                            .w900 // remove underline
                                                        ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),

                      SizedBox(
                        height: screenWidth * .06,
                      ),

                      //               getCashWalletDetail.cashWallet.tpin_status == 0
                      //                   ? Container(
                      //                       // height: 60,
                      //                       width: MediaQuery.of(context).size.width,
                      //                       // color: lightBlack,
                      //                       child: Column(
                      //                         children: [
                      //                           TextField(
                      //                             keyboardType:
                      //                                 const TextInputType.numberWithOptions(
                      //                                     decimal: false),
                      //                             controller: setPinController,
                      //                             autofocus: false,
                      //                             style: const TextStyle(
                      //                                 fontSize: 15.0, color: Colors.black),
                      //                             decoration: InputDecoration(
                      //                               border: InputBorder.none,
                      //                               hintText: 'Set Pin',
                      //                               filled: true,
                      //                               fillColor: Colors.black26,
                      //                               contentPadding: const EdgeInsets.only(
                      //                                   left: 14.0, bottom: 6.0, top: 8.0),
                      //                               focusedBorder: OutlineInputBorder(
                      //                                 borderSide:
                      //                                     const BorderSide(color: primaryColor),
                      //                                 borderRadius: BorderRadius.circular(10.0),
                      //                               ),
                      //                               enabledBorder: UnderlineInputBorder(
                      //                                 borderSide:
                      //                                     const BorderSide(color: Colors.grey),
                      //                                 borderRadius: BorderRadius.circular(10.0),
                      //                               ),
                      //                             ),
                      //                           ),
                      //                           SizedBox(
                      //                             height: 10.0,
                      //                           ),
                      //                           InkWell(
                      //                             onTap: (){
                      //                               if(setPinController.length == 4){
                      //                                 CustomSnackbar.successful(message: 'Pin set successfully');
                      //                               }
                      // else {
                      //   CustomSnackbar.error(message: 'Pin must be 4 digits');
                      // }
                      //                             },
                      //                             child: Container(
                      //                               height: 40,
                      //                               width: 100,
                      //                               decoration: BoxDecoration(
                      //                                   borderRadius: BorderRadius.circular(8.0),
                      //                                   color: primaryColor),
                      //                               child: Center(
                      //                                 child: Text(
                      //                                   'Set Pin',
                      //                                   style: TextStyle(color: white),
                      //                                 ),
                      //                               ),
                      //                             ),
                      //                           )
                      //                         ],
                      //                       ),
                      //                       // child: Text(getCashWalletDetail.cashWallet.tpin_status.toString()),
                      //                     )
                      //                   : Container(),

                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          GestureDetector(
                            onTap: () {
                              Get.to(WalletwithdrawScreen());
                            },
                            child: Image(
                                height: 52,
                                width: 140,
                                image:
                                    AssetImage('assets/images/withdraw.png')),
                          ),
                          SizedBox(
                            width: screenWidth * .1,
                          ),
                          GestureDetector(
                            onTap: () {
                              Get.to(DonateScreen());
                            },
                            child: Image(
                                height: 52,
                                width: 140,
                                image: AssetImage('assets/images/donate.png')),
                          ),
                        ],
                      )
                    ],
                  ), // Adjust the radius as needed
          ),
          // Container(
          //   height: double.infinity,
          //   width: double.infinity,
          //   color: Color.fromRGBO(20, 77, 70, 1.0),
          //   child: Column(
          //     children: [
          //       // Padding(
          //       //   padding: const EdgeInsets.only(left: 10, right: 10),
          //       //   child: Container(
          //       //     height: 60,
          //       //     child: Row(
          //       //       children: [
          //       //         GestureDetector(
          //       //             onTap: () {
          //       //               Navigator.pop(context);
          //       //             },
          //       //             child: Padding(
          //       //               padding: const EdgeInsets.only(left: 5.0, top: 20),
          //       //               child: Container(
          //       //                 height: 30,
          //       //                 width: 30,
          //       //                 decoration: BoxDecoration(
          //       //                     shape: BoxShape.circle,
          //       //                     // color: lightBlack,
          //       //                     border: Border.all(color: white, width: 2)),
          //       //                 child: Center(
          //       //                   child: Icon(Icons.arrow_back,
          //       //                       color: white, size: 20),
          //       //                 ),
          //       //               ),
          //       //             )),
          //       //         SizedBox(
          //       //           width: 100,
          //       //         ),
          //       //         Padding(
          //       //           padding: const EdgeInsets.only(top: 20),
          //       //           child: Row(
          //       //             mainAxisAlignment: MainAxisAlignment.center,
          //       //             children: [
          //       //               Text(
          //       //                 "My Wallet",
          //       //                 style: TextStyle(
          //       //                     color: Color.fromRGBO(
          //       //                         252, 216, 138, 1.0), // or any other color
          //       //                     decoration: TextDecoration.none,
          //       //                     fontSize: 22 // remove underline
          //       //                     ),
          //       //               ),
          //       //               20.widthBox,
          //       //               Image.asset(
          //       //                 'assets/icons/wallet1.png',
          //       //                 height: 30,
          //       //                 width: 30,
          //       //               )
          //       //             ],
          //       //           ),
          //       //         ),
          //       //       ],
          //       //     ),
          //       //   ),
          //       // ),
          //       // Container(
          //       //   width: double.infinity,
          //       //   height: 20,
          //       //   color: Color(0xff144d46),
          //       // ),
          //       // Expanded(
          //       //   child:
          //       // ),
          //       // Container(
          //       //   width: MediaQuery.of(context).size.width,
          //       //   height: 60,
          //       //   child: AdWidget(ad: _bannerAd),
          //       // ),
          //     ],
          //   ), // Set the background color here
          // ),
        ),
      ),
    );
  }
}
