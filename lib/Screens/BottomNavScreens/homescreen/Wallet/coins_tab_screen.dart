import 'package:aas/Model/MLM%20modals/coins_convert_modal.dart';
import 'package:aas/Provider/MLM/Transactions%20Provider/coins_wallet_detail_two_provider.dart';
import 'package:aas/Provider/MLM/coins_convert_provider.dart';
import 'package:aas/Provider/MLM/Transactions%20Provider/coins_wallet_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/Ledgers/ledger_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/edit_profile.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:aas/constants/reusable_widgets.dart';
import 'package:flutter/material.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class CoinsTabScreen extends StatefulWidget {
  const CoinsTabScreen({super.key});

  @override
  State<CoinsTabScreen> createState() => _CoinsTabScreenState();
}

class _CoinsTabScreenState extends State<CoinsTabScreen> {
  // late BannerAd _bannerAd;
  @override
  void initState() {
    super.initState();
    // _bannerAd = context.read<AdProvider>().createBannerAd();
    // Provider.of<CoinsWalletDetailProvider>(context, listen: false)
    //     .coinsWalletDetail();
    Provider.of<CoinsWalletDetailTwoProvider>(context, listen: false)
        .coinsWalletDetailTwo();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    CoinsController.dispose();
  }

  TextEditingController CoinsController = TextEditingController();
  int coinValue = 0;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: white,
      // resizeToAvoidBottomInset: true,
      body: SafeArea(
        child: SingleChildScrollView(
            child: Consumer2<CoinsWalletDetailTwoProvider, UserDataProvider>(
          builder:
              (context, coisWalletDetailTwoProvider, userDataProvider, child) {
            final getCoinsWalletDetailTwo =
                coisWalletDetailTwoProvider.getCoinWalletDetailTwo;

            final userData = userDataProvider.userData;

            return Container(
              margin: const EdgeInsets.symmetric(horizontal: 2),
              padding: const EdgeInsets.all(10),
              width: double.infinity,
              decoration: const BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30))),
              child: getCoinsWalletDetailTwo == null || userData == null
                  ? const Center(child: CircularProgressIndicator())
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        SizedBox(
                        height: 10.0,
                      ),
                        // 10.heightBox,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text("Current Coins", style: normaltextBlackC),
                          ],
                        ),
                        SizedBox(
                        height: 5.0,
                      ),
                        // 5.heightBox,
                        Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              '${getCoinsWalletDetailTwo.coinsWallet.balance ?? ''}',
                              style: const TextStyle(
                                  color: Color.fromRGBO(
                                      20, 77, 70, 1.0), // or any other color
                                  decoration: TextDecoration.none,
                                  fontSize: 35,
                                  fontWeight:
                                      FontWeight.bold // remove underline
                                  ),
                            ),
                            const SizedBox(
                              width: 5.0,
                            ),
                            CoinsPng(),
                          ],
                        ),
                        SizedBox(
                        height: 10.0,
                      ),
                        // 10.heightBox,
                        Card(
                          margin: EdgeInsets.all(8.0),
                          elevation: 10,
                          color: white,
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              children: [
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          // width: 280,
                                          // color: lightBlack,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Joining Reward:  ",
                                                style: const TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color

                                                    fontSize: 15,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.joining ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color

                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                        height: 10.0,
                      ),
                                // 10.heightBox,
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          // width: 280,
                                          // color: lightBlack,
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Community Reward:  ",
                                                style: const TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color

                                                    fontSize: 15,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.community ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color

                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                        height: 10.0,
                      ),
                                // 10.heightBox,
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Prayers Reward:  ",
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color

                                                    fontSize: 15,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.prayers ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color

                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                        height: 10.0,
                      ),
                                // 10.heightBox,
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Quran Reward:  ",
                                                style: TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color

                                                    fontSize: 15,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.quran ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color

                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                        height: 10.0,
                      ),
                                // 10.heightBox,
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Tasbeeh Reward:  ",
                                                style: const TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color

                                                    fontSize: 15,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.tasbeeh ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color

                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                        height: 10.0,
                      ),
                                // 10.heightBox,
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Tasks Reward:  ",
                                                style: const TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color

                                                    fontSize: 15,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.tasks ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color

                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                SizedBox(
                        height: 10.0,
                      ),
                                // 10.heightBox,
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Videos Reward:  ",
                                                style: const TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color

                                                    fontSize: 15,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.videos ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color

                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),SizedBox(
                        height: 10.0,
                      ),
                                // 10.heightBox,
                                Padding(
                                  padding: const EdgeInsets.only(right: 5.0),
                                  child: Row(
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        child: Container(
                                          child: Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            children: [
                                              Text(
                                                "Total Earning:  ",
                                                style: const TextStyle(
                                                    color: Color.fromRGBO(
                                                        20,
                                                        77,
                                                        70,
                                                        1.0), // or any other color
                                                    decoration:
                                                        TextDecoration.none,
                                                    fontSize: 19,
                                                    fontWeight: FontWeight
                                                        .bold // remove underline
                                                    ),
                                              ),
                                              Container(
                                                child: Row(
                                                  children: [
                                                    Text(
                                                      "${getCoinsWalletDetailTwo.coinsWallet.total ?? ''}",
                                                      style: const TextStyle(
                                                          color: Color.fromRGBO(
                                                              20,
                                                              77,
                                                              70,
                                                              1.0), // or any other color
                                                          decoration:
                                                              TextDecoration
                                                                  .none,
                                                          fontSize: 14.5,
                                                          fontWeight: FontWeight
                                                              .bold // remove underline
                                                          ),
                                                    ),
                                                    const SizedBox(
                                                      width: 5.0,
                                                    ),
                                                    CoinsPng(),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),SizedBox(
                        height: 20.0,
                      ),
                                // 20.heightBox,
                              ],
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: InkWell(
                            onTap: () {
                              _loadAndShowInterstitialAd(context);
                              Get.to(LedgerScreen());
                            },
                            child: Container(
                              height: MediaQuery.of(context).size.height * 0.06,
                              width: 150,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: primaryColor),
                              child: Center(
                                child: Text(
                                  'Wallet History',
                                  style: TextStyle(
                                      color: white,
                                      fontSize: 15,
                                      fontWeight: FontWeight.w500),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(
                          height: 10.0,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 20.0),
                          child: Text(
                            'Note: 1000 coins is equal to 1\$.',
                            style: TextStyle(color: greenC, fontSize: 14),
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 10.0),
                          child: Container(
                            height: 70,
                            width: MediaQuery.of(context).size.width,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(15.0),
                              // color: lightBlack,
                            ),
                            child: Row(
                              children: [
                                Container(
                                  // color: red,
                                  height:
                                      MediaQuery.of(context).size.height * 0.06,
                                  width: 180,
                                  child: TextField(
                                    keyboardType:
                                        const TextInputType.numberWithOptions(
                                            decimal: false),
                                    controller: CoinsController,
                                    autofocus: false,
                                    style: const TextStyle(
                                        fontSize: 15.0, color: Colors.black),
                                    decoration: InputDecoration(
                                      border: InputBorder.none,
                                      hintText: 'Enter Coins',
                                      filled: true,
                                      // fillColor: Colors.black26,
                                      contentPadding: const EdgeInsets.only(
                                          left: 14.0, bottom: 6.0, top: 8.0),
                                      focusedBorder: OutlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: primaryColor),
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                      enabledBorder: UnderlineInputBorder(
                                        borderSide: const BorderSide(
                                            color: Colors.grey),
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                      ),
                                    ),
                                  ),
                                ),
                                InkWell(
                                  onTap: () async {
                                    int coinValue =
                                        int.tryParse(CoinsController.text) ?? 0;
                                    double cash = coinValue / 1000;

                                    if (CoinsController.text.isEmpty) {
                                      CustomSnackbar.error(
                                          message: 'Please enter coins');
                                    } else if (coinValue >
                                        (getCoinsWalletDetailTwo
                                                .coinsWallet.balance ??
                                            0)) {
                                      CustomSnackbar.error(
                                          message: 'Insufficient coins.');
                                      CoinsController.clear();
                                    } else {
                                      final coinsConverted =
                                          Provider.of<CoinsConvertProvider>(
                                              context,
                                              listen: false);
                                      final coinsC = CoinsConvertModal(
                                          coins_to_convert: coinValue);
                                      await coinsConverted
                                          .coinsConversion(coinsC);
                                      CoinsController.clear();
                                      CustomSnackbar.successful(
                                          message: 'You got \$ $cash cash');

                                      Provider.of<CoinsWalletDetailTwoProvider>(
                                              context,
                                              listen: false)
                                          .coinsWalletDetailTwo();
                                    }
                                  },
                                  child: Container(
                                    decoration: BoxDecoration(
                                        borderRadius:
                                            BorderRadius.circular(10.0),
                                        color: primaryColor),
                                    height: MediaQuery.of(context).size.height *
                                        0.06,
                                    width: 140,
                                    margin:
                                        EdgeInsets.only(left: 15, right: 15),
                                    child: Center(
                                      child: Text(
                                        'Convert Coins',
                                        style: TextStyle(
                                            color: white,
                                            fontSize: 15,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    ),
                                    // child: Container(
                                    //   // decoration: BoxDecoration(
                                    //   //     borderRadius: BorderRadius.only(
                                    //   //         bottomRight: Radius.circular(10.0),
                                    //   //         topRight: Radius.circular(10.0))),
                                    //   height:
                                    //       MediaQuery.of(context).size.height * 0.06,
                                    //   width: 170,
                                    //   child: CustomPaint(
                                    //     painter: BezierPainter(),
                                    //     child: Center(
                                    //       child: Text(
                                    // 'Convert Coins',
                                    // style: TextStyle(
                                    //     color: white,
                                    //     fontSize: 15,
                                    //     fontWeight: FontWeight.w500),
                                    //       ),
                                    //     ),
                                    //   ),
                                    // ),
                                  ),
                                ),
                                // InkWell(
                                //   child: Container(
                                //     height:
                                //         MediaQuery.of(context).size.height * 0.06,
                                //     width:
                                //         MediaQuery.of(context).size.width / 2.2,
                                //     decoration: BoxDecoration(
                                //         borderRadius: BorderRadius.circular(30.0),
                                //         color: primaryColor),
                                //     child: const Center(
                                // child: Text(
                                //   'Convert Coins',
                                //   style: TextStyle(
                                //       color: white,
                                //       fontSize: 15,
                                //       fontWeight: FontWeight.w500),
                                // ),
                                //     ),
                                //   ),
                                // onTap: () async {
                                //   int coinValue =
                                //       int.tryParse(CoinsController.text) ?? 0;
                                //   double cash = coinValue / 1000;

                                //   if (CoinsController.text.isEmpty) {
                                //     CustomSnackbar.error(
                                //         message: 'Please enter coins');
                                //   } else if (coinValue >
                                //       (getCoinsWalletDetailTwo
                                //               .coinsWallet.balance ??
                                //           0)) {
                                //     CustomSnackbar.error(
                                //         message: 'Insufficient coins.');
                                //     CoinsController.clear();
                                //   } else {
                                //     final coinsConverted =
                                //         Provider.of<CoinsConvertProvider>(
                                //             context,
                                //             listen: false);
                                //     final coinsC = CoinsConvertModal(
                                //         coins_to_convert: coinValue);
                                //     await coinsConverted
                                //         .coinsConversion(coinsC);
                                //     CoinsController.clear();
                                //     CustomSnackbar.successful(
                                //         message: 'You got \$ $cash cash');

                                //     Provider.of<CoinsWalletDetailTwoProvider>(
                                //             context,
                                //             listen: false)
                                //         .coinsWalletDetailTwo();
                                //   }
                                // },
                                // ),

                                //  above
                              ],
                            ),
                          ),
                        ),
                        // Container(
                        //   width: MediaQuery.of(context).size.width,
                        //   child: Column(
                        //     children: [
                        //       Padding(
                        //         padding: const EdgeInsets.symmetric(
                        //             horizontal: 8, vertical: 15),
                        //         child:

                        //     TextField(
                        //   keyboardType:
                        //       const TextInputType.numberWithOptions(
                        //           decimal: false),
                        //   controller: CoinsController,
                        //   autofocus: false,
                        //   style: const TextStyle(
                        //       fontSize: 15.0, color: Colors.black),
                        //   decoration: InputDecoration(
                        //     border: InputBorder.none,
                        //     hintText: 'Enter Coins',
                        //     filled: true,
                        //     fillColor: Colors.black26,
                        //     contentPadding: const EdgeInsets.only(
                        //         left: 14.0, bottom: 6.0, top: 8.0),
                        //     focusedBorder: OutlineInputBorder(
                        //       borderSide:
                        //           const BorderSide(color: primaryColor),
                        //       borderRadius: BorderRadius.circular(10.0),
                        //     ),
                        //     enabledBorder: UnderlineInputBorder(
                        //       borderSide:
                        //           const BorderSide(color: Colors.grey),
                        //       borderRadius: BorderRadius.circular(10.0),
                        //     ),
                        //   ),
                        // ),
                        //       ),
                        //       Row(
                        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        //         children: [
                        // InkWell(
                        //   child: Container(
                        //     height:
                        //         MediaQuery.of(context).size.height *
                        //             0.05,
                        //     width: MediaQuery.of(context).size.width /
                        //         2.2,
                        //     decoration: BoxDecoration(
                        //         borderRadius:
                        //             BorderRadius.circular(8.0),
                        //         color: primaryColor),
                        //     child: const Center(
                        //       child: Text(
                        //         'Convert Coins',
                        //         style: TextStyle(
                        //             color: white,
                        //             fontSize: 15,
                        //             fontWeight: FontWeight.w500),
                        //       ),
                        //     ),
                        //   ),
                        //   onTap: () async {
                        //     int coinValue =
                        //         int.tryParse(CoinsController.text) ??
                        //             0;
                        //     double cash = coinValue / 1000;

                        //     if (CoinsController.text.isEmpty) {
                        //       CustomSnackbar.error(
                        //           message: 'Please enter coins');
                        //     } else if (coinValue >
                        //         (getCoinsWalletDetailTwo
                        //                 .coinsWallet.balance ??
                        //             0)) {
                        //       CustomSnackbar.error(
                        //           message: 'Insufficient coins.');
                        //       CoinsController.clear();
                        //     } else {
                        //       final coinsConverted =
                        //           Provider.of<CoinsConvertProvider>(
                        //               context,
                        //               listen: false);
                        //       final coinsC = CoinsConvertModal(
                        //           coins_to_convert: coinValue);
                        //       await coinsConverted
                        //           .coinsConversion(coinsC);
                        //       CoinsController.clear();
                        //       CustomSnackbar.successful(
                        //           message: 'You got \$ $cash cash');

                        //       Provider.of<CoinsWalletDetailTwoProvider>(
                        //               context,
                        //               listen: false)
                        //           .coinsWalletDetailTwo();
                        //     }
                        //   },
                        // ),

                        //         ],
                        //       ),
                        //     ],
                        //   ),
                        // ),
                      ],
                    ),
            );
          },
        )),
      ),
    );
  }
  // Row(
  //   mainAxisAlignment: MainAxisAlignment.start,
  //   children: [
  //     20.widthBox,
  //     Image.asset(
  //       'assets/icons/icon4.png',
  //       height: 50,
  //       width: 50,
  //     ),
  //     20.widthBox,
  //     // Text(
  //     //   "Total Withdraw: \$ ${getWalletDetail.wallet.withdraw ?? ''}",
  //     //   style: TextStyle(
  //     //       color: Color.fromRGBO(
  //     //           20, 77, 70, 1.0), // or any other color
  //     //       decoration: TextDecoration.none,
  //     //       fontSize: 19,
  //     //       fontWeight: FontWeight.bold // remove underline
  //     //       ),
  //     // ),
  //   ],
  // ),
  // SizedBox(
  //   height: context.screenWidth * .1,
  // ),
  // Row(
  //   mainAxisAlignment: MainAxisAlignment.center,
  //   children: [
  //     GestureDetector(
  //       onTap: () {
  //         Get.to(WalletwithdrawScreen());
  //       },
  //       child: Image(
  //           height: 52,
  //           width: 140,
  //           image: AssetImage('assets/images/withdraw.png')),
  //     ),
  //     SizedBox(
  //       width: context.screenWidth * .1,
  //     ),
  //     GestureDetector(
  //       onTap: () {
  //         Get.to(DonateScreen());
  //       },
  //       child: Image(
  //           height: 52,
  //           width: 140,
  //           image: AssetImage('assets/images/donate.png')),
  //     ),
  //   ],
  // )
  // ],
  // ), // Adjust the radius as needed
  // ),
  // ),
// Container(
//   height: double.infinity,
//   width: double.infinity,
//   color: Color.fromRGBO(20, 77, 70, 1.0),
//   child: Column(
//     children: [
//       // Padding(
//       //   padding: const EdgeInsets.only(left: 10, right: 10),
//       //   child: Container(
//       //     height: 60,
//       //     child: Row(
//       //       children: [
//       //         GestureDetector(
//       //             onTap: () {
//       //               Navigator.pop(context);
//       //             },
//       //             child: Padding(
//       //               padding: const EdgeInsets.only(left: 5.0, top: 20),
//       //               child: Container(
//       //                 height: 30,
//       //                 width: 30,
//       //                 decoration: BoxDecoration(
//       //                     shape: BoxShape.circle,
//       //                     // color: lightBlack,
//       //                     border: Border.all(color: white, width: 2)),
//       //                 child: Center(
//       //                   child: Icon(Icons.arrow_back,
//       //                       color: white, size: 20),
//       //                 ),
//       //               ),
//       //             )),
//       //         SizedBox(
//       //           width: 100,
//       //         ),
//       //         Padding(
//       //           padding: const EdgeInsets.only(top: 20),
//       //           child: Row(
//       //             mainAxisAlignment: MainAxisAlignment.center,
//       //             children: [
//       //               Text(
//       //                 "My Wallet",
//       //                 style: TextStyle(
//       //                     color: Color.fromRGBO(
//       //                         252, 216, 138, 1.0), // or any other color
//       //                     decoration: TextDecoration.none,
//       //                     fontSize: 22 // remove underline
//       //                     ),
//       //               ),
//       //               20.widthBox,
//       //               Image.asset(
//       //                 'assets/icons/wallet1.png',
//       //                 height: 30,
//       //                 width: 30,
//       //               )
//       //             ],
//       //           ),
//       //         ),
//       //       ],
//       //     ),
//       //   ),
//       // ),
//       // Container(
//       //   width: double.infinity,
//       //   height: 20,
//       //   color: Color(0xff144d46),
//       // ),
//       // Expanded(
//       //   child:
//       // ),
//       // Container(
//       //   width: MediaQuery.of(context).size.width,
//       //   height: 60,
//       //   child: AdWidget(ad: _bannerAd),
//       // ),
//     ],
//   ), // Set the background color here
// ),

  void _loadAndShowInterstitialAd(BuildContext context) {
    // Access the AdProvider using the Provider.of method
    AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

    // Load the interstitial ad
    adProvider.loadInterstitialAd();

    // Check if the interstitial ad is loaded
    if (adProvider.isInterstitialAdLoaded) {
      // resetNumber == 0;
      // Show the interstitial ad
      adProvider.showInterstitialAd();
    } else {
      // The ad is not loaded yet, you can handle this case accordingly
      print('Interstitial ad is not loaded yet. Please try again later.');
    }
  }
}

class BezierPainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()
      ..color = primaryColor
      ..style = PaintingStyle.fill;

    Path path = Path()
      ..moveTo(0, 0)
      ..lineTo(size.width, 0)
      ..lineTo(size.width, size.height)
      ..lineTo(0, size.height)
      ..quadraticBezierTo(size.width * 0.15, size.height * 0.5, 0, 0);

    canvas.drawPath(path, paint);
  }

  @override
  bool shouldRepaint(covariant CustomPainter oldDelegate) {
    return false;
  }
}
