import 'package:aas/Model/MLM%20modals/donate_to_other_modal.dart';
import 'package:aas/Provider/MLM/donate_to_other_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

class DonateScreen extends StatefulWidget {
  @override
  State<DonateScreen> createState() => _DonateScreenState();
}

class _DonateScreenState extends State<DonateScreen> {
  TextEditingController amountController = TextEditingController();
  TextEditingController idController = TextEditingController();
  TextEditingController pinController = TextEditingController();

  late BannerAd _bannerAd;

  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
  }

  @override
  Widget build(BuildContext context) {
      final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Color.fromRGBO(20, 77, 70, 1.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5.0, top: 20),
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  // color: lightBlack,
                                  border: Border.all(color: white, width: 2)),
                              child: Center(
                                child: Icon(Icons.arrow_back,
                                    color: white, size: 20),
                              ),
                            ),
                          )),
                      Padding(
                        padding: const EdgeInsets.only(top: 20),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Donate',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(252, 216, 138, 1.0),
                                fontSize: 25,
                              ),
                            ),
                          ],
                        ),
                      ),
                      InkWell(
                        onTap: () {
                          dialogeShow(context);
                        },
                        child: Padding(
                          padding: const EdgeInsets.only(top: 20, right: 20),
                          child: Row(
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: [
                              FaIcon(
                                CupertinoIcons.info,
                                color: Colors.white,
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: 20,
                color: Color(0xff144d46),
              ),
              Expanded(
                child: Container(
                  padding: EdgeInsets.all(10),
                  width: double.infinity,
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(30),
                        topRight: Radius.circular(30)),
                  ),
                  child: Column(
                    children: [
                      SizedBox(
                        height: screenHeight * .1,
                      ),
                      Container(
                        margin: EdgeInsets.symmetric(
                            horizontal: screenWidth * .06),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Text(
                              "Amount",
                              style: TextStyle(
                                  color: Color.fromRGBO(
                                      20, 77, 70, 1.0), // or any other color
                                  fontWeight: FontWeight.bold,
                                  fontSize: 27 // remove underline
                                  ),
                            ),
                            SizedBox(
                              height: screenHeight * .04,
                              width: screenWidth * .5,
                              child: TextFormField(
                                style: TextStyle(color: black),
                                controller: amountController,
                                decoration: InputDecoration(
                                  focusedBorder: OutlineInputBorder(
                                      borderSide: BorderSide(color: black)),
                                  hintText: "amount",
                                  contentPadding: EdgeInsets.symmetric(
                                      vertical: 2.0, horizontal: 16),
                                  border: OutlineInputBorder(
                                      borderSide: BorderSide(
                                    color: black,
                                  )),
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: screenHeight * .06,
                      ),
                      Container(
                        height: screenHeight * .05,
                        width: screenWidth * .8,
                        child: TextFormField(
                          style: TextStyle(color: white),
                          controller: idController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: black)),
                              hintText: "Receiver id",
                              hintStyle: TextStyle(color: Colors.grey),
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 16),
                              filled: true,
                              fillColor: primaryColor,
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: black))),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight * .03,
                      ),
                      Container(
                        height: screenHeight * .05,
                        width: screenWidth * .8,
                        child: TextFormField(
                          obscureText: true,
                          style: TextStyle(color: white),
                          controller: pinController,
                          decoration: InputDecoration(
                              focusedBorder: OutlineInputBorder(
                                  borderSide: BorderSide(color: black)),
                              filled: true,
                              hintText: "Pin",
                              contentPadding: EdgeInsets.symmetric(
                                  vertical: 10.0, horizontal: 16),
                              hintStyle: TextStyle(color: Colors.grey),
                              fillColor: primaryColor,
                              border: OutlineInputBorder(
                                  borderSide: BorderSide(color: black))),
                        ),
                      ),
                      SizedBox(
                        height: screenHeight * .06,
                      ),
                      InkWell(
                        onTap: () {
                          if (amountController.text.isEmpty &&
                              idController.text.isEmpty &&
                              pinController.text.isEmpty) {
                            CustomSnackbar.error(
                                message: 'Please fill all fields');
                          } else if (pinController.length > 4 ||
                              pinController.length < 4) {
                            CustomSnackbar.error(
                                message: 'Pin must be 4 digits');
                          } else {
                            final DTO = Provider.of<DonationToOther>(context,
                                listen: false);
                            final donationO = DonateToOtherModal(
                                amount: int.parse(amountController.text.trim()),
                                tpin: pinController.text.trim(),
                                user_id: idController.text.trim());
                            DTO.doationOther(donationO, context);
                          }
                        },
                        child: Container(
                          height: 45,
                          child: Image.asset('assets/icons/ic_submit.png'),
                        ),
                      ),
                    ],
                  ), // Adjust the radius as needed
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
            ],
          ), // Set the background color here
        ),
      ),
    );
  }

  dialogeShow(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(5.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(5.0),
            content: Container(
              height: 90.0,
              width: screenSize.width * .5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      "This section allows you to donate the amount to any other AAS user",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }
}
