
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/coins_tab_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/cash_wallet_tab.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

import '../../../../Constants/colors.dart';

class WalletScreen extends StatefulWidget {
  
  @override
  State<WalletScreen> createState() => _WalletScreenState();
}

class _WalletScreenState extends State<WalletScreen> with SingleTickerProviderStateMixin {
  late BannerAd _bannerAd;
  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    // Provider.of<walletDetailProvider>(context, listen: false).walletDetail();
    _tabController = TabController(length: 2, vsync: this);

    _tabController.addListener(() {
      setState(() {});
    });
  }
   late TabController _tabController;

  @override
  void dispose() {
    _tabController.dispose();
    super.dispose();
  }


  @override
  Widget build(BuildContext context) {  final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
    return Scaffold(
      backgroundColor: white,
      // appBar: AppBar(centerTitle: true,),
      body: SafeArea(
        
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: primaryColor,
          child: Column(
            children: [
                 Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: 60,
                  child: Row(
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5.0, top: 20),
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  // color: lightBlack,
                                  border: Border.all(color: white, width: 2)),
                              child: Center(
                                child: Icon(Icons.arrow_back,
                                    color: white, size: 20),
                              ),
                            ),
                          )),
                      SizedBox(
                        width: 70,
                      ),
                      Padding(
                        padding: const EdgeInsets.only(top: 20,),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                          children: [
                            Text(
                              "My Wallet",
                              style: TextStyle(
                                  color: Color.fromRGBO(
                                      252, 216, 138, 1.0), // or any other color
                                  decoration: TextDecoration.none,
                                  fontSize: 22 // remove underline
                                  ),
                            ),
                              SizedBox(
                            width: 20.0,
                          ),
                            // 20.widthBox,
                            Image.asset(
                              'assets/icons/wallet1.png',
                              height: 30,
                              width: 30,
                            ),
                              SizedBox(
                            width: 50.0,
                          ),
                            // 50.widthBox,
                            InkWell(
                                onTap: (){
                                  dialogeShow(context);
                                },
                                child: FaIcon(CupertinoIcons.info, color: Colors.white,)),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 20.0,),
              // Container(
              //   child: Column(
              //     children: [
              //       Center(
              //         child: Text(
              //           'رَبِّ انْصُرْنِیْ عَلَی الْقَوْمِ الْمُفْسِدِیْنَِ',
              //           style: TextStyle(fontSize: 20, color: Colors.white),
              //         ),
              //       ),
              //       Center(
              //         child: Text(
              //           'Lord, help me against the corrupt people',
              //           style: TextStyle(fontSize: 13, color: Colors.white),
              //         ),
              //       ),
              //       SizedBox(
              //         height: context.screenHeight * .009,
              //       ),
              //       Center(
              //         child: Text(
              //           'The Quran 29:30',
              //           style: TextStyle(
              //               fontSize: 13, color: Colors.yellow.withOpacity(.6)),
              //         ),
              //       ),
              //       SizedBox(
              //         height: 20,
              //       )
              //     ],
              //   ),
              // ),
              Expanded(
                child: Container(
                  width: double.infinity,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(30),
                          topRight: Radius.circular(30))),
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30.0,
                      ),
                      // 30.heightBox,
                      Container(
                        height: screenHeight * .05,
                        width: screenWidth * .8,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(10),
                        ),
                        child: Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            border: Border.all(
                              color: Color(0xFF111111),
                            ),
                          ),
                          child: TabBar(
                            labelColor: white,
                            indicatorSize: TabBarIndicatorSize.tab,
                            indicator: BoxDecoration(
                              color: Color.fromRGBO(20, 77, 70, 1.0),
                              borderRadius: BorderRadius.circular(10),
                            ),
                            unselectedLabelColor: Color(0xFFABA9A9),
                            controller: _tabController,
                            tabs: [
                              Tab(text: 'Coins Wallet'),
                              Tab(text: 'Cash Wallet'),
                            ],
                          ),
                        ),
                      ),
                      Expanded(
                        child: TabBarView(
                          controller: _tabController,
                          children: [
                            
                          CoinsTabScreen(),
                            CashTabScreen()
                            
                            ],
                        ),
                      ),
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 60,
                        child: AdWidget(ad: _bannerAd),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      //  SafeArea(
      //   child: Container(
      //     height: double.infinity,
      //     width: double.infinity,
      //     color: Color.fromRGBO(20, 77, 70, 1.0),
      //     child: Column(
      //       children: [
      //         Padding(
      //           padding: const EdgeInsets.only(left: 10, right: 10),
      //           child: Container(
      //             height: 60,
      //             child: Row(
      //               children: [
      //                 GestureDetector(
      //                     onTap: () {
      //                       Navigator.pop(context);
      //                     },
      //                     child: Padding(
      //                       padding: const EdgeInsets.only(left: 5.0, top: 20),
      //                       child: Container(
      //                         height: 30,
      //                         width: 30,
      //                         decoration: BoxDecoration(
      //                             shape: BoxShape.circle,
      //                             // color: lightBlack,
      //                             border: Border.all(color: white, width: 2)),
      //                         child: Center(
      //                           child: Icon(Icons.arrow_back,
      //                               color: white, size: 20),
      //                         ),
      //                       ),
      //                     )),
      //                 SizedBox(
      //                   width: 100,
      //                 ),
      //                 Padding(
      //                   padding: const EdgeInsets.only(top: 20),
      //                   child: Row(
      //                     mainAxisAlignment: MainAxisAlignment.center,
      //                     children: [
      //                       Text(
      //                         "My Wallet",
      //                         style: TextStyle(
      //                             color: Color.fromRGBO(
      //                                 252, 216, 138, 1.0), // or any other color
      //                             decoration: TextDecoration.none,
      //                             fontSize: 22 // remove underline
      //                             ),
      //                       ),
      //                       20.widthBox,
      //                       Image.asset(
      //                         'assets/icons/wallet1.png',
      //                         height: 30,
      //                         width: 30,
      //                       )
      //                     ],
      //                   ),
      //                 ),
      //               ],
      //             ),
      //           ),
      //         ),
      //         Container(
      //           width: double.infinity,
      //           height: 20,
      //           color: Color(0xff144d46),
      //         ),
      //         Expanded(
      //           child: Container(
      //             margin: EdgeInsets.symmetric(horizontal: 2),
      //             padding: EdgeInsets.all(10),
      //             width: double.infinity,
      //             decoration: BoxDecoration(
      //                 color: Colors.white,
      //                 borderRadius: BorderRadius.only(
      //                     topLeft: Radius.circular(30),
      //                     topRight: Radius.circular(30))),
      //             child:
      //           getWalletDetail == null || userData == null ?
      //           Center(child: CircularProgressIndicator())
      //           :
      //              Column(
      //               children: [
      //                 30.heightBox,
      //                 Row(
      //                   mainAxisAlignment: MainAxisAlignment.center,
      //                   children: [
      //                     Text("Total Amount", style: normaltextBlackC),
      //                   ],
      //                 ),
      //                 5.heightBox,
      //                 Text(
      //                   '\$ ${userData!.balance ?? ''}',
      //                   style: TextStyle(
      //                       color: Color.fromRGBO(
      //                           20, 77, 70, 1.0), // or any other color
      //                       decoration: TextDecoration.none,
      //                       fontSize: 35,
      //                       fontWeight: FontWeight.bold // remove underline
      //                       ),
      //                 ),
      //                 30.heightBox,
      //                 Row(
      //                   mainAxisAlignment: MainAxisAlignment.start,
      //                   children: [
      //                     20.widthBox,
      //                     Image.asset(
      //                       'assets/icons/icon 1.png',
      //                       height: 50,
      //                       width: 50,
      //                     ),
      //                     20.widthBox,
      //                     Text(
      //                       "Earned by Community: \$ ${getWalletDetail!.wallet.communityEarning ?? ''}",
      //                       style: TextStyle(
      //                           color: Color.fromRGBO(
      //                               20, 77, 70, 1.0), // or any other color

      //                           fontSize: 19,
      //                           fontWeight: FontWeight.bold // remove underline
      //                           ),
      //                     ),
      //                   ],
      //                 ),
      //                 10.heightBox,
      //                 Row(
      //                   mainAxisAlignment: MainAxisAlignment.start,
      //                   children: [
      //                     20.widthBox,
      //                     Image.asset(
      //                       'assets/icons/icon 2.png',
      //                       height: 50,
      //                       width: 50,
      //                     ),
      //                     20.widthBox,
      //                     Text(
      //                       "Earned by Tasks: \$ ${getWalletDetail.wallet.tasksEarning ?? ''}",
      //                       style: TextStyle(
      //                           color: Color.fromRGBO(
      //                               20, 77, 70, 1.0), // or any other color

      //                           fontSize: 19,
      //                           fontWeight: FontWeight.bold // remove underline
      //                           ),
      //                     ),
      //                   ],
      //                 ),
      //                 10.heightBox,
      //                 Row(
      //                   mainAxisAlignment: MainAxisAlignment.start,
      //                   children: [
      //                     20.widthBox,
      //                     Image.asset(
      //                       'assets/icons/icon3.png',
      //                       height: 50,
      //                       width: 50,
      //                     ),
      //                     20.widthBox,
      //                     Text(
      //                       "Total Earning: \$ ${getWalletDetail.wallet.totalEarning ?? ''}",
      //                       style: TextStyle(
      //                           color: Color.fromRGBO(
      //                               20, 77, 70, 1.0), // or any other color
      //                           decoration: TextDecoration.none,
      //                           fontSize: 19,
      //                           fontWeight: FontWeight.bold // remove underline
      //                           ),
      //                     ),
      //                   ],
      //                 ),
      //                 10.heightBox,
      //                 Row(
      //                   mainAxisAlignment: MainAxisAlignment.start,
      //                   children: [
      //                     20.widthBox,
      //                     Image.asset(
      //                       'assets/icons/icon4.png',
      //                       height: 50,
      //                       width: 50,
      //                     ),
      //                     20.widthBox,
      //                     Text(
      //                       "Total Withdraw: \$ ${getWalletDetail.wallet.withdraw ?? ''}",
      //                       style: TextStyle(
      //                           color: Color.fromRGBO(
      //                               20, 77, 70, 1.0), // or any other color
      //                           decoration: TextDecoration.none,
      //                           fontSize: 19,
      //                           fontWeight: FontWeight.bold // remove underline
      //                           ),
      //                     ),
      //                   ],
      //                 ),
      //                 SizedBox(
      //                   height: context.screenWidth * .1,
      //                 ),
      //                 Row(
      //                   mainAxisAlignment: MainAxisAlignment.center,
      //                   children: [
      //                     GestureDetector(
      //                       onTap: () {
      //                         Get.to(WalletwithdrawScreen());
      //                       },
      //                       child: Image(
      //                           height: 52,
      //                           width: 140,
      //                           image:
      //                               AssetImage('assets/images/withdraw.png')),
      //                     ),
      //                     SizedBox(
      //                       width: context.screenWidth * .1,
      //                     ),
      //                     GestureDetector(
      //                       onTap: () {
      //                         Get.to(DonateScreen());
      //                       },
      //                       child: Image(
      //                           height: 52,
      //                           width: 140,
      //                           image: AssetImage('assets/images/donate.png')),
      //                     ),
      //                   ],
      //                 )
      //               ],
      //             ), // Adjust the radius as needed
      //           ),
      //         ),
      //         Container(
      //           width: MediaQuery.of(context).size.width,
      //           height: 60,
      //           child: AdWidget(ad: _bannerAd),
      //         ),
      //       ],
      //     ), // Set the background color here
      //   ),
      // ),
    );
  }

  
  dialogeShow(BuildContext context) {

    return showDialog(
        context: context,

        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(

            insetPadding: const EdgeInsets.all(5.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(5.0),
            content: Container(
              height: 80.0,
              width: screenSize.width * .5,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Column(


                children: <Widget>[


                  SizedBox(height: 5,),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(Icons.info, color: primaryColor,),
                    ],
                  ),
                  SizedBox(height: 10,),

                  Padding(
                    padding: const EdgeInsets.only(left: 10, right: 10),
                    child: Text(
                      "You may convert your coins in to real money.",
                      //                     style: SC.appNormalText,
                      style:TextStyle(fontSize:11),

                    ),
                  ),




                ],
              ),
            ),
          );
        });
  }
}
