import 'package:aas/Model/MLM%20modals/cash_wallet_modal.dart';
import 'package:aas/Model/MLM%20modals/cash_withdraw_modal.dart';
import 'package:aas/Model/MLM%20modals/set_t_pin_modal.dart';
import 'package:aas/Provider/MLM/cash_withdraw_provider.dart';
import 'package:aas/Provider/MLM/forget_trans_pin_provider.dart';
import 'package:aas/Provider/MLM/set_trans_pin_provider.dart';
import 'package:aas/Provider/MLM/Transactions%20Provider/cash_wallet_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/settings_screen.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:intl/intl.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';  final double screenHeight = MediaQuery.of(context).size.height;

class WalletwithdrawScreen extends StatefulWidget {
  @override
  State<WalletwithdrawScreen> createState() => _WalletwithdrawScreenState();
}

class _WalletwithdrawScreenState extends State<WalletwithdrawScreen> {
  late BannerAd _bannerAd;
  @override
  void initState() {
    super.initState();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    Provider.of<CashWalletDetailProvider>(context, listen: false)
        .cashWalletDetail();
  }

  List<Map<String, dynamic>> options = [
    {"menu": "Forget PIN"},
  ];

  void dispose() {
    // TODO: implement dispose
    super.dispose();
    enterAmountController.dispose();
    bankNameController.dispose();
    accountTitleController.dispose();
    accountNumberController.dispose();
    phoneNoController.dispose();
    pinController.dispose();
  }

  TextEditingController enterAmountController = TextEditingController();
  TextEditingController bankNameController = TextEditingController();
  TextEditingController accountTitleController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController phoneNoController = TextEditingController();
  TextEditingController pinController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    // CashWallet? getCashWalletDetail =
    //     Provider.of<CashWalletDetailProvider>(context).getCashWalletDetail;
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: SafeArea(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: const Color.fromRGBO(20, 77, 70, 1.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 10, right: 10),
                child: Container(
                  height: 60,
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: [
                      GestureDetector(
                          onTap: () {
                            Navigator.pop(context);
                          },
                          child: Padding(
                            padding: const EdgeInsets.only(left: 5.0, top: 20),
                            child: Container(
                              height: 30,
                              width: 30,
                              decoration: BoxDecoration(
                                  shape: BoxShape.circle,
                                  // color: lightBlack,
                                  border: Border.all(color: white, width: 2)),
                              child: const Center(
                                child: Icon(Icons.arrow_back,
                                    color: white, size: 20),
                              ),
                            ),
                          )),
                      // const SizedBox(
                      //   width: 100,
                      // ),
                      const Padding(
                        padding: EdgeInsets.only(top: 20, right: 10),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'Withdraw',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                color: Color.fromRGBO(252, 216, 138, 1.0),
                                fontSize: 25,
                              ),
                            ),
                          ],
                        ),
                      ),
                      PopUp.ForgetPinDialogue(options, context),
                    ],
                  ),
                ),
              ),
              Container(
                width: double.infinity,
                height: 20,
                color: const Color(0xff144d46),
              ),
              Container(
                // height: 650,
                width: 700,
                color: white,
                child: Padding(
                  padding: const EdgeInsets.all(10.0),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      const SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: false),
                        controller: enterAmountController,
                        autofocus: false,
                        style: const TextStyle(fontSize: 15.0, color: white),
                        decoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: black)),
                            hintText: "Enter Amount",
                            hintStyle: TextStyle(color: Colors.white60),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 16),
                            filled: true,
                            fillColor: primaryColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: black))),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        keyboardType: TextInputType.text,
                        controller: bankNameController,
                        autofocus: false,
                        style: const TextStyle(fontSize: 15.0, color: white),
                        decoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: black)),
                            hintText: "Enter Bank Name",
                            hintStyle: TextStyle(color: Colors.white60),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 16),
                            filled: true,
                            fillColor: primaryColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: black))),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        keyboardType: TextInputType.name,
                        controller: accountTitleController,
                        autofocus: false,
                        style: const TextStyle(fontSize: 15.0, color: white),
                        decoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: black)),
                            hintText: "Enter Account Title",
                            hintStyle: TextStyle(color: Colors.white60),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 16),
                            filled: true,
                            fillColor: primaryColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: black))),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: false),
                        controller: accountNumberController,
                        autofocus: false,
                        style: const TextStyle(fontSize: 15.0, color: white),
                        decoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: black)),
                            hintText: "Enter Account number",
                            hintStyle: TextStyle(color: Colors.white60),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 16),
                            filled: true,
                            fillColor: primaryColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: black))),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: false),
                        controller: phoneNoController,
                        autofocus: false,
                        style: const TextStyle(fontSize: 15.0, color: white),
                        decoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: black)),
                            hintText: "Enter Phone No.",
                            hintStyle: TextStyle(color: Colors.white60),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 16),
                            filled: true,
                            fillColor: primaryColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: black))),
                      ),
                      const SizedBox(
                        height: 20.0,
                      ),
                      TextField(
                        obscureText: true,
                        keyboardType: const TextInputType.numberWithOptions(
                            decimal: false),
                        controller: pinController,
                        autofocus: false,
                        style: const TextStyle(fontSize: 15.0, color: white),
                        decoration: const InputDecoration(
                            focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: black)),
                            hintText: "Enter Your PIN",
                            hintStyle: TextStyle(color: Colors.white60),
                            contentPadding: EdgeInsets.symmetric(
                                vertical: 10.0, horizontal: 16),
                            filled: true,
                            fillColor: primaryColor,
                            border: OutlineInputBorder(
                                borderSide: BorderSide(color: black))),
                      ),
                      const SizedBox(height: 8.0),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          InkWell(
                              onTap: () {
                                dialogeShow(context);
                              },
                              child: FaIcon(
                                CupertinoIcons.info,
                                color: Colors.black,
                              )),
                          SizedBox(
                            width: 30.w,
                          ),
                          InkWell(
                            onTap: () {
                              setPinPopup(context);
                            },
                            child: const Text('Don\'t have PIN? Create one.',
                                style: TextStyle(
                                  decoration: TextDecoration.underline,
                                  color: red,
                                  decorationColor: red,
                                )),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: screenHeight * .06,
                      ),
                      Center(
                        child: GestureDetector(
                          onTap: () async {
                            int amount = int.parse(enterAmountController.text);
                            // print(pinController.text.length);
                            if (enterAmountController.text.isEmpty &&
                                bankNameController.text.isEmpty &&
                                accountNumberController.text.isEmpty &&
                                accountNumberController.text.isEmpty &&
                                pinController.text.isEmpty) {
                              CustomSnackbar.error(
                                  message: 'Please Fill all fields');
                            } else if (pinController.text.length > 4 ||
                                pinController.text.length < 4) {
                              CustomSnackbar.error(
                                  message: 'Pin must be 4 digits');
                            } else if (amount < 50) {
                              CustomSnackbar.error(
                                  message:
                                      'Amount must be equal or greater than 50\$');
                            } else {
                              final DateFormat todayformatter =
                                  DateFormat('yyyy-MM-dd');
                              final currentDate = DateTime.now();
                              final String currentDateIs =
                                  todayformatter.format(currentDate);
                              final cashWithdraw =
                                  Provider.of<CashWithdrawProvider>(context,
                                      listen: false);
                              final cashC = CashWithdrawModal(
                                  date: currentDateIs,
                                  amount: int.parse(enterAmountController.text),
                                  bankName: bankNameController.text,
                                  accountTitle: accountTitleController.text,
                                  accountNumber: accountNumberController.text,
                                  phoneNo: int.parse(phoneNoController.text),
                                  tpin: int.parse(pinController.text));
                              await cashWithdraw.cashWithdraw(cashC, context);
                            }
                          },
                          child: Container(
                            // width: 100,
                            height: 45,
                            // color: black,
                            child: Image.asset('assets/icons/ic_submit.png'),
                          ),
                        ),
                      )
                    ],
                  ),
                ),
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
            ],
          ), // Set the background color here
        ),
      ),
    );
  }

  dialogeShow(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          return AlertDialog(
            insetPadding: const EdgeInsets.all(5.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(5.0),
            content: Container(
              height: 90.0,
              width: 50,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(20.0),
              ),
              child: Column(
                children: <Widget>[
                  SizedBox(
                    height: 5,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 5, right: 5),
                    child: Text(
                      "You have to set your 4 digit PIN for the transactions.",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                      textAlign: TextAlign.center,
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  setPinPopup(BuildContext context) {
    TextEditingController setPinController = TextEditingController();

    // TextEditingController JoinCommunityController = TextEditingController();
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(30.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 170.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Padding(
                  padding: const EdgeInsets.symmetric(
                      horizontal: 20.0, vertical: 20.0),
                  child: Center(
                    child: Container(
                      // height: 60,
                      width: MediaQuery.of(context).size.width,
                      // color: lightBlack,
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          InkWell(
                              onTap: () {
                                Navigator.pop(context);
                              },
                              child: const Icon(Icons.close)),
                          const SizedBox(
                            height: 7.0,
                          ),
                          TextField(
                            obscureText: true,
                            keyboardType: const TextInputType.numberWithOptions(
                                decimal: false),
                            controller: setPinController,
                            autofocus: false,
                            style: const TextStyle(
                                fontSize: 15.0, color: Colors.black),
                            decoration: InputDecoration(
                              border: InputBorder.none,
                              hintText: 'Set PIN',
                              filled: true,
                              fillColor: Colors.black26,
                              contentPadding: const EdgeInsets.only(
                                  left: 14.0, bottom: 6.0, top: 8.0),
                              focusedBorder: OutlineInputBorder(
                                borderSide:
                                    const BorderSide(color: primaryColor),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                              enabledBorder: UnderlineInputBorder(
                                borderSide:
                                    const BorderSide(color: Colors.grey),
                                borderRadius: BorderRadius.circular(10.0),
                              ),
                            ),
                          ),
                          const SizedBox(
                            height: 10.0,
                          ),
                          InkWell(
                            onTap: () async {
                              int pin = int.parse(setPinController.text) ?? 0;
                              if (pin.toString().length == 4) {
                                print('pin length is: $pin');

                                final setPin =
                                    Provider.of<SetTransactionsPinProvider>(
                                        context,
                                        listen: false);
                                final setP = SetTPinModal(tpin: pin);
                                await setPin.setPin(setP);

                                setPinController.clear();
                                Navigator.pop(context);
                              } else {
                                CustomSnackbar.error(
                                    message: 'Pin must be 4 digits');
                              }
                            },
                            child: Container(
                              height: 40,
                              width: 100,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(8.0),
                                  color: primaryColor),
                              child: const Center(
                                child: Text(
                                  'Set Pin',
                                  style: TextStyle(color: white),
                                ),
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  )),
            ),
          );
        });
  }
}

class PopUp {
  static ForgetPinDialogue(
      List<Map<String, dynamic>> options, BuildContext context) {
    return PopupMenuButton(
      iconSize: 24.0,
      padding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      icon: Padding(
        padding: const EdgeInsets.only(top: 20.0),
        child: Icon(
          Icons.more_vert,
          color: white,
          size: 24.0,
        ),
      ),
      offset: Offset(0, 15),
      itemBuilder: (BuildContext context) {
        return options
            .map(
              (selectedOption) => PopupMenuItem(
                height: 30.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      selectedOption['menu'] ?? "",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14.0),
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        color: Colors.black,
                      ),
                    ),
                    options.length == options.indexOf(selectedOption) + 1
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 8.0,
                            ),
                            child: Divider(
                              color: Colors.grey,
                              height: ScreenUtil().setHeight(1.0),
                            ),
                          ),
                  ],
                ),
                value: selectedOption,
              ),
            )
            .toList();
      },
      onSelected: (value) async {
        resetPinPopup(context);
      },
    );
  }

  static Future<dynamic> resetPinPopup(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          final screenSize =
              MediaQuery.of(dialogContext).size; // Use dialogContext here
          return AlertDialog(
            insetPadding: const EdgeInsets.all(30.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 150.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    const SizedBox(
                      height: 1.0,
                    ),
                    Text(
                      "Are you sure? You want to reset your pin.",
                      style: TextStyle(color: black, fontSize: 16.0),
                      textAlign: TextAlign.center,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextButton(
                            style: TextButton.styleFrom(
                              foregroundColor: Colors.white,
                              padding: const EdgeInsets.all(16.0),
                              textStyle: const TextStyle(fontSize: 20),
                            ),
                            onPressed: () {
                              Provider.of<ForgetTransPinProvider>(context,
                                      listen: false)
                                  .forgetTransPin();
                              Navigator.pop(context);

                              // SystemNavigator.pop();
                            },
                            child: Container(
                              height: 33,
                              width: 63,
                              decoration: BoxDecoration(
                                  borderRadius: BorderRadius.circular(
                                    7.0,
                                  ),
                                  border: Border.all(color: red)),
                              child: const Center(
                                child: Text(
                                  'Yes',
                                  style: redText,
                                ),
                              ),
                            )),
                        const SizedBox(
                          width: 30.0,
                        ),
                        TextButton(
                          style: TextButton.styleFrom(
                            foregroundColor: Colors.white,
                            padding: const EdgeInsets.all(16.0),
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 33,
                            width: 63,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  7.0,
                                ),
                                border: Border.all(color: black)),
                            child: const Center(
                              child: Text(
                                'No',
                                style: mediumText,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}
