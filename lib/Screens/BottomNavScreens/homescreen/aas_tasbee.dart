import 'dart:math';

import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/MLM/tasbeeh_reward_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/constants/ad_helper.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';

import '../../../Constants/colors.dart';

class AasTasbee extends StatefulWidget {
  const AasTasbee({Key? key}) : super(key: key);

  @override
  State<AasTasbee> createState() => _AasTasbeeState();
}

class _AasTasbeeState extends State<AasTasbee> with TickerProviderStateMixin {
  int counter = 0;
  int levelClock = 300;

  void incrementCounter() {
    setState(() {
      counter++;
    });
    log(counter);
  }

  void resetMethod() {
    setState(() {
      counter = 0;
    });
  }

  AnimationController? _controllerAnimate;
  AnimationController? _controllerTimer;
  AnimationController? _controllerReset;
  late BannerAd _bannerAd;
  NativeAd? nativeAd;
  bool isNativeAdLoaded = false;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    loadNativeAd();
  }

  void loadNativeAd() {
    nativeAd = NativeAd(
        adUnitId: AdHelper.nativeAdUnitId,
        listener: NativeAdListener(onAdLoaded: (ad) {
          setState(() {
            isNativeAdLoaded = true;
          });
        }, onAdFailedToLoad: (ad, error) {
          nativeAd!.dispose();
        }),
        request: AdRequest(),
        nativeTemplateStyle:
            NativeTemplateStyle(templateType: TemplateType.small));
    nativeAd!.load();
  }

  @override
  void initState() {
    super.initState();
    // Schedule the execution of _showRewardPopup after a delay
    Future.delayed(Duration(microseconds: 700), () {
      UserModal? userData =
          Provider.of<UserDataProvider>(context, listen: false).userData;
      print('this is tasbeeh value: ${userData!.isTasbeehReward}');
      if (userData != null && userData.isTasbeehReward == 0) {
        // Show reward popup if the condition is met
        _showRewardPopup(context);
      }
    });

    _bannerAd = context.read<AdProvider>().createBannerAd();

    _controllerAnimate = AnimationController(
      duration: const Duration(milliseconds: 600),
      vsync: this,
    );
    _controllerReset = AnimationController(
      duration: const Duration(milliseconds: 600),
      vsync: this,
    );

    _controllerTimer = AnimationController(
      vsync: this,
      duration: Duration(seconds: levelClock),
    );
    _controllerTimer!.forward(from: 0);

    _controllerTimer!.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        Provider.of<UserDataProvider>(context, listen: false).updateUserData();
        tasbeehRewardedApi();
      }
    });
  }

  popupContainer() {}

  tasbeehRewardedApi() {
    Provider.of<TasbeehRewardProvider>(context, listen: false).tasbeehReward();
  }

  @override
  void dispose() {
    _controllerAnimate!.dispose();
    _controllerReset!.dispose();
    _controllerTimer!.dispose();
    super.dispose();
  }

  int resetNumber = 0;

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    UserModal? userData = Provider.of<UserDataProvider>(context).userData;
    return Scaffold(
      backgroundColor: white,
      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Image.asset(
                    "assets/images/tasbee background imaage.png",
                    fit: BoxFit.fill,
                  ),
                  Padding(
                    padding: EdgeInsets.only(left: 5),
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: AdWidget(ad: _bannerAd),
                    ),
                  ),
                  // Positioned(
                  //   top: context.screenHeight * .05,
                  //   left: context.screenWidth * .3,
                  //   right: context.screenWidth * .3,
                  //   child: Row(
                  //     mainAxisAlignment: MainAxisAlignment.center,
                  //     children: [
                  //       Expanded(
                  //         child: Text(
                  //           "AAS Tasbeeh",
                  //           style: TextStyle(
                  //             color: Color(0xffFBD077),
                  //             fontSize: 18.sp,
                  //             fontWeight: FontWeight.w700,
                  //           ),
                  //         ),
                  //       ),
                  //       Image.asset(
                  //         "assets/images/tasbeeh1.png",
                  //         color: Colors.white,
                  //         height: context.screenHeight * 0.05,
                  //         width: context.screenWidth * 0.08,
                  //         fit: BoxFit.fill,
                  //       )
                  //     ],
                  //   ),
                  // ),
                  Positioned(
                    // bottom: context.screenHeight * .15,
                    top: screenHeight * .1,
                    left: screenWidth * .2,
                    right: screenWidth * .2,
                    child: Text(
                      "Tap On Counter Button \n To Increase Counter",
                      style: TextStyle(
                        color: white,
                        fontSize: 16,
                        fontWeight: FontWeight.w600,
                      ),
                      textAlign: TextAlign.center,
                    ),
                  ),
                  Positioned(
                    top: screenHeight * .17,
                    left: screenWidth * .10,
                    right: screenWidth * .10,
                    child: Image.asset(
                      "assets/images/tasbee container.png",
                      height: screenHeight * 0.27,
                      width: screenWidth * 0.56,
                      fit: BoxFit.fill,
                    ),
                  ),
                  Positioned(
                    top: screenHeight * .24,
                    left: screenWidth * .25,
                    right: screenWidth * .25,
                    child: Container(
                      child: Center(
                        child: Text(
                          counter.toString(),
                          style: TextStyle(
                            fontSize: 60,
                            color: Colors.white,
                            // fontFamily: 'Giddyup Std Regular',
                          ),
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: screenHeight * .28,
                    left: screenWidth * .28,
                    right: screenWidth * .28,
                    child: GestureDetector(
                      onTap: () {
                        incrementCounter();
                        _controllerAnimate!.forward();
                        Future.delayed(Duration(milliseconds: 200), () {
                          _controllerAnimate!.reverse();
                        });
                        print('Shrink');
                      },
                      child: ScaleTransition(
                        scale: Tween<double>(begin: 1.0, end: 0.7)
                            .animate(_controllerAnimate!),
                        child: Container(
                          height: screenHeight * 0.4,
                          width: screenWidth * 0.4,
                          decoration: BoxDecoration(
                            shape: BoxShape.circle,
                            gradient: LinearGradient(
                              colors: [Color(0xffefa931), Color(0xfff5d961)],
                              begin: Alignment.topLeft,
                              end: Alignment.bottomRight,
                            ),
                          ),
                          alignment: Alignment.topCenter,
                          padding: EdgeInsets.all(20),
                          child: GestureDetector(
                            onTap: () {
                              incrementCounter();
                              _controllerAnimate!.forward();
                              Future.delayed(Duration(milliseconds: 200), () {
                                _controllerAnimate!.reverse();
                              });
                              print('Shrink');
                            },
                            child: ScaleTransition(
                              scale: Tween<double>(
                                begin: 1.0,
                                end: 0.7,
                              ).animate(_controllerAnimate!),
                              child: Center(
                                child: Text(
                                  "Tasbeeh",
                                  style: TextStyle(
                                      fontSize: 19,
                                      fontWeight: FontWeight.w600),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                  // Positioned(
                  //   bottom: context.screenHeight * .15,
                  //   left: context.screenWidth * .2,
                  //   right: context.screenWidth * .2,
                  //   child: Text(
                  //     "Tap On Counter Button \n To Increase Counter",
                  //     style: TextStyle(
                  //       fontSize: 16,
                  //       fontWeight: FontWeight.w600,
                  //     ),
                  //     textAlign: TextAlign.center,
                  //   ),
                  // ),

                  // Positioned(
                  //   bottom: context.screenHeight * 0.02,
                  //   child: isNativeAdLoaded
                  //       ? Container(
                  //           // decoration: BoxDecoration(color: white),
                  //           // height: 130,
                  //           child: AdWidget(ad: nativeAd!),
                  //         )
                  //       : SizedBox(),
                  // )
                  // Positioned(
                  //   bottom: 10,
                  //   child:
                  // ),
                ],
              ),
              Column(
                children: [
                  // Other widgets
                  Align(
                    alignment: Alignment.center,
                    child: GestureDetector(
                      onTap: () {
                        resetNumber = resetNumber + 1;
                        if (resetNumber > 3) {
                          _loadAndShowInterstitialAd(context);
                          resetNumber++;
                        } else if (resetNumber > 2) {
                          _loadAndShowInterstitialAd(context);
                        } else {}
                        resetMethod();
                        _controllerReset!.forward();
                        Future.delayed(Duration(milliseconds: 200), () {
                          _controllerReset!.reverse();
                        });
                      },
                      child: ScaleTransition(
                        scale: Tween<double>(
                          begin: 1.0,
                          end: 0.7,
                        ).animate(_controllerReset!),
                        child: Image(
                          height: 45,
                          image: AssetImage("assets/images/reset button.png"),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  Container(
                    child: userData!.isTasbeehReward == 0
                        ? Countdown(
                            animation: StepTween(
                              begin:
                                  levelClock, // THIS IS A USER ENTERED NUMBER
                              end: 0,
                            ).animate(_controllerTimer!),
                          )
                        : Text(
                            textAlign: TextAlign.center,
                            'Today\'s reward added to your Wallet successfully.',
                            style: TextStyle(
                                fontSize: 17,
                                fontWeight: FontWeight.bold,
                                color: black),
                          ),
                  ),
                  SizedBox(
                    height: 20,
                  ),
                  isNativeAdLoaded
                      ? Container(
                          decoration: BoxDecoration(color: white),
                          height: 160,
                          child: AdWidget(ad: nativeAd!),
                        )
                      : SizedBox(),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _loadAndShowInterstitialAd(BuildContext context) {
    // Access the AdProvider using the Provider.of method
    AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

    // Load the interstitial ad
    adProvider.loadInterstitialAd();

    // Check if the interstitial ad is loaded
    if (adProvider.isInterstitialAdLoaded) {
      resetNumber == 0;
      // Show the interstitial ad
      adProvider.showInterstitialAd();
    } else {
      // The ad is not loaded yet, you can handle this case accordingly
      print('Interstitial ad is not loaded yet. Please try again later.');
    }
  }

  _showRewardPopup(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: true,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(30.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 150.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(20.0),
                  gradient: LinearGradient(colors: [
                    primaryColor,
                    Color.fromARGB(255, 29, 221, 163)
                  ])),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    const SizedBox(
                      height: 1.0,
                    ),
                    Text(
                      "Stay on this screen for 5 minutes to get your reward.",
                      style: TextStyle(
                        fontSize: 16,
                        color: white,
                        fontWeight: FontWeight.w500,
                      ),
                      textAlign: TextAlign.center,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pop(context);
                      },
                      child: Container(
                          height: 40,
                          width: 100,
                          decoration: BoxDecoration(
                              border: Border.all(color: black, width: 0.6),
                              color: white,
                              borderRadius: BorderRadius.circular(7.0)),
                          child: Center(
                            child: Text(
                              'OK',
                              style: TextStyle(color: black),
                            ),
                          )),
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}

class Countdown extends AnimatedWidget {
  Countdown({Key? key, required this.animation})
      : super(key: key, listenable: animation);
  final Animation<int> animation;

  @override
  build(BuildContext context) {
    Duration clockTimer = Duration(seconds: animation.value);
    String timerText =
        '${clockTimer.inMinutes.remainder(60).toString()}:${clockTimer.inSeconds.remainder(60).toString().padLeft(2, '0')}';

    return Padding(
      padding: const EdgeInsets.only(left: 10.0),
      child: Text(
        textAlign: TextAlign.left,
        "Remaining Time: $timerText",
        style: TextStyle(
          fontSize: 17,
          color: black,
        ),
      ),
    );
  }
}
