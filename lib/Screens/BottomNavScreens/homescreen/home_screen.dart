import 'dart:async';
import 'dart:io';
import 'dart:ui';

import 'package:aas/Model/MLM%20modals/donation_to_aas_modal.dart';
import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/MLM/count_down_provider.dart';
import 'package:aas/Provider/MLM/donation_to_aas_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Provider/get_bookmark_provider.dart';
import 'package:aas/Provider/logout_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/settings_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Quran%20Section/quran_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Dua%20Screens/DuaScreen.dart';

import 'package:aas/Screens/BottomNavScreens/homescreen/aas_tasbee.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/BottomNav2_screens/bottom_nav2.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/qibla_direction.screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/wallet_screen.dart';
import 'package:aas/Services/notification.dart';
import 'package:aas/constants/ad_helper.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:aas/constants/reusable_widgets.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/namaz_timing_screen.dart';
import 'package:aas/main.dart';
import 'package:adhan/adhan.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/cupertino.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:ntp/ntp.dart';
import 'package:package_info_plus/package_info_plus.dart';
import 'package:pinput/pinput.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'package:timezone/timezone.dart' as tz;
// import 'package:velocity_x/velocity_x.dart';

class HomeScreen extends StatefulWidget {
  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @pragma('vm:entry-point')
  TextEditingController amountController = TextEditingController();
  TextEditingController pinController = TextEditingController();
  AndroidDeviceInfo? androidDeviceInfo;
  late BannerAd _bannerAd;
  final CountDownTimerProvider _countDownProvider = CountDownTimerProvider();
  NativeAd? nativeAd;
  bool isNativeAdLoaded = false;
  late Timer timer;
  String? formattedDuration;

  String currentTime = "";
  String networkTime = "";
  double latitude = 0;
  double longitude = 0;
  Location location = Location();
  LocationData? _currentLocation;
  String? appVersion;
  String? mToken;
  Uri? appUrl = Uri.parse(
      'https://play.google.com/store/apps/details?id=com.aasonline.app&hl=en&gl=US');

  @override
  void initState() {
    super.initState();

    _bannerAd = context.read<AdProvider>().createBannerAd();

    Provider.of<GetBookmarkProvider>(context, listen: false).getBookmarkData();

    getFormatedPrayersTime();

    _fetchCountDownTimer();

    getDeviceInfo();
    getPlatformDetails();
    // displayPopupFunction();
    isBlocked();

    deviceToken();
  }

  Future<void> deviceToken() async {
    mToken = await FirebaseMessaging.instance.getToken();
    setState(() {
      mToken;
    });
    print("this is fcm token from init: $mToken");
  }

  // _showRewardPopup(BuildContext context) {
  //   return showDialog(
  //       context: context,
  //       barrierDismissible: true,
  //       builder: (BuildContext dialogContext) {
  //         final screenSize = MediaQuery.of(context).size;
  //         return AlertDialog(
  //           insetPadding: const EdgeInsets.all(30.0),
  //           backgroundColor: Colors.transparent,
  //           contentPadding: const EdgeInsets.all(0.0),
  //           content: Container(
  //             height: 150.0,
  //             width: screenSize.width,
  //             decoration: BoxDecoration(
  //                 borderRadius: BorderRadius.circular(20.0),
  //                 gradient: LinearGradient(colors: [
  //                   primaryColor,
  //                   Color.fromARGB(255, 29, 221, 163)
  //                 ])),
  //             child: Padding(
  //               padding: const EdgeInsets.symmetric(horizontal: 20.0),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: <Widget>[
  //                   const SizedBox(
  //                     height: 1.0,
  //                   ),
  //                   Text(
  //                     "Stay on this screen for 5 minutes to get your reward.",
  //                     style: TextStyle(
  //                       fontSize: 16,
  //                       color: white,
  //                       fontWeight: FontWeight.w500,
  //                     ),
  //                     textAlign: TextAlign.center,
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       Navigator.pop(context);
  //                     },
  //                     child: Container(
  //                         height: 40,
  //                         width: 100,
  //                         decoration: BoxDecoration(
  //                             border: Border.all(color: black, width: 0.6),
  //                             color: white,
  //                             borderRadius: BorderRadius.circular(7.0)),
  //                         child: Center(
  //                           child: Text(
  //                             'OK',
  //                             style: TextStyle(color: black),
  //                           ),
  //                         )),
  //                   )
  //                 ],
  //               ),
  //             ),
  //           ),
  //         );
  //       });
  // }

  //  void getDatafromProvider(BuildContext context) {
  //   try {
  //     // Access the user data from the provider
  //     UserModal? userData = Provider.of<UserDataProvider>(context, listen: false).userData;
  //     if (userData != null) {
  //       print('user data is not null');
  //     } else {
  //       print('user data is null');
  //     }
  //   } catch (e) {
  //     print('Error retrieving user data: $e');
  //   }
  // }
//  Future<UserModal?> fetchUserData() async {
//     // Fetch user data and return it
//     return Provider.of<UserDataProvider>(context, listen: false).userData;
//   }

//   int showPopup = 1;

//  void isAppUpdatedPopup() async {
//       UserModal? userData = Provider.of<UserDataProvider>(context).userData;

//   // UserModal? userModal = Provider.of<UserDataProvider>(context, listen: false).userData;
// if(userData != null){
//   print('user modal is not null');
// } else {
//   print('it is null');
// }
// }

  Future<void> getCurrentTime() async {
    var now = DateTime.now();
    setState(() {
      currentTime = now.toString();
    });
    var networkNow = await NTP.now();
    setState(() {
      networkTime = networkNow.toString();
    });

    var diff = networkNow.difference(now).inMinutes.abs();
    if (diff > 1) {
      showDialog(
        context: context,
        builder: (BuildContext context) {
          return AlertDialog(
            title: Text("Time Mismatch"),
            content: Text("Please adjust your device time"),
            actions: [
              MaterialButton(
                child: Text("OK"),
                onPressed: () {
                  print('Dialog is popped');
                  SystemNavigator.pop();
                },
              ),
            ],
          );
        },
      );
    }
  }

  getDeviceInfo() async {
    final deviceInfo = DeviceInfoPlugin();
    androidDeviceInfo = await deviceInfo.androidInfo;

    setState(() {});
  }

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    loadNativeAd();
  }

  getPlatformDetails() async {
    PackageInfo packageInfo = await PackageInfo.fromPlatform();
    appVersion = packageInfo.version;

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted == PermissionStatus.granted) {
        return;
      }
    }

    _currentLocation = await location.getLocation();
    latitude = _currentLocation!.latitude!;
    longitude = _currentLocation!.longitude!;
    print("this is fcm token from another function: $mToken");

// print("This is Fcm Token: ${mToken}");
    setState(() {
      appVersion;
      latitude;
      longitude;
    });

    Provider.of<UserDataProvider>(context, listen: false).updateUserData(
        lat: latitude,
        long: longitude,
        app_version: appVersion,
        device_key: mToken);
    // UserModal? userData = Provider.of<UserDataProvider>(context).userData;
  }

  void loadNativeAd() {
    storePrayerInStorage();
    nativeAd = NativeAd(
        adUnitId: AdHelper.nativeAdUnitId,
        listener: NativeAdListener(onAdLoaded: (ad) {
          setState(() {
            isNativeAdLoaded = true;
          });
        }, onAdFailedToLoad: (ad, error) {
          nativeAd!.dispose();
        }),
        request: AdRequest(),
        nativeTemplateStyle:
            NativeTemplateStyle(templateType: TemplateType.small));
    nativeAd!.load();
  }

  storePrayerInStorage() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    DateTime currentTime = DateTime.now();
    int CT = int.parse(DateFormat('HHmm').format(currentTime));

    final myCoordinates = Coordinates(latitude, longitude);
    final params = CalculationMethod.karachi.getParameters();
    params.madhab = Madhab.hanafi;
    final prayerTimes = PrayerTimes.today(myCoordinates, params);
    int fajrTime = int.parse(DateFormat('HHmm').format(prayerTimes.fajr));
    int dhuhrTime = int.parse(DateFormat('HHmm').format(prayerTimes.dhuhr));
    int asrTime = int.parse(DateFormat('HHmm').format(prayerTimes.asr));
    int maghribTime = int.parse(DateFormat('HHmm').format(prayerTimes.maghrib));
    int ishaTime = int.parse(DateFormat('HHmm').format(prayerTimes.isha));

    // print('CT is: $CT');
    // print('fajr time is: $fajrTime');
    // print('dhuhr time is: $dhuhrTime');
    // print('asr time is: $asrTime');
    // print('maghrib time is: $maghribTime');
    // print('isha time is: $ishaTime');
    prefs.setInt('current_time', CT);
    prefs.setInt('fajr_time', fajrTime);
    prefs.setInt('dhuhr_time', dhuhrTime);
    prefs.setInt('asr_time', asrTime);
    prefs.setInt('maghrib_time', maghribTime);
    prefs.setInt('isha_time', ishaTime);
    int? forCT = prefs.getInt('current_time');
    int? forFT = prefs.getInt('fajr_time');
    int? forDT = prefs.getInt('dhuhr_time');
    int? forAT = prefs.getInt('asr_time');
    int? forMT = prefs.getInt('maghrib_time');
    int? forIT = prefs.getInt('isha_time');
    // print('CT & FT: ${forCT}, ${forFT}');
    // print('CT & DT: ${forCT}, ${forDT}');
    // print('CT & AT: ${forCT}, ${forAT}');
    // print('CT & MT: ${forCT}, ${forMT}');
    // print('CT & IT: ${forCT}, ${forIT}');


  }

  Future<void> _fetchCountDownTimer() async {
    final myCoordinates = Coordinates(latitude, longitude);
    final params = CalculationMethod.karachi.getParameters();

    params.madhab = Madhab.hanafi;
    final prayerTimes = PrayerTimes.today(myCoordinates, params);
    DateTime now = DateTime.now();
    String formatedT = DateFormat.Hms().format(now);
    String formatFajrT = DateFormat.Hms().format(prayerTimes.fajr);
    String formatDhuhrT = DateFormat.Hms().format(prayerTimes.dhuhr);
    String formatAsrT = DateFormat.Hms().format(prayerTimes.asr);
    // print("This is Asr")
    String formatMaghribT = DateFormat.Hms().format(prayerTimes.maghrib);
    String formatIshaT = DateFormat.Hms().format(prayerTimes.isha);
    String currentTime = formatedT; // Replace with actual value
    String nextPrayerTime = formatAsrT; // Replace with actual value
// **********   time without colon format     ****************

    DateTime currentTimeF = DateTime.now();
    int CT = int.parse(DateFormat('HHmm').format(currentTimeF));

    final myCoordinatesF = Coordinates(latitude, longitude);
    final paramsF = CalculationMethod.karachi.getParameters();
    paramsF.madhab = Madhab.hanafi;

    final prayerTimesF = PrayerTimes.today(myCoordinates, params);
    int fajrTime = int.parse(DateFormat('HHmm').format(prayerTimes.fajr));
    int dhuhrTime = int.parse(DateFormat('HHmm').format(prayerTimes.dhuhr));
    int asrTime = int.parse(DateFormat('HHmm').format(prayerTimes.asr));
    int maghribTime = int.parse(DateFormat('HHmm').format(prayerTimes.maghrib));
    int ishaTime = int.parse(DateFormat('HHmm').format(prayerTimes.isha));

// **********   time without colon format     ****************

    if (CT > fajrTime && CT < dhuhrTime) {
      await _countDownProvider.getTimerValue(formatedT, formatDhuhrT);
      String remainingTime =
          _countDownProvider.countDownTimer?.remainingTime ?? 'N/A';
      // print('Fajr time is: Remaining Time: $remainingTime');
      startTimer(parseDuration(remainingTime));
    } else if (CT > dhuhrTime && CT < asrTime) {
      await _countDownProvider.getTimerValue(formatedT, formatAsrT);
      String remainingTime =
          _countDownProvider.countDownTimer?.remainingTime ?? 'N/A';
      // print('Remaining Time: $remainingTime');
      // print(
      //     'asr time is: $asrTime and remaining dhuhr time is: $remainingTime');
      startTimer(parseDuration(remainingTime));
    } else if (CT > asrTime && CT < maghribTime) {
      await _countDownProvider.getTimerValue(formatedT, formatMaghribT);
      String remainingTime =
          _countDownProvider.countDownTimer?.remainingTime ?? 'N/A';
      // print('Remaining Time: $remainingTime');
      startTimer(parseDuration(remainingTime));
    } else if (CT > maghribTime && CT < ishaTime) {
      await _countDownProvider.getTimerValue(formatedT, formatIshaT);
      String remainingTime =
          _countDownProvider.countDownTimer?.remainingTime ?? 'N/A';
      // print('Remaining Time: $remainingTime');
      startTimer(parseDuration(remainingTime));
    } else if (CT > ishaTime && CT < 2400) {
      await _countDownProvider.getTimerValue(formatedT, formatFajrT);
      String remainingTime =
          _countDownProvider.countDownTimer?.remainingTime ?? 'N/A';
      // print('Remaining Time: $remainingTime');
      startTimer(parseDuration(remainingTime));
    } else {
      await _countDownProvider.getTimerValue(formatedT, formatDhuhrT);
      String remainingTime =
          _countDownProvider.countDownTimer?.remainingTime ?? 'N/A';
      // print('Remaining Time: $remainingTime');
      startTimer(parseDuration(remainingTime));
    }
  }

  Duration parseDuration(String remainingTime) {
    List<String> parts = remainingTime.split(':');
    int hours = int.parse(parts[0]);
    int minutes = int.parse(parts[1]);
    int seconds = int.parse(parts[2]);
    return Duration(hours: hours, minutes: minutes, seconds: seconds);
  }

  String formatDuration(Duration duration) {
    return '${duration.inHours.toString().padLeft(2, '0')}:${(duration.inMinutes % 60).toString().padLeft(2, '0')}:${(duration.inSeconds % 60).toString().padLeft(2, '0')}';
  }

  getFormatedPrayersTime() {
    DateTime now = DateTime.now();
    final myCoordinates = Coordinates(latitude, longitude);
    final params = CalculationMethod.karachi.getParameters();
    params.madhab = Madhab.hanafi;
    final prayerTimes = PrayerTimes.today(myCoordinates, params);
    //    ****************       for api data    *****************

    String formatedT = DateFormat.Hms().format(now);

    String formatFajrT = DateFormat.Hms().format(prayerTimes.fajr);
    String formatDhuhrT = DateFormat.Hms().format(prayerTimes.dhuhr);
    String formatAsrT = DateFormat.Hms().format(prayerTimes.asr);
    String formatMaghribT = DateFormat.Hms().format(prayerTimes.maghrib);
    String formatIshaT = DateFormat.Hms().format(prayerTimes.isha);
  }

  void startTimer(Duration initialDuration) {
    timer = Timer.periodic(Duration(seconds: 1), (timer) {
      if (initialDuration.inSeconds > 0) {
        setState(() {
          initialDuration = initialDuration - Duration(seconds: 1);
          formattedDuration = formatDuration(initialDuration);
        });
      } else {
        timer.cancel();
        // Handle timer completion
        print("Countdown completed!");
        setState(() {});
      }
    });
  }

  Duration parseFormattedDuration(String formattedDuration) {
    List<String> parts = formattedDuration.split(':');
    int hours = int.parse(parts[0]);
    int minutes = int.parse(parts[1]);
    int seconds = int.parse(parts[2]);

    return Duration(hours: hours, minutes: minutes, seconds: seconds);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    amountController.dispose();
    pinController.dispose();
  }

  isBlocked() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();

    int? isBlocked = prefs.getInt('isBlocked');
    print('this is blocked status: $isBlocked');
    isBlocked == 1 ? isBlockedFunctionality() : Container();
  }

  isBlockedFunctionality() async {
    final logoutP = Provider.of<LogoutProvider>(context, listen: false);
    logoutP.logout();
  }

  // displayPopupFunction() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();

  //   int? isUpdatedValue = prefs.getInt('isUpdated');

  //   isUpdatedValue == 1 ? updateAppPopup(context) : Container();
  // }

  void updateAppPopup(BuildContext context) {
    showDialog(
      context: context,
      builder: (BuildContext dialogContext) {
        return AlertDialog(
          title: Text(
            'New Version is Available.',
            style: TextStyle(fontSize: 17),
          ),
          content: Text('Please Update the App.'),
          actions: <Widget>[
            TextButton(
              onPressed: () async {
                if (await canLaunchUrl(Uri.parse(appUrl.toString()))) {
                  // ignore: deprecated_member_use
                  await launch(appUrl.toString());
                } else {
                  CustomSnackbar.error(message: 'Something went wrong');
                }
              },
              child: Text('OK'),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    // getCurrentTime();

    UserModal? userData = Provider.of<UserDataProvider>(context).userData;

    // Show update popup if userData.isUpdated == 1
    if (userData != null && userData.isUpdated == 1) {
      WidgetsBinding.instance.addPostFrameCallback((_) {
        updateAppPopup(context);
      });
      return Scaffold(
        body: Center(
          child: CircularProgressIndicator(),
        ),
      );
    }

    return Scaffold(
      backgroundColor: white,
      drawer: myDrawer(context, userData),
      appBar: AppBar(
        systemOverlayStyle: SystemUiOverlayStyle.light,
        title: Text(
          'C-Logs',
          style: TextStyle(color: white),
        ),
        centerTitle: true,
        backgroundColor: primaryColor,
        iconTheme: const IconThemeData(color: white),
        elevation: 0,
        actions: [
          InkWell(
            onTap: () {
              dialogeShow(context);
            },
            child: Padding(
              padding: const EdgeInsets.only(right: 10.0),
              child: FaIcon(
                CupertinoIcons.info,
                color: Colors.white,
              ),
            ),
          )
        ],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          physics: const BouncingScrollPhysics(),
          child: Column(
            children: [
              InkWell(
                  onTap: () {
                    Get.to(BottomNav2(initialIndex: 2));
                  },
                  child: SizedBox(
                      width: double.infinity,
                      // height: 200,
                      child: Image(
                          image: AssetImage('assets/images/reels-new.png')))),
              Container(
                padding:
                    const EdgeInsets.symmetric(horizontal: 40, vertical: 7),
                decoration: const BoxDecoration(
                  borderRadius:
                      BorderRadius.only(bottomLeft: Radius.circular(60)),
                  color: white,
                ),
              ),
              Container(
                color: white,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Container(
                        color: white,
                        margin: const EdgeInsets.symmetric(horizontal: 15.0),
                        height: 290,
                        // width: MediaQuery.of(context).size.width,
                        // color: Colors.amber,
                        child: Row(children: [
                          leftColumnContainer(),
                          centerColumnContainer(userData),
                          rightColumnContainer(),
                        ])),
                    isNativeAdLoaded
                        ? Container(
                            decoration: BoxDecoration(color: white),
                            height: 130,
                            child: AdWidget(ad: nativeAd!),
                          )
                        : SizedBox(),
                    BecomeDonarAtAssBtn(),
                    SizedBox(
                      height: 10,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  BecomeDonarAtAssBtn() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        GestureDetector(
          onTap: () {
            showDialog(
                context: context,
                barrierDismissible: false,
                builder: (BuildContext dialogContext) {
                  final screenSize = MediaQuery.of(context).size;
                  return AlertDialog(
//           title: Text('Note..',style:TextStyle(color: Colors.red)),
                    insetPadding: const EdgeInsets.all(30.0),
                    backgroundColor: Colors.transparent,
                    contentPadding: const EdgeInsets.all(0.0),
                    content: Container(
                      height: 190.0,
                      width: screenSize.width,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        color: Colors.white,
                      ),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 20.0),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            const SizedBox(
                              height: 10.0,
                            ),
                            TextFormField(
                              // obscureText: widget.obscureText ?? false,
                              controller: amountController,
                              decoration: InputDecoration(
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                contentPadding: EdgeInsets.all(0.13),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  borderSide: BorderSide(
                                    color: primaryColor,
                                    width: 1.0,
                                  ),
                                ),
                                prefixIcon: Icon(Icons.link),
                                hintText: 'Enter Amount',
                                hintStyle: TextStyle(
                                  color: lightBlack,
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            TextFormField(
                              obscureText: true,
                              // obscureText: widget.obscureText ?? false,
                              controller: pinController,
                              decoration: InputDecoration(
                                focusedErrorBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                                contentPadding: EdgeInsets.all(0.13),
                                focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                  borderSide: BorderSide(
                                    color: primaryColor,
                                    width: 1.0,
                                  ),
                                ),
                                prefixIcon: Icon(Icons.link),
                                hintText: 'Enter PIN',
                                hintStyle: TextStyle(
                                  color: lightBlack,
                                ),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(8),
                                ),
                              ),
                            ),
                            SizedBox(height: 10.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                InkWell(
                                  onTap: () {
                                    if (amountController.text.isEmpty &&
                                        pinController.text.isEmpty) {
                                      CustomSnackbar.error(
                                          message: 'Please fill all fields');
                                    } else if (pinController.length > 4 ||
                                        pinController.length < 4) {
                                      CustomSnackbar.error(
                                          message: 'Pin must be 4 digits');
                                    } else {
                                      final DTA = Provider.of<DonationToAAS>(
                                          context,
                                          listen: false);
                                      final donationA = DonationToAASModal(
                                        amount: int.parse(
                                            amountController.text.trim()),
                                        tpin: pinController.text.trim(),
                                      );
                                      DTA.doationAAS(donationA, context);

                                      // CustomSnackbar.successful(message: 'api hit');
                                    }
                                  },
                                  child: Container(
                                      height: 40,
                                      width: 100,
                                      decoration: BoxDecoration(
                                          color: primaryColor,
                                          borderRadius:
                                              BorderRadius.circular(7.0)),
                                      child: Center(
                                        child: Text(
                                          'Send',
                                          style: TextStyle(color: white),
                                        ),
                                      )),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                  );
                });
          },
          child: Image(
              height: 45, image: AssetImage('assets/images/become_donar.png')),
        ),
      ],
    );
  }

  String upcomingPrayerName = '';
  String upcomingPrayerTime = '';
  Expanded centerColumnContainer(UserModal? userData) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    // int endTime = DateTime.now().millisecondsSinceEpoch + 1000 * 30;
    int todayPrayersCount = userData?.today_prayers ?? 0;
    // print('today prayers count are: $todayPrayersCount');

    DateTime currentTime = DateTime.now();
    int CT = int.parse(DateFormat('HHmm').format(currentTime));

    final myCoordinates = Coordinates(latitude, longitude);
    final params = CalculationMethod.karachi.getParameters();
    params.madhab = Madhab.hanafi;
    final prayerTimes = PrayerTimes.today(myCoordinates, params);
    int fajrTime = int.parse(DateFormat('HHmm').format(prayerTimes.fajr));
    int dhuhrTime = int.parse(DateFormat('HHmm').format(prayerTimes.dhuhr));
    int asrTime = int.parse(DateFormat('HHmm').format(prayerTimes.asr));
    int maghribTime = int.parse(DateFormat('HHmm').format(prayerTimes.maghrib));
    int ishaTime = int.parse(DateFormat('HHmm').format(prayerTimes.isha));
    String formatFajrT = DateFormat.Hms().format(prayerTimes.fajr);
    String formatDhuhrT = DateFormat.Hms().format(prayerTimes.dhuhr);
    String formatAsrT = DateFormat.Hms().format(prayerTimes.asr);
    String formatMaghribT = DateFormat.Hms().format(prayerTimes.maghrib);
    String formatIshaT = DateFormat.Hms().format(prayerTimes.isha);

    if (CT > fajrTime && CT < dhuhrTime) {
      // print('current prayer time: $fajrTime && next prayer time: $dhuhrTime');
      setState(() {
        upcomingPrayerName = 'Dhuhr';
        upcomingPrayerTime = formatDhuhrT;
      });
    } else if (CT > dhuhrTime && CT < asrTime) {
      // print('current prayer time: $dhuhrTime && next prayer time: $asrTime');` `
      setState(() {
        upcomingPrayerName = 'Asr';
        upcomingPrayerTime = formatAsrT;
      });
    } else if (CT > asrTime && CT < maghribTime) {
      // print('current prayer time: $asrTime && next prayer time: $maghribTime');
      setState(() {
        upcomingPrayerName = 'Maghrib';
        upcomingPrayerTime = formatMaghribT;
      });
    } else if (CT > maghribTime && CT < ishaTime) {
      // print('current prayer time: $maghribTime && next prayer time: $ishaTime');
      setState(() {
        upcomingPrayerName = 'Isha';
        upcomingPrayerTime = formatIshaT;
      });
    } else if (CT > ishaTime && CT < 2400) {
      // print('current prayer time: $ishaTime && next prayer time: $fajrTime');
      setState(() {
        upcomingPrayerName = 'Fajar';
        upcomingPrayerTime = formatFajrT;
      });
    } else {
      // print('next prayer time is: fajr time');
      setState(() {
        upcomingPrayerName = 'Fajar';
        upcomingPrayerTime = formatFajrT;
      });
    }
    // print('this is CT: $CT and this is isha time: $ishaTime');

    //     **********************        for Current time conversion      ************
    // String formattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(currentTime);
    // DateTime dateTime = DateTime.parse(formattedDate.replaceAll(" – ", " "));
    // var formatedCT = DateFormat('HHmm').format(currentTime);
    // print('Format current date is: $formattedDate');
    // int year = dateTime.year;
    // int month = dateTime.month;
    // int day = dateTime.day;
    // int hour = dateTime.hour;
    // int minutes = dateTime.minute;
    // int seconds = dateTime.second;
    // print('$year, $month, $day, $hour, $minutes');

    //     **********************        for Current time conversion      ************
    //     **********************        for fajr time conversion      ************

//  int currentFajrTime = int.parse(DateFormat('yyyy-MM-dd – kk:mm').format(prayerTimes.fajr));
//  print(DateFormat.jm().format(prayerTimes.fajr));
//  String currentFajrFormattedDate = DateFormat('yyyy-MM-dd – kk:mm').format(currentFajrTime);
//   DateTime fajrDateTime = DateTime.parse(formattedDate.replaceAll(" – ", " "));
//    int fajrYear = dateTime.year;
//     int fajrMonth = dateTime.month;
//     int fajrDay = dateTime.day;
//     int fajrHour = dateTime.hour;
//     int fajrMinutes = dateTime.minute;
//     int fajrSeconds = dateTime.second;
// print('fajr date is: ${DateFormat.HOUR_MINUTE_SECOND.format(prayerTimes.fajr)}');

//  print('fajr time DTC: {$fajrYear, $fajrMonth, $fajrDay, $fajrHour, $fajrMinutes}');
    //     **********************        for fajr time conversion      ************

    return Expanded(
        child: Container(
      height: 250,
      // width: 170,
      // color: Colors.green,
      child: Column(
        children: [
          Container(
            height: 60,
            // color: Colors.red,
            // width: context.screenWidth * .52,
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    CT > fajrTime && CT < dhuhrTime
                        ? Text('Have you prayed Fajar?',
                            style: centerContainerTextStyle)
                        : CT > dhuhrTime && CT < asrTime
                            ? Text('Have you prayed Dhuhr?',
                                style: centerContainerTextStyle)
                            : CT > asrTime && CT < maghribTime
                                ? Text('Have you prayed Asr?',
                                    style: centerContainerTextStyle)
                                : CT > maghribTime && CT < ishaTime
                                    ? Text('Have you prayed Maghrib?',
                                        style: centerContainerTextStyle)
                                    : CT > ishaTime && CT < 2400
                                        ? Text('Have you prayed Isha?',
                                            style: centerContainerTextStyle)
                                        : Text('Have you prayed Fajar?',
                                            style: centerContainerTextStyle),

                    Text(
                      "Remaining Time: $formattedDuration",
                      style: TextStyle(fontSize: 11),
                    ),
                    // Text(
                    //   "Remaining Time: ${remainingDuration.isNegative ? '-' : ''} $remainingTime",
                    //   style: TextStyle(fontSize: 11),
                    // ),
                    // Row(
                    //   children: [
                    //     Text(
                    //       'Remaining Time ',
                    //       style: TextStyle(
                    //           color: primaryColor,
                    //           fontSize: 11,
                    //           fontWeight: FontWeight.w500),
                    //     ),

                    //     // CountdownTimer(
                    //     //   endTime: endTime,
                    //     //   widgetBuilder: (_,CurrentRemainingTime  time) {
                    //     //     if (time == null) {
                    //     //       return Text('Game over');
                    //     //     }
                    //     //     return Text(
                    //     //         'days: [ ${time.days} ], hours: [ ${time.hours} ], min: [ ${time.min} ], sec: [ ${time.sec} ]');
                    //     //   },
                    //     // ),
                    //   ],
                    // ),
                  ],
                ),
                SizedBox(
                  width: 6.0,
                ),
                Column(
                  children: [
                    // Icon(
                    //   Icons.radio_button_unchecked,
                    //   size: 20,
                    // ),
                    Container(
                      height: 30,
                      width: 29,
                      decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(8.0),
                          border: Border.all(color: lightBlack)),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text(
                            todayPrayersCount.toString(),
                            style: TextStyle(fontSize: 12),
                          ),
                          Text(
                            '/5',
                            style: TextStyle(fontSize: 12),
                          ),
                        ],
                      ),
                    ),
                  ],
                )
                //
              ],
            ),
          ),
          Container(
            height: 110,
            width: double.infinity,
            // color: Colors.greenAccent,
            child: const Center(
              child: Image(image: AssetImage('assets/images/aas.png')),
            ),
          ),
          Container(
            height: 60,
            width: screenWidth * .52,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                const Row(
                  children: [
                    SizedBox(
                      child: Text(
                        'UPCOMING PRAYER',
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 16),
                      ),
                    ),
                  ],
                ),
                const SizedBox(
                  height: 10,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    SizedBox(
                        width: 70.w,
                        child: Text(upcomingPrayerName, style: upcomingPrayer)),
                    Text(
                      '$upcomingPrayerTime',
                      style: TextStyle(
                          color: primaryColor,
                          fontSize: 14,
                          fontWeight: FontWeight.w500),
                    ),
                  ],
                )
              ],
            ),
          )
        ],
      ),
    ));
  }

  Container leftColumnContainer() => Container(
        height: 250,
        width: 55,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                // _showExitDialogue(context);
                Get.to(QuranScreen());
              },
              child: Container(
                height: 80,
                // color: black,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                        // color: lightBlack,
                        width: 38,
                        image: AssetImage('assets/icons/quran.png')),
                    Text('QURAN',
                        // style: homeScreenText,
                        style: homeScreenText)
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Get.to(const DuaScreen());
              },
              child: Container(
                height: 80,
                // color: black,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(width: 38, image: AssetImage('assets/icons/dua.png')),
                    Text(
                      'DUA',
                      style: homeScreenText,
                    )
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Get.to(const AasTasbee());
              },
              child: Container(
                height: 80,
                // color: black,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                        width: 38,
                        image: AssetImage('assets/icons/tasbeeh.png')),
                    Text(
                      'TASBEEH',
                      style: homeScreenText,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      );
  Container rightColumnContainer() => Container(
        height: 250,
        width: 55,
        // color: Colors.red,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            GestureDetector(
              onTap: () {
                Get.to(QiblaDirectionScreen());
              },
              child: Container(
                height: 80,
                // color: black,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                        width: 38, image: AssetImage('assets/icons/qibla.png')),
                    Text(
                      'QIBLA',
                      style: homeScreenText,
                    )
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Get.to(NamazTimingScreen());
              },
              child: Container(
                height: 80,
                // color: black,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                        width: 38,
                        image: AssetImage('assets/icons/prayer.png')),
                    Text(
                      'PRAYERS',
                      style: homeScreenText,
                    )
                  ],
                ),
              ),
            ),
            GestureDetector(
              onTap: () {
                Get.to(WalletScreen());
              },
              child: Container(
                height: 80,
                // color: black,
                child: const Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Image(
                      width: 38,
                      image: AssetImage(
                        'assets/my_wallet/wallet1.png',
                      ),
                      color: Colors.black,
                    ),
                    Text(
                      'WALLET',
                      style: homeScreenText,
                    )
                  ],
                ),
              ),
            ),
          ],
        ),
      );
  dialogeShow(BuildContext context) {
    return showDialog(
        context: context,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(15.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 120.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: const Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  SizedBox(
                    height: 2,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Icon(
                        Icons.info,
                        color: primaryColor,
                      ),
                    ],
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(left: 10, right: 10, bottom: 10),
                    child: Text(
                      "C-Logs (short form of Community Logs) is the Video sharing section of the AAS where anyone can upload their videos which will be available to everyone using AAS once approved by the AAS team.",
                      //                     style: SC.appNormalText,
                      style: TextStyle(fontSize: 11),
                    ),
                  ),
                ],
              ),
            ),
          );
        });
  }

  // showJoinCommunityPopUp() {
  //   // TextEditingController JoinCommunityController = TextEditingController();
  //   return showDialog(
  //       context: context,
  //       barrierDismissible: false,
  //       builder: (BuildContext dialogContext) {
  //         final screenSize = MediaQuery.of(context).size;
  //         return AlertDialog(
  //           insetPadding: const EdgeInsets.all(30.0),
  //           backgroundColor: Colors.transparent,
  //           contentPadding: const EdgeInsets.all(0.0),
  //           content: Container(
  //             height: 150.0,
  //             width: screenSize.width,
  //             decoration: BoxDecoration(
  //               borderRadius: BorderRadius.circular(20.0),
  //               color: Colors.white,
  //             ),
  //             child: Padding(
  //               padding: const EdgeInsets.symmetric(horizontal: 20.0),
  //               child: Column(
  //                 mainAxisAlignment: MainAxisAlignment.spaceAround,
  //                 children: <Widget>[
  //                   const SizedBox(
  //                     height: 1.0,
  //                   ),
  //                   Text(
  //                     "Join Community with referral code",
  //                     style: appNormalText,
  //                     textAlign: TextAlign.center,
  //                   ),
  //                   TextFormField(
  //                     // obscureText: widget.obscureText ?? false,
  //                     // controller: JoinCommunityController,
  //                     decoration: InputDecoration(
  //                       focusedErrorBorder: OutlineInputBorder(
  //                         borderRadius: BorderRadius.circular(8),
  //                       ),
  //                       contentPadding: EdgeInsets.all(0.13),
  //                       focusedBorder: OutlineInputBorder(
  //                         borderRadius: BorderRadius.circular(8),
  //                         borderSide: BorderSide(
  //                           color: primaryColor,
  //                           width: 1.0,
  //                         ),
  //                       ),
  //                       prefixIcon: Icon(Icons.link),
  //                       hintText: 'Enter Referral Code',
  //                       hintStyle: TextStyle(
  //                         color: lightBlack,
  //                       ),
  //                       border: OutlineInputBorder(
  //                         borderRadius: BorderRadius.circular(8),
  //                       ),
  //                     ),
  //                   ),
  //                   InkWell(
  //                     onTap: () {
  //                       // if (JoinCommunityController.text.isEmpty) {
  //                       //   CustomSnackbar.error(message: 'Please enter code');
  //                       // } else {
  //                       //   print(
  //                       //       'this is referral code: ${JoinCommunityController.text.trim().toString()}');
  //                       //   final JCP = Provider.of<JoinCommunityProvider>(context,
  //                       //       listen: false);
  //                       //   final jc = JoinCommunityModal(
  //                       //       referral_code:
  //                       //           JoinCommunityController.text.trim().toString());

  //                       //   JCP.joinCommunity(jc);
  //                       // }
  //                     },
  //                     child: Container(
  //                         height: 40,
  //                         width: 100,
  //                         decoration: BoxDecoration(
  //                             color: primaryColor,
  //                             borderRadius: BorderRadius.circular(7.0)),
  //                         child: Center(
  //                           child: Text(
  //                             'Join',
  //                             style: TextStyle(color: white),
  //                           ),
  //                         )),
  //                   )
  //                 ],
  //               ),
  //             ),
  //           ),
  //         );
  //       });
  // }

  _showExitDialogue(BuildContext context) {
    // final auth = FirebaseAuth.instance;
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext dialogContext) {
          final screenSize = MediaQuery.of(context).size;
          return AlertDialog(
            insetPadding: const EdgeInsets.all(30.0),
            backgroundColor: Colors.transparent,
            contentPadding: const EdgeInsets.all(0.0),
            content: Container(
              height: 150.0,
              width: screenSize.width,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(20.0),
                color: Colors.white,
              ),
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceAround,
                  children: <Widget>[
                    const SizedBox(
                      height: 1.0,
                    ),
                    Text(
                      "Updating and Coming Very Soon!",
                      style: appNormalText,
                      textAlign: TextAlign.center,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        TextButton(
                          style: TextButton.styleFrom(
                            foregroundColor: Colors.white,
                            padding: const EdgeInsets.all(16.0),
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                          child: Container(
                            height: 33,
                            width: 63,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  7.0,
                                ),
                                border: Border.all(color: black)),
                            child: const Center(
                              child: Text(
                                'Close',
                                style: appNormalText,
                              ),
                            ),
                          ),
                        ),
                      ],
                    )
                  ],
                ),
              ),
            ),
          );
        });
  }
}

const homeScreenText =
    TextStyle(fontSize: 11, color: black, fontWeight: FontWeight.bold);
const centerContainerTextStyle =
    TextStyle(fontWeight: FontWeight.bold, fontSize: 14, color: primaryColor);
const upcomingPrayer =
    TextStyle(color: primaryColor, fontSize: 14, fontWeight: FontWeight.w500);
