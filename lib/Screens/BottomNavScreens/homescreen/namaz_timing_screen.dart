import 'dart:io';

import 'package:aas/Provider/Prayers/get_prayers_status_provider.dart';
import 'package:aas/Provider/Prayers/mark_prayer_attendance_provider.dart';
import 'package:aas/Provider/ad_provider.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/constants/ad_helper.dart';
import 'package:adhan/adhan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';
import 'package:provider/provider.dart';
// import 'package:velocity_x/velocity_x.dart';
import 'package:dots_indicator/dots_indicator.dart';

import '../../../Constants/colors.dart';

class NamazTimingScreen extends StatefulWidget {
  const NamazTimingScreen({super.key});

  @override
  State<NamazTimingScreen> createState() => _NamazTimingScreenState();
}

class _NamazTimingScreenState extends State<NamazTimingScreen> {
  Location location = Location();
  int currentIndex = 3;
  LocationData? _currentLocation;
  double latitude = 0;
  double longitude = 0;

  late CarouselController _carouselController;

  @override
  void initState() {
    super.initState();
    _carouselController = CarouselController();
    _bannerAd = context.read<AdProvider>().createBannerAd();
    _bannerAd2 = context.read<AdProvider>().createBannerAd();

    // Call getLoc directly in initState
   getLoc();
    loadRewardedAd();
  }

  late BannerAd _bannerAd;
  late BannerAd _bannerAd2;
  RewardedAd? _rewardedAd;

  // init() async {
  //   await 

  //   // After getting the location, you can setState or perform any other actions as needed.
  //   setState(() {
  //     // Update any state variables if needed
  //   });
  // }

  bool forFajar = false;
  bool fajarCurrentDate = false;
  bool dhuhrCurrentDate = false;
  bool asrCurrentDate = false;
  bool maghribCurrentdate = false;
  bool ishaCurrentDate = false;
  bool forDhuhr = false;
  bool forAsr = false;
  bool forMaghrib = false;
  bool forIsha = false;
  bool useless = false;








// Function to format Duration into HH:mm format
  String formatDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, '0');
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    String twoDigitHours = twoDigits(duration.inHours);
    return "$twoDigitHours:$twoDigitMinutes";
  }

  getLoc() async {
    bool _serviceEnabled;
    PermissionStatus _permissionGranted;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted == PermissionStatus.granted) {
        return;
      }
    }

    _currentLocation = await location.getLocation();
    latitude = _currentLocation!.latitude!;
    longitude = _currentLocation!.longitude!;
    print('user lati is: $latitude');
    print('user longi is: $longitude');
    setState(() {
      latitude;
      longitude;
    });
  }

  void dispose() {
    super.dispose();
    _bannerAd.dispose();
    _bannerAd2.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double screenWidth = MediaQuery.of(context).size.width;
    final double screenHeight = MediaQuery.of(context).size.height;
    DateTime currentTime = DateTime.now();
    int CT = int.parse(DateFormat('HHmm').format(currentTime));
    final myCoordinates = Coordinates(latitude, longitude);
    final params = CalculationMethod.karachi.getParameters();
    params.madhab = Madhab.hanafi;
    final prayerTimes = PrayerTimes.today(myCoordinates, params);
    final DateFormat todayformatter = DateFormat('yyyy-MM-dd');
    final currentDate = DateTime.now();
    final String currentDateIs = todayformatter.format(currentDate);

    //    ********    1 days ago  ***********
    final oneDaysAgo = DateTime.now().subtract(Duration(days: 1));

    final oneDayAgoDate = DateComponents.from(oneDaysAgo);
    final oneDayAgoPrayerTimes =
        PrayerTimes(myCoordinates, oneDayAgoDate, params);

    final DateFormat formatter = DateFormat('yyyy-MM-dd');
    final String oneDayAgoDateIs = formatter.format(oneDaysAgo);
    // print('1 day ago date is: $oneDayAgoDateIs');
    //    ********    1 days ago  ***********

    //    ********    2 days ago  ***********
    final twoDaysAgo = DateTime.now().subtract(Duration(days: 2));
    final twoDaysAgoDate = DateComponents.from(twoDaysAgo);
    final twoDaysAgoPrayerTimes =
        PrayerTimes(myCoordinates, twoDaysAgoDate, params);
    final DateFormat twoDaysformatter = DateFormat('yyyy-MM-dd');
    final String twoDayAgoDateIs = twoDaysformatter.format(twoDaysAgo);
    // print('2 day ago date is: $twoDayAgoDateIs');

    //    ********    2 days ago  ***********

    //    ********    3 days ago  ***********
    final threeDaysAgo = DateTime.now().subtract(Duration(days: 3));
    final threeDaysAgoDate = DateComponents.from(threeDaysAgo);
    final threeDaysAgoPrayerTimes =
        PrayerTimes(myCoordinates, threeDaysAgoDate, params);

    final DateFormat threeDaysformatter = DateFormat('yyyy-MM-dd');
    final String threeDayAgoDateIs = threeDaysformatter.format(threeDaysAgo);
    // print('2 day ago date is: $threeDayAgoDateIs');
    //    ********    3 days ago  ***********
    //    ********    tomorrow days time  ***********

    final tomorrowDay = DateTime.now().add(Duration(days: 1));
    final tomorrowDate = DateComponents.from(tomorrowDay);
    final tomorrowDayPrayerTimes =
        PrayerTimes(myCoordinates, tomorrowDate, params);

    final DateFormat tomorrowFormatter = DateFormat('yyyy-MM-dd');
    final String tomorrowDateIs = tomorrowFormatter.format(tomorrowDay);
    // print(' tomorrow date is: $tomorrowDateIs');

    //    ********    tomorrow days time  ***********
    //    ********    overmorrow days time  ***********

    final overmorrowDay = DateTime.now().add(Duration(days: 2));
    final overmorrowDate = DateComponents.from(overmorrowDay);
    final overmorrowDayPrayerTimes =
        PrayerTimes(myCoordinates, overmorrowDate, params);

    final DateFormat overmorrowFormatter = DateFormat('yyyy-MM-dd');
    final String overmorrowDateIs = overmorrowFormatter.format(overmorrowDay);
    // print(' overmorrow date is: $overmorrowDateIs');

    //    ********    overmorrow days time  ***********
    //    ********    day after overmorrow  time  ***********

    final dayAfterOvermorrowDay = DateTime.now().add(Duration(days: 3));
    final dayAfterOvermorrowDate = DateComponents.from(dayAfterOvermorrowDay);
    final dayAfterOvermorrowDayPrayerTimes =
        PrayerTimes(myCoordinates, dayAfterOvermorrowDate, params);

    final DateFormat dayAfterOvermorrowFormatter = DateFormat('yyyy-MM-dd');
    final String dayAfterOvermorrowDateIs =
        dayAfterOvermorrowFormatter.format(dayAfterOvermorrowDay);
    // print(
    //     ' day after overmorrow date is: $dayAfterOvermorrowDateIs');

    //    ********    day after overmorrow  time  ***********

//   ********  today namaz times  **********
    int fajrTime = int.parse(DateFormat('HHmm').format(prayerTimes.fajr));
    int dhuhrTime = int.parse(DateFormat('HHmm').format(prayerTimes.dhuhr));
    int asrTime = int.parse(DateFormat('HHmm').format(prayerTimes.asr));
    int maghribTime = int.parse(DateFormat('HHmm').format(prayerTimes.maghrib));
    int ishaTime = int.parse(DateFormat('HHmm').format(prayerTimes.isha));

//   ********  today namaz times  **********

//    ********    1 days ago namz time ***********

    int oneDaysAgoFajarTime =
        int.parse(DateFormat('HHmm').format(oneDayAgoPrayerTimes.fajr));
    int oneDaysAgoDhuhrTime =
        int.parse(DateFormat('HHmm').format(oneDayAgoPrayerTimes.dhuhr));
    int oneDaysAgoAsrTime =
        int.parse(DateFormat('HHmm').format(oneDayAgoPrayerTimes.asr));
    int oneDaysAgoMaghribTime =
        int.parse(DateFormat('HHmm').format(oneDayAgoPrayerTimes.maghrib));
    int oneDaysAgoIshaTime =
        int.parse(DateFormat('HHmm').format(oneDayAgoPrayerTimes.isha));

//    ********    1 days ago namz time ***********

//    ********    2 days ago namz time ***********

    int twoDaysAgoFajarTime =
        int.parse(DateFormat('HHmm').format(twoDaysAgoPrayerTimes.fajr));
    int twoDaysAgoDhuhrTime =
        int.parse(DateFormat('HHmm').format(twoDaysAgoPrayerTimes.dhuhr));
    int twoDaysAgoAsrTime =
        int.parse(DateFormat('HHmm').format(twoDaysAgoPrayerTimes.asr));
    int twoDaysAgoMaghribTime =
        int.parse(DateFormat('HHmm').format(twoDaysAgoPrayerTimes.maghrib));
    int twoDaysAgoIshaTime =
        int.parse(DateFormat('HHmm').format(twoDaysAgoPrayerTimes.isha));
//    ********    2 days ago namz time ***********
//    ********    3 days ago namz time ***********

    int threeDaysAgoFajarTime =
        int.parse(DateFormat('HHmm').format(threeDaysAgoPrayerTimes.fajr));
    int threeDaysAgoDhuhrTime =
        int.parse(DateFormat('HHmm').format(threeDaysAgoPrayerTimes.dhuhr));
    int threeDaysAgoAsrTime =
        int.parse(DateFormat('HHmm').format(threeDaysAgoPrayerTimes.asr));
    int threeDaysAgoMaghribTime =
        int.parse(DateFormat('HHmm').format(threeDaysAgoPrayerTimes.maghrib));
    int threeDaysAgoIshaTime =
        int.parse(DateFormat('HHmm').format(threeDaysAgoPrayerTimes.isha));
//    ********    3 days ago namz time ***********
//    ********    tomorrow namz time ***********

    int tomorrowFajarTime =
        int.parse(DateFormat('HHmm').format(tomorrowDayPrayerTimes.fajr));
    int tomorrowDhuhrTime =
        int.parse(DateFormat('HHmm').format(tomorrowDayPrayerTimes.dhuhr));
    int tomorrowAsrTime =
        int.parse(DateFormat('HHmm').format(tomorrowDayPrayerTimes.asr));
    int tomorrowMaghribTime =
        int.parse(DateFormat('HHmm').format(tomorrowDayPrayerTimes.maghrib));
    int tomorrowIshaTime =
        int.parse(DateFormat('HHmm').format(tomorrowDayPrayerTimes.isha));

//    ********    tomorrow namz time ***********
//    ********    overmorrow namz time ***********

    int overmorrowDaysAgoFajarTime =
        int.parse(DateFormat('HHmm').format(overmorrowDayPrayerTimes.fajr));
    int overmorrowDaysAgoDhuhrTime =
        int.parse(DateFormat('HHmm').format(overmorrowDayPrayerTimes.dhuhr));
    int overmorrowDaysAgoAsrTime =
        int.parse(DateFormat('HHmm').format(overmorrowDayPrayerTimes.asr));
    int overmorrowDaysAgoMaghribTime =
        int.parse(DateFormat('HHmm').format(overmorrowDayPrayerTimes.maghrib));
    int overmorrowDaysAgoIshaTime =
        int.parse(DateFormat('HHmm').format(overmorrowDayPrayerTimes.isha));

//    ********    overmorrow namz time ***********
//    ******** day after overmorrow namz time ***********

    int dayAfterOvermorrowFajrTime = int.parse(
        DateFormat('HHmm').format(dayAfterOvermorrowDayPrayerTimes.fajr));
    int dayAfterOvermorrowDhuhrTime = int.parse(
        DateFormat('HHmm').format(dayAfterOvermorrowDayPrayerTimes.dhuhr));
    int dayAfterOvermorrowAsrTime = int.parse(
        DateFormat('HHmm').format(dayAfterOvermorrowDayPrayerTimes.asr));
    int dayAfterOvermorrowMaghribTime = int.parse(
        DateFormat('HHmm').format(dayAfterOvermorrowDayPrayerTimes.maghrib));
    int dayAfterOvermorrowIshaTime = int.parse(
        DateFormat('HHmm').format(dayAfterOvermorrowDayPrayerTimes.isha));

    return SafeArea(
      child: Container(
        color: Colors.black,
        // decoration: BoxDecoration(
        //   image: DecorationImage(
        //     image: AssetImage('assets/images/PrayerBackground.png'),
        //     fit: BoxFit.cover,
        //   ),
        // ),
        child: Scaffold(
          backgroundColor: Colors.transparent,
          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              Container(
                width: double.infinity,
                height: screenHeight * .1,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  // crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Container(),
                    Container(
                      width: MediaQuery.of(context).size.width,
                      height: 60,
                      child: AdWidget(ad: _bannerAd2),
                    ),

                    // SizedBox(width: 20,),
                  ],
                ),
              ),
              Column(
                children: [
                  Container(
                      child: Text('Watch Video to mark Attendance',
                          style: TextStyle(fontSize: 18, color: white))),
                  CarouselSlider.builder(
                    carouselController: _carouselController,
                    itemCount: 7,
                    options: CarouselOptions(
                      pageViewKey: PageStorageKey<String>('carousel_slider'),
                      height: 500.0,
                      enableInfiniteScroll: true,
                      initialPage: currentIndex,
                      viewportFraction: 1.0,
                      enlargeCenterPage: true,
                      autoPlay: false,
                      aspectRatio: 16 / 9,
                      scrollDirection: Axis.horizontal,
                      onPageChanged: (index, reason) {
                        setState(() {
                          currentIndex = index;
                          print("This is current Index: $currentIndex");
                        });
                      },
                    ),
                    itemBuilder:
                        (BuildContext context, int index, int realIndex) {
                      if (index == 0) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: currentDayTime(
                            threeDaysAgoPrayerTimes,
                            CT,
                            threeDaysAgoFajarTime,
                            threeDaysAgoDhuhrTime,
                            threeDaysAgoAsrTime,
                            threeDaysAgoMaghribTime,
                            threeDaysAgoIshaTime,
                            threeDayAgoDateIs,
                            index,
                          ),
                        );
                      } else if (index == 1) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: currentDayTime(
                            twoDaysAgoPrayerTimes,
                            CT,
                            twoDaysAgoFajarTime,
                            twoDaysAgoDhuhrTime,
                            twoDaysAgoAsrTime,
                            twoDaysAgoMaghribTime,
                            twoDaysAgoIshaTime,
                            twoDayAgoDateIs,
                            index,
                          ),
                        );
                      } else if (index == 2) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: currentDayTime(
                            oneDayAgoPrayerTimes,
                            CT,
                            oneDaysAgoFajarTime,
                            oneDaysAgoDhuhrTime,
                            oneDaysAgoAsrTime,
                            oneDaysAgoMaghribTime,
                            oneDaysAgoIshaTime,
                            oneDayAgoDateIs,
                            index,
                          ),
                        );
                      } else if (index == 3) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: currentDayTime(
                            prayerTimes,
                            CT,
                            fajrTime,
                            dhuhrTime,
                            asrTime,
                            maghribTime,
                            ishaTime,
                            currentDateIs,
                            index,
                          ),
                        );
                      } else if (index == 4) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: currentDayTime(
                            tomorrowDayPrayerTimes,
                            CT,
                            tomorrowFajarTime,
                            tomorrowDhuhrTime,
                            tomorrowAsrTime,
                            tomorrowMaghribTime,
                            tomorrowIshaTime,
                            tomorrowDateIs,
                            index,
                          ),
                        );
                      } else if (index == 5) {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: currentDayTime(
                            overmorrowDayPrayerTimes,
                            CT,
                            overmorrowDaysAgoFajarTime,
                            overmorrowDaysAgoDhuhrTime,
                            overmorrowDaysAgoAsrTime,
                            overmorrowDaysAgoMaghribTime,
                            overmorrowDaysAgoIshaTime,
                            overmorrowDateIs,
                            index,
                          ),
                        );
                      } else {
                        return Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: currentDayTime(
                            dayAfterOvermorrowDayPrayerTimes,
                            CT,
                            dayAfterOvermorrowFajrTime,
                            dayAfterOvermorrowDhuhrTime,
                            dayAfterOvermorrowAsrTime,
                            dayAfterOvermorrowMaghribTime,
                            dayAfterOvermorrowIshaTime,
                            dayAfterOvermorrowDateIs,
                            index,
                          ),
                        );
                      }
                    },
                  ),
                  DotsIndicator(
                    dotsCount: 7,
                    position: currentIndex,
                    decorator: DotsDecorator(
                      color: Colors.grey, // Inactive dot color
                      activeColor: Colors.white, // Active dot color
                    ),
                  ),
                ],
              ),
              Container(
                width: MediaQuery.of(context).size.width,
                height: 60,
                child: AdWidget(ad: _bannerAd),
              ),
            ],
          ),
        ),
      ),
    );
  }

  currentDayTime(PrayerTimes prayerTimes, int CT, int fajrTime, int dhuhrTime,
      int asrTime, int maghribTime, int ishaTime, String dateis, int index) {
    // print('this is index: $index');
    final DateFormat todayformatter = DateFormat('yyyy-MM-dd');
    final currentDate = DateTime.now();
    final String currentDateIs = todayformatter.format(currentDate);
    // print('current date is: ${currentDateIs} slide date is: ${dateis}');
    // print('this is fetched status: ${fetchedPrayersStatus!["5"] ??0}');
    // bool ishaStatus = fetchedPrayersStatus!["5"]! ?? true;
    // print('isha status: $ishaStatus');

    return FutureBuilder(
      future: fetchData(dateis),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          InitLoading().dismissLoading();
          bool ishaStatus = fetchedPrayersStatus!["5"]!;
          bool maghribStatus = fetchedPrayersStatus!["4"]!;
          bool asrStatus = fetchedPrayersStatus!["3"]!;
          bool dhuhrStatus = fetchedPrayersStatus!["2"]!;
          bool fajrStatus = fetchedPrayersStatus!["1"]!;
          // print('isha status: $ishaStatus');
          // Rest of your code...
          return SingleChildScrollView(
            child: Column(
              children: [
                Container(
                  height: 33,
                  child: Center(
                    child: Text(dateis.toString(),
                        style: TextStyle(
                            color: white,
                            fontSize: 20,
                            fontWeight: FontWeight.bold)),
                  ),
                ),

                Padding(
                    padding: EdgeInsets.all(18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        //    *************    Fajar Row    **************
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Image.asset(
                                'assets/images/fajar.png',
                                height: 50.0,
                                width: 50.0,
                              ),
                            ),
                            Text(
                              'Fajar',
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              DateFormat('hh:mm').format(prayerTimes.fajr) +
                                  ' AM',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                            CT > fajrTime &&
                                    CT < dhuhrTime &&
                                    dateis == currentDateIs &&
                                    index == 3
                                ? Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: black,
                                    activeColor: white,
                                    value: fajrStatus,
                                    onChanged: (value) {
                                      if (index == 3 &&
                                          CT > fajrTime &&
                                          CT < dhuhrTime) {
                                        showRewardedAd();
                                        setState(() {
                                          fajarCurrentDate = value!;
                                        });

                                        Provider.of<MarkPrayerAttendanceProvider>(
                                                context,
                                                listen: false)
                                            .markPrayerAttendance(dateis, 1);

                                        Future.delayed(Duration(seconds: 2),
                                            () {
                                          setState(() {});
                                        });
                                        // showRewardedAd();
                                      } else {}
                                    },
                                  )
                                : Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: Colors.white,
                                    activeColor:
                                        Color.fromRGBO(20, 77, 70, 1.0),
                                    value: fajrStatus,
                                    onChanged: (value) {},
                                  )
                          ],
                        )

                        //    *************    Fajar Row    **************  >>>>

                        //    *************    Dhuhr Row    **************  >>>>>
                      ],
                    )),
                Padding(
                    padding: EdgeInsets.all(18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Image.asset(
                                'assets/images/Zohar.png',
                                height: 50.0,
                                width: 50.0,
                              ),
                            ),
                            Text(
                              'Dhuhr',
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              DateFormat('hh:mm').format(prayerTimes.dhuhr) +
                                  ' PM',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                            CT > dhuhrTime && CT < asrTime
                                ? Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: black,
                                    activeColor: white,
                                    value: dhuhrStatus,
                                    onChanged: (value) async {
                                      if (index == 3 &&
                                          CT > dhuhrTime &&
                                          CT < asrTime) {
                                        // _loadAndShowInterstitialAd(context);
                                        showRewardedAd();
                                        setState(() {
                                          dhuhrCurrentDate = value!;
                                        });
                                        Provider.of<MarkPrayerAttendanceProvider>(
                                                context,
                                                listen: false)
                                            .markPrayerAttendance(dateis, 2);
                                        Future.delayed(Duration(seconds: 2),
                                            () {
                                          setState(() {});
                                        });
                                        // showRewardedAd();
                                        //
                                      }
                                    },
                                  )
                                : Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: Colors.white,
                                    activeColor:
                                        Color.fromRGBO(20, 77, 70, 1.0),
                                    value: dhuhrStatus,
                                    onChanged: (value) {},
                                  )
                          ],
                        )
                      ],
                    )),

                //    *************    Dhuhr Row    **************  >>>>>

                //    *************    Asr Row    **************
                Padding(
                    padding: EdgeInsets.all(18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Image.asset(
                                'assets/images/Asr.png',
                                height: 50.0,
                                width: 50.0,
                              ),
                            ),
                            Text(
                              'Asr',
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              DateFormat('hh:mm').format(prayerTimes.asr) +
                                  ' PM',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                            CT > asrTime && CT < maghribTime
                                ? Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: black,
                                    activeColor: white,
                                    value: asrStatus,
                                    onChanged: (value) {
                                      if (currentIndex == 3 &&
                                          CT > asrTime &&
                                          CT < maghribTime) {
                                        // _loadAndShowInterstitialAd(context);

                                        showRewardedAd();
                                        setState(() {
                                          asrCurrentDate = value!;
                                        });
                                        Provider.of<MarkPrayerAttendanceProvider>(
                                                context,
                                                listen: false)
                                            .markPrayerAttendance(dateis, 3);
                                        setState(() {});
                                        // showRewardedAd();
                                      }
                                    },
                                  )
                                : Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: Colors.white,
                                    activeColor:
                                        Color.fromRGBO(20, 77, 70, 1.0),
                                    value: asrStatus,
                                    onChanged: (value) {
                                      // setState(() {
                                      //   valuefirst = value!;
                                      // });
                                    },
                                  )
                          ],
                        )
                      ],
                    )),
                //    *************    Asr Row    **************>>>>

                //    *************    maghrib Row    **************
                Padding(
                    padding: EdgeInsets.all(18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Image.asset(
                                'assets/images/Magrab.png',
                                height: 50.0,
                                width: 50.0,
                              ),
                            ),
                            Text(
                              'Maghrib',
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              DateFormat('hh:mm').format(prayerTimes.maghrib) +
                                  ' PM',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                            CT > maghribTime && CT < ishaTime
                                ? Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: black,
                                    activeColor: white,
                                    value: maghribStatus,
                                    onChanged: (value) {
                                      if (currentIndex == 3 &&
                                          CT > maghribTime &&
                                          CT < ishaTime) {
                                        // _loadAndShowInterstitialAd(context);
                                        showRewardedAd();
                                        setState(() {
                                          maghribCurrentdate = value!;
                                        });
                                        Provider.of<MarkPrayerAttendanceProvider>(
                                                context,
                                                listen: false)
                                            .markPrayerAttendance(dateis, 4);
                                        setState(() {});
                                        // showRewardedAd();
                                      }
                                    },
                                  )
                                : Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: white,
                                    activeColor:
                                        Color.fromRGBO(20, 77, 70, 1.0),
                                    value: maghribStatus,
                                    onChanged: (value) {
                                      // setState(() {
                                      //   forMaghrib = value!;
                                      // });
                                    },
                                  )
                          ],
                        )
                      ],
                    )),
                //    *************    maghrib Row    **************>>>>

                //    *************    Isha Row    **************
                Padding(
                    padding: EdgeInsets.all(18.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        Row(
                          children: [
                            Container(
                              margin: EdgeInsets.symmetric(horizontal: 5.0),
                              child: Image.asset(
                                'assets/images/Esha.png',
                                height: 50.0,
                                width: 50.0,
                              ),
                            ),
                            Text(
                              'Isha',
                              style: TextStyle(
                                fontSize: 16.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.white,
                              ),
                            ),
                          ],
                        ),
                        Row(
                          children: [
                            Text(
                              DateFormat('hh:mm').format(prayerTimes.isha) +
                                  ' PM',
                              style: TextStyle(
                                fontSize: 18.0,
                                color: Colors.white,
                              ),
                            ),
                            CT > ishaTime && CT < 2359
                                ? Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: black,
                                    activeColor: white,
                                    value: ishaStatus,
                                    onChanged: (value) {
                                      if (index == 3 &&
                                          CT > ishaTime &&
                                          CT < 2359) {
                                        // _loadAndShowInterstitialAd(context);
                                        showRewardedAd();
                                        setState(() {
                                          ishaCurrentDate = value!;
                                        });
                                        Provider.of<MarkPrayerAttendanceProvider>(
                                                context,
                                                listen: false)
                                            .markPrayerAttendance(dateis, 5);
                                        Future.delayed(Duration(seconds: 2),
                                            () {
                                          setState(() {});
                                        });
                                        // showRewardedAd();
                                      }
                                    },
                                  )
                                : Checkbox(
                                    side: BorderSide(color: white),
                                    checkColor: white,
                                    activeColor:
                                        Color.fromRGBO(20, 77, 70, 1.0),
                                    value: ishaStatus,
                                    onChanged: (value) {},
                                  )
                          ],

                          //    *************    Isha Row    **************
                        )
                      ],
                    )),
              ],
            ),
          );
        } else {
          return InitLoading().showLoading('Loading...');
        }
      },
    );
  }

  Map<String, bool>? fetchedPrayersStatus;

  fetchData(String dateis) async {
    var provider =
        Provider.of<GetPrayersStatusProvider>(context, listen: false);
    await provider.getPrayersStatus(dateis);
    // print('slide date is: $dateis');

    // Access and print the data after the getPrayersStatus completes
    if (provider.prayerStatus != null) {
      // print('Status: ${provider.prayerStatus!.status}');
      print(
          'New api hit Prayers Status: ${provider.prayerStatus!.prayersStatus}');
      //  setState(() {
      fetchedPrayersStatus = provider.prayerStatus!.prayersStatus;
      //  print('this is status: ${fetchedPrayersStatus!["1"]}');
      //  });
    } else {
      print('Prayer status is not available yet');
    }
  }

  // void _loadAndShowInterstitialAd(BuildContext context) {
  //   // Access the AdProvider using the Provider.of method
  //   AdProvider adProvider = Provider.of<AdProvider>(context, listen: false);

  //   // Load the interstitial ad
  //   adProvider.loadInterstitialAd();

  //   // Check if the interstitial ad is loaded
  //   if (adProvider.isInterstitialAdLoaded) {
  //     // resetNumber == 0;
  //     // Show the interstitial ad
  //     adProvider.showInterstitialAd();
  //   } else {
  //     // The ad is not loaded yet, you can handle this case accordingly
  //     print('Interstitial ad is not loaded yet. Please try again later.');
  //   }
  // }
  void loadRewardedAd() {
    RewardedAd.load(
        adUnitId: Platform.isIOS
            ? AdHelper.rewardedAdUnitId
            : AdHelper.rewardedAdUnitId,
        request: const AdRequest(),
        rewardedAdLoadCallback:
            RewardedAdLoadCallback(onAdLoaded: (RewardedAd ad) {
          _rewardedAd = ad;
        }, onAdFailedToLoad: (LoadAdError error) {
          print('failed to load reward: $error');
          _rewardedAd = null;
        }));
  }

  void showRewardedAd() {
    if (_rewardedAd != null) {
      _rewardedAd!.fullScreenContentCallback = FullScreenContentCallback(
          onAdShowedFullScreenContent: (RewardedAd ad) {
        print("Ad onAdShowedFullScreenContent");
      }, onAdDismissedFullScreenContent: (RewardedAd ad) {
        ad.dispose();
        loadRewardedAd();
        InitLoading().dismissLoading();
      }, onAdFailedToShowFullScreenContent: (RewardedAd ad, AdError error) {
        ad.dispose();
        loadRewardedAd();
      });

      _rewardedAd!.setImmersiveMode(true);
      _rewardedAd!.show(
          onUserEarnedReward: (AdWithoutView ad, RewardItem reward) {
        print("${reward.amount} ${reward.type}");
      });
    } else {
      print('add is: $_rewardedAd');
      print('ad is null');
    }
  }
}
