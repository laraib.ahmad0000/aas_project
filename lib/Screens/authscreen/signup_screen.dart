import 'package:aas/Model/signup_modal.dart';
import 'package:aas/Provider/signup_provider.dart';
import 'package:aas/Provider/terms_conditions_provider.dart';
import 'package:aas/Screens/authscreen/login_screen.dart';
import 'package:aas/Screens/authscreen/users.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:aas/constants/reusable_widgets.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:device_info_plus/device_info_plus.dart';
import 'package:flutter/gestures.dart';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:get/get.dart';
// import 'package:modal_progress_hud_nsn/modal_progress_hud_nsn.dart';
import 'package:provider/provider.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:velocity_x/velocity_x.dart';

import '../../controller/auth_controller.dart';

class Signup extends StatefulWidget {
  const Signup({super.key});

  @override
  State<Signup> createState() => _SignupState();
}

class _SignupState extends State<Signup> {  AndroidDeviceInfo? androidDeviceInfo;

   @override
  void initState() {
    // TODO: implement initState
    super.initState();
    deviceInfo();
  }

  deviceInfo() async{
     final deviceInfo = DeviceInfoPlugin();
          androidDeviceInfo = await deviceInfo.androidInfo;
          setState(() {});

  }
  @override
  void dispose() {
    // TODO: implement dispose
    super.dispose();
    nameController.dispose();
    emailController.dispose();
    phoneController.dispose();
    passwordController.dispose();
    confirmPasswordController.dispose();
  }

  Uri? termsOfUse = Uri.parse('https://aasonline.co/terms-of-use');
  Uri? privacyPolicyUrl = Uri.parse('https://aasonline.co/privacy-policy');

  TextEditingController nameController = TextEditingController();
  TextEditingController emailController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController passwordController = TextEditingController();
  TextEditingController confirmPasswordController = TextEditingController();
  TextEditingController referralCodeController = TextEditingController();


  final _formKey = GlobalKey<FormState>();
  bool showError = false;
  final auth = Get.put(AuthController());
  double sizedBoxHeight = 450;
  bool showProgress = false;
  bool toggleTermAndCondi = false;
  @override
  Widget build(BuildContext context) {
    androidDeviceInfo != null ?
  print('device: ${androidDeviceInfo!.id}\n')
  :
  Text('');
    return GetBuilder(
      builder: (AuthController cont) {
         final double screenHeight = MediaQuery.of(context).size.height;
    final double screenWidth = MediaQuery.of(context).size.width;
        return Form(
          key: _formKey,
          child: Scaffold(
            body: SafeArea(
              child: SingleChildScrollView(
                scrollDirection: Axis.vertical,
                child: Column(
                  children: [
                    Stack(
                      children: [
                        Image.asset(
                          'assets/images/bg.png',
                          fit: BoxFit.cover,
                          width: double.infinity,
                          height: MediaQuery.sizeOf(context).height,
                        ),
                        Positioned(
                            child: Padding(
                          padding: const EdgeInsets.only(top: 10),
                          child: Center(
                              child: Image.asset("assets/images/logos.jpg")),
                        )),
                        Positioned(
                          top: 150.h,
                          left: MediaQuery.of(context).size.width * 0.05,
                          child: Column(
                            children: [
                              SingleChildScrollView(
                                physics: AlwaysScrollableScrollPhysics(), // Ensure scrollability
                                scrollDirection: Axis.vertical,
                                child: Container(
                                  width: screenWidth * .9,
                                  constraints: BoxConstraints(
                                    maxHeight: screenHeight * .7, // Limit maximum height
                                  ),
                                  child: Card(
                                    shape: RoundedRectangleBorder(
                                        borderRadius: BorderRadius.only(
                                            topRight: Radius.circular(30),
                                            bottomLeft: Radius.circular(30))),
                                    elevation: 10,
                                    child: SingleChildScrollView(
                                      child: Column(
                                        children: [
                                          // 0.heightBox,
                                          // "Sign Up"
                                          //     .text
                                          //     .bold
                                          //     .size(27)
                                          //     .color(
                                          //       Color.fromRGBO(20, 77, 70, 1.0),
                                          //     )
                                          //     .make(),
                                           Padding(
                                             padding: const EdgeInsets.only(top: 10),
                                             child: Text(
                                                'Sign Up',
                                                style: TextStyle(fontWeight: FontWeight.bold,
                                                fontSize: 20,
                                                    color: primaryColor),
                                              ),
                                           ),
                                               SizedBox(height: 10.0,),
                                          // 10.heightBox,
                                          CustomTextField(
                                            hint: 'Enter Name',
                                            textEditingController: nameController,
                                            textInputType: TextInputType.text,
                                            prefixIcon: Icon(
                                              Icons.person_outline,
                                              size: 20,
                                              color: primaryColor,
                                            ),
                                            validatorMessage: 'Please Enter Name',
                                          ),
                                          // 8.heightBox,
                                             SizedBox(height: 8.0,),
                                          CustomTextField(
                                            hint: 'Enter Email',
                                            textEditingController: emailController,
                                            textInputType: TextInputType.text,
                                            prefixIcon: Icon(
                                              Icons.email_outlined,
                                              size: 20,
                                              color: primaryColor,
                                            ),
                                            validatorMessage:
                                                'Please Enter Phone-Number',
                                          ),
                                          // 8.heightBox,
                                             SizedBox(height: 8.0,),
                                          CustomTextField(
                                            hint: 'Enter Number',
                                            textEditingController: phoneController,
                                            textInputType: TextInputType.text,
                                            prefixIcon: Icon(
                                              Icons.phone,
                                              size: 20,
                                              color: primaryColor,
                                            ),
                                            validatorMessage: 'Please Enter Email',
                                          ),
                                          // 8.heightBox,8
                                             SizedBox(height: 10.0,),
                                          CustomTextField(
                                            obscureText: true,
                                            hint: 'Enter Password',
                                            textEditingController:
                                                passwordController,
                                            textInputType: TextInputType.text,
                                            prefixIcon: Icon(
                                              Icons.lock_clock_outlined,
                                              size: 20,
                                              color: primaryColor,
                                            ),
                                            validatorMessage:
                                                'Please Enter Password',
                                          ),
                                          // 8.heightBox,
                                             SizedBox(height: 8.0,),
                                          CustomTextField(
                                            obscureText: true,
                                            hint: 'Enter Confirm Password',
                                            textEditingController:
                                                confirmPasswordController,
                                            textInputType: TextInputType.text,
                                            prefixIcon: Icon(
                                              Icons.lock_clock_outlined,
                                              size: 20,
                                              color: primaryColor,
                                            ),
                                            validatorMessage:
                                                'Please Confirm Password',
                                          ),
                                          // 8.heightBox,
                                             SizedBox(height: 8.0,),
                                      
                                          Padding(
                                            padding: const EdgeInsets.symmetric(
                                                horizontal: 20),
                                            child: TextFormField(
                                              // obscureText: widget.obscureText ?? false,
                                              controller: referralCodeController,
                                              decoration: InputDecoration(
                                                focusedErrorBorder:
                                                    OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                ),
                                                contentPadding:
                                                    EdgeInsets.all(0.13),
                                                focusedBorder: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                  borderSide: BorderSide(
                                                    color: primaryColor,
                                                    width: 1.0,
                                                  ),
                                                ),
                                                prefixIcon: Icon(Icons.link),
                                                hintText: 'Enter Referral Code',
                                                hintStyle: TextStyle(
                                                  color: Colors.black,
                                                ),
                                                border: OutlineInputBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(8),
                                                ),
                                              ),
                                              // validator: (value) {
                                              //   if (widget.textEditingController == referralCodeController &&
                                              //       skipReferralCodeValidation) {
                                              //     // Skip validation for referralCodeController
                                              //     return null;
                                              //   }
                                      
                                              //   if (value == null || value.isEmpty) {
                                              //     return widget.validatorMessage ?? 'Please Fill Field.';
                                              //   }
                                      
                                              //   return null;
                                              // },
                                            ),
                                          ),
                                             SizedBox(height: 4.0,),
                                          // 4.heightBox,
                                          Container(
                                            height: 50,
                                            width:
                                                MediaQuery.of(context).size.width,
                                            // color: lightBlack,
                                            child: Row(
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceBetween,
                                              children: [
                                                Container(
                                                  // color: yellowC,
                                                  width: 60,
                                                  child: Column(
                                                    crossAxisAlignment:
                                                        CrossAxisAlignment.start,
                                                    mainAxisAlignment:
                                                        MainAxisAlignment.start,
                                                    children: [
                                                      Checkbox(
                                                          checkColor: white,
                                                          activeColor: primaryColor,
                                                          value: context
                                                              .watch<
                                                                  TermsConditionProvider>()
                                                              .isChecked,
                                                          onChanged: (value) {
                                                            context
                                                                .read<
                                                                    TermsConditionProvider>()
                                                                .toggleCheckBox();
                                                          }),
                                                    ],
                                                  ),
                                                ),
                                                Expanded(
                                                    child: Padding(
                                                  padding: const EdgeInsets.only(
                                                      top: 7.0),
                                                  child: Container(
                                                    // color: greenC,
                                                    child: Column(
                                                      // crossAxisAlignment: CrossAxisAlignment.start,
                                                      // mainAxisAlignment: MainAxisAlignment.center,
                                                      children: [
                                                        RichText(
                                                            text: TextSpan(
                                                                children: [
                                                              TextSpan(
                                                                  text:
                                                                      'By clicking you agree our ',
                                                                  style: TextStyle(
                                                                      color:
                                                                          black)),
                                                              TextSpan(
                                                                  recognizer:
                                                                      TapGestureRecognizer()
                                                                        ..onTap =
                                                                            () async {
                                                                          if (await canLaunchUrl(
                                                                              Uri.parse(
                                                                                  termsOfUse!.toString()))) {
                                                                            // ignore: deprecated_member_use
                                                                            await launch(
                                                                                termsOfUse!.toString());
                                                                          } else {
                                                                            // print("could not launch url");
                                                                            CustomSnackbar.error(
                                                                                message:
                                                                                    'Something went wrong');
                                                                          }
                                                                        },
                                                                  text:
                                                                      'Terms of Use',
                                                                  style: TextStyle(
                                                                      color: greenC,
                                                                      decoration:
                                                                          TextDecoration
                                                                              .underline,
                                                                      decorationColor:
                                                                          greenC)),
                                                              TextSpan(
                                                                  text: '  &  ',
                                                                  style: TextStyle(
                                                                      color:
                                                                          black)),
                                                              TextSpan(
                                                                  recognizer:
                                                                      TapGestureRecognizer()
                                                                        ..onTap =
                                                                            () async {
                                                                          if (await canLaunchUrl(
                                                                              Uri.parse(
                                                                                  privacyPolicyUrl!.toString()))) {
                                                                            // ignore: deprecated_member_use
                                                                            await launch(
                                                                                privacyPolicyUrl!.toString());
                                                                          } else {
                                                                            // print("could not launch url");
                                                                            CustomSnackbar.error(
                                                                                message:
                                                                                    'Something went wrong');
                                                                          }
                                                                        },
                                                                  text:
                                                                      'Privacy Policy',
                                                                  style: TextStyle(
                                                                      color: greenC,
                                                                      decoration:
                                                                          TextDecoration
                                                                              .underline,
                                                                      decorationColor:
                                                                          greenC)),
                                                            ]))
                                                        // Text(
                                                        //     'By clicking you agree our '),
                                                        // InkWell(
                                                        //   onTap: () async {
                                                        // if (await canLaunchUrl(
                                                        //     Uri.parse(url!
                                                        //         .toString()))) {
                                                        //   // ignore: deprecated_member_use
                                                        //   await launch(
                                                        //       url!.toString());
                                                        // } else {
                                                        //   // print("could not launch url");
                                                        //   CustomSnackbar.error(
                                                        //       message:
                                                        //           'Something went wrong');
                                                        // }
                                                        //   },
                                                        //   child: Text(
                                                        //     'Terms and Services.',
                                                        // style: TextStyle(
                                                        // color: greenC,
                                                        // decoration:
                                                        //     TextDecoration
                                                        //         .underline,
                                                        // decorationColor:
                                                        //     greenC),
                                                        //   ),
                                                        // ),
                                                        // InkWell(
                                                        //   onTap: () async {
                                                        //     if (await canLaunchUrl(
                                                        //         Uri.parse(url!
                                                        //             .toString()))) {
                                                        //       // ignore: deprecated_member_use
                                                        //       await launch(
                                                        //           url!.toString());
                                                        //     } else {
                                                        //       // print("could not launch url");
                                                        //       CustomSnackbar.error(
                                                        //           message:
                                                        //               'Something went wrong');
                                                        //     }
                                                        //   },
                                                        //   child: Text(
                                                        //     'Privacy Policy.',
                                                        //     style: TextStyle(
                                                        //         color: greenC,
                                                        //         decoration:
                                                        //             TextDecoration
                                                        //                 .underline,
                                                        //         decorationColor:
                                                        //             greenC),
                                                        //   ),
                                                        // ),
                                                      ],
                                                    ),
                                                  ),
                                                ))
                                              ],
                                            ),
                                          ),
                                          // Row(
                                          //   children: [
                                      
                                          //   ],
                                          // ),
                                          signUpButton(cont, context),
                                      
                                          Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.center,
                                            children: [
                                              Text(
                                                'Already have an account? ',
                                                style: normaltextBlackC,
                                              ),
                                              GestureDetector(
                                                  onTap: () {
                                                    Get.to(LoginScreen());
                                                  },
                                                  child: Text('Login',
                                                      style: normaltextPrimaryC)),
                                            ],
                                          ),
                                          SizedBox(
                                            height: 50.0,
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),

                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        );
      },
    );
  }

  signUpButton(AuthController cont, BuildContext context) {
    return SizedBox(
      height: 45,
      width: 180,
      child: ElevatedButton(
        onPressed: () async {
          if (_formKey.currentState!.validate()) {
            String password = passwordController.text.trim().toString();
            String cPassword =
                confirmPasswordController.text.trim().toString();
            if (context.read<TermsConditionProvider>().isChecked == false) {
              CustomSnackbar.error(
                  message: 'Please agree with our terms & services.');
            } else if (password == cPassword) {
              final userProvider =
                  Provider.of<UserProvider>(context, listen: false);

              print(
                  'this is reffer code: ${referralCodeController.text.trim().toString()}');

              final newUser = User(
                  phoneno: phoneController.text.trim().toString(),
                  email: emailController.text.trim().toString(),
                  fname: nameController.text.trim().toString(),
                  password: passwordController.text.trim().toString(),
                  password_confirmation:
                      confirmPasswordController.text.trim().toString(),
                  referral_code:
                      referralCodeController.text.trim().toString(),
                      device_id: androidDeviceInfo!.id.toString(),
                      );

              userProvider.registerUser(newUser);
            } else {
              CustomSnackbar.error(message: 'Password doesn\'t match');
            }
            // if (password == cPassword) {
            // final userProvider =
            //     Provider.of<UserProvider>(context, listen: false);

            // final newUser = User(
            //   phoneno: phoneController.text.trim().toString(),
            //   email: emailController.text.trim().toString(),
            //   fname: nameController.text.trim().toString(),
            //   password: passwordController.text.trim().toString(),
            //   password_confirmation:
            //       confirmPasswordController.text.trim().toString(),
            // );

            // userProvider.registerUser(newUser);
            // }  else {
            //   CustomSnackbar.error(message: 'Password doesn\'t match');
            // }
            // final user = UserModel(
            //   name: cont.signup_namecontroller.text.trim(),
            //   phone: cont.signup_phone.text.trim(),
            //   email: cont.signup_email.text.trim(),
            //   password: cont.signup_password.text.trim(),
            // );

            // await AuthController.instance.createUser(user);

            // Navigator.of(context).pushReplacement(
            //   MaterialPageRoute(builder: (context) => LoginScreen()),
            // );

            // setState(() {
            //   showProgress = false;
            // });
          } else {
            // setState(() {
            //   showError = true;
            //   sizedBoxHeight = 500;
            //   showProgress = false;
            // });
          }
        },
        style: ElevatedButton.styleFrom(
          backgroundColor: primaryColor,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(20),
          ),
        ),
        child: CustomButton(
          text: 'Sign Up',
        ),
      ),
    );
  }

  // clearAllControllerFunction() {
  //   nameController.clear();
  //   emailController.clear();
  //   phoneController.clear();
  //   passwordController.clear();
  //   confirmPasswordController.clear();
  // }
}
