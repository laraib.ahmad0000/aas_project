import 'dart:io';
import 'package:country_picker/country_picker.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';
import 'package:intl_phone_field/intl_phone_field.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import '../../Provider/kyc/kyc_provider.dart';
import '../../constants/colors.dart';
import '../../constants/reusable_widgets.dart';

class KycScreen extends StatefulWidget {
  const KycScreen({super.key});

  @override
  State<KycScreen> createState() => _KycScreenState();
}

class _KycScreenState extends State<KycScreen> {
  final _formKey = GlobalKey<FormState>();
  TextEditingController nameController = TextEditingController();
  TextEditingController phoneController = TextEditingController();
  TextEditingController addressController = TextEditingController();
  TextEditingController idController = TextEditingController();
  TextEditingController accountTypeController = TextEditingController();
  TextEditingController bankNameController = TextEditingController();
  TextEditingController accountTitleController = TextEditingController();
  TextEditingController accountNumberController = TextEditingController();
  TextEditingController idTypeController = TextEditingController();
  TextEditingController dobController = TextEditingController();
  TextEditingController countryController = TextEditingController();

  String? selectedWalletType;
  String? selectedIdType;

  List<String> walletType = [
    "PayPal",
    "Payoneer",
    "Bank Account",
    "JazzCash",
    "EasyPesa",
  ];

  List<String> idType = [
    "CNIC",
    "Passport",
    "Driving Licence",
    "Face ID (under 18)",
  ];

  final Map<String, String> idTypeMap = {
    "CNIC": "1",
    "Passport": "2",
    "Driving Licence": "3",
    "Face ID (under 18)": "4",
  };

  @override
  void initState() {
    super.initState();
    selectedWalletType = walletType[0];
    selectedIdType = idType[0];
    accountTypeController.text = selectedWalletType!;
    idTypeController.text = idTypeMap[selectedIdType!]!;
  }

  File? _imgFile;
  File? _imgFile2;

  Future<void> pickImageFromGallery() async {
    final ImagePicker picker = ImagePicker();

    final XFile? img = await picker.pickImage(
      source: ImageSource.gallery,
      maxWidth: 320,
    );

    if (img == null) return;
    setState(() {
      _imgFile = File(img.path);
    });
  }

  Future<void> pickImageFromGalleryFile2() async {
    final ImagePicker picker = ImagePicker();

    final XFile? img = await picker.pickImage(
      source: ImageSource.gallery,
      maxWidth: 320,
    );

    if (img == null) return;
    setState(() {
      _imgFile2 = File(img.path);
    });
  }
  bool isFaceIDSelected() {

    return selectedIdType == "Face ID (under 18)";
  }



  @override
  Widget build(BuildContext context) {
    final kycProvider = Provider.of<KycProvider>(context);

    return SafeArea(
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          iconTheme: IconThemeData(color: white),
          backgroundColor: primaryColor,
          title: Text("KYC", style: TextStyle(color: white)),
          centerTitle: true,
        ),
        body: Form(
          key: _formKey,
          child: SingleChildScrollView(
            scrollDirection: Axis.vertical,
            child: Column(
              children: [
                buildTextField("Full Name:", nameController, "Enter Full Name"),
                buildDateTextField("Date of Birth:", dobController,
                    "Enter your Date of Birth"),
                SizedBox(height: 10),
                buildIDType(),
              if (!isFaceIDSelected()) ...[
                buildTextField(
                    "ID Number (Citizenship, Passport, Driving Licence)", idController, "Enter Your ID Number"),
                ],

                Row(
                  children: [
                    Padding(
                      padding: const EdgeInsets.only(top: 10,left: 20),
                      child: Text(
                        (!isFaceIDSelected()) ? "Choose file front side" : "Choose Selfi",
                        maxLines: 2,
                        style: TextStyle(
                            color: Colors.black, fontWeight: FontWeight.bold),
                      ),
                    ),
                  ],
                ),
                SizedBox(height: 10),
                buildImagePicker(),
                if (!isFaceIDSelected()) ...[
                  Row(
                    children: [
                      Padding(
                        padding: const EdgeInsets.only(top: 10,left: 20),
                        child: Text(
                          "Choose file back side",
                          maxLines: 2,
                          style: TextStyle(
                              color: Colors.black, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 10),
                  buildImagePickerFile2(),
                ],
                buildAccountTypeField(),
                buildTextField(
                    "Bank Name:", bankNameController, "Enter Your Bank Name"),
                buildTextField("Account Title:", accountTitleController,
                    "Enter Your Account Title"),
                buildTextField("Account Number:", accountNumberController,
                    "Enter Account Number"),
                buildCountryTextField(),
                buildPhoneTextField(),
                buildAddressField(),
                SizedBox(height: 20),
                submitButton(kycProvider, context),
                SizedBox(height: 10),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Padding buildTextField(
      String label, TextEditingController controller, String hint) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(label,
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0),
          TextFormField(
            controller: controller,
            decoration: InputDecoration(
              hintText: hint,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Padding buildAddressField() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text("Address:",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0),
          SizedBox(
            height: 100,
            child: TextField(
              controller: addressController,
              decoration: InputDecoration(
                hintText: "Enter Your Address",
                border: OutlineInputBorder(
                  borderSide: const BorderSide(),
                  borderRadius: BorderRadius.circular(8.0),
                ),
                contentPadding: const EdgeInsets.symmetric(
                    vertical: 16.0, horizontal: 12.0),
              ),
              keyboardType: TextInputType.multiline,
              maxLines: 3,
            ),
          ),
        ],
      ),
    );
  }

  Padding buildAccountTypeField() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text("Account Type:",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0),
          DropdownButtonFormField<String>(
            value: selectedWalletType,
            decoration: InputDecoration(
              hintText: 'Enter Account Type',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onChanged: (String? newValue) {
              setState(() {
                selectedWalletType = newValue;
                accountTypeController.text = newValue!;
              });
            },
            items: walletType.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ],
      ),
    );
  }

  Padding buildImagePicker() {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 20),
      child: Row(
        children: [
          InkWell(
            onTap: pickImageFromGallery,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: 120,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.black),
                  ),
                ),
                Text(
                  (!isFaceIDSelected()) ?   "Choose File" : "Choose Selfi",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 10),
          _imgFile != null
              ? Image.file(
            _imgFile!,
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          )
              : Text("No Image")
        ],
      ),
    );
  }

  Padding buildImagePickerFile2() {
    return Padding(
      padding: const EdgeInsets.only(top: 10, left: 20),
      child: Row(
        children: [
          InkWell(
            onTap: pickImageFromGalleryFile2,
            child: Stack(
              alignment: Alignment.center,
              children: [
                Container(
                  width: 120,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    border: Border.all(color: Colors.black),
                  ),
                ),
                Text(
                  "Choose File",
                  style: TextStyle(
                    color: Colors.black,
                    fontSize: 15,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ],
            ),
          ),
          SizedBox(width: 10),
          _imgFile2 != null
              ? Image.file(
            _imgFile2!,
            width: 100,
            height: 100,
            fit: BoxFit.cover,
          )
              : Text("No Image")
        ],
      ),
    );
  }

  Widget submitButton(KycProvider kycProvider, BuildContext context) {
    return GestureDetector(
      onTap: () async {
        final SharedPreferences prefs = await SharedPreferences.getInstance();
        String? token = prefs.getString('sessionToken');

        kycProvider.registerKYC(
          token: token,
          fullName: nameController.text,
          phone: phoneController.text,
          address: addressController.text,
          idNumber: (!isFaceIDSelected()) ? idController.text : '0',
          accType: accountTypeController.text,
          bankName: bankNameController.text,
          accTitle: accountTitleController.text,
          accNumber: accountNumberController.text,
          imgFile: _imgFile,
          imgFile2: (!isFaceIDSelected()) ? _imgFile2 : _imgFile,
          dob: dobController.text,
          country: countryController.text,
          number: idTypeController.text,
          context: context,
        );
      },
      child: CustomButton(text: 'Submit'),
    );
  }

  Widget buildIDType() {
    return Column(
      children: [
        Padding(
          padding: const EdgeInsets.only(left: 20),
          child: Row(
            children: [
              Text(
                "ID Type",
                style:
                TextStyle(color: Colors.black, fontWeight: FontWeight.bold),
              ),
            ],
          ),
        ),
        SizedBox(height: 10),
        Padding(
          padding: const EdgeInsets.only(left: 20, right: 20),
          child: DropdownButtonFormField<String>(
            value: selectedIdType,
            decoration: InputDecoration(
              hintText: 'Enter ID Type',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onChanged: (String? newValue) {
              setState(() {
                selectedIdType = newValue;
                idTypeController.text = idTypeMap[newValue!]!;
              });
            },
            items: idType.map<DropdownMenuItem<String>>((String value) {
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }).toList(),
          ),
        ),
      ],
    );
  }

  Padding buildCountryTextField() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text("Country:",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0),
          TextFormField(
            readOnly: true,
            controller: countryController,
            onTap: () {
              showCountryPicker(
                context: context,
                showPhoneCode: true,
                onSelect: (Country country) {
                  setState(() {
                    countryController.text = country.displayNameNoCountryCode;
                  });
                },
              );
            },
            decoration: InputDecoration(
              hintText: 'Enter Your Country',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget buildPhoneTextField() {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text("Phone Number:",
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0),
          IntlPhoneField(
            controller: phoneController,
            decoration: InputDecoration(
              hintText: 'Enter Phone Number',
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            initialCountryCode: 'US', // Initial country code
            onChanged: (phone) {
              print(phone.completeNumber); // Use phone.completeNumber for the full phone number
            },
          ),
        ],
      ),
    );
  }


  Padding buildDateTextField(
      String label, TextEditingController controller, String hint) {
    return Padding(
      padding: const EdgeInsets.only(left: 20, right: 20, top: 10),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.only(top: 10),
            child: Text(label,
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold)),
          ),
          SizedBox(height: 10.0),
          TextFormField(
            controller: controller,
            decoration: InputDecoration(
              hintText: hint,
              border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(10),
              ),
            ),
            onTap: () async {
              DateTime? pickedDate = await showDatePicker(
                context: context,
                initialDate: DateTime.now(),
                firstDate: DateTime(1900),
                lastDate: DateTime(2101),
              );
              if (pickedDate != null) {
                setState(() {
                  controller.text =
                  "${pickedDate.day}/${pickedDate.month}/${pickedDate.year}";
                });
              }
            },
          ),
        ],
      ),
    );
  }



}









