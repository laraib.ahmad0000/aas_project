import 'package:flutter_cache_manager/flutter_cache_manager.dart';

class VideoCacheManager {
  static const String kReelCacheKey = "reelsCacheKey";

  static final CacheManager cacheManager = CacheManager(
    Config(
      kReelCacheKey,
      stalePeriod: const Duration(days: 7),
      maxNrOfCacheObjects: 100,
      repo: JsonCacheInfoRepository(databaseName: kReelCacheKey),
      fileService: HttpFileService(),
    ),
  );

  static Future<void> cacheVideo(String videoUrl) async {
    try {
      FileInfo fileInfo = await cacheManager.downloadFile(videoUrl);

      print("Video cached successfully: ${fileInfo.file.path}");
    } catch (e) {
      print("Error caching video: $e");
    }
  }
}
