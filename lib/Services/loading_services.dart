import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';

class InitLoading {
Widget showLoading(String msg) {
    EasyLoading.show(status: msg);
    return Container(); // Placeholder container, replace it with your actual loading widget.
  }
  
  void dismissLoading() {
    EasyLoading.dismiss();
  }
}
