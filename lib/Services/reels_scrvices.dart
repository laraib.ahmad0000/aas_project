// import 'dart:convert';
// import 'dart:developer';

// import 'package:flutter_cache_manager/flutter_cache_manager.dart';
// import 'package:http/http.dart' as http;
// import 'package:shared_preferences/shared_preferences.dart';

// import 'cache_config.dart';

// class ReelService {
//   List<String> _reels = [];

//   String? videoIdForLike;
//   String? token;

//   Future<Object?> getVideosFromApI(Function(bool) onProgressUpdate) async {
//     SharedPreferences prefs = await SharedPreferences.getInstance();

//     final String apiUrl = 'https://aasonline.co/api/c-logs';

//     if (token == null) {
//       token =
//           "eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiJ9.eyJhdWQiOiIxIiwianRpIjoiNzc4ODlhNDg2ZGU4MzNkMTEzMzRiNDg0NzI4NTM5MzEwNWFmMzM5MDc1MWM5NzM5YmVmNDFjMmFmMGU4NDYzNDVmYWY3ZjVkNzA4MGZlMDUiLCJpYXQiOjE3MTIzOTE2NjUuMTgzOTUzLCJuYmYiOjE3MTIzOTE2NjUuMTgzOTU3LCJleHAiOjE3NDM5Mjc2NjUuMTgxNDk3LCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.SSD27tLkhyj935MEZ_t507n9a89HBiBuhYFLa4K1SVJHGOHoGz6vZXdhpNmExqIn8C82JMdrqBFNWExD3nG7mAL6WFWLvqmFotu7ZzuPVhzu4yVS8tdqWiMkjanx8wuvlkZEdJYci3AzaM9Nu-Oo5nA4S4-MmeJG1RzMJva_q5VCSwXPLDtialYRovGMTr5SiW-nTZc_ZbqDjT_WK_iAbstJymaXrG2ZxyaXVbn1XrfIZOVw9_IUkUfsEqo5pcdLsSthFvMapTegGNaT_a1HYzzdnrqcz-iw3Ns_rdZoEWvKCEV2UU8NQgomf_isfzdUesJv8mWUlosMoB8aGW3_yiE2HU7nhg55jQ1sxJr5GOM8F2x4QveDoP2sVFz2zBxOLCtYoLZv_LkGFWCxlEFndU-auCvaQoLPyfcZ3H_F7EfuY2bfOufaFBkgZI6jK74LnlsDBsUOZ7YWlIErcWYLLdAX80MYx_AkovTdB4xX5EEL5Zwvvb7z6Vwpc_DrKKkj9aMIvnFL5EXjOfPM8jW4jWR_6Q6f2yKXOCFKb3e6kvUEC54sk_sWHr-_UBdn942ooMugWNitopeFnHfvB4HxE7-jPB-i6SD16v5s-TNe_FBT-BagJ0-loI77-ZZHtf95Oy-QXP6Ar7V0g7jzuZPHpltBx5nlOARa2wWf4_s_XIE";
//     } else {
//        token = prefs.getString('sessionToken');
//     }

//     final data = {'id': 't'};
//     final response = await http.post(
//       Uri.parse('$apiUrl'),
//       body: json.encode(data),
//       headers: {
//         'Content-Type': 'application/json',
//         'Authorization': 'Bearer $token',
//       },
//     );

//     if (response.statusCode == 200) {
//       final List<dynamic> videoList = jsonDecode(response.body)['videos'];

//       for (var video in videoList) {
//         final String baseUrl = 'https://aasonline.co/storage/app';
//         var url = '$baseUrl/${video['url']}';
//         _reels.add(url);
//       }

//       for (var i = 0; i < _reels.length; i++) {
//         await cacheVideos(_reels[i], i, onProgressUpdate);
//       }

//       return _reels;
//     } else {
//       return [];
//     }
//   }

//   Future<void> cacheVideos(
//       String url, int i, Function(bool) onProgressUpdate) async {
//     onProgressUpdate(true); // Indicate that caching has started


//     FileInfo? fileInfo =
//     await VideoCacheManager.cacheManager.getFileFromCache(url);
//     if (fileInfo == null) {
//       await VideoCacheManager.cacheManager.downloadFile(url);
//       log('Downloaded file: $url');
//     } else {
//       log('File already in cache: $url');
//     }

//     if (i == _reels.length - 1) {
//       onProgressUpdate(false); // Indicate that caching has finished
//       log('Caching finished');
//     }
//   }

// }
