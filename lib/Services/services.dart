import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:aas/Model/MLM%20modals/cash_wallet_modal.dart';
import 'package:aas/Model/MLM%20modals/coins_wallet_modal.dart';
import 'package:aas/Model/MLM%20modals/coins_wallet_modal_two.dart';
import 'package:aas/Model/MLM%20modals/community_modal.dart';
import 'package:aas/Model/MLM%20modals/count_down_modal.dart';
import 'package:aas/Model/MLM%20modals/left_side_modal.dart';
import 'package:aas/Model/MLM%20modals/right_side_modal.dart';
import 'package:aas/Model/Other%20Users%20modals/other_followers_modal.dart';
import 'package:aas/Model/Other%20Users%20modals/other_following_modal.dart';
import 'package:aas/Model/Prayers/prayers_status_modal.dart';
import 'package:aas/Model/Profile%20Modals/cash_ledger_modal.dart';
import 'package:aas/Model/Profile%20Modals/notification_modal.dart';
import 'package:aas/Model/Transaction%20Modal/wallet_modal.dart';
import 'package:aas/Model/followers_modal.dart';
import 'package:aas/Model/following_modal.dart';
import 'package:aas/Model/gallery_modal.dart';
import 'package:aas/Model/get_bookmark_modal.dart';
import 'package:aas/Model/juzz_modal.dart';
import 'package:aas/Model/Profile%20Modals/coins_ledger_modal.dart';
import 'package:aas/Model/Profile%20Modals/task_modal.dart';
import 'package:aas/Model/total_prayers_modal.dart';
import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Model/user_post.dart';
import 'package:aas/Screens/BottomNavScreens/bottomnav.dart';
import 'package:aas/Screens/authscreen/login_screen.dart';
import 'package:aas/Screens/authscreen/otp_screen.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/constants/colors.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as http;
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../Provider/following_video_provider.dart';
import '../Provider/reels_video_provider.dart';

class ApiService {
  Future<void> registerUserApiService({
    required String phoneno,
    required String email,
    required String fname,
    required String password,
    required String password_confirmation,
    required String referral_code,
    required String device_id,
    required TextEditingController nameController,
    required TextEditingController emailController,
    required TextEditingController phoneController,
    required TextEditingController passwordController,
    required TextEditingController confirmPasswordController,
    required TextEditingController referralCodeController,
  }) async {
    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading....');
    String successfulMsg =
        '{"msg": "You have been successfully registered check your email"}';

    final Map<String, dynamic> data = {
      'phoneno': phoneno,
      'email': email,
      'fname': fname,
      'password': password,
      'password_confirmation': password_confirmation,
      'referral_code': referral_code,
      'device_id': device_id,
    };

    final response = await http.post(
      Uri.parse('$baseUrl/api/signup'),
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      // print('User registered successfully!');
      Map<String, dynamic> responseJson = json.decode(successfulMsg);
      String message = responseJson['msg'];
      CustomSnackbar.successful(message: message);
      InitLoading().dismissLoading();

      Get.to(LoginScreen());
      phoneController.clear();
      emailController.clear();
      nameController.clear();
      passwordController.clear();
      confirmPasswordController.clear();
      confirmPasswordController.clear();
      referralCodeController.clear();
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      final dynamic errorData = json.decode(response.body);
      if (errorData.containsKey("error") &&
          errorData["error"].containsKey("phoneno") &&
          errorData["error"]["phoneno"].isNotEmpty) {
        String phoneError = errorData["error"]["phoneno"][0];
        print('401 code error for phoneno is: $phoneError');
        // Display the error message as needed (e.g., show in a Snackbar or an AlertDialog)
        CustomSnackbar.error(message: phoneError);
      } else if (errorData.containsKey("error") &&
          errorData["error"].containsKey("email") &&
          errorData["error"]["email"].isNotEmpty) {
        InitLoading().dismissLoading();
        String emailError = errorData["error"]["email"][0];
        // print('401 code error for phoneno is: $emailError');
        CustomSnackbar.error(message: emailError);
      } else {
        InitLoading().dismissLoading();
        throw Exception('Failed to register user');
      }
    }
  }

  Future<void> userVerifyApiService({
    required String email,
    required String otp,
  }) async {
    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading....');
    String successfulMsg = '{"msg": "User Verified Successfully."}';

    final Map<String, dynamic> data = {
      'email': email,
      'pin': otp,
    };

    final response = await http.post(
      Uri.parse('$baseUrl/api/verify-pin'),
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      Map<String, dynamic> responseJson = json.decode(successfulMsg);
      String message = responseJson['msg'];
      await CustomSnackbar.successful(message: message);
      InitLoading().dismissLoading();

      Get.to(BottomNavScreen());
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      final dynamic errorData = json.decode(response.body)["msg"];

      CustomSnackbar.error(message: errorData);
    } else {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong Please try again!.');
      throw Exception('Failed to register user');
    }
  }

  Future<void> forgetPasswordApiService({
    required String email,
  }) async {
    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading....');
    String successMsg = 'Check your email for your password.';

    final Map<String, dynamic> data = {
      'email': email,
    };

    final response = await http.post(
      Uri.parse('$baseUrl/api/forgot-password'),
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      print(response.statusCode);
      InitLoading().dismissLoading();
      // Map<String, dynamic> responseJson = json.decode(successMsg);
      // String message = responseJson['msg'];
      // print(message);

      await CustomSnackbar.successful(
          message: 'Check your email for your password.');

      Get.to(const LoginScreen());
    } else if (response.statusCode == 404) {
      InitLoading().dismissLoading();
      final dynamic errorData = json.decode(response.body)["error"];
      CustomSnackbar.error(message: errorData);
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      final dynamic errorData = json.decode(response.body);
      String errorMSg = errorData["error"]["email"][0];
      // print('line number 151: ${errorData["error"]["email"][0]}');
      CustomSnackbar.error(message: errorMSg);
    } else {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong Please try again!.');
      throw Exception('Failed to register user');
    }
  }

  Future<void> loginUserService({
    required String phoneno,
    required String password,
  }) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading....');

    final Map<String, dynamic> data = {
      'phoneno': phoneno,
      'password': password,
    };

    final response = await http.post(
      Uri.parse('$baseUrl/api/login'),
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );
    if (response.statusCode == 200) {
      print(response.statusCode);
      InitLoading().dismissLoading();

      String successMsg = response.body;

      try {
        Map<String, dynamic> responseJson = json.decode(successMsg);

        if (responseJson.containsKey('token')) {
          String message = responseJson['token'];
          await prefs.setString('sessionToken', message);
          Get.to(BottomNavScreen());
        } else {
          print('Token not found in JSON response');
        }
      } catch (e) {
        print('Error decoding JSON: $e');
      }
    } else if (response.statusCode == 400) {
      print(response.statusCode);

      final dynamic errorData = json.decode(response.body);
      String errorMSg = errorData["failed"];
      InitLoading().dismissLoading();

      CustomSnackbar.error(message: errorMSg);
    } else if (response.statusCode == 401) {
      String successMsg = response.body;
      try {
        Map<String, dynamic> responseJson = json.decode(successMsg);

        if (responseJson.containsKey('token')) {
          String message = responseJson['token'];
          await prefs.setString('sessionToken', message);
          Get.to(OtpScreen(email: phoneno));
        } else {
          print('Token not found in JSON response');
        }
      } catch (e) {
        print('Error decoding JSON: $e');
      }

      print(response.statusCode);
      InitLoading().dismissLoading();
    } else if (response.statusCode == 404) {
      final msg = json.decode(response.body)["msg"];
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: msg);
    } else {
      print('this is status code: ${response.statusCode}');
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong Please try again!.');

      throw Exception('Failed to register user');
    }
  }

  Future<void> googleSignIn({
    required String email,
    String? name,
    String? profile_url,
    String? id,
  }) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();

    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading...');
    final Map<String, dynamic> data = {
      'email': email.toString(),
      'name': name.toString(),
      'profile_url': profile_url.toString(),
      'id': id.toString(),
    };
    final response = await http.post(
      Uri.parse('$baseUrl/api/login-with-google'),
      body: json.encode(data),
      headers: {'Content-Type': 'application/json'},
    );

    if (response.statusCode == 200) {
      print(response.statusCode);

      String successMsg = response.body;

      try {
        Map<String, dynamic> responseJson = json.decode(successMsg);
        print('this is sucess msg: $successMsg');

        if (responseJson.containsKey('token')) {
          String message = responseJson['token'];
          await prefs.setString('sessionToken', message);
          Get.to(BottomNavScreen());
        } else {
          print('Token not found in JSON response');
        }
      } catch (e) {
        print('Error decoding JSON: $e');
      }
      InitLoading().dismissLoading();
    } else if (response.statusCode == 400) {
      print(response.statusCode);

      final dynamic errorData = json.decode(response.body);
      // String errorMSg = errorData["failed"];
      InitLoading().dismissLoading();

      // CustomSnackbar.error(message: errorMSg);
    } else if (response.statusCode == 401) {
      print(response.statusCode);
      InitLoading().dismissLoading();
      // Get.to(OtpScreen(email: phoneno));
    } else if (response.statusCode == 404) {
      // final msg = json.decode(response.body)["msg"];
      InitLoading().dismissLoading();
      // CustomSnackbar.error(message: msg);
    } else {
      print('this is status code: ${response.statusCode}');
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong Please try again!.');

      throw Exception('Failed to register user');
    }
  }

  Future<void> uploadFile(File file, String description) async {
    InitLoading().showLoading('Loading...');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? uploadFile = prefs.getString('sessionToken');
    final url = Uri.parse('https://aasonline.co/api/video-store');
    Map<String, String> headers = {
      "Accesstoken": "access_token",
      'Authorization': 'Bearer $uploadFile',
    };

    var request = http.MultipartRequest('POST', url);
    request.files.add(await http.MultipartFile.fromPath(
      'video',
      file.path,
    ));
    request.headers.addAll(headers);
    // Add additional body parameters
    request.fields['video'] = file.path;
    request.fields['description'] = description;

    try {
      var response = await request.send();
      if (response.statusCode == 200) {
        print('file uploaded successfully');
        CustomSnackbar.successful(
            message: 'Uploaded Successfully. Wait untill it Approves.');
      } else {
        print(' failed with status code: ${response.statusCode}');
        InitLoading().dismissLoading();
      }
    } catch (error) {
      print('error uploading file: $error');
      InitLoading().dismissLoading();
    }
    InitLoading().dismissLoading();
  }

  Future<List<Map<String, dynamic>>> fetchSurahData() async {
    InitLoading().showLoading('Loading...');
    final response =
        await http.get(Uri.parse('http://api.alquran.cloud/v1/surah'));
    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body)['data'];
      // print('this is data: $data');
      InitLoading().dismissLoading();
      return List<Map<String, dynamic>>.from(data);
    } else {
      InitLoading().dismissLoading();
      print('error');
      throw Exception(response.statusCode);
    }
  }

  Future<List<Map<String, dynamic>>> fetchSurahDetails(int suradhId) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forGetSurahDetail = prefs.getString('sessionToken');
    InitLoading().showLoading('Loading...');
    final response = await http.get(
        Uri.parse('https://aasonline.co/api/get-surah?surah_no=$suradhId'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $forGetSurahDetail',
        });
    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body)['result'];
      InitLoading().dismissLoading();
      // print('this is surahs detail data: $data');
      return List<Map<String, dynamic>>.from(data);
    } else {
      InitLoading().dismissLoading();
      print('error');
      throw Exception(response.statusCode);
    }
  }

  Future<List<Map<String, dynamic>>> fetchParaDetails(int parah_no) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forGetParaDetail = prefs.getString('sessionToken');
    InitLoading().showLoading('Loading...');
    final response = await http.get(
        Uri.parse('https://aasonline.co/api/get-parah?parah_no=$parah_no'),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $forGetParaDetail',
        });
    if (response.statusCode == 200) {
      final List<dynamic> data = json.decode(response.body)['result'];
      InitLoading().dismissLoading();
      // print('this is surahs detail data: $data');
      return List<Map<String, dynamic>>.from(data);
    } else {
      InitLoading().dismissLoading();
      print('error');
      throw Exception(response.statusCode);
    }
  }

  Future<List<Map<String, dynamic>>> fetchQariDetails() async {
    InitLoading().showLoading('Loading...');
    final response =
        await http.get(Uri.parse('https://quranicaudio.com/api/qaris'));

    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      final List<dynamic> data = json.decode(response.body);

      return List<Map<String, dynamic>>.from(data);
    } else {
      InitLoading().dismissLoading();
      print('error');
      throw Exception(response.statusCode);
    }
  }

  Future<JuzModel> getJuzz(int index) async {
    String url = "http://api.alquran.cloud/v1/juz/$index/quran-uthmani";
    final response = await http.get(Uri.parse(url));

    if (response.statusCode == 200) {
      return JuzModel.fromJSON(json.decode(response.body));
    } else {
      print("Failed to load");
      throw Exception("Failed  to Load Post");
    }
  }

  static Future<UserModal> fetchUserData(
      {double? lat,
      double? long,
      String? app_version,
      String? device_key}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? homeScreen = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/home';
    final Map<String, dynamic> data = {
      'lat': lat,
      'long': long,
      'app_version': app_version,
      'device_key': device_key
    };
    try {
      // print('token from home screen: $homeScreen');

      final response = await http.post(
        Uri.parse(url),
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $homeScreen',
        },
      );

      if (response.statusCode == 200) {
        final List<dynamic> responseData = json.decode(response.body);

        if (responseData.isNotEmpty) {
          UserModal user = UserModal.fromJson(responseData[0]['user']);
          SharedPreferences prefs = await SharedPreferences.getInstance();
          // await prefs.setInt('isUpdated', user.isUpdated);
          await prefs.setInt('isBlocked', user.isBlocked);
          // int? isUpdatedValue = prefs.getInt('isUpdated');
          // print('from services class value is: $isUpdatedValue');

          return UserModal.fromJson(responseData[0]['user']);
        }
      } else if (response.statusCode == 302) {
        print('Failed to fetch user data. Status code: ${response.statusCode}');
      } else {
        print('home screen: ${response.statusCode}');
      }
    } catch (error) {
      print('Error fetching user data: $error');
    }

    return UserModal(
      id: 0,
      name: '',
      email: '',
      followers: 0,
      profileimage: '',
      following: 0,
      posts: 0,
      cash_balance: '',
      referral_code: '',
      isParent: 0,
      isChild: 0,
      today_prayers: 0,
      left_members: 0,
      right_members: 0,
      left_members_prayers: 0,
      right_members_prayers: 0,
      coins_balance: '',
      phoneNumber: '',
      isDelete: 0,
      isQuranReward: 0,
      isTasbeehReward: 0,
      isUpdated: 0,
      isBlocked: 0,
      kyc: 0,
    );
  }

  Future<void> toggleBM(
      {required String arabicName,
      required String englishName,
      required int surahNo}) async {
    InitLoading().showLoading('Loading...');
    String url = 'https://aasonline.co/api/toggle-bm';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? toggleBM = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'arabic_name': arabicName,
      'english_name': englishName,
      'surah_no': surahNo,
    };
    final response =
        await http.post(Uri.parse(url), body: json.encode(data), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $toggleBM',
    });
    if (response.statusCode == 200) {
      print(response.statusCode);
      Map<String, dynamic> responseJson = json.decode(response.body);
      InitLoading().dismissLoading();
      // print(
      // 'this is stored and not stored bookmarked data from get bookmark: $responseJson');
    } else {
      InitLoading().dismissLoading();
      print('this is status code from get bookmark: ${response.statusCode}');
    }
  }

  static Future<GetBookmarkModal> getBookmarkData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? getBookmarks = prefs.getString('sessionToken');
    InitLoading().showLoading('Loading...');
    String url = 'https://aasonline.co/api/bookmarks';

    try {
      final response = await http.get(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $getBookmarks',
        },
      );

      if (response.statusCode == 200) {
        final Map<String, dynamic> responseData = json.decode(response.body);
        InitLoading().dismissLoading();
        if (responseData.isNotEmpty) {
          // print('this is get bookmark data: ${responseData['bookmarks']}');
          return GetBookmarkModal.fromJson(responseData['bookmarks']);
        }
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        // print('no bookmark');
      } else {
        InitLoading().dismissLoading();
        // print('Failed to get bookmark. Status code: ${response.statusCode}');
      }

      // Return a default or empty GetBookmarkModal object in case of error
      InitLoading().dismissLoading();
      return GetBookmarkModal(status: 0, bookmarks: []);
    } catch (error) {
      InitLoading().dismissLoading();
      print('Error during getting bookmark: $error');
      InitLoading().dismissLoading();
      // Return a default or empty GetBookmarkModal object in case of error
      return GetBookmarkModal(status: 0, bookmarks: []);
    }
  }

  Future<void> likeUnlikeUser({
    required int videoId,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forLikeUnlikeUser = prefs.getString('sessionToken');

    String url = 'https://aasonline.co/api/like';

    final data = {'video_id': videoId.toString()};
    print("This is vidoe Id Data: ${data}");
    try {
      final response = await http.post(
        Uri.parse(url),
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $forLikeUnlikeUser',
        },
      );
      if (response.statusCode == 200) {
        final responseData = jsonDecode(response.body);
      } else {
        print('error during like user: ${response.statusCode}');
      }
    } catch (e) {
      print(e);
    }
  }

  Future<void> shareVideo({
    required String video_id,
    required BuildContext context,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forShareVideo = prefs.getString('sessionToken');
    // InitLoading().showLoading('Loading...');
    String url = 'https://aasonline.co/api/share';

    final data = {'video_id': video_id.toString()};
    try {
      final response = await http.post(
        Uri.parse(url),
        body: json.encode(data),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $forShareVideo',
        },
      );
      if (response.statusCode == 200) {
        InitLoading().showLoading("Shares Successfully");

        print(response.statusCode);

        Timer(Duration(seconds: 2), () {
          InitLoading().dismissLoading();


        });
      } else {
        // InitLoading().dismissLoading();
        print('error during sharevideo: ${response.statusCode}');
      }
    } catch (e) {
      // InitLoading().dismissLoading();
      print(e);
    }
  }

  static Future<Map<String, dynamic>> fetchFollow({
    required String user_id,
  }) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/follow';

    try {
      final response = await http.post(
        Uri.parse(url),
        headers: {
          'Content-Type': 'application/json',
          'Authorization': 'Bearer $token',
        },
        body: jsonEncode({'user_id': user_id.toString()}),
      );

      if (response.statusCode == 200) {
        Map<String, dynamic> responseData = json.decode(response.body);
        return responseData;
      } else {
        throw Exception(
            'Failed to fetch follow. Status code: ${response.statusCode}');
      }
    } catch (error) {
      throw Exception('Error fetching user data: $error');
    }
  }

  static Future<Follower> getFollowersList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forFollowers = prefs.getString('sessionToken');
    // InitLoading().showLoading('Loading...');
    String url = 'https://aasonline.co/api/followers';

    try {
      final response = await http.post(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forFollowers',
      });

      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        // InitLoading().dismissLoading();
        if (responseData.isNotEmpty) {
          // print('this is get bookmark data: ${responseData['bookmarks']}');
          return Follower.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        // print('no bookmark');
      } else {
        InitLoading().dismissLoading();
        // print('Failed to get bookmark. Status code: ${response.statusCode}');
      }

      // Return a default or empty GetBookmarkModal object in case of error
      InitLoading().dismissLoading();
      return Follower(status: 0, msg: '0', followers: []);
    } catch (error) {
      InitLoading().dismissLoading();
      print('Error during getting followers list: $error');
      InitLoading().dismissLoading();
      // Return a default or empty GetBookmarkModal object in case of error
      return Follower(status: 0, msg: '0', followers: []);
    }
  }

  static Future<FollowingModal> getFollowingList() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forFollowings = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/following';
    try {
      final response = await http.post(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forFollowings',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return FollowingModal.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        print('status code is: ${response.statusCode}');
        InitLoading().dismissLoading();
      } else {
        print(
            'Failed to get following list. Status code: ${response.statusCode}');
      }
      return FollowingModal(status: 0, msg: '', following: []);
    } catch (error) {
      print('error during getting following list: $error');
      return FollowingModal(status: 0, msg: '', following: []);
    }
  }

  Future<List<dynamic>> getComments(String videoId) async {
    String url = "https://aasonline.co/api/comment?video_id=$videoId";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    final response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    });

    if (response.statusCode == 200) {
      final responseData = json.decode(response.body);
      List<dynamic> comments = responseData['comments'];
      print("This is Comments: $comments");

      return comments;
    } else {
      throw Exception('Failed to load comments');
    }
  }

  Future<void> deleteComment(int commentId) async {
    String url = "https://aasonline.co/api/comment?comment_id=$commentId";
    print("This is deleted URL: $url");
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    final response = await http.delete(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");

    if (response.statusCode == 200) {
      print("Deleted Successfully");
    } else {
      throw Exception('Failed to delete comment');
    }
  }

  Future<void> postComment(String videoId, String comment) async {
    String url = "https://aasonline.co/api/comment";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    final data = {'video_id': videoId, 'comment': comment};

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode != 200) {
      throw Exception('Failed to post comment');
    }
  }

  Future<void> postLikeComment(int commentId) async {
    String url = "https://aasonline.co/api/clike";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'comment_id': commentId,
    };

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      print("Comment Liked Successfully");
    } else {
      throw Exception('Failed to Like comment');
    }
  }



  Future<void> postReply(int commentId, String message) async {
    String url = "https://aasonline.co/api/reply";

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'comment_id': commentId,
      'reply': message,
    };
    final response =
        await http.post(Uri.parse(url), body: json.encode(data), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");

    if (response.statusCode == 200) {
      print("Deleted Successfully");
    } else {
      throw Exception('Failed to delete comment');
    }
  }



  Future<void> postLikeReply(int replyId) async {
    String url = "https://aasonline.co/api/rlike";
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'reply_id': replyId,
    };

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      },
    );

    if (response.statusCode == 200) {
      print("Comment Liked Successfully");
    } else {
      throw Exception('Failed to Like comment');
    }
  }

  Future<void> viewClogs(int videoId) async {
    String url = "https://aasonline.co/api/view";

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'video_id': videoId,
    };
    final response =
    await http.post(Uri.parse(url), body: json.encode(data), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $token',
    });

    print("Response status: ${response.statusCode}");
    print("Response body: ${response.body}");

    if (response.statusCode == 200) {
      print("View incremented successfully");
    } else {
      throw Exception('Failed to increment view');
    }
  }


  Future<void> userReport({
    required String user_id,
    required String report,
  }) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');
    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading....');

    final Map<String, dynamic> data = {
      'user_id': user_id,
      'description': report,
    };

    final response = await http.post(
      Uri.parse('$baseUrl/api/report-against-user'),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.toString()}'
      },
    );
    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      CustomSnackbar.successful(message: 'User report Successfully');
      try {
        Map<String, dynamic> responseData = json.decode(response.body);
        print("THis is UserReport Response: $responseData");
        print("THis is UserReport Body: ${data['description']}");
      } catch (e) {
        print('Error decoding JSON: $e');
      }
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Please enter message');
    } else {
      print('this is status code: ${response.statusCode}');
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong Please try again!.');

      throw Exception('Failed to Report user');
    }
  }

  Future<void> userBlock({
    required String user_id,
  }) async {
    // String? msg;
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');
    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading....');

    final Map<String, dynamic> data = {
      'user_id': user_id,
    };

    final response = await http.post(
      Uri.parse('$baseUrl/api/block'),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${token.toString()}'
      },
    );
    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      CustomSnackbar.successful(message: "User Blocked Successfully");

      try {
        Map<String, dynamic> responseData = json.decode(response.body);
        print("THis is User Block Response: $responseData");

        // msg = responseData['msg'];

        // msg == "Unblocked success"
        //     ? CustomSnackbar.error(message: "${msg}")
        //     : CustomSnackbar.successful(message: "${msg}");
      } catch (e) {
        print('Error decoding JSON: $e');
      }
    } else {
      print('this is status code: ${response.statusCode}');
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong Please try again!.');

      throw Exception('Failed to Block user');
    }
  }

  static Future<UserPost> getUserPost() async {
    // List? userVideos;
    String apiUrl = 'https://aasonline.co/api/posts';

    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? getUserPost = prefs.getString('sessionToken');

    try {
      final response = await http.get(Uri.parse(apiUrl), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $getUserPost',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return UserPost.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        // print('no bookmark');
      } else {
        throw Exception('Failed to load videos');
      }
      return UserPost();
    } catch (e) {
      print('error from services: $e');
    }
    return UserPost();
  }

  static Future<OtherFollower> getOtherFollowersList(
      {required String? id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');

    String url = 'https://aasonline.co/api/followers-other?user_id=$id';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });

      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        print("THis is other followers: ${responseData}");
        if (responseData.isNotEmpty) {
          return OtherFollower.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        // print('no bookmark');
      } else {
        InitLoading().dismissLoading();
      }

      InitLoading().dismissLoading();
      return OtherFollower(status: 0, msg: '0', followers: []);
    } catch (error) {
      InitLoading().dismissLoading();
      print('Error during getting Other followers list: $error');
      InitLoading().dismissLoading();

      return OtherFollower(status: 0, msg: '0', followers: []);
    }
  }

  static Future<OtherFollowingModal> getOtherFollowingList(
      {required String? id}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/following-other?user_id=$id';
    print("THis is other followinglist URL: ${url}");
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return OtherFollowingModal.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        print('status code is: ${response.statusCode}');
        InitLoading().dismissLoading();
      } else {
        print(
            'Failed to get following list. Status code: ${response.statusCode}');
      }
      return OtherFollowingModal(status: 0, msg: '', following: []);
    } catch (error) {
      print('error during getting following list: $error');
      return OtherFollowingModal(status: 0, msg: '', following: []);
    }
  }

  static Future<TaskModel> getTask({required String date}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? token = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/tasks?date=$date';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $token',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return TaskModel.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        print('status code is: ${response.statusCode}');
      } else {
        print('Failed to get Tasks list. Status code: ${response.statusCode}');
      }
      return TaskModel(status: 0, msg: '', taskdetail: []);
    } catch (error) {
      print('error during getting Tasks list: $error');
      return TaskModel(status: 0, msg: '', taskdetail: []);
    }
  }

  Future<void> taskCompleted({
    required int task_id,
    required String date,
  }) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forTaskCompleted = prefs.getString('sessionToken');
    String baseUrl = 'https://aasonline.co';
    InitLoading().showLoading('Loading....');

    final Map<String, dynamic> data = {
      'task_id': task_id,
      'date': date,
    };

    final response = await http.post(
      Uri.parse('$baseUrl/api/task-completed'),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ${forTaskCompleted.toString()}'
      },
    );

    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      CustomSnackbar.successful(message: 'Task Completed Successfully');
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      // CustomSnackbar.error(message: 'Task Already Completed');
      Get.snackbar('Note', 'Note',
          duration: Duration(seconds: 3),
          backgroundColor: Colors.green,
          colorText: black,
          snackPosition: SnackPosition.BOTTOM,
          messageText: Text(
            'Task Already Completed',
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.w400),
          ));
    } else {
      print('this is status code: ${response.statusCode}');
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong Please try again!.');
    }
    InitLoading().dismissLoading();
  }

  static Future<TotalPrayers> totalPrayers() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forTotalPrayers = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/total-prayers';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forTotalPrayers',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        print('this is response data: ${responseData}');
        if (responseData.isNotEmpty) {
          return TotalPrayers(totalPrayers: responseData['total_prayers']);
        }
      } else if (response.statusCode == 404) {
        print('status code is: ${response.statusCode}');
      } else {
        print(
            'Failed to get total prayers. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('error during getting Tasks list: $e');
    }

    return totalPrayers();
  }

  Future<void> joinCommunity({
    required String referral_code,
  }) async {
    InitLoading().showLoading('Loading....');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forJoinCommunity = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/join-community';
    final data = {
      'referral_code': referral_code,
    };

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forJoinCommunity'
      },
    );

    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      Get.to(BottomNavScreen());
      // Navigator.push(context, MaterialPageRoute(builder: (context)=> HomeScreen()));

      CustomSnackbar.successful(message: 'Community Joined Successfully');
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      final Map<String, dynamic> errorData = json.decode(response.body);
      print('Error message: ${errorData['error']}');
      CustomSnackbar.error(
          message: 'Sorry, user already have parent or child.');
    } else if (response.statusCode == 401) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Invalid referral Code');
    } else {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong');
    }
  }

  static const String _obfuscatedKey = 'k235sfK_asfsfK23';

  Future<void> namazTimingStoreInShared() async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    final String? s = prefs.getString(_obfuscatedKey);
    if (s == null) {
      final String d = DateTime.now().toIso8601String();
      await prefs.setString(_obfuscatedKey, d);
    } else {
      final DateTime sd = DateTime.parse(s);
      final DateTime cd = DateTime.now();
      final Duration diff = cd.difference(sd);

      if (diff.inDays >= 10) {
        removeNamazTimingFromStorage();
      }
    }
  }

  static Future<PrayerStatusModal> getPrayersStatus(
      {required String? date}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forGetPrayersStatus = prefs.getString('sessionToken');

    String url = 'https://aasonline.co/api/prayers-status?date=$date';
    final response = await http.get(Uri.parse(url), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $forGetPrayersStatus',
    });
    if (response.statusCode == 200) {
      final Map<String, dynamic> responseData = json.decode(response.body);
      return PrayerStatusModal.fromJson(responseData);
    } else {
      throw Exception('Failed to load prayers status');
    }
  }

  Future markPrayerAttendance(
      {required String date, required int prayer_number}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forMarkAttendance = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/prayer-attendance';
    final data = {
      'date': date,
      'prayer_number': prayer_number,
    };

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forMarkAttendance'
      },
    );
    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      CustomSnackbar.successful(message: 'Attendance marked Successfully');
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();

      // print('Error message: ${errorData['error']}'
      CustomSnackbar.error(message: 'Unable to mark your attendance');
    } else if (response.statusCode == 401) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Invalid Date');
    } else if (response.statusCode == 406) {
      InitLoading().dismissLoading();
      // CustomSnackbar.error(message: 'Attendance already marked');
      Get.snackbar('Note', 'Note',
          duration: Duration(seconds: 3),
          backgroundColor: Colors.green,
          colorText: black,
          snackPosition: SnackPosition.BOTTOM,
          messageText: Text(
            'Attendance already marked',
            style: const TextStyle(
                color: Colors.black, fontWeight: FontWeight.w400),
          ));
    } else {
      print(response.statusCode);
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong');
    }
  }

  static Future<LedgerDetailModal> getLedgerDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? getLedgerDetail = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/coins-ledger';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $getLedgerDetail'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return LedgerDetailModal.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        print('status code is: ${response.statusCode}');
      } else {
        print(
            'Failed to get ledger detail. Status code: ${response.statusCode}');
      }
      return LedgerDetailModal(status: 0, msg: '', ledger: []);
    } catch (e) {
      print('error during getting ledger detail:  $e');
      return LedgerDetailModal(status: 0, msg: '', ledger: []);
    }
  }

  static Future<CashLedgerDetailModal> getCashLedgerDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? getCashLedgerDetail = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/cash-ledger';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $getCashLedgerDetail'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return CashLedgerDetailModal.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        print('status code is: ${response.statusCode}');
      } else {
        print(
            'Failed to get cash ledger detail. Status code: ${response.statusCode}');
      }
      return CashLedgerDetailModal(status: 0, msg: '', ledger: []);
    } catch (e) {
      print('error during cash getting ledger detail:  $e');
      return CashLedgerDetailModal(status: 0, msg: '', ledger: []);
    }
  }

  static Future<NotificationModal> getNotification() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? getNotiDetails = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/notifications';
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $getNotiDetails'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return NotificationModal.fromJson(responseData);
        } else if (response.statusCode == 404) {
          print('status code is: ${response.statusCode}');
        }
      } else {
        print(
            'Failed to get ledger detail. Status code: ${response.statusCode}');
      }
      return NotificationModal(status: 0, msg: '', notifications: []);
    } catch (e) {
      print('error during getting noti detail:  $e');
      return NotificationModal(status: 0, msg: '', notifications: []);
    }
  }

  static Future<GalleryModal> getGalleryData() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? getGalleryData = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/islamic-gallery';
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $getGalleryData'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return GalleryModal.fromJson(responseData);
        } else if (response.statusCode == 404) {
          print('status code is: ${response.statusCode}');
        }
      } else {
        print(
            'Failed to get gallery detail. Status code: ${response.statusCode}');
      }
      return GalleryModal(status: 0, msg: '', islamic_gallery: []);
    } catch (e) {
      print('error during getting gallery detail:  $e');
      return GalleryModal(status: 0, msg: '', islamic_gallery: []);
    }
  }

  static Future<WalletDetailModal?> getWalletDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forWallet = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/wallet';
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forWallet'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);

        if (responseData.isNotEmpty) {
          return WalletDetailModal.fromJson(responseData);
        } else if (response.statusCode == 404) {
          print('status code is: ${response.statusCode}');
        }
      } else {
        print(
            'Failed to get wallet detail. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('error during getting wallet detail:  $e');
    }
  }

  static Future<Map<String, dynamic>> getCountDownData(
      {required String current_time, required String next_prayer_time}) async {
    String url = 'https://aasonline.co/api/count-down';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forCountDown = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'current_time': current_time,
      'next_prayer_time': next_prayer_time,
    };

    try {
      final response =
          await http.post(Uri.parse(url), body: json.encode(data), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forCountDown'
        // Add any additional headers as needed
      });

      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return responseData[0];
        }
      } else {
        print('Failed to get timer. Status code: ${response.statusCode}');
      }
      return {'status': 0, 'msg': '', 'remaining_time': ''};
    } catch (error) {
      print('Error during getting timer : $error');
      return {'status': 0, 'msg': '', 'remaining_time': ''};
    }
  }

  // static Future<CoinsWallet?> getCoinsWalletDetail() async {
  //   SharedPreferences prefs = await SharedPreferences.getInstance();
  //   String? forCoinWallet = prefs.getString('sessionToken');
  //   String url = 'https://aasonline.co/api/coins-wallet';
  //   try {
  //     final response = await http.get(Uri.parse(url), headers: {
  //       'Content-Type': 'application/json',
  //       'Authorization': 'Bearer $forCoinWallet'
  //     });
  //     if (response.statusCode == 200) {
  //       final responseData = json.decode(response.body);

  //       if (responseData.isNotEmpty) {
  //         return CoinsWallet.fromJson(responseData);
  //       } else if (response.statusCode == 404) {
  //         print('status code is: ${response.statusCode}');
  //       }
  //     } else {
  //       print(
  //           'Failed to getcoins  wallet detail. Status code: ${response.statusCode}');
  //     }
  //   } catch (e) {
  //     print('error during getting coins wallet detail:  $e');
  //   }
  // }

  static Future<CoinsWalletModalTwo?> getCoinsWalletDetailTwo() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forCoinWalletTwo = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/coins-wallet-2';
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forCoinWalletTwo'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);

        if (responseData.isNotEmpty) {
          print('Response data: $responseData'); // Print response data
          return CoinsWalletModalTwo.fromJson(responseData);
        } else if (response.statusCode == 404) {
          print('status code is: ${response.statusCode}');
        }
      } else {
        print(
            'Failed to getcoins  wallet detail two. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('error during getting coins wallet detail two:  $e');
    }
  }

  void removeNamazTimingFromStorage() {
    if (Platform.isAndroid) {
      exit(0);
    } else if (Platform.isIOS) {
      // exit(0); // Uncomment this at your own risk
    }
  }

  static Future<CashWallet?> getCashWalletDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forCashWallet = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/cash-wallet';
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forCashWallet'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);

        if (responseData.isNotEmpty) {
          return CashWallet.fromJson(responseData);
        } else if (response.statusCode == 404) {
          print('status code is: ${response.statusCode}');
        }
      } else {
        print(
            'Failed to get cash  wallet detail. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('error during getting cash wallet detail:  $e');
    }
  }

  Future<void> convertCoins({required int coinsToConvert}) async {
    String url = 'https://aasonline.co/api/convert-coins';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forCoinsConvert = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'coins_to_convert': coinsToConvert,
    };

    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode(data),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $forCoinsConvert'
          });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
      } else {
        print('failed to convert coins status code: ${response.statusCode}');
      }
    } catch (e) {
      print('Error during converting coins : $e');
    }
  }

  Future<void> setTransactionsPin({required int tpin}) async {
    InitLoading().showLoading('Loading...');

    String url = 'https://aasonline.co/api/set-tpin';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? toSetTPin = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'tpin': tpin,
    };

    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode(data),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $toSetTPin'
          });
      if (response.statusCode == 200) {
        InitLoading().dismissLoading();
        final responseData = json.decode(response.body);

        CustomSnackbar.successful(message: 'Pin set successfully');
      } else if (response.statusCode == 400) {
        InitLoading().dismissLoading();
        CustomSnackbar.error(message: 'PIN already exists.');
      } else {
        InitLoading().dismissLoading();
        CustomSnackbar.error(
            message: 'failed to set pin: ${response.statusCode}');
        // print('failed to set pin: ${response.statusCode}');
      }
    } catch (e) {
      InitLoading().dismissLoading();
      print('Error during setting pin : $e');
    }
  }

  static Future<void> forgetTransactionsPin() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forgetTransPin = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/forget-tpin';
    InitLoading().showLoading('Loading...');
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forgetTransPin'
      });
      if (response.statusCode == 200) {
        CustomSnackbar.successful(message: 'Check your email for new Pin.');
        InitLoading().dismissLoading();

        // print('status code is: ${response.statusCode}');
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();

        print('status code is: ${response.statusCode}');
      } else if (response.statusCode == 400) {
        InitLoading().dismissLoading();

        CustomSnackbar.error(message: 'PIN has not been set.');
        print('status code is: ${response.statusCode}');
      } else {
        CustomSnackbar.error(message: 'Failed to reset pin. Try again later.');
        InitLoading().dismissLoading();

        print(
            'Failed to forget t pin detail. Status code: ${response.statusCode}');
      }
    } catch (e) {
      InitLoading().dismissLoading();

      print('error during reset t pin:  $e');
    }
  }

  Future<void> cashWithdraw({
    required String date,
    required int amount,
    required String bankName,
    required String accountTitle,
    required String accountNumber,
    required int phoneNo,
    required int tpin,
    required BuildContext context,
  }) async {
    InitLoading().showLoading('Loading...');
    TextEditingController enterAmountController = TextEditingController();
    TextEditingController bankNameController = TextEditingController();
    TextEditingController accountTitleController = TextEditingController();
    TextEditingController accountNumberController = TextEditingController();
    TextEditingController phoneNoController = TextEditingController();
    TextEditingController pinController = TextEditingController();
    String url = 'https://aasonline.co/api/withdraw';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forCashWithdraw = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'date': date,
      'amount': amount.toString(), // Convert to string
      'bank_name': bankName,
      'account_title': accountTitle,
      'account_no': accountNumber,
      'phoneno': phoneNo.toString(), // Convert to string
      'tpin': tpin.toString(), // Convert to string
    };
    final response =
        await http.post(Uri.parse(url), body: json.encode(data), headers: {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer $forCashWithdraw',
      // 'Accept': 'application/json',
    });
    if (response.statusCode == 200) {
      InitLoading().dismissLoading();

      final responseData = json.decode(response.body);
      CustomSnackbar.successful(
          message:
              'Withdraw request has been sent to AAS. It may take 48 hours to get approved.');
      enterAmountController.clear();
      bankNameController.clear();
      accountTitleController.clear();
      accountNumberController.clear();
      phoneNoController.clear();
      pinController.clear();
      Navigator.of(context).pop();
      // Get.back();
    } else if (response.statusCode == 401) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'PIN code is invalid');
    } else if (response.statusCode == 403) {
      InitLoading().dismissLoading();
      print(response.statusCode);
      CustomSnackbar.error(message: 'Minimum \$10 is required to withdraw');
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: '${response.statusCode} ${response.body}');
      print('failed to withdraw: ${response.statusCode}');
    } else {
      print('else withdraw');
      print(response.statusCode);
      InitLoading().dismissLoading();
    }
  }

  Future<void> donationToAAS({
    required int amount,
    required String tpin,
    required BuildContext context,
  }) async {
    TextEditingController amountController = TextEditingController();
    TextEditingController pinController = TextEditingController();
    InitLoading().showLoading('Loading....');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forDonationToASS = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/donation-to-aas';
    final data = {
      'amount': amount,
      'tpin': tpin,
    };

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forDonationToASS'
      },
    );

    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      CustomSnackbar.successful(message: 'Donated to AAS Successfully');
      amountController.clear();
      pinController.clear();
      Navigator.pop(context);
    } else if (response.statusCode == 401) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Invalid PIN');
    } else if (response.statusCode == 403) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: '  Insufficient Balance.');
    } else {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong');
    }
  }

  Future<void> donationToOthers({
    required int amount,
    required String tpin,
    required String user_id,
    required BuildContext context,
  }) async {
    TextEditingController amountController = TextEditingController();
    TextEditingController idController = TextEditingController();
    TextEditingController pinController = TextEditingController();
    InitLoading().showLoading('Loading....');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forDonationToOthers = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/donation-to-other';
    final data = {
      'amount': amount,
      'tpin': tpin,
      'user_id': user_id,
    };

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forDonationToOthers'
      },
    );

    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      CustomSnackbar.successful(message: 'Donated Successfully');
      amountController.clear();
      pinController.clear();
      idController.clear();
      Navigator.pop(context);
    } else if (response.statusCode == 401) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Invalid PIN');
    } else if (response.statusCode == 400) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'The user id must be 6 digits.');
    } else if (response.statusCode == 403) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Insufficient Balance.');
    } else if (response.statusCode == 406) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'User not exist with this ID');
    } else {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Something went wrong');
    }
  }

  static Future<dynamic> community({required String date}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forCommunity = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/community?date=$date';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forCommunity',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        return responseData;
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        print('status code is: ${response.statusCode}');
      } else {
        print('Failed to get community. Status code: ${response.statusCode}');
      }
      return null;
    } catch (error) {
      print('error during getting community list: $error');
      return null;
    }
  }

  static Future<RightSideModal> getRightSideDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forRightSide = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/right-members';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forRightSide',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return RightSideModal.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        print('status code is: ${response.statusCode}');
      } else {
        print('Failed to get right side. Status code: ${response.statusCode}');
      }
      return RightSideModal(status: 0, msg: '', right_members: []);
      // Return an empty list if there's an error or missing data
    } catch (error, stackTrace) {
      print('error during getting right side list: $error');
      print('Error fetching right side list: $error');
      print('StackTrace: $stackTrace');
      // Return an empty list if there's an error
      return RightSideModal(status: 0, msg: '', right_members: []);
    }
  }

  static Future<LeftSideModal> getLeftSideDetail() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forLeftSide = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/left-members';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forLeftSide',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        if (responseData.isNotEmpty) {
          return LeftSideModal.fromJson(responseData);
        }
      } else if (response.statusCode == 404) {
        InitLoading().dismissLoading();
        print('status code is: ${response.statusCode}');
      } else {
        print('Failed to get left side. Status code: ${response.statusCode}');
      }
      return LeftSideModal(status: 0, msg: '', left_members: []);
      // Return an empty list if there's an error or missing data
    } catch (error) {
      print('error during getting left side list: $error');
      // Return an empty list if there's an error
      return LeftSideModal(status: 0, msg: '', left_members: []);
    }
  }

  Future<void> deleteAccount({
    required String password,
    required BuildContext context,
  }) async {
    InitLoading().showLoading('Loading...');

    String url = 'https://aasonline.co/api/delete-account';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forDeleteAccount = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'password': password,
    };

    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode(data),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $forDeleteAccount'
          });
      if (response.statusCode == 200) {
        InitLoading().dismissLoading();

        final responseData = json.decode(response.body);
        String msg = responseData['msg'];

        CustomSnackbar.successful(message: msg);
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      } else if (response.statusCode == 400) {
        InitLoading().dismissLoading();
        CustomSnackbar.error(message: 'Incorrect Password');
        Navigator.of(context).pop();
      } else {
        InitLoading().dismissLoading();
        CustomSnackbar.error(
            message: 'Failed to delete. Try Again!. ${response.statusCode}');
        // print('failed to set pin: ${response.statusCode}');
      }
    } catch (e) {
      InitLoading().dismissLoading();
      print('Error during deletion account : $e');
    }
  }

  static Future<void> cancelAccountDeletion(
      {required BuildContext context}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forCancelDeletion = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/cancel-delete';
    InitLoading().showLoading('Loading...');
    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forCancelDeletion'
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        CustomSnackbar.successful(message: 'Deletion cancelled successfully.');
        Navigator.of(context).pop();
        InitLoading().dismissLoading();
      } else {
        print(
            'Failed to get wallet detail. Status code: ${response.statusCode}');
      }
    } catch (e) {
      print('error during getting wallet detail:  $e');
    }
  }

  static Future<dynamic> communityReward({required String date}) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forClaimReward = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/community-reward?date=$date';

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forClaimReward',
      });
      if (response.statusCode == 200) {
        final responseData = json.decode(response.body);
        print('response data 200 is: $responseData');
        return responseData;
      } else if (response.statusCode == 400) {
        InitLoading().dismissLoading();
        print('status code is: ${response.statusCode}');
      } else {
        print('Failed to claim reward. Status code: ${response.statusCode}');
      }
      return null;
    } catch (error) {
      print('error during claim reward: $error');
      return null;
    }
  }

  Future<void> claimReward({
    required String date,
    required int rewarded_coins,
  }) async {
    InitLoading().showLoading('Loading....');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forClaimReward = prefs.getString('sessionToken');
    String url = 'https://aasonline.co/api/claim-now';
    final data = {
      'date': date,
      'rewarded_coins': rewarded_coins,
    };

    final response = await http.post(
      Uri.parse(url),
      body: json.encode(data),
      headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forClaimReward'
      },
    );
    if (response.statusCode == 200) {
      InitLoading().dismissLoading();
      CustomSnackbar.successful(message: 'Rewarded Successfully');
    } else if (response.statusCode == 402) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(message: 'Already rewarded');
    } else if (response.statusCode == 406) {
      InitLoading().dismissLoading();
      CustomSnackbar.error(
          message: 'Not Eligible due to incomplete Self-Prayers');
    }
    // else if(response.statusCode == 400){

    //     InitLoading().dismissLoading();
    //   CustomSnackbar.error(
    //       message: 'i');
    // }
    else {
      InitLoading().dismissLoading();
      print('plzz fill all fields.');
    }
  }

  Future<void> resendOtp({
    required String email,
  }) async {
    InitLoading().showLoading('Loading...');

    String url = 'https://aasonline.co/api/resend-pin';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forResendOtp = prefs.getString('sessionToken');

    final Map<String, dynamic> data = {
      'email': email,
    };

    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode(data),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $forResendOtp'
          });
      if (response.statusCode == 200) {
        InitLoading().dismissLoading();

        CustomSnackbar.successful(message: 'Check your email for OTP.');
      } else if (response.statusCode == 400) {
        InitLoading().dismissLoading();
        CustomSnackbar.error(message: 'User Doesn\'t exist.');
      } else {
        InitLoading().dismissLoading();
        CustomSnackbar.error(
            message: 'Failed to send OTP. Try Again!. ${response.statusCode}');
        // print('failed to set pin: ${response.statusCode}');
      }
    } catch (e) {
      InitLoading().dismissLoading();
      print('Error during deletion account : $e');
    }
  }

  Future<void> logout() async {
    InitLoading().showLoading('Loading...');

    String url = 'https://aasonline.co/api/logout';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forLogout = prefs.getString('sessionToken');
    GoogleSignIn _googleSignIn = GoogleSignIn();

    try {
      final response = await http.post(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forLogout'
      });
      if (response.statusCode == 200) {
        InitLoading().dismissLoading();
        bool abc = (await prefs.remove('sessionToken'));

        await _googleSignIn.signOut();
        Get.offAll(LoginScreen());
      }
    } catch (e) {
      InitLoading().dismissLoading();
      print('Error during Logout : $e');
    }
  }

  static Future<void> quranReward() async {
    String url = 'https://aasonline.co/api/quran-reward';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forQuranReward = prefs.getString('sessionToken');

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $forQuranReward'
      });
      if (response.statusCode == 200) {
        print('status code is: 200');
        // InitLoading().dismissLoading();
        CustomSnackbar.successful(message: 'Successfully Rewarded');
      } else if (response.statusCode == 400) {
        print('status code is: 400');
        // InitLoading().dismissLoading();
        CustomSnackbar.error(message: 'Already Rewarded');
      }
    } catch (e) {
      // InitLoading().dismissLoading();
      print('Error during rewarding : $e');
    }
  }

  static Future<void> tasbeehReward() async {
    String url = 'https://aasonline.co/api/tasbeeh-reward';
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? fortasbeehReward = prefs.getString('sessionToken');

    try {
      final response = await http.get(Uri.parse(url), headers: {
        'Content-Type': 'application/json',
        'Authorization': 'Bearer $fortasbeehReward'
      });
      if (response.statusCode == 200) {
        print('status code is: 200');
        // InitLoading().dismissLoading();
        CustomSnackbar.successful(message: 'Successfully Rewarded');
      } else if (response.statusCode == 400) {
        print('status code is: 400');
        // InitLoading().dismissLoading();
        CustomSnackbar.error(message: 'Already Rewarded');
      }
    } catch (e) {
      // InitLoading().dismissLoading();
      print('Error during rewarding : $e');
    }
  }

  static Future<void> registerKYC({
    required String? token,
    required String fullName,
    required String phone,
    required String address,
    required String idNumber,
    required String accType,
    required String bankName,
    required String accTitle,
    required String accNumber,
    required String dob,
    required String country,
    required String number,
    required BuildContext context,
    File? imgFile,
    File? imgFile2,
  }) async {
    String apiUrl = 'https://aasonline.co/api/kyc';

    var request = http.MultipartRequest('POST', Uri.parse(apiUrl));
    request.headers['Authorization'] = 'Bearer $token';

    request.fields['full_name'] = fullName;
    request.fields['phone'] = phone;
    request.fields['address'] = address;
    request.fields['id_number'] = idNumber;
    request.fields['acc_type'] = accType;
    request.fields['bank_name'] = bankName;
    request.fields['acc_title'] = accTitle;
    request.fields['acc_number'] = accNumber;
    request.fields['dob'] = dob;
    request.fields['country'] = country;
    request.fields['id_type'] = number;

    if (imgFile != null) {
      var imageStream = http.ByteStream(imgFile.openRead());
      var length = await imgFile.length();

      var multipartFile = http.MultipartFile(
        'file',
        imageStream,
        length,
        filename: imgFile.path.split('/').last,
      );
      request.files.add(multipartFile);
    }

    if (imgFile2 != null) {
      var imageStream = http.ByteStream(imgFile2.openRead());
      var length = await imgFile2.length();

      var multipartFile = http.MultipartFile(
        'file2',
        imageStream,
        length,
        filename: imgFile2.path.split('/').last,
      );
      request.files.add(multipartFile);
    }

    try {
      var response = await request.send();
      //var responseData = await response.stream.bytesToString();

      if (response.statusCode == 200) {
        CustomSnackbar.successful(message: "KYC is Updated");
        Navigator.of(context).pop();
        Navigator.of(context).pop();
      } else if (response.statusCode == 302) {
        print("Redirection detected. Check the new location header.");
      } else {
        CustomSnackbar.error(message: "KYC is not Updated");
        print('Failed to update KYC. Status: ${response.statusCode}');
      }
    } catch (error) {
      print('Error occurred while updating KYC: $error');
    }
  }

  Future<void> changePassword({
    required String current_password,
    required String new_password,
    required String new_password_confirmation,
    required BuildContext context,
  }) async {
    String url = 'https://aasonline.co/api/change-password';
    InitLoading().showLoading('Loading....');
    SharedPreferences prefs = await SharedPreferences.getInstance();
    String? forChangePassword = prefs.getString('sessionToken');
    final Map<String, dynamic> data = {
      'current_password': current_password,
      'new_password': new_password,
      'new_password_confirmation': new_password_confirmation,
    };
    try {
      final response = await http.post(Uri.parse(url),
          body: json.encode(data),
          headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer $forChangePassword'
          });
      if (response.statusCode == 200) {
        InitLoading().dismissLoading();
        CustomSnackbar.successful(message: 'Password changed Successfully');
        Map<String, dynamic> responseJson = json.decode(response.body);
        Navigator.of(context).pop();
        print(responseJson);
      } else if (response.statusCode == 400) {
        InitLoading().dismissLoading();
        final dynamic errorData = json.decode(response.body);
        if (errorData.containsKey('errors') &&
            errorData['errors'].containsKey('current_password')) {
          String errorMsg = errorData['errors']['current_password'][0];
          CustomSnackbar.error(message: errorMsg);
        }
      } else {
        // final dynamic errorData = json.decode(response.body);
        // if (errorData.containsKey('data') &&
        //     errorData['data'].containsKey('phone')) {
        //   String errorMessage = errorData['data']['phone'][0];
        //   CustomSnackbar.error(message: errorMessage);
        // }
        InitLoading().dismissLoading();
        print('status code is: ${response.statusCode}');
      }
    } catch (e) {
      InitLoading().dismissLoading();
      print('Error during change password : $e');
    }
  }
}
