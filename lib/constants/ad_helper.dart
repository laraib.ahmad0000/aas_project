// import 'dart:io';

// import 'package:google_mobile_ads/google_mobile_ads.dart';

// class AdHelper {
//   static String get bannerAdUnitId {
//     if (Platform.isAndroid) {
//       return 'ca-app-pub-3940256099942544/6300978111'; // Your bannerAdUnitId for Android
//     } else if (Platform.isIOS) {
//       return 'ca-app-pub-3940256099942544/6300978111'; // Your bannerAdUnitId for Ios
//     } else {
//       throw UnsupportedError('Unsupported platform');
//     }
//   }

//   static String get nativeAdUnitId {
//     if (Platform.isAndroid) {
//       return 'ca-app-pub-3940256099942544/2247696110';
//     } else if (Platform.isIOS) {
//       return 'ca-app-pub-3940256099942544/2247696110';
//     } else {
//       throw UnsupportedError('Unsupported platform');
//     }
//   }

//   static String get interstitialAdUnitId {
//     if (Platform.isAndroid) {
//       return "ca-app-pub-3940256099942544/1033173712"; // Your intersitialAdUnitId for Android
//     } else if (Platform.isIOS) {
//       return "ca-app-pub-3940256099942544/1033173712"; // Your intersitialAdUnitId for Ios
//     } else {
//       throw UnsupportedError("Unsupported platform");
//     }
//   }

//   static String get rewardedAdUnitId {
//     if (Platform.isAndroid) {
//       return "ca-app-pub-3940256099942544/5224354917"; // Your rewarderAdUnitId for Android
//     } else if (Platform.isIOS) {
//       return "ca-app-pub-3940256099942544/5224354917"; // Your rewarderAdUnitId for Ios
//     } else {
//       throw UnsupportedError("Unsupported platform");
//     }
//   }
// }
//   above code  id for test ads

import 'dart:io';


class AdHelper {
  static String get bannerAdUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-3007541505786179/8981933485'; // Your bannerAdUnitId for Android
    } else if (Platform.isIOS) {
      return 'ca-app-pub-3007541505786179/8981933485'; // Your bannerAdUnitId for Ios
    } else {
      throw UnsupportedError('Unsupported platform');
    }
  }

  static String get nativeAdUnitId {
    if (Platform.isAndroid) {
      return 'ca-app-pub-3007541505786179/6307097864';
    } else if (Platform.isIOS) {
      return 'ca-app-pub-3007541505786179/6307097864';
    } else {
      throw UnsupportedError('Unsupported platform');
    }
  }

  static String get interstitialAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-3007541505786179/6843635550"; // Your intersitialAdUnitId for Android
    } else if (Platform.isIOS) {
      return "ca-app-pub-3007541505786179/6843635550"; // Your intersitialAdUnitId for Ios
    } else {
      throw UnsupportedError("Unsupported platform");
    }
  }

  static String get rewardedAdUnitId {
    if (Platform.isAndroid) {
      return "ca-app-pub-3007541505786179/2802967129"; // Your rewarderAdUnitId for Android
    } else if (Platform.isIOS) {
      return "ca-app-pub-3007541505786179/2802967129"; // Your rewarderAdUnitId for Ios
    } else {
      throw UnsupportedError("Unsupported platform");
    }
  }
}
