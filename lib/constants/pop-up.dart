import 'package:aas/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class CustomSnackbar {
  static successful({
    required String message,
    Duration duration = const Duration(seconds: 3),
    Color backgroundColor = Colors.green,
    Color textColor = black,
  }) {
    Get.snackbar('Successful', message,
        duration: duration,
        backgroundColor: backgroundColor,
        colorText: textColor,
        snackPosition: SnackPosition.BOTTOM,
        messageText: Text(
          message,
          style:
              const TextStyle(color: Colors.black, fontWeight: FontWeight.w400),
        ));
  }

  static void error({
    required String message,
    Duration duration = const Duration(seconds: 3),
    Color backgroundColor = Colors.red,
    Color textColor = white,
  }) {
    Get.snackbar('Failed', message,
        duration: duration,
        backgroundColor: backgroundColor,
        colorText: textColor,
        snackPosition: SnackPosition.BOTTOM,
        messageText: Text(
          message,
          style: const TextStyle(color: white, fontWeight: FontWeight.w400),
        ));
  }
}
