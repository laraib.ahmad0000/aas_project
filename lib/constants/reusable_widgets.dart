import 'package:aas/Model/MLM%20modals/join_community_modal.dart';
import 'package:aas/Model/juzz_modal.dart';
import 'package:aas/Model/user_data_modal.dart';
import 'package:aas/Provider/MLM/join_community_provider.dart';
import 'package:aas/Provider/logout_provider.dart';
import 'package:aas/Provider/user_data_provider.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/aas_gallery.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/help_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Menu%20Screens/settings_screen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/Profile_Screen.dart';
import 'package:aas/Screens/BottomNavScreens/Profile%20Screens/badges_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Dua%20Screens/DuaScreen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/home_screen.dart';
import 'package:aas/Screens/BottomNavScreens/homescreen/Wallet/wallet_screen.dart';
import 'package:aas/Screens/authscreen/login_screen.dart';
import 'package:aas/Screens/kyc/kyc_screen.dart';
import 'package:aas/Services/loading_services.dart';
import 'package:aas/constants/pop-up.dart';
import 'package:aas/constants/text_constants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/svg.dart';
import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:share_plus/share_plus.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';

import 'colors.dart';

class CustomButton extends StatelessWidget {
  final String text;

  // final onPressed;

  CustomButton({
    required this.text,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
        height: 45,
        width: 180,
        decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(30.0), color: primaryColor),
        child: Center(
          child: Text(text,
              style: TextStyle(
                  fontWeight: FontWeight.bold, fontSize: 20, color: white)),
        ));
  }
}

TextEditingController referralCodeController = TextEditingController();

class CustomTextField extends StatefulWidget {
  String hint;
  TextEditingController textEditingController;
  TextInputType textInputType;
  bool? obscureText;
  Function(String)? onChanged;
  String? validatorMessage;
  Widget? prefixIcon;

  CustomTextField(
      {super.key,
      required this.hint,
      required this.textEditingController,
      required this.textInputType,
      this.obscureText = false,
      this.onChanged,
      this.validatorMessage,
      required this.prefixIcon});

  @override
  State<CustomTextField> createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> {
  bool showError = false;
  bool skipReferralCodeValidation = false; // Declaration goes here

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20),
            child: TextFormField(
              obscureText: widget.obscureText ?? false,
              controller: widget.textEditingController,
              decoration: InputDecoration(
                focusedErrorBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
                errorBorder: OutlineInputBorder(
                  borderSide: BorderSide(
                    color: skipReferralCodeValidation
                        ? Colors.transparent
                        : Colors.red,
                  ),
                  borderRadius: BorderRadius.circular(8),
                ),
                contentPadding: EdgeInsets.all(0.13),
                focusedBorder: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                  borderSide: BorderSide(
                    color: skipReferralCodeValidation
                        ? Colors.transparent
                        : primaryColor,
                    width: 1.0,
                  ),
                ),
                prefixIcon: widget.prefixIcon,
                hintText: widget.hint,
                hintStyle: TextStyle(
                  color: showError && widget.textEditingController.text.isEmpty
                      ? Colors.red
                      : Colors.black,
                ),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(8),
                ),
              ),
              validator: (value) {
                if (widget.textEditingController == referralCodeController &&
                    skipReferralCodeValidation) {
                  // Skip validation for referralCodeController
                  return null;
                }

                if (value == null || value.isEmpty) {
                  return widget.validatorMessage ?? 'Please Fill Field.';
                }

                return null;
              },
            ),
          ),
        ],
      ),
    );
  }
}

class JuzCustomTile extends StatelessWidget {
  final List<JuzAyahs> list;
  final int index;

  JuzCustomTile({required this.list, required this.index});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      decoration: BoxDecoration(color: Colors.white, boxShadow: [
        BoxShadow(
          color: Colors.black12,
          blurRadius: 3.0,
        ),
      ]),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.end,
        children: [
          //
          Container(
            // height: 30,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(6.0),
                topRight: Radius.circular(6.0),
              ),
              color: primaryColor,
            ),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 5.0),
                  child: Text(list[index].ayahNumber.toString(),
                      style: TextStyle(
                        color: white,
                        fontSize: 22,
                        fontFamily: 'Amiri',
                      )),
                ),
                Padding(
                  padding: const EdgeInsets.symmetric(vertical: 5.0),
                  child: Center(
                    child: Text(list[index].surahName.toString(),
                        style: TextStyle(
                          color: white,
                          fontSize: 20,
                          fontFamily: 'Amiri',
                        )),
                  ),
                ),
                SizedBox(
                  width: 5.0,
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 12.0),
            child: Text(
              list[index].ayahsText,
              style: TextStyle(
                fontSize: 17,
                fontWeight: FontWeight.w700,
                color: primaryColor,
                fontFamily: 'al-qalam',
              ),
              textAlign: TextAlign.end,
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 7.0),
            child: Text(
              list[index].surahName,
              textAlign: TextAlign.end,
              style: TextStyle(
                fontFamily: 'al-qalam',
              ),
            ),
          ),
        ],
      ),
    );
  }
}

Widget myDrawer(context, UserModal? userData) {
  Uri? whatsappUrl =
      Uri.parse('https://whatsapp.com/channel/0029VaKsoz26buMMbU34yy2O');
  Uri? facebookUrl = Uri.parse('https://www.facebook.com/AASonlineOfficialae');
  Uri? instagramUrl = Uri.parse('https://www.instagram.com/aasonlineae/');
  Uri? twitterUrl = Uri.parse('https://twitter.com/AASOfficial12');
  Uri? youTubeUrl = Uri.parse('https://www.youtube.com/@AASOnlineOfficial');

  Uri? appUrl = Uri.parse(
      'https://play.google.com/store/apps/details?id=com.aasonline.app&hl=en&gl=US');
  String userName = userData?.name ?? 'No Name';
  String profileImg = userData?.profileimage ?? 'No Name';
  int isParent = userData?.isParent ?? 0;
  int isChild = userData?.isChild ?? 0;
  String referralCode = userData?.referral_code ?? '';
  int isKyc = userData?.kyc ?? 0;
  return
      // ignore: dead_code
      Drawer(
    backgroundColor: white,
    child: ListView(
      padding: EdgeInsets.zero,
      children: [
        DrawerHeader(
            decoration: BoxDecoration(
                gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [
                primaryColor,
                Colors.blue,
                // primaryColor,
              ],
            )),
            child: Column(
              children: [
                CircleAvatar(
                  radius: 53,
                  backgroundImage: NetworkImage(profileImg),
                ),
                // SizedBox(height: 5.0),
                Text(
                  userName,
                  style: whiteArabicText,
                )
              ],
            )),
        isParent == 0 && isChild == 0
            ? ListTile(
                title: const Text(
                  'Join Community',
                ),
                leading: const Icon(
                  Icons.groups,
                  color: primaryColor,
                ),
                onTap: () async {
                  return _showJoinCommunityPopUp(context);
                })
            : ListTile(
                title: const Text(
                  'Join Community',
                  style: TextStyle(color: Colors.black26),
                ),
                leading: const Icon(
                  Icons.groups,
                ),
                onTap: () {
                  // Navigator.push(context,
                  //     MaterialPageRoute(builder: (context) => SettingsScreen()));
                }),
        GestureDetector(
          onTap: () async {
            await Share.share(
                'Hi, \nWelcome to AAS -  A Premium Islamic Social Media App. \nJoin AAS with my code $referralCode and join my Community to earn free! \nhttps://aasonline.co/ \nDownload the App and install it here: \nhttps://play.google.com/store/apps/details?id=com.aasonline.app');
          },
          child: const ListTile(
            title: Text(
              'Reffer a Friend',
            ),
            leading: Icon(
              Icons.link_outlined,
              color: primaryColor,
            ),
          ),
        ),
        ListTile(
            title: const Text(
              'Update the App',
            ),
            leading: const Icon(
              Icons.upgrade,
              color: primaryColor,
            ),
            onTap: () async {
              if (await canLaunchUrl(Uri.parse(appUrl.toString()))) {
                // ignore: deprecated_member_use
                await launch(appUrl.toString());
              } else {
                // print("could not launch url");
                CustomSnackbar.error(message: 'Something went wrong');
              }
            }),
        GestureDetector(
          onTap: () {
            Navigator.push(
                context, MaterialPageRoute(builder: (context) => ASSGallery()));
          },
          child: const ListTile(
            title: Text(
              'Islamic Gallery',
            ),
            leading: Icon(
              Icons.collections,
              color: primaryColor,
            ),
          ),
        ),
        GestureDetector(
          onTap: () async {
            // Navigator.push(context,
            //     MaterialPageRoute(builder: (context) => const KycScreen()));
            // await    commingSoonPopup(context);
            // Navigator.push(
            //     context, MaterialPageRoute(builder: (context) => KycScreen()));
            switch (isKyc) {
              case 0:
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => const KycScreen()));
                break;
              case 1:
                await kycPopup(context,
                    'Your KYC is being verified. It may take upto 10 working days. \n You will be notified via email.');
                break;
              case 2:
                await kycPopup(context, 'KYC is approved');
                break;
            }
          },
          child: ListTile(
            title: Text(
              'KYC',
              style:
                  TextStyle(color: isKyc == 0 ? Colors.black : Colors.black26),
            ),
            leading: Icon(
              Icons.verified,
              color: isKyc == 0 ? primaryColor : Colors.black26,
            ),
          ),
        ),
        // isKyc == 0
        // ? GestureDetector(
        //     onTap: () async {
        //       // await    commingSoonPopup(context);
        //       Navigator.push(context,
        //           MaterialPageRoute(builder: (context) => KycScreen()));
        //     },
        //     child: const ListTile(
        //       title: Text(
        //         'KYC',
        //       ),
        //       leading: Icon(
        //         Icons.verified,
        //         color: primaryColor,
        //       ),
        //     ),
        //   )
        //     : GestureDetector(
        //         onTap: () async {
        //           CustomSnackbar.error(message: 'Already Submitted');
        //         },
        //         child: const ListTile(
        //           title: Text(
        //             'KYC',
        //             style: TextStyle(color: Colors.black26),
        //           ),
        //           leading: Icon(
        //             Icons.verified,
        //             color: Colors.black26,
        //           ),
        //         ),
        //       ),
        ListTile(
            title: const Text(
              'App Settings',
            ),
            leading: const Icon(
              Icons.settings,
              color: primaryColor,
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => SettingsScreen()));
            }),
        ListTile(
            title: const Text(
              'Help Center',
            ),
            leading: const Icon(
              Icons.help_center,
              color: primaryColor,
            ),
            onTap: () {
              Navigator.push(context,
                  MaterialPageRoute(builder: (context) => const HelpScreen()));
            }),
        ListTile(
            title: const Text(
              'Rate us',
            ),
            leading: const Icon(
              Icons.star_half,
              color: primaryColor,
            ),
            onTap: () async {
              if (await canLaunchUrl(Uri.parse(appUrl.toString()))) {
                // ignore: deprecated_member_use
                await launch(appUrl.toString());
              } else {
                // print("could not launch url");
                CustomSnackbar.error(message: 'Something went wrong');
              }
            }),
        ListTile(
            title: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Padding(
                  padding: const EdgeInsets.only(left: 26.0),
                  child: Text('Get in touch',
                      style:
                          TextStyle(fontWeight: FontWeight.w500, fontSize: 16)),
                ),
                SizedBox(
                  height: 8.0,
                ),
                Row(
                  children: [
                    InkWell(
                      onTap: () async {
                        if (await canLaunchUrl(
                            Uri.parse(whatsappUrl!.toString()))) {
                          // ignore: deprecated_member_use
                          await launch(whatsappUrl!.toString());
                        } else {
                          // print("could not launch url");
                          CustomSnackbar.error(message: 'Something went wrong');
                        }
                      },
                      child: Image(
                          height: 32,
                          width: 32,
                          image: AssetImage('assets/images/whatsapp.png')),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    InkWell(
                      onTap: () async {
                        if (await canLaunchUrl(
                            Uri.parse(facebookUrl.toString()))) {
                          // ignore: deprecated_member_use
                          await launch(facebookUrl.toString());
                        } else {
                          // print("could not launch url");
                          CustomSnackbar.error(message: 'Something went wrong');
                        }
                      },
                      child: Image(
                          height: 30,
                          width: 30,
                          image: AssetImage('assets/images/fb.png')),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    InkWell(
                      onTap: () async {
                        if (await canLaunchUrl(
                            Uri.parse(instagramUrl.toString()))) {
                          // ignore: deprecated_member_use
                          await launch(instagramUrl.toString());
                        } else {
                          // print("could not launch url");
                          CustomSnackbar.error(message: 'Something went wrong');
                        }
                      },
                      child: Image(
                          height: 30,
                          width: 30,
                          image: AssetImage('assets/images/insta.png')),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    InkWell(
                      onTap: () async {
                        if (await canLaunchUrl(
                            Uri.parse(twitterUrl.toString()))) {
                          // ignore: deprecated_member_use
                          await launch(twitterUrl.toString());
                        } else {
                          // print("could not launch url");
                          CustomSnackbar.error(message: 'Something went wrong');
                        }
                      },
                      child: Image(
                          height: 25,
                          width: 25,
                          image: AssetImage('assets/images/twitter.png')),
                    ),
                    SizedBox(
                      width: 10.0,
                    ),
                    InkWell(
                      onTap: () async {
                        if (await canLaunchUrl(
                            Uri.parse(youTubeUrl.toString()))) {
                          // ignore: deprecated_member_use
                          await launch(youTubeUrl.toString());
                        } else {
                          // print("could not launch url");
                          CustomSnackbar.error(message: 'Something went wrong');
                        }
                      },
                      child: Image(
                          height: 30,
                          width: 30,
                          image: AssetImage('assets/images/youtube.png')),
                    ),
                  ],
                ),
              ],
            ),
            // leading: const Icon(
            //   Icons.star_half,
            //   color: primaryColor,
            // ),
            onTap: () async {
              if (await canLaunchUrl(Uri.parse(appUrl.toString()))) {
                // ignore: deprecated_member_use
                await launch(appUrl.toString());
              } else {
                // print("could not launch url");
                CustomSnackbar.error(message: 'Something went wrong');
              }
            }),
        const Divider(
          indent: 20.0,
          endIndent: 10.0,
          thickness: 1,
        ),
        GestureDetector(
          onTap: () async {
            return _showExitDialogue(context);
          },
          child: const ListTile(
            title: Text(
              'Logout',
              style: TextStyle(color: red),
            ),
            leading: Icon(
              Icons.logout,
              color: red,
            ),
          ),
        ),
      ],
    ),
  );
}

kycPopup(BuildContext context, $msg) {
  // final auth = FirebaseAuth.instance;
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext dialogContext) {
        final screenSize = MediaQuery.of(context).size;
        return AlertDialog(
          insetPadding: const EdgeInsets.all(30.0),
          backgroundColor: Colors.transparent,
          contentPadding: const EdgeInsets.all(0.0),
          content: Container(
            height: 150.0,
            width: screenSize.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  const SizedBox(
                    height: 1.0,
                  ),
                  Text(
                    $msg,
                    style: appNormalText,
                    textAlign: TextAlign.center,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextButton(
                        style: TextButton.styleFrom(
                          foregroundColor: Colors.white,
                          padding: const EdgeInsets.all(16.0),
                          textStyle: const TextStyle(fontSize: 20),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 33,
                          width: 63,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                7.0,
                              ),
                              border: Border.all(color: black)),
                          child: const Center(
                            child: Text(
                              'Close',
                              style: appNormalText,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      });
}

_showExitDialogue(BuildContext context) {
  // final auth = FirebaseAuth.instance;
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext dialogContext) {
        final screenSize = MediaQuery.of(context).size;
        return AlertDialog(
          insetPadding: const EdgeInsets.all(30.0),
          backgroundColor: Colors.transparent,
          contentPadding: const EdgeInsets.all(0.0),
          content: Container(
            height: 150.0,
            width: screenSize.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  const SizedBox(
                    height: 1.0,
                  ),
                  Text(
                    "Do you want to Logout?",
                    style: appNormalText,
                    textAlign: TextAlign.center,
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      TextButton(
                          style: TextButton.styleFrom(
                            foregroundColor: Colors.white,
                            padding: const EdgeInsets.all(16.0),
                            textStyle: const TextStyle(fontSize: 20),
                          ),
                          onPressed: () {
                            // logoutUser();
                            final logoutP = Provider.of<LogoutProvider>(context,
                                listen: false);

                            logoutP.logout();
                          },
                          child: Container(
                            height: 33,
                            width: 63,
                            decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(
                                  7.0,
                                ),
                                border: Border.all(color: red)),
                            child: const Center(
                              child: Text(
                                'Yes',
                                style: redText,
                              ),
                            ),
                          )),
                      const SizedBox(
                        width: 30.0,
                      ),
                      TextButton(
                        style: TextButton.styleFrom(
                          foregroundColor: Colors.white,
                          padding: const EdgeInsets.all(16.0),
                          textStyle: const TextStyle(fontSize: 20),
                        ),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                        child: Container(
                          height: 33,
                          width: 63,
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(
                                7.0,
                              ),
                              border: Border.all(color: black)),
                          child: const Center(
                            child: Text(
                              'No',
                              style: appNormalText,
                            ),
                          ),
                        ),
                      ),
                    ],
                  )
                ],
              ),
            ),
          ),
        );
      });
}

_showJoinCommunityPopUp(BuildContext context) {
  TextEditingController JoinCommunityController = TextEditingController();
  return showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext dialogContext) {
        final screenSize = MediaQuery.of(context).size;
        return AlertDialog(
          insetPadding: const EdgeInsets.all(30.0),
          backgroundColor: Colors.transparent,
          contentPadding: const EdgeInsets.all(0.0),
          content: Container(
            height: 150.0,
            width: screenSize.width,
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(20.0),
              color: Colors.white,
            ),
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 20.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  const SizedBox(
                    height: 1.0,
                  ),
                  Text(
                    "Join Community with referral code",
                    style: appNormalText,
                    textAlign: TextAlign.center,
                  ),
                  TextFormField(
                    // obscureText: widget.obscureText ?? false,
                    controller: JoinCommunityController,
                    decoration: InputDecoration(
                      focusedErrorBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                      contentPadding: EdgeInsets.all(0.13),
                      focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                        borderSide: BorderSide(
                          color: primaryColor,
                          width: 1.0,
                        ),
                      ),
                      prefixIcon: Icon(Icons.link),
                      hintText: 'Enter Referral Code',
                      hintStyle: TextStyle(
                        color: lightBlack,
                      ),
                      border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8),
                      ),
                    ),
                  ),
                  InkWell(
                    onTap: () {
                      if (JoinCommunityController.text.isEmpty) {
                        CustomSnackbar.error(message: 'Please enter code');
                      } else {
                        print(
                            'this is referral code: ${JoinCommunityController.text.trim().toString()}');
                        final JCP = Provider.of<JoinCommunityProvider>(context,
                            listen: false);
                        final jc = JoinCommunityModal(
                            referral_code:
                                JoinCommunityController.text.trim().toString());

                        JCP.joinCommunity(jc);
                      }
                    },
                    child: Container(
                        height: 40,
                        width: 100,
                        decoration: BoxDecoration(
                            color: primaryColor,
                            borderRadius: BorderRadius.circular(7.0)),
                        child: Center(
                          child: Text(
                            'Join',
                            style: TextStyle(color: white),
                          ),
                        )),
                  )
                ],
              ),
            ),
          ),
        );
      });
}

logoutUser() async {
  InitLoading().showLoading('Loading...');
  GoogleSignIn _googleSignIn = GoogleSignIn();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  // prefs.remove('sessionToken');
  bool abc = (await prefs.remove('sessionToken'));
  InitLoading().dismissLoading();
  await _googleSignIn.signOut();
  Get.offAll(LoginScreen());

  print('token from logout: $abc');
}

class FollowDialoge {
  static followdialoge(
      List<Map<String, dynamic>> options, BuildContext context) {
    return PopupMenuButton(
      iconSize: 24.0,
      padding: EdgeInsets.zero,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(7.0),
      ),
      icon: Icon(
        Icons.more_horiz_rounded,
        color: Colors.black,
        size: 24.0,
      ),
      offset: Offset(0, 15),
      itemBuilder: (BuildContext context) {
        return options
            .map(
              (selectedOption) => PopupMenuItem(
                height: 30.0,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text(
                      selectedOption['menu'] ?? "",
                      style: TextStyle(
                        fontSize: ScreenUtil().setSp(14.0),
                        fontWeight: FontWeight.w400,
                        fontStyle: FontStyle.normal,
                        color: Colors.black,
                      ),
                    ),
                    options.length == options.indexOf(selectedOption) + 1
                        ? Container()
                        : Padding(
                            padding: EdgeInsets.symmetric(
                              vertical: 8.0,
                            ),
                            child: Divider(
                              color: Colors.grey,
                              height: ScreenUtil().setHeight(1.0),
                            ),
                          ),
                  ],
                ),
                value: selectedOption,
              ),
            )
            .toList();
      },
      onSelected: (value) async {},
    );
  }
}

class CoinsSvg extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return SvgPicture.asset(
      'assets/images/coins.svg',
      height: 21,
      width: 11,
    );
  }
}

class CoinsPng extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Image.asset(
      'assets/images/AAScoinpng.png',
      height: 29,
      width: 20,
    );
  }
}
