
import 'package:timezone/timezone.dart' as tz;
import 'package:adhan/adhan.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:geolocator/geolocator.dart';
import 'package:intl/intl.dart';
import 'package:timezone/data/latest.dart' as tz;
import 'main.dart';

Future<void> initializeService() async {
  final service = FlutterBackgroundService();

  // Configure background service
  await service.configure(
    androidConfiguration: AndroidConfiguration(
        onStart: onStart,
        autoStart: true,
        isForegroundMode: true,
        initialNotificationTitle: 'Azaan Notification',
        initialNotificationContent: 'App running in background'
    ),
    iosConfiguration: IosConfiguration(
      autoStart: true,
      onForeground: onStart,
      onBackground: onIosBackground,
    ),
  );
  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('@mipmap/aas');
  final InitializationSettings initializationSettings =
  InitializationSettings(android: initializationSettingsAndroid);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  service.startService();
}


void onStart(ServiceInstance service) async {
  // Ensure Widgets binding is initialized
  WidgetsFlutterBinding.ensureInitialized();

  // Initialize time zone data in the background isolate
  tz.initializeTimeZones();

  // Only available on Android!


  if (service is AndroidServiceInstance) {
    service.on('setAsForeground').listen((event) {
      service.setAsForegroundService();
    });

    service.on('setAsBackground').listen((event) {
      service.setAsBackgroundService();
    });
  }

  // Schedule notifications
  scheduleNotifications();
}

bool onIosBackground(ServiceInstance service) {
  WidgetsFlutterBinding.ensureInitialized();
  return true;
}

String? getPrayerNameForTime(DateTime time, List<DateTime> times) {
  if (time == times[0]) return "Fajar";
  if (time == times[1]) return "Dhuhr";
  if (time == times[2]) return "Asr";
  if (time == times[3]) return "Maghrib";
  if (time == times[4]) return "Isha";
  return null;
}




Future<void> scheduleNotifications() async{

  Position position = await Geolocator.getCurrentPosition(
      desiredAccuracy: LocationAccuracy.high);






  final myCoordinates = Coordinates(position.latitude, position.longitude);



  final params = CalculationMethod.karachi.getParameters();
  params.madhab = Madhab.hanafi;
  final prayerTimes = PrayerTimes.today(myCoordinates, params);

  String fajarStr = DateFormat('HHmm').format(prayerTimes.fajr);
  int fajarHour = int.parse(fajarStr.substring(0, 2));
  int fajarMinute = int.parse(fajarStr.substring(2, 4));

  String duhrTimeStr = DateFormat('HHmm').format(prayerTimes.dhuhr);
  int duhrHour = int.parse(duhrTimeStr.substring(0, 2));
  int duhrMinute = int.parse(duhrTimeStr.substring(2, 4));

  String asrTimeStr = DateFormat('HHmm').format(prayerTimes.asr);
  int asrHour = int.parse(asrTimeStr.substring(0, 2));
  int asrMinute = int.parse(asrTimeStr.substring(2, 4));

  String maghribStr = DateFormat('HHmm').format(prayerTimes.maghrib);
  int maghribHour = int.parse(maghribStr.substring(0, 2));
  int maghribMinute = int.parse(maghribStr.substring(2, 4));

  String ishaStr = DateFormat('HHmm').format(prayerTimes.isha);
  int ishaHour = int.parse(ishaStr.substring(0, 2));
  int ishaMinute = int.parse(ishaStr.substring(2, 4));

  final fajarTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, fajarHour, fajarMinute);
  final duhrTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, duhrHour, duhrMinute);
  final asrTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, asrHour, asrMinute);
  final maghribTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, maghribHour, maghribMinute);
  final ishaTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, ishaHour, ishaMinute);

  print("This is Fajar time: ${fajarTime}");


  final times = [
    fajarTime,
    duhrTime,
    asrTime,
    maghribTime,
    ishaTime,
  ];

  for (int i = 0; i < times.length; i++) {
    String? prayerName = getPrayerNameForTime(times[i], times);
    scheduleNotification(i, times[i], prayerName);
  }



}

Future<void> scheduleNotification(int id, DateTime time, String? prayerName) async {
  var scheduledTZTime = tz.TZDateTime.from(time, tz.local);



  // Define notification details
  const AndroidNotificationDetails androidPlatformChannelSpecifics =
  AndroidNotificationDetails(
    'your_channel_id', // channel ID
    'Channel Name', // channel name
    importance: Importance.max,
    priority: Priority.high,
    sound: RawResourceAndroidNotificationSound('notification'),
  );
  const NotificationDetails platformChannelSpecifics =
  NotificationDetails(android: androidPlatformChannelSpecifics);

  // Schedule notification
  await flutterLocalNotificationsPlugin.zonedSchedule(
    id, // notification ID
    prayerName, // title
    'Alarm', // body
    scheduledTZTime, // schedule time
    platformChannelSpecifics,
    androidAllowWhileIdle: true,
    uiLocalNotificationDateInterpretation:
    UILocalNotificationDateInterpretation.absoluteTime,
    matchDateTimeComponents: DateTimeComponents.time,
  );
}