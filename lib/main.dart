import 'package:aas/Provider/Profile%20Provider/change_password_provider.dart';
import 'package:aas/Screens/BottomNavScreens/bottomnav.dart';
import 'package:aas/Screens/kyc/kyc_screen.dart';

import 'package:adhan/adhan.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter_background_service/flutter_background_service.dart';
import 'package:flutter_background_service_android/flutter_background_service_android.dart';
import 'package:intl/intl.dart';
import 'package:timezone/timezone.dart' as tz;
import 'package:aas/Screens/splashscreen.dart';
import 'package:aas/Services/services.dart';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:google_mobile_ads/google_mobile_ads.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:timezone/data/latest.dart' as tz;

import 'Provider/MLM/Transactions Provider/cash_wallet_provider.dart';
import 'Provider/MLM/Transactions Provider/coins_wallet_detail_two_provider.dart';
import 'Provider/MLM/Transactions Provider/wallet_detail_provider.dart';
import 'Provider/MLM/cash_withdraw_provider.dart';
import 'Provider/MLM/claim_reward_provider.dart';
import 'Provider/MLM/coins_convert_provider.dart';
import 'Provider/MLM/community_provider.dart';
import 'Provider/MLM/community_reward_provider.dart';
import 'Provider/MLM/count_down_provider.dart';
import 'Provider/MLM/donate_to_other_provider.dart';
import 'Provider/MLM/donation_to_aas_provider.dart';
import 'Provider/MLM/forget_trans_pin_provider.dart';
import 'Provider/MLM/join_community_provider.dart';
import 'Provider/MLM/left_side_provider.dart';
import 'Provider/MLM/quran_reward_provider.dart';
import 'Provider/MLM/right_side_provider.dart';
import 'Provider/MLM/set_trans_pin_provider.dart';
import 'Provider/MLM/tasbeeh_reward_provider.dart';


import 'Provider/Other Users Provider/other_user_follower_provider.dart';
import 'Provider/Other Users Provider/other_user_following_provider.dart';
import 'Provider/Prayers/get_prayers_status_provider.dart';
import 'Provider/Prayers/mark_prayer_attendance_provider.dart';
import 'Provider/Profile Provider/cancel_deletion_provider.dart';
import 'Provider/Profile Provider/cash_ledger_provider.dart';
import 'Provider/Profile Provider/delete_account_provider.dart';
import 'Provider/Profile Provider/ledger_detail_provider.dart';
import 'Provider/Profile Provider/notification_provider.dart';
import 'Provider/Profile Provider/task_completed_provider.dart';
import 'Provider/Profile Provider/task_provider.dart';
import 'Provider/ad_provider.dart';
import 'Provider/bookmark_provider.dart';
import 'Provider/comments_provider.dart';
import 'Provider/file_provider.dart';
import 'Provider/follow_unfollow_provider.dart';
import 'Provider/followers_provider.dart';
import 'Provider/following_block_provider.dart';
import 'Provider/following_provider.dart';
import 'Provider/following_video_provider.dart';
import 'Provider/forget_password_provider.dart';
import 'Provider/gallery_provider.dart';
import 'Provider/get_bookmark_provider.dart';
import 'Provider/get_user_post_provider.dart';
import 'Provider/google_sign_provider.dart';
import 'Provider/kyc/kyc_provider.dart';
import 'Provider/likeUnlike_user_provider.dart';
import 'Provider/login_provider.dart';
import 'Provider/logout_provider.dart';
import 'Provider/otp_provider.dart';
import 'Provider/para_detail_provider.dart';
import 'Provider/qari_detail_provider.dart';
import 'Provider/reels_video_provider.dart';
import 'Provider/report_user_provider.dart';
import 'Provider/resend_otp_provider.dart';
import 'Provider/share_video_provider.dart';
import 'Provider/signup_provider.dart';

import 'Provider/surah_detail_provider.dart';
import 'Provider/surah_number_provider.dart';
import 'Provider/surah_provider.dart';
import 'Provider/terms_conditions_provider.dart';
import 'Provider/timer_provider.dart';
import 'Provider/total_prayers_provider.dart';
import 'Provider/user_block_provider.dart';
import 'Provider/user_data_provider.dart';
import 'Provider/user_user_stats_proivder.dart';
import 'Provider/viewProvider.dart';

String? token;

Future requestPermission() async {
  FirebaseMessaging messaging = await FirebaseMessaging.instance;
  NotificationSettings settings = await messaging.requestPermission(
    alert: true,
    announcement: true,
    badge: true,
    carPlay: false,
    criticalAlert: false,
    provisional: false,
    sound: true,
  );

  if (settings.authorizationStatus == AuthorizationStatus.authorized) {
    print('User authorized notifications');
  } else if (settings.authorizationStatus == AuthorizationStatus.provisional) {
    print('User authorized notifications provisionnal');
  } else {
    print('User declined notifications');
  }
}

final FlutterLocalNotificationsPlugin flutterLocalNotificationsPlugin =
FlutterLocalNotificationsPlugin();

Future<void> _firebaseMessagingBackgroundHandler(RemoteMessage message) async {
  print('Handling a background message ${message.messageId}');
}

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  await Firebase.initializeApp(
    options: FirebaseOptions(
        apiKey: 'AIzaSyBlLFimynY34BC5VXCqI3NIFPtRkJD0c0E',
        appId: '1:91693767586:android:4811c51206fa91a53e30fb',
        messagingSenderId: '91693767586',
        projectId: 'prayertimes-cff2d',
        storageBucket: 'prayertimes-cff2d.appspot.com'),
  );
  await requestPermission();
  FirebaseMessaging.onBackgroundMessage(_firebaseMessagingBackgroundHandler);
  tz.initializeTimeZones();
  MobileAds.instance.initialize();

  // await Firebase.initializeApp();
  SharedPreferences prefs = await SharedPreferences.getInstance();
  token = prefs.getString('sessionToken');

  // Initialize local notifications
  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('@mipmap/aas');
  final InitializationSettings initializationSettings =
  InitializationSettings(android: initializationSettingsAndroid);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  // Initialize background service
  await initializeService();

  runApp(
    MultiProvider(
      providers: [
        // ChangeNotifierProvider for UserProvider
        ChangeNotifierProvider(
          create: (context) => UserProvider(apiService: ApiService()),
        ),
        // ChangeNotifierProvider for OtpCodeProvider
        ChangeNotifierProvider(
          create: (context) => OtpCodeProvider(apiService: ApiService()),
        ),
        // ChangeNotifierProvider for forgetPassword
        ChangeNotifierProvider(
            create: (context) =>
                ForgetPasswordProvider(apiService: ApiService())),
        // ChangeNotifierProvider for Login
        ChangeNotifierProvider(
            create: (context) => LoginProvider(apiService: ApiService())),

        // ChangeNotifierProvider for timer

        ChangeNotifierProvider(create: (context) => TimerProvider()),
        // ChangeNotifierProvider for Google sign in

        ChangeNotifierProvider(
            create: (context) =>
                GoogleSignInProvider(apiService: ApiService())),
        // ChangeNotifierProvider for reels screen

        ChangeNotifierProvider(
            create: (context) => CommentProvider(apiService: ApiService())),

        // ChangeNotifierProvider for upload video

        ChangeNotifierProvider(
          create: (context) => FileProvider(),
        ),
        // ChangeNotifierProvider for surah

        ChangeNotifierProvider(
          create: (context) => SurahProvider(apiService: ApiService()),
        ),
        // ChangeNotifierProvider for FavUnFavProvider

        // ChangeNotifierProvider for surah detail

        ChangeNotifierProvider(
          create: (context) => SurahDetailProvider(),
        ),

        // ChangeNotifierProvider for para detail

        ChangeNotifierProvider(
          create: (context) => ParaDetailProvider(),
        ),

        // ChangeNotifierProvider for qar detail
        ChangeNotifierProvider(
          create: (context) => ViewProvider(apiService: ApiService()),
        ),

        ChangeNotifierProvider(
          create: (context) => QariDetailProvider(),
        ),
        // ChangeNotifierProvider for surahs numbers

        ChangeNotifierProvider(
          create: (context) => SurahNumberProvider(),
        ),
        // ChangeNotifierProvider for bookmark

        // ChangeNotifierProvider(
        //   create: (context) => BookmarkProvider(114),
        // ),
        // ChangeNotifierProvider for bookmark

        ChangeNotifierProvider(
            create: (context) => BookmarkProvider(apiService: ApiService())),
        // ChangeNotifierProvider for get bookmark

        ChangeNotifierProvider(create: (context) => GetBookmarkProvider()),
        // ChangeNotifierProvider for get user data

        ChangeNotifierProvider(create: (context) => UserDataProvider()),
        // ChangeNotifierProvider for like unline post

        ChangeNotifierProvider(create: (context) => ReelsVideoProvider()),
        ChangeNotifierProvider(create: (context) => FollowingScreenProvider()),

        ChangeNotifierProvider(
            create: (context) =>
                LikeUnlikeUserProvider(apiService: ApiService())),
        // ChangeNotifierProvider for share video

        ChangeNotifierProvider(
            create: (context) => ShareVideoProvier(apiService: ApiService())),
        ChangeNotifierProvider(create: (_) => KycProvider()),
        // ChangeNotifierProvider for follow unfollow user

        ChangeNotifierProvider(
          create: (context) => UserFollow(),
        ),
        // ChangeNotifierProvider for followers list

        ChangeNotifierProvider(
          create: (context) => FollowersProvider(),
        ),
        // ChangeNotifierProvider for terms and conditions

        ChangeNotifierProvider(
          create: (context) => TermsConditionProvider(),
        ),
        // ChangeNotifierProvider for following list

        ChangeNotifierProvider(
          create: (context) => FollowingProvider(),
        ),
        // ChangeNotifierProvider for ads

        ChangeNotifierProvider(
          create: (context) => AdProvider(),
        ),
        // ChangeNotifierProvider for following block in user profile screen

        ChangeNotifierProvider(
            create: (context) =>
                FollowingBlockProvider(apiService: ApiService())),

        // ChangeNotifierProvider for  followers report provider

        ChangeNotifierProvider(
            create: (context) => ReportProvider(apiService: ApiService())),
        // ChangeNotifierProvider for followers block provider

        ChangeNotifierProvider(
            create: (context) => UserBlockProvider(apiService: ApiService())),
        // ChangeNotifierProvider for get user post data

        ChangeNotifierProvider(create: (context) => GetUserPostProvider()),
        // ChangeNotifierProvider for other followers provider

        ChangeNotifierProvider(
          create: (context) => OtherFollowersProvider(),
        ),
        // ChangeNotifierProvider for other following provider

        ChangeNotifierProvider(
          create: (context) => OtherFollowingProvider(),
        ),
        // ChangeNotifierProvider for other user stats provider

        ChangeNotifierProvider(
          create: (context) => StatsProvider(),
        ),
        // ChangeNotifierProvider for task provider

        ChangeNotifierProvider(create: (context) => TaskProvider()),


        // ChangeNotifierProvider for task completed provider

        ChangeNotifierProvider(
            create: (context) =>
                TaskCompletedProvider(apiService: ApiService())),
        // ChangeNotifierProvider for total prayers. provider



        ChangeNotifierProvider(create: (context) => TotalPrayersProvider()),

        // ChangeNotifierProvider for join community provider

        ChangeNotifierProvider(
            create: (context) =>
                JoinCommunityProvider(apiService: ApiService())),

        // ChangeNotifierProvider for get prayers status

        ChangeNotifierProvider(create: (context) => GetPrayersStatusProvider()),
        // ChangeNotifierProvider for mark prayer attendance

        ChangeNotifierProvider(
            create: (context) =>
                MarkPrayerAttendanceProvider(apiService: ApiService())),
        // ChangeNotifierProvider for get coin ledger detail

        ChangeNotifierProvider(create: (context) => LedgerDetailProvider()),
        // ChangeNotifierProvider for get cash ledger detail

        ChangeNotifierProvider(create: (context) => CashLedgerDetailProvider()),
        // ChangeNotifierProvider for get notification detail

        ChangeNotifierProvider(
            create: (context) => NotificationDetailProvider()),
        // ChangeNotifierProvider for get notification detail

        ChangeNotifierProvider(create: (context) => GalleryProvider()),
        // ChangeNotifierProvider for get wallet detail

        ChangeNotifierProvider(create: (context) => walletDetailProvider()),
        // ChangeNotifierProvider for count down timer

        ChangeNotifierProvider(create: (context) => CountDownTimerProvider()),

        // ChangeNotifierProvider for get coins wallet detail two

        ChangeNotifierProvider(
            create: (context) => CoinsWalletDetailTwoProvider()),
        // ChangeNotifierProvider for get cash wallet detail

        ChangeNotifierProvider(create: (context) => CashWalletDetailProvider()),
        // ChangeNotifierProvider for coins conversion

        ChangeNotifierProvider(
            create: (context) =>
                CoinsConvertProvider(apiService: ApiService())),
        // ChangeNotifierProvider for set pin

        ChangeNotifierProvider(
            create: (context) =>
                SetTransactionsPinProvider(apiService: ApiService())),
        // ChangeNotifierProvider for forget pin

        ChangeNotifierProvider(
            create: (context) =>
                ForgetTransPinProvider(apiService: ApiService())),
        // ChangeNotifierProvider for cash withdraw

        ChangeNotifierProvider(
            create: (context) =>
                CashWithdrawProvider(apiService: ApiService())),
        // ChangeNotifierProvider for donation to aas

        ChangeNotifierProvider(
            create: (context) => DonationToAAS(apiService: ApiService())),
        // ChangeNotifierProvider for donation to other

        ChangeNotifierProvider(
            create: (context) => DonationToOther(apiService: ApiService())),
        // ChangeNotifierProvider for community

        ChangeNotifierProvider(create: (context) => CommunityProvider()),
        // ChangeNotifierProvider for right side data

        ChangeNotifierProvider(create: (context) => RightSideProvider()),
        // ChangeNotifierProvider for right side data

        ChangeNotifierProvider(create: (context) => LeftSideProvider()),

        // ChangeNotifierProvider fo delete account

        ChangeNotifierProvider(
            create: (context) =>
                DeleteAccountProvider(apiService: ApiService())),

        // ChangeNotifierProvider fo cancel account deletion

        ChangeNotifierProvider(
            create: (context) =>
                CancelDeletionProvider(apiService: ApiService())),
        // ChangeNotifierProvider for community reward

        ChangeNotifierProvider(create: (context) => CommunityRewardProvider()),

        // ChangeNotifierProvider fo delete account

        ChangeNotifierProvider(
            create: (context) => ClaimRewardprovider(apiService: ApiService())),
        // ChangeNotifierProvider fo delete account

        ChangeNotifierProvider(
            create: (context) => ResendOtpProvider(apiService: ApiService())),
        // ChangeNotifierProvider for logout

        ChangeNotifierProvider(
            create: (context) => LogoutProvider(apiService: ApiService())),
        // ChangeNotifierProvider for quran reward

        ChangeNotifierProvider(
            create: (context) => QuranRewardProvider(apiService: ApiService())),
        // ChangeNotifierProvider for tasbeeh reward

        ChangeNotifierProvider(
            create: (context) =>
                TasbeehRewardProvider(apiService: ApiService())),
        ChangeNotifierProvider(
            create: (context) =>
                ChangePasswordProvider(apiService: ApiService())),
      ],
      child: MyApp(),
    ),
  );
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  final ApiService namazTimingStorage = ApiService();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    namazTimingStorage.namazTimingStoreInShared();
  }

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
        designSize: const Size(375, 812),
        minTextAdapt: true,
        splitScreenMode: true,
        builder: (context, child) {
          // ReelService().getVideosFromApI((bool cachingInProgress) {
          //   cachingInProgress = cachingInProgress;
          // });
          return GetMaterialApp(
            theme: Theme.of(context).copyWith(
              appBarTheme: Theme.of(context)
                  .appBarTheme
                  .copyWith(systemOverlayStyle: SystemUiOverlayStyle.dark),
            ),
            title: 'AAS',
            debugShowCheckedModeBanner: false,
            home: token == null ? SplashScreen() : BottomNavScreen(),

            builder: EasyLoading.init(),
          );
        });
  }
}

double latitude = 0;
double longitude = 0;

// Schedule multiple notifications

Future<void> initializeService() async {
  final service = FlutterBackgroundService();

  // Configure background service
  await service.configure(
    androidConfiguration: AndroidConfiguration(
        onStart: onStart,
        autoStart: true,
        isForegroundMode: true,
        initialNotificationTitle: 'Azaan Notification',
        initialNotificationContent: 'App running in background'),
    iosConfiguration: IosConfiguration(
      autoStart: true,
      onForeground: onStart,
      onBackground: onIosBackground,
    ),
  );
  const AndroidInitializationSettings initializationSettingsAndroid =
  AndroidInitializationSettings('@mipmap/aas');
  final InitializationSettings initializationSettings =
  InitializationSettings(android: initializationSettingsAndroid);
  await flutterLocalNotificationsPlugin.initialize(initializationSettings);

  service.startService();
}

void onStart(ServiceInstance service) async {
  // Ensure Widgets binding is initialized
  WidgetsFlutterBinding.ensureInitialized();

  // Initialize time zone data in the background isolate
  tz.initializeTimeZones();

  // Only available on Android!

  if (service is AndroidServiceInstance) {
    service.on('setAsForeground').listen((event) {
      service.setAsForegroundService();
    });

    service.on('setAsBackground').listen((event) {
      service.setAsBackgroundService();
    });
  }

  // Schedule notifications
  scheduleNotifications();
}

bool onIosBackground(ServiceInstance service) {
  WidgetsFlutterBinding.ensureInitialized();
  return true;
}

void scheduleNotifications() {
  final myCoordinates = Coordinates(latitude, longitude);
  final params = CalculationMethod.karachi.getParameters();
  params.madhab = Madhab.hanafi;
  final prayerTimes = PrayerTimes.today(myCoordinates, params);

  String fajarStr = DateFormat('HHmm').format(prayerTimes.fajr);
  int fajarHour = int.parse(fajarStr.substring(0, 2));
  int fajarMinute = int.parse(fajarStr.substring(2, 4));

  String duhrTimeStr = DateFormat('HHmm').format(prayerTimes.dhuhr);
  int duhrHour = int.parse(duhrTimeStr.substring(0, 2));
  int duhrMinute = int.parse(duhrTimeStr.substring(2, 4));

  String asrTimeStr = DateFormat('HHmm').format(prayerTimes.asr);
  int asrHour = int.parse(asrTimeStr.substring(0, 2));
  int asrMinute = int.parse(asrTimeStr.substring(2, 4));

  String magribStr = DateFormat('HHmm').format(prayerTimes.maghrib);
  int magribHour = int.parse(magribStr.substring(0, 2));
  int magribMinute = int.parse(magribStr.substring(2, 4));

  String ishaStr = DateFormat('HHmm').format(prayerTimes.isha);
  int ishaHour = int.parse(ishaStr.substring(0, 2));
  int ishaMinute = int.parse(ishaStr.substring(2, 4));

  final fajarTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, fajarHour, fajarMinute);
  final duhrTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, duhrHour, duhrMinute);
  final asrTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, asrHour, asrMinute);
  final magribTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, magribHour, magribMinute);
  final ishaTime = DateTime(DateTime.now().year, DateTime.now().month,
      DateTime.now().day, ishaHour, ishaMinute);

  final times = [
    fajarTime,
    duhrTime,
    asrTime,
    magribTime,
    ishaTime,
  ];

  for (int i = 0; i < times.length; i++) {
    scheduleNotification(i, times[i]);
  }
}

Future<void> scheduleNotification(int id, DateTime time) async {
  var scheduledTZTime = tz.TZDateTime.from(time, tz.local);

  print("Scheduling notification for: $scheduledTZTime");

  // Define notification details
  const AndroidNotificationDetails androidPlatformChannelSpecifics =
  AndroidNotificationDetails(
    'your_channel_id', // channel ID
    'Channel Name', // channel name
    importance: Importance.max,
    priority: Priority.high,
    sound: RawResourceAndroidNotificationSound('notification'),
  );
  const NotificationDetails platformChannelSpecifics =
  NotificationDetails(android: androidPlatformChannelSpecifics);

  // Schedule notification
  await flutterLocalNotificationsPlugin.zonedSchedule(
    id, // notification ID
    "Namaz", // title
    'Alarm', // body
    scheduledTZTime, // schedule time
    platformChannelSpecifics,
    androidAllowWhileIdle: true,
    uiLocalNotificationDateInterpretation:
    UILocalNotificationDateInterpretation.absoluteTime,
    matchDateTimeComponents: DateTimeComponents.time,
  );
}
